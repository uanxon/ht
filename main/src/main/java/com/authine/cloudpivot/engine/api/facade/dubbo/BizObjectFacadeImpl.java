package com.authine.cloudpivot.engine.api.facade.dubbo;

import cn.hutool.core.util.BooleanUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.authine.cloudpivot.engine.api.constants.ModelConstant;
import com.authine.cloudpivot.engine.api.exceptions.ServiceException;
import com.authine.cloudpivot.engine.api.facade.BizFormFacade;
import com.authine.cloudpivot.engine.api.facade.BizObjectFacade;
import com.authine.cloudpivot.engine.api.facade.BizQueryFacade;
import com.authine.cloudpivot.engine.api.facade.BizSchemaFacade;
import com.authine.cloudpivot.engine.api.facade.BizWorkitemFacade;
import com.authine.cloudpivot.engine.api.facade.BizWorkitemFinishedFacade;
import com.authine.cloudpivot.engine.api.facade.DepartmentFacade;
import com.authine.cloudpivot.engine.api.facade.OrganizationFacade;
import com.authine.cloudpivot.engine.api.facade.PermissionManagementFacade;
import com.authine.cloudpivot.engine.api.facade.UserFacade;
import com.authine.cloudpivot.engine.api.facade.WorkflowInstanceFacade;
import com.authine.cloudpivot.engine.api.facade.WorkflowManagementFacade;
import com.authine.cloudpivot.engine.api.model.ModelViewQueryModel;
import com.authine.cloudpivot.engine.api.model.bizform.BizFormCommentModel;
import com.authine.cloudpivot.engine.api.model.bizform.BizFormModel;
import com.authine.cloudpivot.engine.api.model.bizmodel.BizMethodMappingModel;
import com.authine.cloudpivot.engine.api.model.bizmodel.BizPropertyModel;
import com.authine.cloudpivot.engine.api.model.bizmodel.BizSchemaModel;
import com.authine.cloudpivot.engine.api.model.bizmodel.FilterModel;
import com.authine.cloudpivot.engine.api.model.bizmodel.QueryDataModel;
import com.authine.cloudpivot.engine.api.model.bizmodel.QuoteModel;
import com.authine.cloudpivot.engine.api.model.bizquery.BizQueryActionModel;
import com.authine.cloudpivot.engine.api.model.bizquery.BizQueryChildColumnModel;
import com.authine.cloudpivot.engine.api.model.bizquery.BizQueryColumnModel;
import com.authine.cloudpivot.engine.api.model.bizquery.BizQueryConditionModel;
import com.authine.cloudpivot.engine.api.model.bizquery.BizQueryGanttModel;
import com.authine.cloudpivot.engine.api.model.bizquery.BizQueryHeaderModel;
import com.authine.cloudpivot.engine.api.model.bizquery.BizQueryModel;
import com.authine.cloudpivot.engine.api.model.bizquery.BizQuerySchemaRelationModel;
import com.authine.cloudpivot.engine.api.model.bizquery.option.BizQueryOption;
import com.authine.cloudpivot.engine.api.model.importData.FileOperationResult;
import com.authine.cloudpivot.engine.api.model.organization.DepartmentModel;
import com.authine.cloudpivot.engine.api.model.organization.UserModel;
import com.authine.cloudpivot.engine.api.model.runtime.AttachmentModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizCommentModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectBatchUpdateRecordModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectQueryModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectQueryRelationModel;
import com.authine.cloudpivot.engine.api.model.runtime.CheckResultForRemoveBizObject;
import com.authine.cloudpivot.engine.api.model.runtime.SelectionValue;
import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkflowInstanceModel;
import com.authine.cloudpivot.engine.api.model.system.PairSettingModel;
import com.authine.cloudpivot.engine.api.utils.ModelUtil;
import com.authine.cloudpivot.engine.api.utils.ObjectMapperUtils;
import com.authine.cloudpivot.engine.component.query.api.FilterExpression;
import com.authine.cloudpivot.engine.component.query.api.Order;
import com.authine.cloudpivot.engine.component.query.api.Page;
import com.authine.cloudpivot.engine.component.query.api.Pageable;
import com.authine.cloudpivot.engine.component.query.api.Sortable;
import com.authine.cloudpivot.engine.component.query.api.helper.OrderImpl;
import com.authine.cloudpivot.engine.component.query.api.helper.PageImpl;
import com.authine.cloudpivot.engine.component.query.api.helper.PageableImpl;
import com.authine.cloudpivot.engine.component.query.api.helper.Q;
import com.authine.cloudpivot.engine.component.query.api.helper.SortableImpl;
import com.authine.cloudpivot.engine.domain.bizform.BizForm;
import com.authine.cloudpivot.engine.domain.bizform.BizFormComment;
import com.authine.cloudpivot.engine.domain.bizform.BizFormCommentAttachment;
import com.authine.cloudpivot.engine.domain.bizmodel.BizDataRule;
import com.authine.cloudpivot.engine.domain.bizmodel.BizMethodMapping;
import com.authine.cloudpivot.engine.domain.bizmodel.BizProperty;
import com.authine.cloudpivot.engine.domain.bizmodel.BizSchema;
import com.authine.cloudpivot.engine.domain.bizquery.BizQuery;
import com.authine.cloudpivot.engine.domain.bizquery.BizQueryColumn;
import com.authine.cloudpivot.engine.domain.bizquery.BizQueryPresent;
import com.authine.cloudpivot.engine.domain.bizquery.BizQuerySort;
import com.authine.cloudpivot.engine.domain.runtime.Attachment;
import com.authine.cloudpivot.engine.domain.runtime.BizObject;
import com.authine.cloudpivot.engine.domain.runtime.BizObjectBatchUpdateRecord;
import com.authine.cloudpivot.engine.domain.runtime.BizObjectExtendInfo;
import com.authine.cloudpivot.engine.domain.runtime.Comment;
import com.authine.cloudpivot.engine.domain.runtime.CreateComment;
import com.authine.cloudpivot.engine.domain.system.PairSetting;
import com.authine.cloudpivot.engine.enums.ErrCode;
import com.authine.cloudpivot.engine.enums.status.DataRowStatus;
import com.authine.cloudpivot.engine.enums.status.SequenceStatus;
import com.authine.cloudpivot.engine.enums.status.UserStatus;
import com.authine.cloudpivot.engine.enums.status.WorkflowInstanceStatus;
import com.authine.cloudpivot.engine.enums.type.ActionType;
import com.authine.cloudpivot.engine.enums.type.BizPropertyType;
import com.authine.cloudpivot.engine.enums.type.ClientType;
import com.authine.cloudpivot.engine.enums.type.DefaultPropertyType;
import com.authine.cloudpivot.engine.enums.type.DefaultSubPropertyType;
import com.authine.cloudpivot.engine.enums.type.DefaultTreePropertyType;
import com.authine.cloudpivot.engine.enums.type.FormActionType;
import com.authine.cloudpivot.engine.enums.type.ModelViewType;
import com.authine.cloudpivot.engine.enums.type.OrgType;
import com.authine.cloudpivot.engine.enums.type.ProcessRunMode;
import com.authine.cloudpivot.engine.enums.type.QueryActionRelativeType;
import com.authine.cloudpivot.engine.enums.type.QueryActionType;
import com.authine.cloudpivot.engine.enums.type.QueryDisplayType;
import com.authine.cloudpivot.engine.enums.type.QueryFilterType;
import com.authine.cloudpivot.engine.enums.type.QueryPresentationType;
import com.authine.cloudpivot.engine.enums.type.SortDirectionType;
import com.authine.cloudpivot.engine.enums.type.UnitType;
import com.authine.cloudpivot.engine.enums.type.bizmodel.QueryLevelType;
import com.authine.cloudpivot.engine.service.bizform.BizFormCommentService;
import com.authine.cloudpivot.engine.service.bizform.BizFormJsonResolveService;
import com.authine.cloudpivot.engine.service.bizform.BizFormService;
import com.authine.cloudpivot.engine.service.bizmodel.BizPropertyService;
import com.authine.cloudpivot.engine.service.bizmodel.BizSchemaService;
import com.authine.cloudpivot.engine.service.bizquery.BizQueryColumnService;
import com.authine.cloudpivot.engine.service.bizquery.BizQueryConditionService;
import com.authine.cloudpivot.engine.service.bizquery.BizQueryPresentService;
import com.authine.cloudpivot.engine.service.bizquery.BizQueryService;
import com.authine.cloudpivot.engine.service.bizquery.BizQuerySortService;
import com.authine.cloudpivot.engine.service.bizrule.CalculateRuleService;
import com.authine.cloudpivot.engine.service.runtime.BizObjectBatchUpdateRecordService;
import com.authine.cloudpivot.engine.service.runtime.BizObjectConverter;
import com.authine.cloudpivot.engine.service.runtime.BizObjectLocalCacheService;
import com.authine.cloudpivot.engine.service.runtime.BizObjectService;
import com.authine.cloudpivot.engine.service.runtime.EngineRuntimeException;
import com.authine.cloudpivot.engine.service.system.PairSettingService;
import com.authine.cloudpivot.engine.service.system.SequenceSettingService;
import com.authine.cloudpivot.engine.service.util.BizFormUtil;
import com.authine.cloudpivot.engine.utils.ThreadLocalAttributes;
import com.authine.cloudpivot.foundation.orm.api.Db;
import com.authine.cloudpivot.foundation.orm.api.model.BizObjectOptions;
import com.authine.cloudpivot.foundation.orm.api.model.BizObjectQueryObject;
import com.authine.cloudpivot.foundation.orm.api.model.DisplayField;
import com.authine.cloudpivot.foundation.orm.api.model.OrderByField;
import com.authine.cloudpivot.foundation.orm.api.model.QueryRelationFilter;
import com.authine.cloudpivot.foundation.orm.api.model.QueryRelationObject;
import com.authine.cloudpivot.foundation.orm.api.model.RelationObject;
import com.authine.cloudpivot.foundation.util.common.DateHelpUtils;
import com.authine.cloudpivot.foundation.util.common.TokenizerUtils;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Nullable;
import javax.annotation.PreDestroy;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author longhai
 */
@Service
@Transactional
@Slf4j
public class BizObjectFacadeImpl implements BizObjectFacade {

    private static final int DEFAULT_CAPACITY = 10;
    @Qualifier("batchInsertPoolExecutor")
    @Autowired
    private ThreadPoolExecutor batchInsertPool;
    @Qualifier("checkRelativePoolExecutor")
    @Autowired
    private ThreadPoolExecutor checkRelativePool;
    /**
     * 批量插入数据库批次大小
     */
    private static final int BATCH_INSERT_SIZE = 2000;

    private final String OR = "or";

    private final String AND = "and";

    @Autowired
    private BizObjectLocalCacheService bizObjectLocalCacheService;

    @Autowired
    private BizObjectService bizObjectService;

    @Autowired
    private BizQueryColumnService bizQueryColumnService;

    @Autowired
    private BizQueryConditionService bizQueryConditionService;

    @Autowired
    private BizQuerySortService bizQuerySortService;

    @Autowired
    private BizQueryService bizQueryService;

    @Autowired
    private BizQueryPresentService bizQueryPresentService;

    @Autowired
    private DepartmentFacade departmentFacade;

    @Autowired
    private UserFacade userFacade;

    @Autowired
    private Db db;

    @Autowired
    private BizSchemaService bizSchemaService;

    @Autowired
    private BizPropertyService bizPropertyService;

    @Autowired
    private SequenceSettingService sequenceSettingService;

    @Autowired
    private BizFormService bizFormService;

    @Autowired
    private WorkflowInstanceFacade bizWorkflowInstanceService;

    @Autowired
    private WorkflowManagementFacade workflowManagementFacade;

    @Autowired
    private BizWorkitemFacade bizWorkitemService;

    @Autowired
    private BizWorkitemFinishedFacade bizWorkitemFinishedService;

    @Autowired
    private BizObjectConverter bizObjectConverter;

    @Autowired
    private PairSettingService pairSettingService;

    @Autowired
    private BizFormCommentService bizFormCommentService;

    @Autowired
    private CalculateRuleService calculateRuleService;

    @Autowired
    private BizObjectBatchUpdateRecordService batchUpdateRecordService;

    @Autowired
    private BizQueryFacade bizQueryFacade;

    @Autowired
    private BizFormFacade bizFormFacade;

    @Autowired
    private BizSchemaFacade bizSchemaFacade;

    @Autowired
    private PermissionManagementFacade permissionManagementFacade;

    @Autowired
    private OrganizationFacade organizationFacade;

    @Autowired
    private BizFormJsonResolveService bizFormJsonResolveService;

    @Value("#{'${cloudpivot.notFilter.draft.schemaCodes:QYJCXX}'.split(',')}")
    private Set<String> notFilterDraftSet;

    @Value("${cloudpivot.import.rule.flag:false}")
    private Boolean exeRuleFlag;

    private Map<String, DepartmentModel> userDepartmentMap = new HashMap<>();

    static String getIdFromObject(Object m) {
        if (m == null) {
            return null;
        }
        if (m instanceof String) {
            final String s = ((String) m).trim();
            if (s.startsWith("{")) {
                return JSON.parseObject(s, SelectionValue.class).getId();
            }
            if (s.startsWith("[")) {
                return JSON.parseObject(s, new TypeReference<List<SelectionValue>>() {
                })
                        .stream()
                        .map(SelectionValue::getId)
                        .findFirst()
                        .orElse(null);
            } else {
                return s;
            }
        }
        if (m instanceof SelectionValue) {
            return ((SelectionValue) m).getId();
        }
        if (m instanceof Map) {
            return (String) ((Map) m).get("id");
        }

        if (isIterable(m)) {
            List list = asList(m);
            Object o = list.stream().findFirst().orElse(null);
            return getIdFromObject(o);
        }
        throw new RuntimeException("不支持的从:" + m + "获取id值");
    }

    public static boolean isIterable(@Nullable Object value) {
        if (value == null) {
            return false;
        }
        if (value instanceof Iterable) {
            return true;
        }
        if (value instanceof Iterator) {
            return true;
        }

        return value.getClass().isArray();
    }

    public static List asList(@Nullable Object value) {
        if (value == null) {
            return null;
        }
        if (value instanceof List) {
            return (List) value;
        }
        if (value instanceof Iterable) {
            return Lists.newArrayList((Iterable) value);
        }
        if (value instanceof Iterator) {
            return Lists.newArrayList((Iterator) value);
        }

        if (value.getClass().isArray()) {
            if (!value.getClass().getComponentType().isPrimitive()) {
                return Arrays.asList((Object[]) value);
            } else {
                int size = Array.getLength(value);
                Object[] objects = new Object[size];
                for (int i = 0; i < size; i++) {
                    objects[i] = Array.get(value, i);
                }
                return Arrays.asList(objects);
            }
        }

        return Collections.singletonList(value);
    }

    private UserModel getUser(String userId) {
        UserModel user = userFacade.get(userId);
        if (user == null) {
            log.debug("用户不存在, userId ={}.", userId);
            throw new ServiceException(ErrCode.ORG_USER_NONEXISTENT);
        }
        return user;
    }

    private DepartmentModel getDepartmentByUserId(String userId) {
        DepartmentModel department = null;
        UserModel user = userFacade.get(userId);
        if (user != null && !StringUtils.isEmpty(user.getDepartmentId())) {
            department = departmentFacade.get(user.getDepartmentId());
        }

        if (department == null) {
            List<DepartmentModel> departmentList = departmentFacade.listByUserId(userId);
            if (CollectionUtils.isNotEmpty(departmentList)) {
                department = departmentList.get(0);
            }
        }
        return department;
    }

    @Override
    public BizObjectCreatedModel getBizObject(final String userId, final String schemaCode, final String objectId) {
        return getBizObject(userId, schemaCode, objectId, null);
    }

    @Override
    public BizObjectCreatedModel getBizObjectWithoutChild(String userId, String schemaCode, String objectId) {
        return getBizObject(userId, schemaCode, objectId, BizObjectOptions.builder().notQueryChild().build());
    }

    @Override
    public BizObjectCreatedModel getBizObject(final String userId, final String schemaCode, final String objectId, BizObjectOptions options) {
        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(schemaCode)) {
            log.debug("用户Id或schemaCode为空.");
            throw new ServiceException(ErrCode.SYS_PARAMETER_EMPTY);
        }

        if (bizSchemaService.getBySchemaCode(schemaCode) == null) {
            throw new ServiceException(ErrCode.BIZ_SCHEMA_MODEL_NOT_EXIST, schemaCode);
        }

        //new bizobject
        if (StringUtils.isEmpty(objectId)) {
            UserModel user = getUser(userId);
            DepartmentModel department = departmentFacade.get(user.getDepartmentId());
//            String departmentName = department == null ? null : department.getName();

            Map<String, Object> data = Maps.newHashMap();
            if (user != null) {
                SelectionValue userSelectionValue = new SelectionValue();
                userSelectionValue.setId(user.getId());
                userSelectionValue.setType(UnitType.USER);
                userSelectionValue.setName(user.getName());
                data.put(DefaultPropertyType.CREATER.getCode(), Lists.newArrayList(userSelectionValue));
            }
            if (department != null) {
                SelectionValue deptSelectionValue = new SelectionValue();
                deptSelectionValue.setId(department.getId());
                deptSelectionValue.setType(UnitType.DEPARTMENT);
                deptSelectionValue.setName(department.getName());
                data.put(DefaultPropertyType.CREATED_DEPT_ID.getCode(), Lists.newArrayList(deptSelectionValue));
            }

            BizObjectCreatedModel bizObject = new BizObjectCreatedModel(schemaCode, data, false);
            String newId = bizObjectService.genBizObjectId();
            log.debug("返回一个新对象， objectId={}.", newId);
            bizObject.setId(newId);
//            bizObject.setCreater(user.getName());
//            bizObject.setCreatedDeptId(departmentName);
            return bizObject;
        }
        // from db
        BizObject object = null;
        try {
            ThreadLocalAttributes.setThreadAttribute("currentUserId", userId);
            object = getBizObjectWithOptions(schemaCode, objectId, options);
        } catch (Exception e) {
            throw e;
        } finally {
            ThreadLocalAttributes.removeThreadAttribute("currentUserId");
        }
        return new BizObjectCreatedModel(schemaCode, object.getData(), object.isLoadedFromDb());
    }

    @Override
    public BizObjectCreatedModel getBizObject(String schemaCode, String objectId) {
        return getBizObjectModelWithOptions(schemaCode, objectId, null);
    }

    @Override
    public BizObjectCreatedModel getBizObjectWithoutChild(String schemaCode, String objectId) {
        return getBizObjectModelWithOptions(schemaCode, objectId, BizObjectOptions.builder().notQueryChild().build());
    }

    private BizObject getBizObjectWithOptions(String schemaCode, String objectId, BizObjectOptions options) {
        return bizObjectService.loadBizObject(schemaCode, objectId, null, options);
    }

    private BizObjectCreatedModel getBizObjectModelWithOptions(String schemaCode, String objectId, BizObjectOptions options) {
        BizObject bizObject = getBizObjectWithOptions(schemaCode, objectId, options);
        return new BizObjectCreatedModel(schemaCode, bizObject.getData(), bizObject.isLoadedFromDb());
    }


    @Override
    public boolean existsBizObject(String schemaCode, String objectId) {
        return bizObjectService.existsBizObject(schemaCode, objectId);
    }

    @Override
    public String saveBizObject(final String userId, final BizObjectModel bizObject, Boolean syncInstance) {
        return saveBizObject(userId, bizObject, syncInstance, null);
    }

    @Override
    public String saveBizObject(final String userId, final BizObjectModel bizObject, Boolean syncInstance, BizObjectOptions options) {
        if (bizObject == null) {
            throw new ServiceException(ErrCode.SYS_PARAMETER_EMPTY);
        }

        //当更新了拥有者后，把拥有者部门也修改
        if (Objects.nonNull(bizObject.getData())) {
            Object owner = bizObject.getData().get("owner");
            if (Objects.nonNull(owner)) {
                Object boOwner = bizObject.getObject(DefaultPropertyType.OWNER.getCode());
                String ownerId = this.getSelectionId(boOwner, DefaultPropertyType.OWNER.getCode());

                String objectId = bizObject.getId();
                if (StringUtils.isEmpty(objectId)) {
                    modifyOwnerInfo(ownerId, bizObject);
                } else {
                    BizObjectCreatedModel oldObj = getBizObject(bizObject.getSchemaCode(), objectId);
                    if (isModifyOwner(ownerId, oldObj)) {
                        modifyOwnerInfo(ownerId, bizObject);
                    }
                }
            }
        }

        updateChildTableModifyProperties(userId, bizObject);

        //long start = System.currentTimeMillis();

        if (StringUtils.isEmpty(bizObject.getFormCode())) {
            List<BizForm> bizForms = bizFormService.getListBySchemaCode(bizObject.getSchemaCode());
            if (CollectionUtils.isNotEmpty(bizForms)) {
                bizObject.setFormCode(bizForms.get(0).getCode());
            }
        }

        //校验单行文本是否重复 add by lijianglong  2019-11-8 beginnames的数据
        log.debug("------开始校验单行文本是否重复------");
        bizFormService.verifyControlProperty(bizObject);
        log.debug("------校验单行文本结束------");
        //校验单行文本是否重复 add by lijianglong  2019-11-8 end

        //log.info("setFormCode cost : {}", System.currentTimeMillis() - start);

        if (log.isTraceEnabled()) {
            log.trace("bizObject json = {}", JSONObject.toJSONString(bizObject));
        }
        final BizSchema bizSchema = bizSchemaService.getBySchemaCode(bizObject.getSchemaCode());
        if (bizSchema == null) {
            log.debug("数据模型为空，schemaCode = {}", bizObject.getSchemaCode());
            throw new ServiceException(ErrCode.BIZ_SCHEMA_MODEL_NOT_EXIST, bizObject.getSchemaCode());
        }
        if (log.isDebugEnabled()) {
            log.debug("summary = {}", bizSchema.getSummary());
        }

        //log.info("getBySchemaCode cost : {}", System.currentTimeMillis() - start);

        if (StringUtils.isEmpty(bizObject.getId())) {
            log.debug("objectId is null...");
            return createBizObject(userId, bizSchema, bizObject);

        } else {
            Optional<BizObject> load = bizObjectLocalCacheService.load(bizObject.getSchemaCode(), bizObject.getId());

            if (!load.isPresent()) {
                log.debug("bizObject do not from db...");
                return createBizObject(userId, bizSchema, bizObject);
            } else {
                log.debug("bizObject from db...");
                return updateBizObject(userId, bizSchema, bizObject, load.get(), syncInstance, options);
            }
        }
    }

    private boolean isModifyOwner(String ownerId, BizObjectCreatedModel oldObj) {
        if (Objects.isNull(oldObj)) {
            return false;
        }
        String oldOwnerId = this.getSelectionId(oldObj.getOwner(), DefaultPropertyType.OWNER.getCode());
        if (StringUtils.isEmpty(oldOwnerId)) {
            return false;
        }
        return !StringUtils.equals(ownerId, oldOwnerId);
    }

    private void modifyOwnerInfo(String ownerId, BizObjectModel bizObject) {
        UserModel userById = userFacade.get(ownerId);
        String ownerDepartmentId = userById.getDepartmentId();
        bizObject.setOwnerDeptId(ownerDepartmentId);
    }

    /**
     * 更新子表修改人和修改时间字段
     * @param userId
     * @param bizObject
     */
    private void updateChildTableModifyProperties(String userId, BizObjectModel bizObject) {
        for (Map.Entry<String, Object> entry : bizObject.getData().entrySet()) {
            BizProperty bizProperty = bizPropertyService.get(bizObject.getSchemaCode(), entry.getKey());

            if (Objects.isNull(bizProperty)) {
                continue;
            }

            if (bizProperty.getPropertyType() != BizPropertyType.CHILD_TABLE) {
                continue;
            }

            List<BizProperty> bizProperties = bizPropertyService.getListBySchemaCode(entry.getKey(), true);

            List<Map<String, Object>> childTableList = (List) entry.getValue();
            if (CollectionUtils.isEmpty(childTableList)) {
                continue;
            }

            boolean exists = bizProperties.stream().anyMatch(property -> DefaultSubPropertyType.MODIFIED_TIME.getCode().equals(property.getCode()));

            //判断当前子表是否存在修改时间字段，如果有才进行数据填充，用于兼容历史版本
            if (exists) {
                for (Map<String, Object> valueMap : childTableList) {
                    valueMap.put(DefaultSubPropertyType.MODIFIED_TIME.getCode(), new Date());
                    valueMap.put(DefaultSubPropertyType.MODIFIER.getCode(), userId);
                }
            }
        }
    }

    /**
     * 列表导入校验表单是否关联流程实例
     * @param bizObject
     * @param queryField
     * @return
     */
    @Override
    public Boolean checkBindWorkFlowInstance(final String userId, final BizObjectModel bizObject, final String queryField) {
        if (bizObject == null) {
            throw new ServiceException(ErrCode.SYS_PARAMETER_EMPTY);
        }
        String schemaCode = bizObject.getSchemaCode();
        if (StringUtils.isEmpty(bizObject.getFormCode())) {
            List<BizForm> bizForms = bizFormService.getListBySchemaCode(schemaCode);
            if (CollectionUtils.isNotEmpty(bizForms)) {
                bizObject.setFormCode(bizForms.get(0).getCode());
            }
        }
        final BizSchema bizSchema = bizSchemaService.getBySchemaCode(schemaCode);
        if (bizSchema == null) {
            log.debug("数据模型为空，schemaCode = {}", schemaCode);
            throw new ServiceException(ErrCode.BIZ_SCHEMA_CODE_INVALID);
        }
        //queryField为空，则是新增
        if (StringUtils.isEmpty(queryField)) {
            log.debug("queryField is null...");
            return Boolean.FALSE;
        }
        //系统数据项---单据号
        if (queryField.equals(DefaultPropertyType.SEQUENCE_NO.getCode())) {
            String sequenceNo = bizObject.getSequenceNo();
            if (!StringUtils.isEmpty(sequenceNo)) {
                List<BizQuery> bizQuerys = bizQueryService.getListBySchemaCodeFromDB(schemaCode);
                if (CollectionUtils.isNotEmpty(bizQuerys)) {
                    String code = bizQuerys.get(0).getCode();
                    //若选择单据号，则会根据所传单据号对已有数据做更新
                    if (!StringUtils.isEmpty(code)) {
                        //处理id查询条件信息
                        List<FilterExpression> filterExprCodes = new ArrayList<>();
                        FilterExpression filterExprCode = FilterExpression.empty;
                        //获取数据查询条件
                        filterExprCodes.add(Q.it("sequenceNo", FilterExpression.Op.Eq, bizObject.getSequenceNo()));
                        filterExprCode = filterExprCodes.get(0);
                        BizObjectQueryModel bizObjectQueryModel = new BizObjectQueryModel();
                        bizObjectQueryModel.setFilterExpr(filterExprCode);
                        bizObjectQueryModel.setSchemaCode(schemaCode);
                        //分页信息
                        int page = 0;
                        int size = Integer.MAX_VALUE;
                        PageableImpl pageable = new PageableImpl(page * size, size);
                        bizObjectQueryModel.setPageable(pageable);
                        bizObjectQueryModel.setQueryCode(code);

                        Page<BizObjectModel> bizObjectModelPage = queryBizObjects(bizObjectQueryModel);
                        List<? extends BizObjectModel> content = bizObjectModelPage.getContent();

                        if (!CollectionUtils.isEmpty(content)) {
                            BizObjectModel bizObjectModel = content.get(0);
                            String bizObjectModelId = bizObjectModel.getId();
                            BizObjectCreatedModel bizObjectDb = getBizObject(userId, schemaCode, bizObjectModelId);
                            String sequenceNoDb = bizObjectDb.getSequenceNo();
                            if (sequenceNo.equals(sequenceNoDb)) {
                                String bizObjectId = bizObjectModel.getId();
                                return isFormBindWorkflowInstance(bizObjectId);
                            }
                        }
                    }
                }
            }
        }
        //系统数据项---数据标题
        if (queryField.equals(DefaultPropertyType.NAME.getCode())) {
            String name = bizObject.getName();
            //若选择数据标题，则会根据所传数据标题对已有数据做更新
            if (!StringUtils.isEmpty(name)) {
                List<BizQuery> bizQuerys = bizQueryService.getListBySchemaCode(schemaCode);
                if (CollectionUtils.isNotEmpty(bizQuerys)) {
                    String code = bizQuerys.get(0).getCode();
                    if (!StringUtils.isEmpty(code)) {
                        List<FilterExpression> filterExprNames = new ArrayList<>();
                        FilterExpression filterExprName = FilterExpression.empty;
                        //获取数据查询条件
                        filterExprNames.add(Q.it("name", FilterExpression.Op.Eq, bizObject.getName()));
                        filterExprName = filterExprNames.get(0);
                        BizObjectQueryModel bizObjectQueryModel = new BizObjectQueryModel();
                        bizObjectQueryModel.setFilterExpr(filterExprName);
                        bizObjectQueryModel.setSchemaCode(schemaCode);
                        //分页信息
                        int page = 0;
                        int size = Integer.MAX_VALUE;
                        PageableImpl pageable = new PageableImpl(page * size, size);
                        bizObjectQueryModel.setPageable(pageable);
                        bizObjectQueryModel.setQueryCode(code);

                        Page<BizObjectModel> bizObjectModelPage = queryBizObjects(bizObjectQueryModel);
                        List<? extends BizObjectModel> content = bizObjectModelPage.getContent();

                        if (!CollectionUtils.isEmpty(content)) {
                            BizObjectModel bizObjectModel = content.get(0);
                            String bizObjectId = bizObjectModel.getId();
                            log.info("bizObjectId的数据：bizObjectId={}", JSONObject.toJSONString(bizObjectId));
                            BizObjectCreatedModel bizObjectDb = getBizObject(userId, schemaCode, bizObjectId);
                            String nameDb = bizObjectDb.getName();
                            if (name.equals(nameDb)) {
                                String bizObjectIdDb = bizObjectModel.getId();
                                return isFormBindWorkflowInstance(bizObjectIdDb);
                            }
                        }
                    }
                }
            }
        }
        //已发布短文本数据项
        List<BizProperty> properties = bizPropertyService.getListBySchemaCode(schemaCode, true);
        if (CollectionUtils.isEmpty(properties)) {
            return Boolean.FALSE;
        }
        for (BizProperty bizPropertyMode : properties) {
            if (bizPropertyMode.getPropertyType() != BizPropertyType.SHORT_TEXT) {
                continue;
            }
            Boolean published = bizPropertyMode.getPublished();
            if (published == null || !published) {
                continue;
            }
            if (!queryField.equals(bizPropertyMode.getCode())) {
                continue;
            }
            Object propertyValue = bizObject.getData().get(bizPropertyMode.getCode());
            if (propertyValue != null && StringUtils.isNotBlank(propertyValue.toString())) {
                List<BizQuery> bizQuerys = bizQueryService.getListBySchemaCode(schemaCode);
                if (CollectionUtils.isEmpty(bizQuerys)) {
                    continue;
                }
                String code = bizQuerys.get(0).getCode();
                if (StringUtils.isEmpty(code)) {
                    continue;
                }

                //获取数据查询条件
                FilterExpression filterExprValue = Q.it(queryField, FilterExpression.Op.Eq, propertyValue);

                if (log.isDebugEnabled()) {
                    log.debug("===333====已发布短文本数据项 filterExprValue==============={}", JSONObject.toJSONString(filterExprValue));
                }
                BizObjectQueryModel bizObjectQueryModel = new BizObjectQueryModel();
                bizObjectQueryModel.setFilterExpr(filterExprValue);
                //分页信息
                PageableImpl pageable = new PageableImpl(0, Integer.MAX_VALUE);
                bizObjectQueryModel.setPageable(pageable);
                bizObjectQueryModel.setSchemaCode(schemaCode);
                bizObjectQueryModel.setQueryCode(code);
                Page<BizObjectModel> bizObjectModelPage = queryBizObjects(bizObjectQueryModel);
                List<? extends BizObjectModel> content = bizObjectModelPage.getContent();
                if (!CollectionUtils.isEmpty(content)) {
                    for (BizObjectModel bizObjectModel : content) {
                        String bizObjectModelId = bizObjectModel.getId();
                        log.info("bizObjectModelId的数据：bizObjectModelId={}", JSONObject.toJSONString(bizObjectModelId));
                        BizObjectCreatedModel bizObjectDb = getBizObject(userId, schemaCode, bizObjectModelId);
                        Object propertyDb = bizObjectDb.getData().get(bizPropertyMode.getCode());
                        if (propertyValue.equals(propertyDb)) {
                            String bizObjectId = bizObjectModel.getId();
                            return isFormBindWorkflowInstance(bizObjectId);
                        }
                    }
                }
            }
        }
        return Boolean.FALSE;
    }

    @Override
    public String saveBizObjectModel(final String userId, final BizObjectModel bizObject, final String queryField) {
        if (bizObject == null) {
            throw new ServiceException(ErrCode.SYS_PARAMETER_EMPTY);
        }
        String schemaCode = bizObject.getSchemaCode();
        if (StringUtils.isEmpty(bizObject.getFormCode())) {
            List<BizForm> bizForms = bizFormService.getListBySchemaCode(schemaCode);
            if (CollectionUtils.isNotEmpty(bizForms)) {
                bizObject.setFormCode(bizForms.get(0).getCode());
            }
        }
        if (log.isTraceEnabled()) {
            log.trace("bizObject json = {}", JSONObject.toJSONString(bizObject));
        }
        final BizSchema bizSchema = bizSchemaService.getBySchemaCode(schemaCode);
        if (bizSchema == null) {
            log.debug("数据模型为空，schemaCode = {}", schemaCode);
            throw new ServiceException(ErrCode.BIZ_SCHEMA_CODE_INVALID);
        }
        if (log.isDebugEnabled()) {
            log.debug("summary = {}", bizSchema.getSummary());
        }
        log.info("查询参数：queryField={}", JSONObject.toJSONString(queryField));
        if (StringUtils.isEmpty(queryField)) {
            log.debug("queryField is null...");
            return createBizObject(userId, bizSchema, bizObject);
        } else {
            bizObject.setExtendInfo(BizObjectExtendInfo.builder().creatorId(userId).build());
            switch (queryField) {
                //系统数据项---单据号
                case "sequenceNo":
                    return ProcessingSequenceNo(userId, bizObject, schemaCode, bizSchema);
                //系统数据项---数据标题
                case "name":
                    return ProcessingName(userId, bizObject, schemaCode, bizSchema);
            }
            //已发布短文本数据项
            return ProcessingShortText(userId, bizObject, schemaCode, bizSchema, queryField);
        }

    }

    @Override
    public void batchSaveBizObjectModel(String userId, List<BizObjectModel> bizObjectModelList, String queryFiled) {
        if (CollectionUtils.isEmpty(bizObjectModelList)) {
            return;
        }
        // 构建一个模板模型，生成共同属性，不用每次重复生成
        BizObjectModel defaultTemplateModel = bizObjectModelList.get(0);
        String schemaCode = defaultTemplateModel.getSchemaCode();
        if (StringUtils.isEmpty(defaultTemplateModel.getFormCode())) {
            List<BizForm> bizForms = bizFormService.getListBySchemaCode(schemaCode);
            if (CollectionUtils.isNotEmpty(bizForms)) {
                defaultTemplateModel.setFormCode(bizForms.get(0).getCode());
            }
        }

        //批量生成单据号需要对应bo，提前生成bo id
        for (BizObjectModel bizObjectModel : bizObjectModelList) {
            //校验有没有id 没有就生成uuid
            if (StringUtils.isEmpty(bizObjectModel.getId())) {
                bizObjectModel.setId(genId());
            }
        }

        //是否需要批量生成单据号，如果上层已传递单据号，不再生成
        boolean needGenSequenceNo = bizObjectModelList.stream().allMatch(bizObjectModel -> StringUtils.isBlank(bizObjectModel.getSequenceNo()));

        List<BizDataRule> calculateRuleList = new ArrayList<>();
        BizSchema bizSchema = bizSchemaService.getBySchemaCode(schemaCode);
        String boCreate = defaultTemplateModel.getString(DefaultPropertyType.CREATER.getCode());
        String createBy = StringUtils.isEmpty(boCreate) ? userId : boCreate;
        defaultTemplateModel.setCreater(createBy);
        String sequenceStatus = defaultTemplateModel.getString(DefaultPropertyType.SEQUENCE_STATUS.getCode());
        if (StringUtils.isEmpty(sequenceStatus)) {
            defaultTemplateModel.setSequenceStatus(WorkflowInstanceStatus.PROCESSING.toString());
        }
        DepartmentModel department = getDepartmentByUserId(createBy);
        if (department != null) {
            defaultTemplateModel.setCreatedDeptId(department.getId());
        }
        defaultTemplateModel.setModifier(userId);
        defaultTemplateModel.setCreatedTime(parseData(new Date()));
        defaultTemplateModel.setModifiedTime(new Date());
        //预先批量生成单据号
        if (needGenSequenceNo) {
            sequenceSettingService.batchGenerateSequenceNo(bizSchema.getCode(), defaultTemplateModel.getFormCode(), bizObjectModelList);
        }
        Map<String, List<Map<String, Object>>> queryFiledMap = genQueryFiledMap(schemaCode, queryFiled);
        List<List<BizObjectModel>> partition = ListUtils.partition(bizObjectModelList, BATCH_INSERT_SIZE);
        AtomicInteger index = new AtomicInteger();
        CountDownLatch countDownLatch = new CountDownLatch(partition.size());
        partition.forEach(item -> batchInsertPool.execute(() -> {
            try {
                List<BizObject> bizObjectInsertList = new ArrayList<>(item.size());
                long begin = System.currentTimeMillis();
                item.forEach(bizObjectModel -> {
                    if (MapUtils.isNotEmpty(queryFiledMap) && queryFiledMap.containsKey(String.valueOf(bizObjectModel.getData().get(queryFiled)))) {
                        List<Map<String, Object>> dataDbList = queryFiledMap.get(String.valueOf((bizObjectModel.get(queryFiled))));
                        dataDbList.forEach(dataDb -> {
                            BizObject bizObject = buildBizObject(userId, bizObjectModel, defaultTemplateModel, bizSchema, calculateRuleList, dataDb);
                            bizObjectLocalCacheService.update(bizObject);
                        });
                    } else {
                        bizObjectInsertList.add(buildBizObject(userId, bizObjectModel, defaultTemplateModel, bizSchema, calculateRuleList, null));
                    }
                });
                long end = System.currentTimeMillis();
                log.info("batch{}, buildData ,size:{}, time:{}", index.get(), item.size(), end - begin);
                begin = end;
                bizObjectLocalCacheService.batchInsertObject(bizObjectInsertList, "insert");
                end = System.currentTimeMillis();
                log.info("batch{} batchInsert,size:{}, time:{}", index.get(), item.size(), end - begin);
            } catch (Exception e) {
                log.error("数据插入失败,", e);
            } finally {
                countDownLatch.countDown();
            }
        }));
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private Map<String, List<Map<String, Object>>> genQueryFiledMap(String schemaCode, String queryFiled) {
        if (StringUtils.isBlank(queryFiled) || "false".equals(queryFiled)) {
            return null;
        }
        Page<Map<String, Object>> page = bizObjectService.queryBizObjects(new BizObjectQueryObject() {
            @Override
            public String getSchemaCode() {
                return schemaCode;
            }

            @Override
            public FilterExpression getFilterExpression() {

                return Q.it(queryFiled, FilterExpression.Op.IsNotNull, "");
            }

            @Override
            public List<String> getDisplayFields() {
                HashSet<String> displayFields = Sets.newHashSet(DefaultPropertyType.ID.getCode(), queryFiled, DefaultPropertyType.NAME.getCode(), DefaultPropertyType.SEQUENCE_NO.getCode(), DefaultPropertyType.CREATED_TIME.getCode());
                return new ArrayList<>(displayFields);
            }

            @Override
            public Pageable getPageable() {
                return new PageableImpl(0, Integer.MAX_VALUE);
            }
        });
        if (page == null || CollectionUtils.isEmpty(page.getContent())) {
            return null;
        }
        List<Map<String, Object>> content = (List<Map<String, Object>>) page.getContent();
        Map<String, List<Map<String, Object>>> result = Maps.newHashMapWithExpectedSize(content.size());
        content.forEach(item -> {
            String key = String.valueOf(item.get(queryFiled));
            String id = String.valueOf(item.get(DefaultPropertyType.ID.getCode()));

            List<Map<String, Object>> list = result.get(key);
            if (list == null) {
                list = new ArrayList<>();
                result.put(key, list);
            }
            list.add(item);
        });
        return result;
    }


    private BizObject buildBizObject(String userId, BizObjectModel bizObjectModel, BizObjectModel defaultTemplateModel, BizSchema bizSchema, List<BizDataRule> calculateRuleList, Map<String, Object> dataDb) {
        if (bizObjectModel == null) {
            throw new ServiceException(ErrCode.SYS_PARAMETER_EMPTY);
        }
        bizObjectModel.setFormCode(defaultTemplateModel.getFormCode());
        String creater = defaultTemplateModel.getData().get(DefaultPropertyType.CREATER.getCode()).toString();
        bizObjectModel.setCreater(creater);
        String owner = bizObjectModel.getString(DefaultPropertyType.OWNER.getCode());
        if (StringUtils.isEmpty(owner)) {
            owner = userId;
            bizObjectModel.setOwner(owner);
        }
        String sequenceStatus = defaultTemplateModel.getSequenceStatus();
        bizObjectModel.setSequenceStatus(sequenceStatus);
        DepartmentModel department = getDepartmentInUserMap(creater);
        if (department != null) {
            bizObjectModel.setCreatedDeptId(department.getId());
        }
        //设置所属人部门
        String ownerId = getIdFromObject(bizObjectModel.get(DefaultPropertyType.OWNER.getCode()));
        if (!creater.equals(ownerId)) {
            DepartmentModel ownerDepartment = getDepartmentInUserMap(ownerId);
            if (ownerDepartment != null) {
                bizObjectModel.setOwnerDeptId(ownerDepartment.getId());
                bizObjectModel.setOwnerDeptQueryCode(ownerDepartment.getQueryCode());
            }
        } else if (department != null) {
            bizObjectModel.setOwnerDeptId(department.getId());
            bizObjectModel.setOwnerDeptQueryCode(department.getQueryCode());
        }

        if (dataDb == null) {
            bizObjectModel.setCreatedTime(defaultTemplateModel.getCreatedTime());
            String name = bizSchemaService.getName(bizSchema.getCode(), bizSchema.getName(), bizSchema.getSummary(), bizObjectModel.getData());
            bizObjectModel.setName(name);
            //暂存不生成单据号，提交生成单据号。导入数据状态已完成生成单据号
            if (SequenceStatus.COMPLETED.toString().equals(sequenceStatus) || Objects.equals(Boolean.TRUE, bizObjectModel.getData().get(Integer.MAX_VALUE + "_sequenceNo"))) {
                //单据号有值的情况 不重新生成
                if (StringUtils.isBlank(bizObjectModel.getSequenceNo())) {
                    String sequenceNo = sequenceSettingService.genSequenceNoFromQueue(bizSchema.getCode(), defaultTemplateModel.getFormCode(), bizObjectModel.getId());
                    if (StringUtils.isBlank(sequenceNo)) {
                        sequenceNo = sequenceSettingService.createSequenceNo(bizSchema.getCode(), defaultTemplateModel.getFormCode(), bizObjectModel);
                    }
                    bizObjectModel.setSequenceNo(sequenceNo);
                }
            }
        } else {
            // 覆盖导入 不修改 标题、单据号、创建时间
            bizObjectModel.setId((String) dataDb.get(DefaultPropertyType.ID.getCode()));
            bizObjectModel.setCreatedTime((Date) dataDb.get(DefaultPropertyType.CREATED_TIME.getCode()));
            bizObjectModel.setName((String) dataDb.get(DefaultPropertyType.NAME.getCode()));
            bizObjectModel.setSequenceNo((String) dataDb.get(DefaultPropertyType.SEQUENCE_NO.getCode()));
        }
        bizObjectModel.setModifier(userId);
        bizObjectModel.setModifiedTime(defaultTemplateModel.getModifiedTime());
        bizObjectModel.getData().remove(Integer.MAX_VALUE + "_sequenceNo");

        //校验有没有id 没有就生成uuid
        if (StringUtils.isEmpty(bizObjectModel.getId())) {
            bizObjectModel.setId(genId());
        }
        return new BizObject(bizObjectModel.getSchemaCode(), bizObjectModel.getId(), bizObjectModel.getData());
    }

    private DepartmentModel getDepartmentInUserMap(String userId) {
        DepartmentModel departmentModel = userDepartmentMap.get(userId);
        if (departmentModel != null) {
            return departmentModel;
        }
        DepartmentModel department = getDepartmentByUserId(userId);
        userDepartmentMap.put(userId, department);
        return department;
    }

    @Override
    public BizObjectModel updateBizObjectModel(String userId, BizObjectModel bizObject, BizSchemaModel bizSchemaModel, BizObjectOptions options) {
        if (bizObject == null) {
            throw new ServiceException(ErrCode.SYS_PARAMETER_EMPTY);
        }

        if (StringUtils.isEmpty(bizObject.getFormCode())) {
            List<BizForm> bizForms = bizFormService.getListBySchemaCode(bizObject.getSchemaCode());
            if (CollectionUtils.isNotEmpty(bizForms)) {
                bizObject.setFormCode(bizForms.get(0).getCode());
            }
        }

        bizFormService.verifyControlProperty(bizObject);

        if (log.isTraceEnabled()) {
            log.trace("bizObject json = {}", JSONObject.toJSONString(bizObject));
        }
        if (bizSchemaModel == null) {
            log.debug("数据模型为空，schemaCode = {}", bizObject.getSchemaCode());
            throw new ServiceException(ErrCode.BIZ_SCHEMA_MODEL_NOT_EXIST, bizObject.getSchemaCode());
        }
        if (log.isDebugEnabled()) {
            log.debug("summary = {}", bizSchemaModel.getSummary());
        }

        if (StringUtils.isEmpty(bizObject.getId())) {
            log.debug("objectId is null...");
            //插入bo的方法需要后续完成
            return null;

        } else {
            final BizObject objectExisted = bizObjectService.loadBizObject(bizObject.getSchemaCode(), bizObject.getId(), null);

            if (!objectExisted.isLoadedFromDb()) {
                log.debug("bizObject do not from db...");
                //插入bo的方法需要后续完成
                return null;
            } else {
                log.debug("bizObject from db...");
                //插入bo的方法需要后续完成
                BizSchema bizSchema = ModelUtil.toEntity(bizSchemaModel, BizSchema.class);
                BizObjectModel bizObjectModel = modifiedBizObject(userId, bizSchema, bizObject, objectExisted, options);
                return bizObjectModel;
            }
        }
    }

    //系统数据项---单据号
    private String ProcessingSequenceNo(final String userId, final BizObjectModel bizObject, final String schemaCode, final BizSchema bizSchema) {
        String sequenceNo = bizObject.getSequenceNo();
        if (!StringUtils.isEmpty(sequenceNo)) {
            log.info("sequenceNo的数据：sequenceNo={}", JSONObject.toJSONString(sequenceNo));
            List<BizQuery> bizQuerys = bizQueryService.getListBySchemaCode(schemaCode);
            if (CollectionUtils.isNotEmpty(bizQuerys)) {
                String code = bizQuerys.get(0).getCode();
                log.info("code的数据：code={}", JSONObject.toJSONString(code));
                if (!StringUtils.isEmpty(code)) {

                    //处理id查询条件信息
                    List<FilterExpression> filterExprCodes = new ArrayList<>();
                    FilterExpression filterExprCode = FilterExpression.empty;
                    log.info("生成id数据查询条件");
                    //获取数据查询条件
                    filterExprCodes.add(Q.it("sequenceNo", FilterExpression.Op.Eq, bizObject.getSequenceNo()));
                    filterExprCode = filterExprCodes.get(0);
                    BizObjectQueryModel bizObjectQueryModel = new BizObjectQueryModel();
                    bizObjectQueryModel.setFilterExpr(filterExprCode);
                    bizObjectQueryModel.setSchemaCode(schemaCode);
                    //分页信息
                    int page = 0;
                    int size = Integer.MAX_VALUE;
                    PageableImpl pageable = new PageableImpl(page * size, size);
                    bizObjectQueryModel.setPageable(pageable);
                    bizObjectQueryModel.setQueryCode(code);
                    Page<BizObjectModel> bizObjectModelPage = queryBizObjects(bizObjectQueryModel);
                    List<? extends BizObjectModel> content = bizObjectModelPage.getContent();
                    if (!CollectionUtils.isEmpty(content)) {
                        BizObjectModel bizObjectModel = content.get(0);
                        String bizObjectModelId = bizObjectModel.getId();
                        BizObjectCreatedModel bizObjectDb = getBizObject(userId, schemaCode, bizObjectModelId);
                        String sequenceNoDb = bizObjectDb.getSequenceNo();
                        if (log.isDebugEnabled()) {
                            log.debug("bizObjectModelId的数据：bizObjectModelId={}", bizObjectModelId);
                            log.info("sequenceNoDb的数据：sequenceNoDb={}", JSONObject.toJSONString(sequenceNoDb));
                        }
                        if (sequenceNo.equals(sequenceNoDb)) {
                            Map<String, Object> data = bizObjectModel.getData();
                            String schemaCode1 = bizObjectModel.getSchemaCode();
                            String id = bizObjectModel.getId();
                            data.put(DefaultPropertyType.NAME.getCode(), bizObjectDb.getName());
                            data.put(DefaultPropertyType.CREATED_TIME.getCode(), bizObjectDb.getCreatedTime());
                            BizObject exitBizObject = new BizObject(schemaCode1, id, data);
                            this.tagChildTableDataTODeleted(schemaCode, bizObject, exitBizObject);
                            return updateBizObject(userId, bizSchema, bizObject, exitBizObject, Boolean.TRUE);
                        }
                    }
                }
            }
        }
        return createBizObject(userId, bizSchema, bizObject);
    }

    // 系统数据项---数据标题
    private String ProcessingName(final String userId, final BizObjectModel bizObject, final String schemaCode, final BizSchema bizSchema) {
        String name = bizObject.getName();
        log.info("数据标题：name={}", JSONObject.toJSONString(name));
        if (!StringUtils.isEmpty(name)) {
            List<BizQuery> bizQuerys = bizQueryService.getListBySchemaCode(schemaCode);
            if (CollectionUtils.isNotEmpty(bizQuerys)) {
                String code = bizQuerys.get(0).getCode();
                log.info("列表编码：code={}", JSONObject.toJSONString(code));
                if (!StringUtils.isEmpty(code)) {
                    List<FilterExpression> filterExprNames = new ArrayList<>();
                    FilterExpression filterExprName = FilterExpression.empty;
                    log.info("生成id数据查询条件");
                    //获取数据查询条件
                    filterExprNames.add(Q.it("name", FilterExpression.Op.Eq, bizObject.getName()));
                    filterExprName = filterExprNames.get(0);
                    BizObjectQueryModel bizObjectQueryModel = new BizObjectQueryModel();
                    bizObjectQueryModel.setFilterExpr(filterExprName);
                    bizObjectQueryModel.setSchemaCode(schemaCode);
                    //分页信息
                    int page = 0;
                    int size = Integer.MAX_VALUE;
                    PageableImpl pageable = new PageableImpl(page * size, size);
                    bizObjectQueryModel.setPageable(pageable);
                    bizObjectQueryModel.setQueryCode(code);
                    Page<BizObjectModel> bizObjectModelPage = queryBizObjects(bizObjectQueryModel);
                    List<? extends BizObjectModel> content = bizObjectModelPage.getContent();
                    if (!CollectionUtils.isEmpty(content)) {
                        BizObjectModel defaultBizObjectModel = content.get(0);
                        String bizObjectId = defaultBizObjectModel.getId();
                        log.info("bizObjectId的数据：bizObjectId={}", JSONObject.toJSONString(bizObjectId));
                        BizObjectCreatedModel bizObjectDb = getBizObject(userId, schemaCode, bizObjectId);
                        String nameDb = bizObjectDb.getName();
                        if (name.equals(nameDb)) {
                            Map<String, Object> data = bizObjectDb.getData();
                            String schemaCode1 = bizObjectDb.getSchemaCode();
                            String id = bizObjectDb.getId();
                            data.put(DefaultPropertyType.SEQUENCE_NO.getCode(), bizObjectDb.getSequenceNo());
                            data.put(DefaultPropertyType.CREATED_TIME.getCode(), bizObjectDb.getCreatedTime());
                            BizObject exitBizObject = new BizObject(schemaCode1, id, data);
                            this.tagChildTableDataTODeleted(schemaCode, bizObject, exitBizObject);
                            return updateBizObject(userId, bizSchema, bizObject, exitBizObject, Boolean.TRUE);
                        }
                    }
                }
            }
        }

        return createBizObject(userId, bizSchema, bizObject);
    }

    //已发布短文本数据项
    private String ProcessingShortText(final String userId, final BizObjectModel bizObject, final String schemaCode, final BizSchema bizSchema, final String queryField) {
        List<BizProperty> properties = bizPropertyService.getListBySchemaCode(schemaCode, true);
        properties = properties.stream().filter(propertyModel -> (propertyModel.getPublished() && (!propertyModel.getDefaultProperty()))).collect(Collectors.toList());
        log.debug("业务数据项：properties={}", JSONObject.toJSONString(properties.toString()));
        if (CollectionUtils.isEmpty(properties)) {
            return createBizObject(userId, bizSchema, bizObject);
        }
        List<BizQuery> bizQuerys = bizQueryService.getListBySchemaCode(schemaCode);
        for (BizProperty bizPropertyMode : properties) {
            if (!(bizPropertyMode.getPropertyType() == BizPropertyType.SHORT_TEXT)) {
                continue;
            }
            if (!queryField.equals(bizPropertyMode.getCode())) {
                continue;
            }
            Object propertyValue = bizObject.getData().get(bizPropertyMode.getCode());
            if (Objects.isNull(propertyValue) || StringUtils.isEmpty(propertyValue.toString())) {
                continue;
            }
            if (CollectionUtils.isEmpty(bizQuerys)) {
                continue;
            }
            String code = bizQuerys.get(0).getCode();
            if (StringUtils.isEmpty(code)) {
                continue;
            }
            log.info("生成id数据查询条件");
            //获取数据查询条件
            FilterExpression filterExprValue = Q.it(queryField, FilterExpression.Op.Eq, bizObject.getData().get(bizPropertyMode.getCode()));
            BizObjectQueryModel bizObjectQueryModel = new BizObjectQueryModel();
            bizObjectQueryModel.setFilterExpr(filterExprValue);
            //分页信息
            int page = 0;
            int size = Integer.MAX_VALUE;
            PageableImpl pageable = new PageableImpl(page * size, size);
            bizObjectQueryModel.setPageable(pageable);
            bizObjectQueryModel.setSchemaCode(schemaCode);
            bizObjectQueryModel.setQueryCode(code);
            Page<BizObjectModel> bizObjectModelPage = queryBizObjects(bizObjectQueryModel);
            List<? extends BizObjectModel> content = bizObjectModelPage.getContent();
            if (CollectionUtils.isEmpty(content)) {
                continue;
            }
            for (BizObjectModel bizObjectModel : content) {
                String bizObjectModelId = bizObjectModel.getId();
                log.info("bizObjectModelId的数据：bizObjectModelId={}", JSONObject.toJSONString(bizObjectModelId));
                BizObjectCreatedModel bizObjectDb = getBizObject(userId, schemaCode, bizObjectModelId);
                Object propertyDb = bizObjectDb.getData().get(bizPropertyMode.getCode());
                if (propertyValue.equals(propertyDb)) {
                    Map<String, Object> data = bizObjectModel.getData();
                    String schemaCode1 = bizObjectModel.getSchemaCode();
                    String id = bizObjectModel.getId();
                    data.put(DefaultPropertyType.SEQUENCE_NO.getCode(), bizObjectDb.getSequenceNo());
                    data.put(DefaultPropertyType.NAME.getCode(), bizObjectDb.getName());
                    data.put(DefaultPropertyType.CREATED_TIME.getCode(), bizObjectDb.getCreatedTime());
                    BizObject exitBizObject = new BizObject(schemaCode1, id, data);
                    this.tagChildTableDataTODeleted(schemaCode, bizObject, exitBizObject);
                    return updateBizObject(userId, bizSchema, bizObject, exitBizObject, Boolean.TRUE);
                }
            }
        }
        return createBizObject(userId, bizSchema, bizObject);
    }

    /**
     * 子表增量提交模式，标记历史字表数据为删除状态
     * @param schemaCode
     * @param bizObject
     * @param objectExisted
     */
    private void tagChildTableDataTODeleted(String schemaCode, BizObjectModel bizObject, BizObject objectExisted) {
        List<BizProperty> properties = bizPropertyService.getListBySchemaCode(schemaCode, true);
        if (org.springframework.util.CollectionUtils.isEmpty(properties)) {
            return;
        }

        List<BizProperty> childTablePropertys = properties.stream().filter(bizProperty -> bizProperty.getPropertyType() == BizPropertyType.CHILD_TABLE).collect(Collectors.toList());
        if (org.springframework.util.CollectionUtils.isEmpty(childTablePropertys)) {
            return;
        }

        childTablePropertys.stream().forEach(bizProperty -> {
            List<Map<String, Object>> childTables = (List<Map<String, Object>>) objectExisted.getData().get(bizProperty.getCode());
            if (org.springframework.util.CollectionUtils.isEmpty(childTables)) {
                return;
            }

            childTables.stream().forEach(map -> {
                map.put(BizObject.DATAROWSTATUS, DataRowStatus.Deleted.name());
            });

            if (bizObject.getData().get(bizProperty.getCode()) == null) {
                bizObject.getData().put(bizProperty.getCode(), childTables);
            } else {
                ((List<Map<String, Object>>) bizObject.getData().get(bizProperty.getCode())).addAll(childTables);
            }
        });
    }

    /**
     * 根据业务对象ID 关联到流程实例表 再根据流程实例ID关联到 biz_workflow表中是否存在数据 不为空则流程还未启动或表单未关联任何流程
     * @param bizObjectId
     * @return
     */
    private Boolean isFormBindWorkflowInstance(String bizObjectId) {
        if (log.isDebugEnabled()) {
            log.debug("开始查询业务表单是否关联流程实例，业务对象ID{}", bizObjectId);
        }
        Assert.notNull(bizObjectId, " isFormBindWorkflowInstance bizObjectId is null");
        WorkflowInstanceModel bizWorkflowInstance = bizWorkflowInstanceService.getByObjectId(bizObjectId);
        //流程实例表为空，则表单未关联流程
        if (Objects.isNull(bizWorkflowInstance)) {
            return Boolean.FALSE;
        }
        String instanceId = bizWorkflowInstance.getId();
        List<WorkItemModel> bizWorkItemList = bizWorkitemService.getListByInstanceId(instanceId);
        List<WorkItemModel> bizWorkItemFinishedList = bizWorkitemFinishedService.getListByInstanceId(instanceId);
        //流程待办列表为空&流程已完成列表都为空，则表单未关联流程实例
        if (CollectionUtils.isEmpty(bizWorkItemFinishedList) && CollectionUtils.isEmpty(bizWorkItemList)) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    /**
     * 业务对象不存在，创建业务对象
     * @param userId    用户id
     * @param bizObject 业务对象
     * @param bizSchema 业务模型
     * @return objectId
     */
    private String createBizObject(final String userId, final BizSchema bizSchema, final BizObjectModel bizObject) {
        if (log.isDebugEnabled()) {
            log.debug("bizObject： bizobject={}", JSONObject.toJSONString(bizObject));
        }
        String boCreate = bizObject.getString(DefaultPropertyType.CREATER.getCode());
        String createBy = StringUtils.isEmpty(boCreate) ? userId : boCreate;
        bizObject.setCreater(createBy);

        String owner = bizObject.getString(DefaultPropertyType.OWNER.getCode());
        if (StringUtils.isEmpty(owner)) {
            owner = userId;
            bizObject.setOwner(owner);
        }
        String sequenceStatus = bizObject.getString(DefaultPropertyType.SEQUENCE_STATUS.getCode());
        if (StringUtils.isEmpty(sequenceStatus)) {
            bizObject.setSequenceStatus(WorkflowInstanceStatus.PROCESSING.toString());
        }

        DepartmentModel department = null;
        Object createdDeptId = bizObject.getData().get(DefaultPropertyType.CREATED_DEPT_ID.getCode());
        if (Objects.nonNull(createdDeptId)) {
            department = departmentFacade.get(createdDeptId.toString());
        } else {
            department = getDepartmentByUserId(createBy);
            if (Objects.nonNull(department)) {
                bizObject.setCreatedDeptId(department.getId());
            }
        }

        //设置所属人部门
        Object boOwnerDepartment = bizObject.getObject(DefaultPropertyType.OWNER_DEPT_ID.getCode());
        if (boOwnerDepartment == null) {
            String ownerId = getIdFromObject(bizObject.get(DefaultPropertyType.OWNER.getCode()));
            if (!createBy.equals(ownerId)) {
                DepartmentModel ownerDepartment = getDepartmentByUserId(ownerId);
                if (ownerDepartment != null) {
                    bizObject.setOwnerDeptId(ownerDepartment.getId());
                    bizObject.setOwnerDeptQueryCode(ownerDepartment.getQueryCode());
                }
            } else if (department != null) {
                bizObject.setOwnerDeptId(department.getId());
                bizObject.setOwnerDeptQueryCode(department.getQueryCode());
            }
        } else {
            DepartmentModel ownerDepartment;
            String ownerDepartmentId = this.getSelectionId(boOwnerDepartment, DefaultPropertyType.OWNER_DEPT_ID.getCode());
            if (department != null && ownerDepartmentId.equals(department.getId())) {
                ownerDepartment = department;
            } else {
                ownerDepartment = this.departmentFacade.get(ownerDepartmentId);
            }
            if (ownerDepartment != null) {
                bizObject.setOwnerDeptId(ownerDepartment.getId());
                bizObject.setOwnerDeptQueryCode(ownerDepartment.getQueryCode());
            }
        }

        bizObject.setModifier(userId);
        //处理毫秒
        Date date = parseData(new Date());
        bizObject.setCreatedTime(date);
        bizObject.setModifiedTime(new Date());
        //暂存不生成单据号，提交生成单据号。导入数据状态已完成生成单据号
        if (SequenceStatus.COMPLETED.toString().equals(bizObject.getSequenceStatus())
                || Objects.equals(Boolean.TRUE, bizObject.getData().get(Integer.MAX_VALUE + "_sequenceNo"))) {
            if (StringUtils.isBlank(bizObject.getSequenceNo())) {
                String sequenceNo = sequenceSettingService.createSequenceNo(bizSchema.getCode(), bizObject.getFormCode(), bizObject);
                bizObject.setSequenceNo(sequenceNo);
            }
        }
        bizObject.getData().remove(Integer.MAX_VALUE + "_sequenceNo");

        String name = bizSchemaService.getName(bizSchema.getCode(), bizSchema.getName(), bizSchema.getSummary(), bizObject.getData());
        bizObject.setName(name);

        //校验有没有id 没有就生成uuid
        String id = null;
        if (StringUtils.isEmpty(bizObject.getId())) {
            id = genId();
        } else {
            id = bizObject.getId();
        }
        BizObject bo = new BizObject(bizObject.getSchemaCode(), id, bizObject.getData());
        Object objId = bo.getData().get("id");
        if (Objects.isNull(objId)) {
            bo.getData().put("id", bo.getId());
        }
        String objectId = null;
        try {
            ThreadLocalAttributes.setThreadAttribute("currentUserId", userId);
            objectId = bizObjectService.createBizObject(bo);
        } catch (Exception e) {
            throw e;
        } finally {
            ThreadLocalAttributes.removeThreadAttribute("currentUserId");
        }
        log.info("object id = {}", objectId);
        return objectId;
    }

    private String genId() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }


    private Date parseData(Date date) {
        if (date == null) {
            date = new Date();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 更新业务对象
     * @param userId        用户id
     * @param bizSchema     业务模型
     * @param bizObject     表单传递过来的业务对象
     * @param objectExisted 数据库存在的业务对象
     * @return objectId
     */
    public String updateBizObject(String userId, BizSchema bizSchema, BizObjectModel bizObject, BizObject objectExisted, Boolean syncInstance) {
        return updateBizObject(userId, bizSchema, bizObject, objectExisted, syncInstance, null);
    }

    /**
     * 更新业务对象
     * @param userId        用户id
     * @param bizSchema     业务模型
     * @param bizObject     表单传递过来的业务对象
     * @param objectExisted 数据库存在的业务对象
     * @param options       orm参数(可选)
     * @return objectId
     */
    public String updateBizObject(String userId, BizSchema bizSchema, BizObjectModel bizObject, BizObject objectExisted,
                                  Boolean syncInstance, BizObjectOptions options) {
        log.debug("update bizObject...");
        // 查询bo数据，避免生成摘要时丢失信息
        final BizObjectCreatedModel oldBizObject = new BizObjectCreatedModel(bizObject.getSchemaCode(), objectExisted.getData(), false);
        if (log.isTraceEnabled()) {
            log.trace("获取原表单数据:{}", JSON.toJSONString(oldBizObject));
            log.trace("当前表单数据:{}", JSON.toJSONString(bizObject));
        }

        String create = bizObject.getString(DefaultPropertyType.CREATER.getCode());
        log.info("=====================create================ {}", create);
        if (StringUtils.isEmpty(create)) {
            String createId = oldBizObject.getCreater() != null ? oldBizObject.getCreater().getId() : null;
            log.debug("获取到的用户ID:{}", createId);
            String uid = StringUtils.isEmpty(createId) ? userId : createId;
            bizObject.setCreater(uid);

            DepartmentModel department = getDepartmentByUserId(uid);
            if (department != null) {
                bizObject.setCreatedDeptId(department.getId());
            }
        }

        String createdTime = bizObject.getString(DefaultPropertyType.CREATED_TIME.getCode());
        log.info("=====================createdTime================ {}", createdTime);
        if (StringUtils.isEmpty(createdTime)) {
            bizObject.setCreatedTime(oldBizObject.getCreatedTime());
        }

        this.setOwner(bizObject, oldBizObject, userId);
        Object boOwner = bizObject.getObject(DefaultPropertyType.OWNER.getCode());
        String ownerId = this.getSelectionId(boOwner, DefaultPropertyType.OWNER.getCode());
        UserModel ownerUser = this.userFacade.get(ownerId);
        log.info("=====================ownerUser================ {}", ownerUser);

        Object boOwnerDepartment = bizObject.getObject(DefaultPropertyType.OWNER_DEPT_ID.getCode());
        String ownerDepartmentId;
        log.info("=====================boOwnerDepartment================ {}", boOwnerDepartment);
        //修改时未提交拥有者部门id，使用数据库旧有数据的拥有者部门ID。
        if (boOwnerDepartment == null) {
            boOwnerDepartment = oldBizObject.getObject(DefaultPropertyType.OWNER_DEPT_ID.getCode());
        }
        if (boOwnerDepartment == null) {
            ownerDepartmentId = ownerUser == null ? null : ownerUser.getDepartmentId();
        } else {
            ownerDepartmentId = this.getSelectionId(boOwnerDepartment, DefaultPropertyType.OWNER_DEPT_ID.getCode());
        }
        bizObject.setOwnerDeptId(ownerDepartmentId);
        DepartmentModel ownerDepartment = this.departmentFacade.get(ownerDepartmentId);
        if (ownerDepartment != null) {
            bizObject.setOwnerDeptQueryCode(ownerDepartment.getQueryCode());
        }

        String creater = getIdFromObject(bizObject.get(DefaultPropertyType.CREATER.getCode()));
        boolean createDeptChanged = false;
        if (creater != null && creater.equals(ownerId)) {//如果创建人和拥护者是同一个人，部门保存一致
            Object oldCreateDepartment = oldBizObject.getObject(DefaultPropertyType.CREATED_DEPT_ID.getCode());
            if (oldCreateDepartment != null) {
                String oldCreateDepartmentId = this.getSelectionId(oldCreateDepartment, DefaultPropertyType.CREATED_DEPT_ID.getCode());
                if (!StringUtils.isEmpty(oldCreateDepartmentId) && !oldCreateDepartmentId.equals(ownerDepartmentId)) {
                    bizObject.setCreatedDeptId(ownerDepartmentId);
                    createDeptChanged = true;
                }
            }
        }

        //当修改时原数据没有单据号，暂存操作不生成单据号，提交操作生成单据号。导入操作数据状态已完成生成单据号
        if (StringUtils.isEmpty(oldBizObject.getSequenceNo())
                && (SequenceStatus.COMPLETED.toString().equals(bizObject.getSequenceStatus()) || Objects.equals(Boolean.TRUE, bizObject.getData().get(Integer.MAX_VALUE + "_sequenceNo")))) {
            String sequenceNo = sequenceSettingService.createSequenceNo(bizSchema.getCode(), bizObject.getFormCode(), bizObject);
            bizObject.setSequenceNo(sequenceNo);
        } else {
            if (!(StringUtils.isEmpty(oldBizObject.getSequenceNo()))) {
                bizObject.setSequenceNo(oldBizObject.getSequenceNo());
            }
        }
        Object remove = bizObject.getData().remove(Integer.MAX_VALUE + "_sequenceNo");
        if (null != remove) {
            log.info("单据号：remove={}", JSONObject.toJSONString(remove.toString()));
        }

        bizObject.setModifier(userId);
        bizObject.setModifiedTime(new Date());


        Map<String, Object> nameInfo = mergeObject(bizObject, objectExisted);
        nameInfo.putAll(bizObject.getData());
        nameInfo.put(DefaultPropertyType.CREATER.getCode(), oldBizObject.getCreater().getId());

        //合并对象
        bizObject.setData(nameInfo);

        String name = bizSchemaService.getName(bizSchema.getCode(), bizSchema.getName(), bizSchema.getSummary(), nameInfo);

        bizObject.setName(name);

        if (log.isDebugEnabled()) {
            log.info("=====================bizObject================ {}", bizObject);
        }
        //修改时不能修改以下字段
        bizObject.getData().remove(DefaultPropertyType.CREATER.getCode());
        bizObject.getData().remove(DefaultPropertyType.CREATED_TIME.getCode());

        if (!createDeptChanged) {
            bizObject.getData().remove(DefaultPropertyType.CREATED_DEPT_ID.getCode());
        }

        if (log.isTraceEnabled()) {
            log.trace("bizObject json2 = {}", JSONObject.toJSONString(bizObject.getData()));
        }

        String wfInstanceId = oldBizObject.getWorkflowInstanceId();
        if (StringUtils.isBlank(wfInstanceId)) {
            WorkflowInstanceModel bizWorkflowInstance = bizWorkflowInstanceService.getByObjectId(oldBizObject.getId());
            wfInstanceId = bizWorkflowInstance == null ? wfInstanceId : bizWorkflowInstance.getId();
            bizObject.setWorkflowInstanceId(wfInstanceId);
        }

        try {
            ThreadLocalAttributes.setThreadAttribute("currentUserId", userId);
            ThreadLocalAttributes.removeThreadAttribute("triggerData");
            if (StringUtils.isNotBlank(objectExisted.getId())) {
                bizObject.getData().put(DefaultPropertyType.ID.getCode(), objectExisted.getId());
            }
            BizObject newBizObject = new BizObject(bizObject.getSchemaCode(), objectExisted.getId(), bizObject.getData(), objectExisted);
            newBizObject.setFormCode(bizObject.getFormCode());
            newBizObject.setExtendInfo(bizObject.getExtendInfo());
            bizObjectService.updateBizObject(newBizObject, options);
        } catch (Exception e) {
            throw e;
        } finally {
            ThreadLocalAttributes.removeThreadAttribute("currentUserId");
        }
        return bizObject.getId();
    }

    /**
     * 更新业务对象
     * @param userId        用户id
     * @param bizSchema     业务模型
     * @param bizObject     表单传递过来的业务对象
     * @param objectExisted 数据库存在的业务对象
     * @return objectId
     */
    public BizObjectModel modifiedBizObject(String userId, BizSchema bizSchema, BizObjectModel bizObject, BizObject objectExisted, BizObjectOptions options) {
        log.debug("update bizObject...");
        // 查询bo数据，避免生成摘要时丢失信息
        final BizObjectCreatedModel oldBizObject = new BizObjectCreatedModel(bizObject.getSchemaCode(), objectExisted.getData(), false);
        if (log.isTraceEnabled()) {
            log.trace("获取原表单数据:{}", JSON.toJSONString(oldBizObject));
            log.trace("当前表单数据:{}", JSON.toJSONString(bizObject));
        }

        String create = bizObject.getString(DefaultPropertyType.CREATER.getCode());
        log.info("=====================create================ {}", create);
        if (StringUtils.isEmpty(create)) {
            String createId = oldBizObject.getCreater() != null ? oldBizObject.getCreater().getId() : null;
            log.debug("获取到的用户ID:{}", createId);
            String uid = StringUtils.isEmpty(createId) ? userId : createId;
            bizObject.setCreater(uid);

            DepartmentModel department = getDepartmentByUserId(uid);
            if (department != null) {
                bizObject.setCreatedDeptId(department.getId());
            }
        }

        String createdTime = bizObject.getString(DefaultPropertyType.CREATED_TIME.getCode());
        log.info("=====================createdTime================ {}", createdTime);
        if (StringUtils.isEmpty(createdTime)) {
            bizObject.setCreatedTime(oldBizObject.getCreatedTime());
        }

        this.setOwner(bizObject, oldBizObject, userId);
        Object boOwner = bizObject.getObject(DefaultPropertyType.OWNER.getCode());
        String ownerId = this.getSelectionId(boOwner, DefaultPropertyType.OWNER.getCode());
        UserModel ownerUser = this.userFacade.get(ownerId);
        log.info("=====================ownerUser================ {}", ownerUser);

        Object boOwnerDepartment = bizObject.getObject(DefaultPropertyType.OWNER_DEPT_ID.getCode());
        String ownerDepartmentId;
        log.info("=====================boOwnerDepartment================ {}", boOwnerDepartment);
        //修改时未提交拥有者部门id，使用数据库旧有数据的拥有者部门ID。
        if (boOwnerDepartment == null) {
            boOwnerDepartment = oldBizObject.getObject(DefaultPropertyType.OWNER_DEPT_ID.getCode());
        }
        if (boOwnerDepartment == null) {
            ownerDepartmentId = ownerUser == null ? null : ownerUser.getDepartmentId();
        } else {
            ownerDepartmentId = this.getSelectionId(boOwnerDepartment, DefaultPropertyType.OWNER_DEPT_ID.getCode());
        }
        bizObject.setOwnerDeptId(ownerDepartmentId);
        DepartmentModel ownerDepartment = this.departmentFacade.get(ownerDepartmentId);
        if (ownerDepartment != null) {
            bizObject.setOwnerDeptQueryCode(ownerDepartment.getQueryCode());
        }

        //当修改时原数据没有单据号，暂存操作不生成单据号，提交操作生成单据号。导入操作数据状态已完成生成单据号
        if (StringUtils.isEmpty(oldBizObject.getSequenceNo())
                && (SequenceStatus.COMPLETED.toString().equals(bizObject.getSequenceStatus()) || Objects.equals(Boolean.TRUE, bizObject.getData().get(Integer.MAX_VALUE + "_sequenceNo")))) {
            String sequenceNo = sequenceSettingService.createSequenceNo(bizSchema.getCode(), bizObject.getFormCode(), bizObject);
            bizObject.setSequenceNo(sequenceNo);
        } else {
            if (!(StringUtils.isEmpty(oldBizObject.getSequenceNo()))) {
                bizObject.setSequenceNo(oldBizObject.getSequenceNo());
            }
        }
        Object remove = bizObject.getData().remove(Integer.MAX_VALUE + "_sequenceNo");

        if (null != remove) {
            log.info("单据号：remove={}", JSONObject.toJSONString(remove.toString()));
        }

        bizObject.setModifier(userId);
        bizObject.setModifiedTime(new Date());

        Map<String, Object> nameInfo = mergeObject(bizObject, objectExisted);
        nameInfo.put(DefaultPropertyType.CREATER.getCode(), oldBizObject.getCreater().getId());
        nameInfo.put(DefaultPropertyType.CREATED_TIME.getCode(), oldBizObject.getCreatedTime());
        String name = bizSchemaService.getName(bizSchema.getCode(), bizSchema.getName(), bizSchema.getSummary(), nameInfo);

        bizObject.setName(name);
        log.info("=====================bizObject================ {}", bizObject);
        //修改时不能修改以下字段
        bizObject.getData().remove(DefaultPropertyType.CREATER.getCode());
        bizObject.getData().remove(DefaultPropertyType.CREATED_TIME.getCode());
        bizObject.getData().remove(DefaultPropertyType.CREATED_DEPT_ID.getCode());

        if (log.isTraceEnabled()) {
            log.debug("bizObject json2 = {}", JSONObject.toJSONString(bizObject.getData()));
        }

        String wfInstanceId = oldBizObject.getWorkflowInstanceId();
        if (StringUtils.isBlank(wfInstanceId)) {
            WorkflowInstanceModel bizWorkflowInstance = bizWorkflowInstanceService.getByObjectId(oldBizObject.getId());
            wfInstanceId = bizWorkflowInstance == null ? wfInstanceId : bizWorkflowInstance.getId();
            bizObject.setWorkflowInstanceId(wfInstanceId);
        }
        BizObject obj = new BizObject(bizObject.getSchemaCode(), objectExisted.getId(), bizObject.getData(), objectExisted);
        bizObjectService.modifiedBizObject(obj, options);
        return new BizObjectModel(bizObject.getSchemaCode(), obj.getData(), false);
    }

    private Map<String, Object> mergeObject(BizObjectModel bizObject, BizObject objectExisted) {
        Map<String, Object> map = Maps.newHashMap();
        if (bizObject != null && MapUtils.isNotEmpty(bizObject.getData())) {
            map.putAll(bizObject.getData());
        }
        if (objectExisted != null && MapUtils.isNotEmpty(objectExisted.getData())) {
            objectExisted.getData().forEach(map::putIfAbsent);
        }
        return map;
    }

    /**
     * 取选人/部门的id
     * @param value
     * @param code
     * @return
     */
    private String getSelectionId(Object value, String code) {
        BizProperty bizProperty = new BizProperty();
        bizProperty.setCode(code);
        bizProperty.setPropertyType(BizPropertyType.SELECTION);
        String ownerDepartmentId = (String) this.bizObjectConverter.fromFormValue(value, bizProperty);
        return ownerDepartmentId;
    }

    private void setOwner(BizObjectModel bizObject, BizObjectCreatedModel oldBizObject, String userId) {
        String boOwner = bizObject.getString(DefaultPropertyType.OWNER.getCode());
        if (StringUtils.isEmpty(boOwner)) {
            String ownerId = (oldBizObject.getOwner() != null) ? oldBizObject.getOwner().getId() : null;
            String owner = StringUtils.isEmpty(ownerId) ? userId : ownerId;
            bizObject.setOwner(owner);
        }
    }

    @Override
    public Boolean removeBizObject(String userId, String schemaCode, String objectId) {
        if (Strings.isNullOrEmpty(userId)) {
            throw new ServiceException(ErrCode.ORG_USER_ID_EMPTY);
        }
        if (Strings.isNullOrEmpty(schemaCode)) {
            throw new ServiceException(ErrCode.BIZ_SCHEMA_CODE_EMPTY);
        }
        if (Strings.isNullOrEmpty(objectId)) {
            throw new ServiceException(ErrCode.BIZ_OBJECT_ID_EMPTY);
        }

        try {
            ThreadLocalAttributes.setThreadAttribute("currentUserId", userId);
            bizObjectService.deleteBizObject(schemaCode, objectId);
        } catch (Exception e) {
            throw e;
        } finally {
            ThreadLocalAttributes.removeThreadAttribute("currentUserId");
        }
        return Boolean.TRUE;
    }


    public Page<BizObjectModel> queryTransferBizObjects(BizObjectQueryModel bizObjectQueryModel) {
        final String schemaCode = bizObjectQueryModel.getSchemaCode();
        final String queryCode = bizObjectQueryModel.getQueryCode();
        if (StringUtils.isEmpty(schemaCode) && StringUtils.isEmpty(queryCode)) {
            log.debug("bizQuery is null. {}", ErrCode.BIZ_QUERY_CODE_SCHEMACODE_NOT_EXIST.getErrMsg());
            return new PageImpl<>(0L, Lists.newArrayList());
        }
        //获取第一个列表
        final List<BizQuery> bizQuerys = bizQueryService.getListBySchemaCode(schemaCode);
        if (CollectionUtils.isEmpty(bizQuerys)) {
            log.debug("bizQuery is null. {}", queryCode);
            return new PageImpl<>(0L, Lists.newArrayList());
        }
        //判断当前表是否存在
        if (!db.ddl().tableExist(schemaCode)) {
            log.error("业务表:{} 不存在", schemaCode);
            return new PageImpl<>(0L, Lists.newArrayList());
        }

        //判断数据项是否存在
        String queryId = bizQuerys.get(0).getId();
        //排序信息
        final List<OrderImpl> orders = new ArrayList<>();
        //使用默认排序
        orders.add(new OrderImpl(DefaultPropertyType.CREATED_TIME.getCode(), Order.Dir.DESC));

        final List<String> columnList = Lists.newArrayList();
        columnList.add(DefaultPropertyType.ID.getCode());
        columnList.add(DefaultPropertyType.NAME.getCode());
        columnList.add(DefaultPropertyType.OWNER_DEPT_ID.getCode());
        columnList.add(DefaultPropertyType.CREATED_TIME.getCode());
        columnList.add(DefaultPropertyType.SEQUENCE_STATUS.getCode());

        if (log.isDebugEnabled()) {
            log.debug("==================columnList={}", columnList);
        }
        Page<Map<String, Object>> queryData = null;
        try {
            queryData = bizObjectService.queryBizObjectsWithNoBind(new BizObjectQueryObject() {
                @Override
                public String getSchemaCode() {
                    return schemaCode;
                }

                @Override
                public FilterExpression getFilterExpression() {
                    return bizObjectQueryModel.getFilterExpr();
                }

                @Override
                public List<String> getDisplayFields() {
                    columnList.add(DefaultPropertyType.ID.getCode());
                    columnList.add(0, DefaultPropertyType.NAME.getCode());
                    if (!columnList.contains(DefaultPropertyType.OWNER.getCode())) {
                        columnList.add(DefaultPropertyType.OWNER.getCode());
                    }
                    if (!columnList.contains(DefaultPropertyType.CREATER.getCode())) {
                        columnList.add(DefaultPropertyType.CREATER.getCode());
                    }
                    if (!columnList.contains(DefaultPropertyType.CREATED_TIME.getCode())) {
                        columnList.add(DefaultPropertyType.CREATED_TIME.getCode());
                    }
                    if (!columnList.contains(DefaultPropertyType.MODIFIED_TIME.getCode())) {
                        columnList.add(DefaultPropertyType.MODIFIED_TIME.getCode());
                    }
                    if (!columnList.contains(DefaultPropertyType.SEQUENCE_STATUS.getCode())) {
                        columnList.add(DefaultPropertyType.SEQUENCE_STATUS.getCode());
                    }
                    return columnList;
                }


                @Override
                public Pageable getPageable() {
                    return bizObjectQueryModel.getPageable();
                }

                @Override
                public Sortable getSortable() {
                    return orders == null ? null : new SortableImpl(orders);
                }
            });
        } catch (Exception e) {
            log.info("工作交接schemaCode:{},queryCode{},异常数据：{}", schemaCode, queryCode, e);
            return new PageImpl<>(0L, Lists.newArrayList());
        }
        List<? extends Map<String, Object>> content = queryData.getContent();
        final List<BizObjectModel> bizObjectList = Lists.newArrayListWithExpectedSize(content.size());
        for (Map<String, Object> map : content) {
            final BizObjectModel bizObjectModel = new BizObjectModel(schemaCode, map, false);
            bizObjectList.add(bizObjectModel);
        }
        if (log.isTraceEnabled()) {
            log.trace("==================bizObjectList= {}", JSON.toJSONString(bizObjectList));
        }
        return new PageImpl<>(queryData.getTotal(), bizObjectList);
    }

    @Override
    public Page<BizObjectModel> queryBizObjects(BizObjectQueryModel bizObjectQueryModel) {
        final String schemaCode = bizObjectQueryModel.getSchemaCode();
        final String queryCode = bizObjectQueryModel.getQueryCode();
        final ClientType clientType = Objects.nonNull(bizObjectQueryModel.getClientType()) ? bizObjectQueryModel.getClientType() : ClientType.PC;
        if (StringUtils.isEmpty(schemaCode) && StringUtils.isEmpty(queryCode)) {
            throw new ServiceException(ErrCode.BIZ_QUERY_CODE_SCHEMACODE_NOT_EXIST, bizObjectQueryModel);
        }

        final BizQuery bizQuery = bizQueryService.getBySchemaCodeAndCode(schemaCode, queryCode);
        if (bizQuery == null) {
            log.debug("bizQuery is null. {}", queryCode);
            throw new ServiceException(ErrCode.BIZ_QUERY_NOT_EXIST, queryCode);
        }
        String queryId = bizQuery.getId();
        //排序信息
        List<BizQuerySort> sortList = bizQuerySortService.getListByQueryId(queryId);
        List<BizQuerySort> sorts = sortList.stream().filter(t -> Objects.equals(clientType, t.getClientType())).collect(Collectors.toList());
        final List<OrderImpl> orders = new ArrayList<>();
        if (CollectionUtils.isEmpty(sorts)) {
            //使用默认排序
            orders.add(new OrderImpl(DefaultPropertyType.CREATED_TIME.getCode(), Order.Dir.DESC));
        } else {
            orders.addAll(getOrders(sorts));
        }

        List<BizProperty> properties = bizPropertyService.getListBySchemaCode(schemaCode, true);
        if (CollectionUtils.isEmpty(properties)) {
            return new PageImpl<BizObjectModel>(0, Lists.newArrayList());
        }
        List<String> propertyCodes = properties.stream().filter(property -> property != null).map(BizProperty::getCode).collect(Collectors.toList());
        List<String> columns = Lists.newArrayList();
        Boolean display = bizObjectQueryModel.getDisplay();
        columns = listQueryColumn(schemaCode, queryCode, queryId, bizObjectQueryModel.getOptions(), clientType, display);
        List<String> columnList = columns.stream().filter(code -> propertyCodes.contains(code)).collect(Collectors.toList());
        if (log.isDebugEnabled()) {
            log.debug("==================columnList={}", columnList);
        }

        final Page<Map<String, Object>> queryData = bizObjectService.queryBizObjects(new BizObjectQueryObject() {

            @Override
            public String getSchemaCode() {
                return schemaCode;
            }

            @Override
            public FilterExpression getFilterExpression() {
                return bizObjectQueryModel.getFilterExpr();
            }

            @Override
            public List<String> getDisplayFields() {
                return bizObjectConverter.completeSystemFields(schemaCode, bizObjectQueryModel.getQuotes(), columnList);
            }

            @Override
            public Pageable getPageable() {
                return bizObjectQueryModel.getPageable();
            }

            @Override
            public Sortable getSortable() {
                return orders == null ? null : new SortableImpl(orders);
            }

            @Override
            public Map<String, Object> getQuotes() {
                return bizObjectQueryModel.getQuotes();
            }
        });
        //转换WEB-API需要的数据
        List<? extends Map<String, Object>> content = queryData.getContent();
        final List<BizObjectModel> bizObjectList = Lists.newArrayListWithExpectedSize(content.size());
        for (Map<String, Object> map : content) {
            final BizObjectModel bizObjectModel = new BizObjectModel(schemaCode, map, false);
            bizObjectList.add(bizObjectModel);
        }
        if (log.isTraceEnabled()) {
            log.trace("==================bizObjectList={}", JSON.toJSONString(bizObjectList));
        }
        return new PageImpl<>(queryData.getTotal(), bizObjectList);
    }

    @Override
    public Page<BizObjectModel> queryExportDatas(BizObjectQueryModel bizObjectQueryModel, List<BizPropertyModel> properties, Boolean mobile, List<BizQueryColumnModel> columns, String userId, Map<String, Map<String, Object>> childNumricalFormat) {
        final String schemaCode = bizObjectQueryModel.getSchemaCode();
        final String queryCode = bizObjectQueryModel.getQueryCode();
        final ClientType clientType = Objects.nonNull(bizObjectQueryModel.getClientType()) ? bizObjectQueryModel.getClientType() : ClientType.PC;
        if (StringUtils.isEmpty(schemaCode) && StringUtils.isEmpty(queryCode)) {
            throw new ServiceException(ErrCode.BIZ_QUERY_CODE_SCHEMACODE_NOT_EXIST, bizObjectQueryModel);
        }

        final BizQuery bizQuery = bizQueryService.getBySchemaCodeAndCode(schemaCode, queryCode);
        if (bizQuery == null) {
            log.debug("bizQuery is null. {}", queryCode);
            throw new ServiceException(ErrCode.BIZ_QUERY_NOT_EXIST, queryCode);
        }
        String queryId = bizQuery.getId();
        //排序信息
        List<BizQuerySort> sortList = bizQuerySortService.getListByQueryId(queryId);
        List<BizQuerySort> sorts = sortList.stream().filter(t -> Objects.equals(clientType, t.getClientType())).collect(Collectors.toList());
        final List<OrderImpl> orders = new ArrayList<>();
        if (CollectionUtils.isEmpty(sorts)) {
            //使用默认排序
            orders.add(new OrderImpl(DefaultPropertyType.CREATED_TIME.getCode(), Order.Dir.DESC));
        } else {
            orders.addAll(getOrders(sorts));
        }
        final List<String> columnList = Lists.newArrayList();
        boolean showParticipant = showParticipant(bizQuery.getId(), clientType);
        final Page<Map<String, Object>> queryData = bizObjectService.queryBizObjects(new BizObjectQueryObject() {
            @Override
            public String getSchemaCode() {
                return schemaCode;
            }

            @Override
            public FilterExpression getFilterExpression() {
                return bizObjectQueryModel.getFilterExpr();
            }

            @Override
            public List<String> getDisplayFields() {
                for (BizPropertyModel propertyModel : properties) {
                    if (Objects.equals(propertyModel.getPropertyType(), BizPropertyType.ATTACHMENT)
                            || Objects.equals(propertyModel.getPropertyType(), BizPropertyType.COMMENT)
                    ) {
                        continue;

                    }
                    String code = propertyModel.getCode();
                    if (!StringUtils.isEmpty(code)) {
                        if (!columnList.contains(code)) {
                            columnList.add(code);
                        }

                    }
                }
                if (showParticipant) {
                    columnList.add(DefaultPropertyType.WORKFLOW_INSTANCE_ID.getCode());
                }
                return columnList;
            }


            @Override
            public Pageable getPageable() {
                return bizObjectQueryModel.getPageable();
            }

            @Override
            public Sortable getSortable() {
                return orders == null ? null : new SortableImpl(orders);
            }

            @Override
            public boolean showParticipant() {
                return showParticipant;
            }
        });


        //转换WEB-API需要的数据
        final List<BizObjectModel> bizObjectList = Lists.newArrayListWithExpectedSize(queryData.getContent().size());
        for (Map<String, Object> map : queryData.getContent()) {
            final BizObjectModel bizObjectModel = new BizObjectModel(schemaCode, map, false);
            bizObjectList.add(bizObjectModel);
        }

        long start = System.currentTimeMillis();
        UserModel user = getUser(userId);
        String corpId = user.getCorpId();
        DepartmentModel root = departmentFacade.getCorpRootDepartment(corpId);
        String rootId = root.getId();

        machesProperty(columns, bizObjectList, mobile, schemaCode, queryCode, userId, rootId);

        long pa = System.currentTimeMillis();
        log.info("engine处理1：--{}", pa - start);
        String formCode = null;
        if (StringUtils.isEmpty(formCode)) {
            List<BizForm> bizForms = bizFormService.getListBySchemaCode(schemaCode);
            if (CollectionUtils.isNotEmpty(bizForms)) {
                formCode = bizForms.get(0).getCode();
            }
        }
        BizForm sheetModel = bizFormService.getBySchemaCodeAndFormCode(schemaCode, formCode);
        BizSchema bizSchemaModel = bizSchemaService.getBySchemaCode(schemaCode);

        for (BizObjectModel bizObject : bizObjectList) {
            for (BizQueryColumnModel column : columns) {
                Map<String, Object> format = Maps.newHashMap();
                if (column.getPropertyType() != BizPropertyType.CHILD_TABLE) {
                    continue;
                }
                Map<String, Object> data = bizObject.getData();
                List<Map<String, Object>> objectChildDatas = (List<Map<String, Object>>) data.get(column.getPropertyCode());
                List<BizQueryChildColumnModel> childColumns = column.getChildColumns();
                String subSchemaCode = column.getPropertyCode();
                if (CollectionUtils.isEmpty(objectChildDatas)) {
                    continue;
                }
                parseChildDatas(userId, objectChildDatas, childColumns, subSchemaCode, bizSchemaModel, sheetModel, childNumricalFormat, format, rootId);
            }
        }
        long end = System.currentTimeMillis();
        log.info("engine处理：--{}", end - pa);
        if (log.isTraceEnabled()) {
            log.trace("================bizObjectList={}", JSON.toJSONString(bizObjectList));
        }
        return new PageImpl<BizObjectModel>(queryData.getTotal(), bizObjectList);
    }

    @Override
    public Map<String, Map<String, Object>> queryChildNumricalFormat(BizObjectQueryModel bizObjectQueryModel, List<BizPropertyModel> properties, Boolean mobile, List<BizQueryColumnModel> columns, String userId) {
        final String schemaCode = bizObjectQueryModel.getSchemaCode();
        final String queryCode = bizObjectQueryModel.getQueryCode();
        final ClientType clientType = Objects.nonNull(bizObjectQueryModel.getClientType()) ? bizObjectQueryModel.getClientType() : ClientType.PC;
        if (StringUtils.isEmpty(schemaCode) && StringUtils.isEmpty(queryCode)) {
            throw new ServiceException(ErrCode.BIZ_QUERY_CODE_SCHEMACODE_NOT_EXIST, bizObjectQueryModel);
        }

        final BizQuery bizQuery = bizQueryService.getBySchemaCodeAndCode(schemaCode, queryCode);
        if (bizQuery == null) {
            log.debug("bizQuery is null. {}", queryCode);
            throw new ServiceException(ErrCode.BIZ_QUERY_NOT_EXIST, queryCode);
        }
        String queryId = bizQuery.getId();
        //排序信息
        List<BizQuerySort> sortList = bizQuerySortService.getListByQueryId(queryId);
        List<BizQuerySort> sorts = sortList.stream().filter(t -> Objects.equals(clientType, t.getClientType())).collect(Collectors.toList());
        final List<OrderImpl> orders = new ArrayList<>();
        if (CollectionUtils.isEmpty(sorts)) {
            //使用默认排序
            orders.add(new OrderImpl(DefaultPropertyType.CREATED_TIME.getCode(), Order.Dir.DESC));
        } else {
            orders.addAll(getOrders(sorts));
        }
        final List<String> columnList = Lists.newArrayList();
        final Page<Map<String, Object>> queryData = bizObjectService.queryBizObjects(new BizObjectQueryObject() {
            @Override
            public String getSchemaCode() {
                return schemaCode;
            }

            @Override
            public FilterExpression getFilterExpression() {
                return bizObjectQueryModel.getFilterExpr();
            }

            @Override
            public List<String> getDisplayFields() {
                for (BizPropertyModel propertyModel : properties) {
                    if (Objects.equals(propertyModel.getPropertyType(), BizPropertyType.ATTACHMENT)
                            || Objects.equals(propertyModel.getPropertyType(), BizPropertyType.COMMENT)
                    ) {
                        continue;

                    }
                    String code = propertyModel.getCode();
                    if (!StringUtils.isEmpty(code)) {
                        if (!columnList.contains(code)) {
                            columnList.add(code);
                        }

                    }
                }
                return columnList;
            }


            @Override
            public Pageable getPageable() {
                return bizObjectQueryModel.getPageable();
            }

            @Override
            public Sortable getSortable() {
                return orders == null ? null : new SortableImpl(orders);
            }
        });


        //转换WEB-API需要的数据
        final List<BizObjectModel> bizObjectList = Lists.newArrayListWithExpectedSize(queryData.getContent().size());
        for (Map<String, Object> map : queryData.getContent()) {
            final BizObjectModel bizObjectModel = new BizObjectModel(schemaCode, map, false);
            bizObjectList.add(bizObjectModel);
        }

        UserModel user = getUser(userId);
        String corpId = user.getCorpId();
        DepartmentModel root = departmentFacade.getCorpRootDepartment(corpId);
        String rootId = root.getId();

        machesProperty(columns, bizObjectList, mobile, schemaCode, queryCode, userId, rootId);

        String formCode = null;
        List<BizForm> bizFormHeaders = bizFormService.getListBySchemaCode(schemaCode);
        if (CollectionUtils.isNotEmpty(bizFormHeaders)) {
            formCode = bizFormHeaders.get(0).getCode();
        }
        BizForm sheetModel = bizFormService.getBySchemaCodeAndFormCode(schemaCode, formCode);
        BizSchema bizSchemaModel = bizSchemaService.getBySchemaCode(schemaCode);
        Map<String, Map<String, Object>> map = Maps.newHashMap();
        for (BizObjectModel bizObject : bizObjectList) {
            for (BizQueryColumnModel column : columns) {

                if (column.getPropertyType() != BizPropertyType.CHILD_TABLE) {
                    continue;
                }
                Map<String, Object> data = bizObject.getData();
                List<Map<String, Object>> objectChildDatas = (List<Map<String, Object>>) data.get(column.getPropertyCode());
                List<BizQueryChildColumnModel> childColumns = column.getChildColumns();
                String subSchemaCode = column.getPropertyCode();
                if (CollectionUtils.isEmpty(objectChildDatas)) {
                    continue;
                }
                map.putAll(getChildNumricalFormat(objectChildDatas, childColumns, subSchemaCode, bizSchemaModel, sheetModel));
            }
        }
        return map;
    }

    protected Map<String, Map<String, Object>> getChildNumricalFormat(List<Map<String, Object>> maps, List<BizQueryChildColumnModel> childColumns, String subSchemaCode,
                                                                      BizSchema bizSchemaModel, BizForm sheetModel) {
        Map<String, Map<String, Object>> childNumricalFormat = Maps.newHashMap();
        Map<String, Object> format = Maps.newHashMap();
        List<BizProperty> bizProperties = bizPropertyService.getListBySchemaCode(subSchemaCode, true);
        List<BizProperty> collect = bizProperties.stream().filter(t -> t.getPropertyType() == BizPropertyType.NUMERICAL).collect(Collectors.toList());
        List<BizProperty> collectAddress = bizProperties.stream().filter(t -> t.getPropertyType() == BizPropertyType.ADDRESS).collect(Collectors.toList());
        List<BizProperty> workSheets = bizProperties.stream().filter(t -> t.getPropertyType() == BizPropertyType.WORK_SHEET).collect(Collectors.toList());
        List<BizProperty> multWorkSheets = bizProperties.stream().filter(t -> t.getPropertyType() == BizPropertyType.MULT_WORK_SHEET).collect(Collectors.toList());
        workSheets.addAll(multWorkSheets);

        Map<String, String> mapTemplate = new HashMap<>();

        //数值或者地址
        if (CollectionUtils.isNotEmpty(collect) || CollectionUtils.isNotEmpty(collectAddress)) {
            Map<String, String> mapTemplateAddress = Maps.newHashMapWithExpectedSize(collectAddress.size());
            List<Map> childColumnList = getChildColumns(sheetModel.getPublishedAttributesJson(), subSchemaCode);
            for (BizProperty bizPropertyModel : collect) {
                if (CollectionUtils.isNotEmpty(childColumnList)) {
                    for (Map childColumn : childColumnList) {
                        if (childColumn.get("key").toString().equals(bizPropertyModel.getCode())) {
                            Map map2 = JSON.parseObject(childColumn.get("options").toString(), Map.class);
                            Object format1 = map2.get("format");
                            if (Objects.isNull(format1)) {
                                continue;
                            }
                            mapTemplate.put(bizPropertyModel.getCode(), format1.toString());
                        }
                    }
                }
            }
            for (BizProperty bizPropertyModel : collectAddress) {
                for (BizQueryChildColumnModel childColumn : childColumns) {
                    if (CollectionUtils.isNotEmpty(childColumnList)) {
                        if (childColumn.getPropertyCode().equals(bizPropertyModel.getCode())) {
                            mapTemplateAddress.put(bizPropertyModel.getCode(), childColumn.getPropertyCode());
                        }
                    }
                }
            }
        }

        if (CollectionUtils.isNotEmpty(childColumns)) {
            for (BizQueryChildColumnModel columnModel : childColumns) {
                for (Map<String, Object> data : maps) {
                    if (columnModel.getPropertyType() == BizPropertyType.ADDRESS) {
                        String json = null;
                        if (data.get(columnModel.getPropertyCode()) != null) {
                            json = data.get(columnModel.getPropertyCode()).toString();
                        }
                        if ("{}".equals(json)) {
                            json = null;
                            data.put(columnModel.getPropertyCode(), null);
                        }
                        if (StringUtils.isNotEmpty(json)) {
                            Map map = JSONObject.parseObject(json, Map.class);
                            String name = "";
                            if (map.get("provinceName") != null) {
                                name = name + map.get("provinceName");
                            }
                            if (map.get("cityName") != null) {
                                name = name + map.get("cityName");
                            }
                            if (map.get("districtName") != null) {
                                name = name + map.get("districtName");
                            }
                            if (map.get("address") != null) {
                                name = name + map.get("address");
                            }
                            data.put(columnModel.getPropertyCode(), name);
                        }
                    }
                    //关联表单
                    if (columnModel.getPropertyType() == BizPropertyType.WORK_SHEET || columnModel.getPropertyType() == BizPropertyType.MULT_WORK_SHEET) {

                        if (data.get(columnModel.getPropertyCode()) != null) {
                            String properCode = columnModel.getPropertyCode();
                            Object o = data.get(properCode);
                            List<BizProperty> workSheet = workSheets.stream().filter(property -> Objects.equals(property.getCode(), columnModel.getPropertyCode())).collect(Collectors.toList());
                            if (CollectionUtils.isEmpty(workSheet)) {
                                data.put(properCode, "");
                                continue;
                            }
                            BizProperty bizPropertyModel = workSheet.get(0);
                            String relativePropertyCode = bizPropertyModel.getRelativePropertyCode();
                            if (StringUtils.isEmpty(relativePropertyCode)) {
                                relativePropertyCode = "name";
                            }
                            Map<String, Object> workSheetData = (Map<String, Object>) o;
                            String value = Objects.isNull(workSheetData.get(relativePropertyCode)) ? "" : workSheetData.get(relativePropertyCode).toString();
                            data.put(properCode, value);

                        }

                    }

                    //数值类型
                    if (columnModel.getPropertyType() == BizPropertyType.NUMERICAL) {
                        if (data.get(columnModel.getPropertyCode()) == null) {
                            continue;
                        }
//                        DecimalFormat df = null;
                        String code = columnModel.getPropertyCode();
                        String displatFormat = mapTemplate.get(code);
                        if (StringUtils.isEmpty(displatFormat)) {
                            displatFormat = "null";
                            format.put(columnModel.getPropertyCode(), displatFormat);
                            childNumricalFormat.put(subSchemaCode, format);
                            continue;
                        }
                        format.put(columnModel.getPropertyCode(), displatFormat);
                        childNumricalFormat.put(subSchemaCode, format);
                    }

                }

            }
        }
        return childNumricalFormat;

    }


    /**
     * 处理子表数据项导出的格式
     * @param childColumns  展示字段
     * @param maps          业务数据对象
     * @param subSchemaCode
     * @return
     */

    private void parseChildDatas(String userId, List<Map<String, Object>> maps, List<BizQueryChildColumnModel> childColumns, String subSchemaCode,
                                 BizSchema bizSchemaModel, BizForm sheetModel, Map<String, Map<String, Object>> childNumricalFormat, Map<String, Object> format, String rootId) {
        List<BizProperty> bizProperties = bizPropertyService.getListBySchemaCode(subSchemaCode, true);
        List<BizProperty> collect = bizProperties.stream().filter(t -> t.getPropertyType() == BizPropertyType.NUMERICAL).collect(Collectors.toList());
        List<BizProperty> collectAddress = bizProperties.stream().filter(t -> t.getPropertyType() == BizPropertyType.ADDRESS).collect(Collectors.toList());
        List<BizProperty> workSheets = bizProperties.stream().filter(t -> t.getPropertyType() == BizPropertyType.WORK_SHEET).collect(Collectors.toList());
        List<BizProperty> multWorkSheets = bizProperties.stream().filter(t -> t.getPropertyType() == BizPropertyType.MULT_WORK_SHEET).collect(Collectors.toList());
        workSheets.addAll(multWorkSheets);
        Map<String, String> mapTemplate = new HashMap<>();

        //数值或者地址
        if (CollectionUtils.isNotEmpty(collect) || CollectionUtils.isNotEmpty(collectAddress)) {
            List<Map> childColumnList = getChildColumns(sheetModel.getPublishedAttributesJson(), subSchemaCode);
            for (BizProperty bizPropertyModel : collect) {
                if (CollectionUtils.isNotEmpty(childColumnList)) {
                    for (Map childColumn : childColumnList) {
                        if (childColumn.get("key").toString().equals(bizPropertyModel.getCode())) {
                            Map map2 = JSON.parseObject(childColumn.get("options").toString(), Map.class);
                            Object format1 = map2.get("format");
                            if (Objects.isNull(format1)) {
                                continue;
                            }
                            mapTemplate.put(bizPropertyModel.getCode(), format1.toString());
                        }
                    }
                }
            }
            Map<String, String> mapTemplateAddress = Maps.newHashMapWithExpectedSize(collectAddress.size());
            for (BizProperty bizPropertyModel : collectAddress) {
                for (BizQueryChildColumnModel childColumn : childColumns) {
                    if (CollectionUtils.isNotEmpty(childColumnList)) {
                        if (childColumn.getPropertyCode().equals(bizPropertyModel.getCode())) {
                            mapTemplateAddress.put(bizPropertyModel.getCode(), childColumn.getPropertyCode());
                        }
                    }
                }
            }
        }

        if (CollectionUtils.isNotEmpty(childColumns)) {

            for (BizQueryChildColumnModel columnModel : childColumns) {
                for (Map<String, Object> data : maps) {
                    if (columnModel.getPropertyType() == BizPropertyType.ADDRESS) {
                        String json = null;
                        if (data.get(columnModel.getPropertyCode()) != null) {
                            json = data.get(columnModel.getPropertyCode()).toString();
                        }
                        if ("{}".equals(json)) {
                            json = null;
                            data.put(columnModel.getPropertyCode(), null);
                        }
                        if (StringUtils.isNotEmpty(json)) {
                            Map map = JSONObject.parseObject(json, Map.class);
                            String name = "";
                            if (map.get("provinceName") != null) {
                                name = name + map.get("provinceName");
                            }
                            if (map.get("cityName") != null) {
                                name = name + map.get("cityName");
                            }
                            if (map.get("districtName") != null) {
                                name = name + map.get("districtName");
                            }
                            if (map.get("address") != null) {
                                name = name + map.get("address");
                            }
                            data.put(columnModel.getPropertyCode(), name);
                        }
                    }
                    //关联表单
                    if (columnModel.getPropertyType() == BizPropertyType.WORK_SHEET || columnModel.getPropertyType() == BizPropertyType.MULT_WORK_SHEET) {

                        if (data.get(columnModel.getPropertyCode()) != null) {
                            String properCode = columnModel.getPropertyCode();
                            Object o = data.get(properCode);
                            List<BizProperty> workSheet = workSheets.stream().filter(property -> Objects.equals(property.getCode(), columnModel.getPropertyCode())).collect(Collectors.toList());
                            if (CollectionUtils.isEmpty(workSheet)) {
                                data.put(properCode, "");
                                continue;
                            }
                            BizProperty bizPropertyModel = workSheet.get(0);
                            String relativePropertyCode = bizPropertyModel.getRelativePropertyCode();
                            if (StringUtils.isEmpty(relativePropertyCode)) {
                                relativePropertyCode = "name";
                            }
                            Map<String, Object> workSheetData = (Map<String, Object>) o;
                            if (workSheetData.get("propertyType") == BizPropertyType.SELECTION) {
                                List<SelectionValue> selectionValues = (List<SelectionValue>) workSheetData.get(relativePropertyCode);
                                data.put(properCode, getUserNameList(selectionValues, rootId));
                            } else if (workSheetData.get("propertyType") == BizPropertyType.ADDRESS) {
                                String json = null;
                                if (workSheetData.get(relativePropertyCode) == null) {
                                    data.put(properCode, null);
                                    continue;
                                }
                                json = workSheetData.get(relativePropertyCode).toString();
                                if ("{}".equals(json)) {
                                    json = null;
                                    data.put(properCode, null);
                                }
                                if (StringUtils.isNotEmpty(json)) {
                                    Map map = JSONObject.parseObject(json, Map.class);
                                    String name = "";
                                    if (map.get("provinceName") != null) {
                                        name = name + map.get("provinceName");
                                    }
                                    if (map.get("cityName") != null) {
                                        name = name + map.get("cityName");
                                    }
                                    if (map.get("districtName") != null) {
                                        name = name + map.get("districtName");
                                    }
                                    if (map.get("address") != null) {
                                        name = name + map.get("address");
                                    }
                                    data.put(properCode, name);
                                }
                            } else if (workSheetData.get("propertyType") == BizPropertyType.DATE) {
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                data.put(properCode, Objects.isNull(workSheetData.get(relativePropertyCode)) ? "" : simpleDateFormat.format(workSheetData.get(relativePropertyCode)));
                            } else {
                                String value = Objects.isNull(workSheetData.get(relativePropertyCode)) ? "" : workSheetData.get(relativePropertyCode).toString();
                                data.put(properCode, value);
                            }

                        }

                    }


                    //数值类型
                    if (columnModel.getPropertyType() == BizPropertyType.NUMERICAL) {
                        if (data.get(columnModel.getPropertyCode()) == null) {
                            continue;
                        }
                        DecimalFormat df = null;
                        String code = columnModel.getPropertyCode();
                        String displatFormat = mapTemplate.get(code);
                        if (StringUtils.isEmpty(displatFormat)) {
                            displatFormat = "null";
                            format.put(columnModel.getPropertyCode(), displatFormat);
                            childNumricalFormat.put(subSchemaCode, format);
                            continue;
                        }
//                    if (StringUtils.isEmpty(mapTemplate.get(columnModel.getPropertyCode()))){
//                        displatFormat = "null";
//                        format.put(columnModel.getPropertyCode(),displatFormat);
//                        childNumricalFormat.put(subSchemaCode,format);
//                        continue;
//                    }
                        switch (mapTemplate.get(columnModel.getPropertyCode())) {
                            case "integer":
                                df = new DecimalFormat("##0");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "tenths":
                                df = new DecimalFormat("###,##0.0");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "percentile":
                                df = new DecimalFormat("###,##0.00");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "Millimeter":
                                df = new DecimalFormat("###,##0.000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "tenThousand":
                                df = new DecimalFormat("###,##0.0000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "hundredThousand":
                                df = new DecimalFormat("###,##0.00000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "millionDecimals":
                                df = new DecimalFormat("###,##0.000000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "tenMillionDecimals":
                                df = new DecimalFormat("###,##0.0000000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "billionDecimals":
                                df = new DecimalFormat("###,##0.00000000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "ratio":
                                df = new DecimalFormat("###,##0%");
                                BigDecimal ratioResult = (BigDecimal) data.get(columnModel.getPropertyCode());
                                ratioResult = ratioResult.divide(new BigDecimal("100"));
                                data.put(columnModel.getPropertyCode(), df.format(ratioResult));
                                childNumricalFormat.put(code, data);
                                break;
                            case "ratio.tenths":
                                df = new DecimalFormat("###,##0.0" + "%");
                                BigDecimal ratioTenths = (BigDecimal) data.get(columnModel.getPropertyCode());
                                ratioTenths = ratioTenths.divide(new BigDecimal("100"));
                                data.put(columnModel.getPropertyCode(), df.format(ratioTenths));
                                childNumricalFormat.put(code, data);
                                break;
                            case "ratio.percentile":
                                df = new DecimalFormat("###,##0.00" + "%");
                                BigDecimal ratioPercentile = (BigDecimal) data.get(columnModel.getPropertyCode());
                                ratioPercentile = ratioPercentile.divide(new BigDecimal("100"));
                                data.put(columnModel.getPropertyCode(), df.format(ratioPercentile));
                                childNumricalFormat.put(code, data);
                                break;
                            case "ratio.Millimeter":
                                df = new DecimalFormat("###,##0.000" + "%");
                                BigDecimal ratioPercentile1 = (BigDecimal) data.get(columnModel.getPropertyCode());
                                ratioPercentile1 = ratioPercentile1.divide(new BigDecimal("100"));
                                data.put(columnModel.getPropertyCode(), df.format(ratioPercentile1));
                                childNumricalFormat.put(code, data);
                                break;
                            case "ratio.tenThousand":
                                df = new DecimalFormat("###,##0.0000" + "%");
                                BigDecimal ratioPercentile2 = (BigDecimal) data.get(columnModel.getPropertyCode());
                                ratioPercentile2 = ratioPercentile2.divide(new BigDecimal("100"));
                                data.put(columnModel.getPropertyCode(), df.format(ratioPercentile2));
                                childNumricalFormat.put(code, data);
                                break;
                            case "ratio.hundredThousand":
                                df = new DecimalFormat("###,##0.00000" + "%");
                                BigDecimal ratioPercentile3 = (BigDecimal) data.get(columnModel.getPropertyCode());
                                ratioPercentile3 = ratioPercentile3.divide(new BigDecimal("100"));
                                data.put(columnModel.getPropertyCode(), df.format(ratioPercentile3));
                                childNumricalFormat.put(code, data);
                                break;
                            case "ratio.millionDecimals":
                                df = new DecimalFormat("###,##0.000000" + "%");
                                BigDecimal ratioPercentile4 = (BigDecimal) data.get(columnModel.getPropertyCode());
                                ratioPercentile4 = ratioPercentile4.divide(new BigDecimal("100"));
                                data.put(columnModel.getPropertyCode(), df.format(ratioPercentile4));
                                childNumricalFormat.put(code, data);
                                break;
                            case "ratio.tenMillionDecimals":
                                df = new DecimalFormat("###,##0.0000000" + "%");
                                BigDecimal ratioPercentile5 = (BigDecimal) data.get(columnModel.getPropertyCode());
                                ratioPercentile5 = ratioPercentile5.divide(new BigDecimal("100"));
                                data.put(columnModel.getPropertyCode(), df.format(ratioPercentile5));
                                childNumricalFormat.put(code, data);
                                break;
                            case "ratio.billionDecimals":
                                df = new DecimalFormat("###,##0.00000000" + "%");
                                BigDecimal ratioPercentile6 = (BigDecimal) data.get(columnModel.getPropertyCode());
                                ratioPercentile6 = ratioPercentile6.divide(new BigDecimal("100"));
                                data.put(columnModel.getPropertyCode(), df.format(ratioPercentile6));
                                childNumricalFormat.put(code, data);
                                break;
                            case "RMB.percentile":
                                df = new DecimalFormat("¥#,##0.00");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "RMB.Millimeter":
                                df = new DecimalFormat("¥#,##0.000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "RMB.tenThousand":
                                df = new DecimalFormat("¥#,##0.0000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "RMB.hundredThousand":
                                df = new DecimalFormat("¥#,##0.00000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "RMB.millionDecimals":
                                df = new DecimalFormat("¥#,##0.000000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "RMB.tenMillionDecimals":
                                df = new DecimalFormat("¥#,##0.0000000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "RMB.billionDecimals":
                                df = new DecimalFormat("¥#,##0.00000000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "dollar.percentile":
                                df = new DecimalFormat("$#,##0.00");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "dollar.Millimeter":
                                df = new DecimalFormat("$#,##0.000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "dollar.tenThousand":
                                df = new DecimalFormat("$#,##0.0000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "dollar.hundredThousand":
                                df = new DecimalFormat("$#,##0.00000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "dollar.millionDecimals":
                                df = new DecimalFormat("$#,##0.000000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "dollar.tenMillionDecimals":
                                df = new DecimalFormat("$#,##0.0000000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "dollar.billionDecimals":
                                df = new DecimalFormat("$#,##0.00000000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "euro.percentile":
                                df = new DecimalFormat("€#,##0.00");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "euro.Millimeter":
                                df = new DecimalFormat("€#,##0.000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "euro.tenThousand":
                                df = new DecimalFormat("€#,##0.0000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "euro.hundredThousand":
                                df = new DecimalFormat("€#,##0.00000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "euro.millionDecimals":
                                df = new DecimalFormat("€#,##0.000000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "euro.tenMillionDecimals":
                                df = new DecimalFormat("€#,##0.0000000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "euro.billionDecimals":
                                df = new DecimalFormat("€#,##0.00000000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "HK.percentile":
                                df = new DecimalFormat("HK$#,##0.00");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "HK.Millimeter":
                                df = new DecimalFormat("HK$#,##0.000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "HK.tenThousand":
                                df = new DecimalFormat("HK$#,##0.0000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "HK.hundredThousand":
                                df = new DecimalFormat("HK$#,##0.00000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "HK.millionDecimals":
                                df = new DecimalFormat("HK$#,##0.000000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "HK.tenMillionDecimals":
                                df = new DecimalFormat("HK$#,##0.0000000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            case "HK.billionDecimals":
                                df = new DecimalFormat("HK$#,##0.00000000");
                                data.put(columnModel.getPropertyCode(), df.format(data.get(columnModel.getPropertyCode())));
                                childNumricalFormat.put(code, data);
                                break;
                            default:
                                break;
                        }
                    }
                }
                if (BizPropertyType.selectionTypes.contains(columnModel.getPropertyType())) {
                    childParseSpecialPropertiess(maps, true, columnModel, rootId);
                }
                if (columnModel.getPropertyType() == BizPropertyType.LOGICAL) {
                    childParselogicalPropertiess(maps, true, columnModel);
                }

            }
        }

    }

    /**
     * 处理PC端子表逻辑控件展示
     * @param datas       分页数据
     * @param columnModel 子表的选人控件字段
     * @param isExport    是否导出
     */
    private void childParselogicalPropertiess(List<Map<String, Object>> datas, Boolean isExport, BizQueryChildColumnModel columnModel) {

        if (CollectionUtils.isNotEmpty(datas)) {
            for (int i = 0; i < datas.size(); i++) {
                Map<String, Object> data = datas.get(i);
                String propertyCode = columnModel.getPropertyCode();
                Object logical = data.get(propertyCode);
                if (logical == null) {
                    String logic = "null";
                    logical = logic;
                }
                if (StringUtils.isEmpty(logical.toString())) {
                    data.put(columnModel.getPropertyCode(), "否");
                } else {
                    switch (logical.toString()) {
                        case "1":
                            data.put(columnModel.getPropertyCode(), "是");
                            break;
                        case "0":
                            data.put(columnModel.getPropertyCode(), "否");
                            break;
                        case "true":
                            data.put(columnModel.getPropertyCode(), "是");
                            break;
                        case "false":
                            data.put(columnModel.getPropertyCode(), "否");
                            break;
                        default:
                            data.put(columnModel.getPropertyCode(), "否");
                            break;
                    }
                }
            }
        }
    }

    /**
     * 处理PC端子表选人控件展示
     * @param datas 分页数据
     */
    private void childParseSpecialPropertiess(List<Map<String, Object>> datas, Boolean isExport, BizQueryChildColumnModel columnModel, String rootId) {
        if (CollectionUtils.isNotEmpty(datas)) {
            for (int i = 0; i < datas.size(); i++) {
                Map<String, Object> data = datas.get(i);
                Object o = data.get(columnModel.getPropertyCode());
                if (o instanceof String) {
                    data.put(columnModel.getPropertyCode(), o);
                } else {
                    List<SelectionValue> selectionValues = (List<SelectionValue>) data.get(columnModel.getPropertyCode());
                    if (CollectionUtils.isNotEmpty(selectionValues)) {
                        Object name = getUserNameList(selectionValues, rootId);
                        if (Objects.nonNull(name)) {
                            if (Objects.nonNull(name)) {
                                data.put(columnModel.getPropertyCode(), name);
                            } else {
                                data.put(columnModel.getPropertyCode(), "");
                            }
                        } else {
                            data.put(columnModel.getPropertyCode(), "");
                        }
                    } else {
                        data.put(columnModel.getPropertyCode(), "");
                    }
                }
            }
        }
    }

    /**
     * 解析表单配置信息
     * @param jsonString
     * @param subSchemaCode
     * @return
     */
    protected List<Map> getChildColumns(String jsonString, String subSchemaCode) {
        if (StringUtils.isEmpty(jsonString)) {
            return null;
        }
        Map map = JSON.parseObject(jsonString, Map.class);
        Object object = map.get(subSchemaCode);

        if (null != object) {
            Object columns = ((JSONObject) object).get("columns");
            if (columns instanceof JSONArray) {
                return JSONObject.parseArray(JSON.toJSONString(columns), Map.class);
            } else if (columns instanceof List) {
                return (List) columns;
            }
        }

        return null;
    }

    /**
     * 处理主表数据项导出的格式
     * @param columns 展示字段
     * @param data    业务数据对象
     * @return
     */
    protected void machesProperty(List<BizQueryColumnModel> columns, List<BizObjectModel> data, Boolean mobile, String schemaCode, String queryCode, String userId, String rootId) {

        if (CollectionUtils.isEmpty(data)) {
            return;
        }
        if (CollectionUtils.isEmpty(columns)) {
            return;
        }
        List<BizProperty> bizProperties = bizPropertyService.getListBySchemaCode(schemaCode, true);
        if (CollectionUtils.isNotEmpty(data)) {
            parseAddressAndReferences(data, columns, true, queryCode, schemaCode, userId, bizProperties, rootId);
            if (!mobile) {
                //处理PC端选人
                parseSpecialPropertiess(data, true, rootId);
            } else {
                //处理移动端用户图像
                disposeUserInfos(data);
            }
        }

    }

    /**
     * 处理PC端选人控件展示
     * @param data 分页数据
     */
    private void parseSpecialPropertiess(List<BizObjectModel> data, Boolean isExport, String rootId) {
        //获选人数据项
        BizObjectModel objectModel = data.get(0);
        BizSchema bizSchema = bizSchemaService.getBySchemaCode(objectModel.getSchemaCode());
        if (isExport) {
            List<String> selectCodes = getPropertyCodeList(objectModel, BizPropertyType.selectionTypes);
            if (CollectionUtils.isEmpty(selectCodes)) {
                return;
            }
//            log.debug("需要处理的选人数据项为{}", JSON.toJSONString(selectCodes));
            Set<String> dataCodes = null;
            for (String selectCode : selectCodes) {
                for (BizObjectModel bizObject : data) {
                    if (CollectionUtils.isEmpty(dataCodes)) {
                        dataCodes = bizObject.getData().keySet();
//                        log.debug("需要展示的数据项为{}", JSON.toJSONString(dataCodes));
                    }
                    if (!dataCodes.contains(selectCode)) {
//                        log.debug("不需要展示的选人数据项为{}", selectCode);
                        break;
                    }
                    if (bizObject.get(selectCode) == null) {
                        continue;
                    }
                    List<SelectionValue> selectionValues = (List<SelectionValue>) bizObject.get(selectCode);
                    bizObject.put(selectCode, getUserNameList(selectionValues, rootId));
                }
            }
            //处理单据状态
            if (data.get(0).getData().containsKey(DefaultPropertyType.SEQUENCE_STATUS.getCode())) {
                for (BizObjectModel bizObjectModel : data) {
                    bizObjectModel.put(DefaultPropertyType.SEQUENCE_STATUS.getCode(), parseSequenceStatus(bizObjectModel));
                }
            }
            List<BizProperty> bizProperties = bizPropertyService.getListBySchemaCode(objectModel.getSchemaCode(), true);
            //获取逻辑型
            List<BizProperty> logicalModels = bizProperties.stream().filter(t -> t.getPropertyType() == BizPropertyType.LOGICAL).collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(logicalModels)) {
                List<String> logicals = getPropertyCodes(logicalModels);
                Set<String> logicalDataCodes = null;
                for (String logical : logicals) {
                    for (BizObjectModel bizObjectModel : data) {
                        if (CollectionUtils.isEmpty(logicalDataCodes)) {
                            logicalDataCodes = bizObjectModel.getData().keySet();
                        }
                        if (!logicalDataCodes.contains(logical)) {
                            break;
                        }
                        if (bizObjectModel.get(logical) == null) {
                            continue;
                        }
                        bizObjectModel.put(logical, disposeLogics(bizObjectModel.getString(logical)));
                    }

                }
            }
        }

    }

    /**
     * 处理审批意见
     * @param logic
     * @return
     */
    public Object disposeLogics(String logic) {
        switch (logic) {
            case "1":
                return "是";
            case "0":
                return "否";
            case "true":
                return "是";
            case "false":
                return "否";
            default:
                return "否";
        }

    }

    /**
     * 处理单据状态
     * @param bizObjectModel
     * @return
     */
    private Object parseSequenceStatus(BizObjectModel bizObjectModel) {
        switch (bizObjectModel.get(DefaultPropertyType.SEQUENCE_STATUS.getCode()).toString()) {
            case "DRAFT":
                return SequenceStatus.DRAFT.getName();
            case "PROCESSING":
                return SequenceStatus.PROCESSING.getName();
            case "CANCELLED":
                return SequenceStatus.CANCELLED.getName();
            case "COMPLETED":
                return SequenceStatus.COMPLETED.getName();
            case "CANCELED":
                return SequenceStatus.CANCELED.getName();

            default:
                return null;
        }
    }

    /**
     * 从列表数据中获取到用户名
     * @return Object
     */
    public Object getUserNameList(List<SelectionValue> selectionValues, String rootId) {
        if (selectionValues == null) {
            return "";
        }
        List<SelectionValue> unitType = selectionValues.stream().filter(selectionValue -> selectionValue != null && Objects.equals(selectionValue.getType(), UnitType.DEPARTMENT)).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(unitType)) {
            return selectionValues.stream().filter(selectionValue -> selectionValue != null && StringUtils.isNotEmpty(selectionValue.getName())).map(SelectionValue::getName).collect(Collectors.joining(";"));
        }
        Map<String, String> deptMap = selectionValues.stream().filter(selectionValue -> ((selectionValue != null) && StringUtils.isNotEmpty(selectionValue.getName()) && StringUtils.isNotEmpty(selectionValue.getId()) && Objects.equals(selectionValue.getType(), UnitType.DEPARTMENT))).collect(Collectors.toMap(SelectionValue::getId, SelectionValue::getName));
        Set<String> deptIds = deptMap.keySet();
        List<String> nameList = Lists.newArrayListWithExpectedSize(deptIds.size());
        for (String id : deptIds) {
            Map<String, String> pathMap = getParentDepartNamesById(id, rootId);
            String path = pathMap.get(id);
            nameList.add(path);
        }
        List<String> userNameList = selectionValues.stream().filter(selectionValue -> ((selectionValue != null) && StringUtils.isNotEmpty(selectionValue.getName()) && StringUtils.isNotEmpty(selectionValue.getId()) && Objects.equals(selectionValue.getType(), UnitType.USER))).map(SelectionValue::getName).collect(Collectors.toList());
        nameList.addAll(userNameList);
        if (CollectionUtils.isEmpty(nameList)) {
            return "";
        }
        if (nameList.get(0) == null) {
            return "";
        }
        return Joiner.on(";").skipNulls().join(nameList);
    }

    public Map<String, String> getParentDepartNamesById(String id, String rootId) {
        DepartmentModel department = departmentFacade.get(id);
        Map<String, String> map = null;
        if (department != null) {
            map = new HashMap<>();
            boolean equals = Objects.equals(id, rootId);

            String path = null;
            if (equals) {
                path = department.getName();
                map.put(department.getId(), path);
            } else {
                path = departmentFacade.getDeptPath(department.getId());
                if (StringUtils.isNotEmpty(path)) {
                    path = path + "/" + department.getName();
                    map.put(department.getId(), path);
                } else {
                    map.put(department.getId(), department.getName());
                }
            }
        }
        return map;
    }

    /*
     *
     *
     * */
    private List<String> getPropertyCodeList(BizObjectModel objectModel, Set<BizPropertyType> propertyTypes) {
        List<BizProperty> bizProperties = bizPropertyService.getListBySchemaCode(objectModel.getSchemaCode(), true);
        if (CollectionUtils.isEmpty(bizProperties)) {
            return Collections.emptyList();
        }
        List<BizProperty> bizPropertyModels = bizProperties.stream().filter(t -> propertyTypes.contains(t.getPropertyType())).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(bizPropertyModels)) {
            return getPropertyCodes(bizPropertyModels);
        }
        return Collections.emptyList();
    }

    /**
     * 设置用户显示图像
     * @param data 分页数据
     */
    private void disposeUserInfos(List<BizObjectModel> data) {
        if (CollectionUtils.isEmpty(data)) {
            return;
        }
        for (BizObjectModel bizObject : data) {
            List<SelectionValue> selectionValues = (List<SelectionValue>) bizObject.get(DefaultPropertyType.CREATER.getCode());
            if (CollectionUtils.isNotEmpty(selectionValues)) {
                bizObject.put("imgUrl", selectionValues.get(0).getImgUrl());
                bizObject.put("originator", selectionValues.get(0).getName());
            }
        }
    }

    /**
     * 处理位置、数值、日期、关联表单、关联表单多选数据
     * @param data
     */
    protected void parseAddressAndReferences(List<BizObjectModel> data, List<BizQueryColumnModel> columns, Boolean isExport, String queryCode, String schemaCode, String userId, List<BizProperty> propertyModels, String rootId) {
        if (CollectionUtils.isEmpty(columns) || CollectionUtils.isEmpty(data)) {
            return;
        }
        if (isExport) {
            //获取模板信息
            BizQueryModel bizQuery = bizQueryService.getBizQueryModel(schemaCode, queryCode, ClientType.PC);
            columns = columns.stream().filter(t -> t.getPropertyType() == BizPropertyType.ADDRESS || t.getPropertyType() == BizPropertyType.WORK_SHEET || t.getPropertyType() == BizPropertyType.MULT_WORK_SHEET || t.getPropertyType() == BizPropertyType.NUMERICAL || t.getPropertyType() == BizPropertyType.DATE).collect(Collectors.toList());
            for (BizQueryColumnModel columnModel : columns) {
                for (BizObjectModel bizObjectModel : data) {
                    if (columnModel.getPropertyType() == BizPropertyType.ADDRESS) {
                        parseAddress(bizObjectModel, columnModel);
                    }
                    if ((columnModel.getPropertyType() == BizPropertyType.WORK_SHEET || columnModel.getPropertyType() == BizPropertyType.MULT_WORK_SHEET) && !("业务对象ID".equals(columnModel.getName()))) {
                        if (bizObjectModel.get(columnModel.getPropertyCode()) == null) {
                            continue;
                        }
                        if (bizObjectModel.getData().get(columnModel.getPropertyCode()) != null) {
                            String code = columnModel.getPropertyCode();
                            Object o1 = bizObjectModel.getData().get(code);
                            List<BizProperty> collect = propertyModels.stream().filter(property -> Objects.equals(property.getCode(), columnModel.getPropertyCode())).collect(Collectors.toList());
                            if (CollectionUtils.isEmpty(collect)) {
                                bizObjectModel.put(code, "");
                                continue;
                            }
                            BizProperty bizProperty = collect.get(0);
                            String relativePropertyCode = bizProperty.getRelativePropertyCode();
                            if (StringUtils.isEmpty(relativePropertyCode)) {
                                relativePropertyCode = "name";
                            }
                            Map<String, Object> workSheet = (Map<String, Object>) o1;
                            if (workSheet.get("propertyType") == BizPropertyType.SELECTION) {
                                List<SelectionValue> selectionValues = (List<SelectionValue>) workSheet.get(relativePropertyCode);
                                bizObjectModel.put(code, getUserNameList(selectionValues, rootId));
                            } else if (workSheet.get("propertyType") == BizPropertyType.ADDRESS) {
                                String json = null;
                                if (workSheet.get(relativePropertyCode) == null) {
                                    bizObjectModel.put(code, null);
                                    continue;
                                }
                                json = workSheet.get(relativePropertyCode).toString();
                                if ("{}".equals(json)) {
                                    json = null;
                                    bizObjectModel.put(code, null);
                                }
                                if (StringUtils.isNotEmpty(json)) {
                                    Map map = JSONObject.parseObject(json, Map.class);
                                    String name = "";
                                    if (map.get("provinceName") != null) {
                                        name = name + map.get("provinceName");
                                    }
                                    if (map.get("cityName") != null) {
                                        name = name + map.get("cityName");
                                    }
                                    if (map.get("districtName") != null) {
                                        name = name + map.get("districtName");
                                    }
                                    if (map.get("address") != null) {
                                        name = name + map.get("address");
                                    }
                                    bizObjectModel.put(code, name);
                                }
                            } else if (workSheet.get("propertyType") == BizPropertyType.DATE) {
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                bizObjectModel.put(code, Objects.isNull(workSheet.get(relativePropertyCode)) ? "" : simpleDateFormat.format(workSheet.get(relativePropertyCode)));
                            } else {
                                String value = Objects.isNull(workSheet.get(relativePropertyCode)) ? "" : workSheet.get(relativePropertyCode).toString();
                                bizObjectModel.put(code, value);
                            }

                        }
                    }
                    if (columnModel.getPropertyType() == BizPropertyType.NUMERICAL) {
                        parseNumber(bizObjectModel, columnModel, bizQuery);
                    }
                    if (columnModel.getPropertyType() == BizPropertyType.DATE) {
                        parseDate(bizObjectModel, columnModel, bizQuery);
                    }
                }
            }
        }
    }


    //处理地址数据
    private void parseAddress(BizObjectModel bizObjectModel, BizQueryColumnModel columnModel) {
        String json = null;
        if (bizObjectModel.get(columnModel.getPropertyCode()) != null) {
            json = bizObjectModel.get(columnModel.getPropertyCode()).toString();
        }
        if ("{}".equals(json)) {
            json = null;
            bizObjectModel.put(columnModel.getPropertyCode(), null);
        }
        if (StringUtils.isNotEmpty(json)) {
            Map map = JSONObject.parseObject(json, Map.class);
            String name = "";
            if (map.get("provinceName") != null) {
                name = name + map.get("provinceName");
            }
            if (map.get("cityName") != null) {
                name = name + map.get("cityName");
            }
            if (map.get("districtName") != null) {
                name = name + map.get("districtName");
            }
            if (map.get("address") != null) {
                name = name + map.get("address");
            }
            bizObjectModel.put(columnModel.getPropertyCode(), name);
        }
    }

    //处理数值数据
    private void parseNumber(BizObjectModel bizObjectModel, BizQueryColumnModel columnModel, BizQueryModel bizQuery) {

        List<BizQueryColumnModel> queryColumns = bizQuery.getQueryColumns();
        BizQueryColumnModel bizQueryColumnMode = null;
        for (BizQueryColumnModel bizQueryColumnModel : queryColumns) {
            boolean exits = bizQueryColumnModel.getPropertyType() == BizPropertyType.NUMERICAL;
            if (exits && Objects.equals(columnModel.getPropertyCode(), bizQueryColumnModel.getPropertyCode())) {
                bizQueryColumnMode = bizQueryColumnModel;
            }
        }
        if (bizObjectModel.getData().get(columnModel.getPropertyCode()) == null) {
            return;
        }
        DecimalFormat df = null;
        if (bizQueryColumnMode == null) {
            df = new DecimalFormat("##0.######");
            bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
            return;
        }
        Integer displayFormat = bizQueryColumnMode.getDisplayFormat();
        if (displayFormat == null) {
            displayFormat = 0;
        }
        switch (displayFormat) {
            case 1:
                df = new DecimalFormat("##0");
                df.setRoundingMode(RoundingMode.DOWN);
                Object value = bizObjectModel.getData().get(columnModel.getPropertyCode());
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(value));
                break;
            case 2:
                df = new DecimalFormat("###,##0.0");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 3:
                df = new DecimalFormat("###,##0.00");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 11:
                df = new DecimalFormat("###,##0.000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 12:
                df = new DecimalFormat("###,##0.0000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            //百分符号
            case 4:
                df = new DecimalFormat("##0");
                df.setRoundingMode(RoundingMode.DOWN);
                BigDecimal ratioResult = (BigDecimal) bizObjectModel.get(columnModel.getPropertyCode());
                ratioResult = ratioResult.divide(new BigDecimal("100"));
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(ratioResult));
                break;
            case 5:
                df = new DecimalFormat("###,##0.0");
                df.setRoundingMode(RoundingMode.DOWN);
                BigDecimal ratioTenths = (BigDecimal) bizObjectModel.get(columnModel.getPropertyCode());
                ratioTenths = ratioTenths.divide(new BigDecimal("100"));
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(ratioTenths));
                break;
            case 6:
                df = new DecimalFormat("###,##0.00");
                df.setRoundingMode(RoundingMode.DOWN);
                BigDecimal ratioPercentile = (BigDecimal) bizObjectModel.get(columnModel.getPropertyCode());
                ratioPercentile = ratioPercentile.divide(new BigDecimal("100"));
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(ratioPercentile));
                break;
            case 13:
                df = new DecimalFormat("###,##0.000");
                df.setRoundingMode(RoundingMode.DOWN);
                BigDecimal ratioPercentile1 = (BigDecimal) bizObjectModel.get(columnModel.getPropertyCode());
                ratioPercentile1 = ratioPercentile1.divide(new BigDecimal("100"));
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(ratioPercentile1));
                break;
            case 14:
                df = new DecimalFormat("###,##0.0000");
                df.setRoundingMode(RoundingMode.DOWN);
                BigDecimal ratioPercentile2 = (BigDecimal) bizObjectModel.get(columnModel.getPropertyCode());
                ratioPercentile2 = ratioPercentile2.divide(new BigDecimal("100"));
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(ratioPercentile2));
                break;
            case 27:
                df = new DecimalFormat("###,##0.00000");
                df.setRoundingMode(RoundingMode.DOWN);
                BigDecimal ratioPercentile3 = (BigDecimal) bizObjectModel.get(columnModel.getPropertyCode());
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(ratioPercentile3));
                break;
            case 28:
                df = new DecimalFormat("###,##0.0000000");
                df.setRoundingMode(RoundingMode.DOWN);
                BigDecimal ratioPercentile4 = (BigDecimal) bizObjectModel.get(columnModel.getPropertyCode());
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(ratioPercentile4));
                break;
            case 29:
                df = new DecimalFormat("###,##0.00000000");
                df.setRoundingMode(RoundingMode.DOWN);
                BigDecimal ratioPercentile5 = (BigDecimal) bizObjectModel.get(columnModel.getPropertyCode());
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(ratioPercentile5));
                break;
            case 30:
                df = new DecimalFormat("###,##0.000000000");
                df.setRoundingMode(RoundingMode.DOWN);
                BigDecimal ratioPercentile6 = (BigDecimal) bizObjectModel.get(columnModel.getPropertyCode());
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(ratioPercentile6));
                break;
            case 7:
                df = new DecimalFormat("¥#,##0.00");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 15:
                df = new DecimalFormat("¥#,##0.000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 16:
                df = new DecimalFormat("¥#,##0.0000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 31:
                df = new DecimalFormat("¥#,##0.00000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 32:
                df = new DecimalFormat("¥#,##0.000000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 33:
                df = new DecimalFormat("¥#,##0.0000000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 34:
                df = new DecimalFormat("¥#,##0.00000000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 8:
                df = new DecimalFormat("$#,##0.00");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 17:
                df = new DecimalFormat("$#,##0.000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 18:
                df = new DecimalFormat("$#,##0.0000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 35:
                df = new DecimalFormat("$#,##0.00000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 36:
                df = new DecimalFormat("$#,##0.000000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 37:
                df = new DecimalFormat("$#,##0.0000000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 38:
                df = new DecimalFormat("$#,##0.00000000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 9:
                df = new DecimalFormat("€#,##0.00");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 19:
                df = new DecimalFormat("€#,##0.000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 20:
                df = new DecimalFormat("€#,##0.0000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 39:
                df = new DecimalFormat("€#,##0.00000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 40:
                df = new DecimalFormat("€#,##0.000000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 41:
                df = new DecimalFormat("€#,##0.0000000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 42:
                df = new DecimalFormat("€#,##0.00000000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 10:
                df = new DecimalFormat("HK$#,##0.00");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 21:
                df = new DecimalFormat("HK$#,##0.000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 22:
                df = new DecimalFormat("HK$#,##0.0000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 43:
                df = new DecimalFormat("HK$#,##0.00000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 44:
                df = new DecimalFormat("HK$#,##0.000000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 45:
                df = new DecimalFormat("HK$#,##0.0000000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 46:
                df = new DecimalFormat("HK$#,##0.00000000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            //追加小数
            case 23:
                df = new DecimalFormat("###,##0.00000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 24:
                df = new DecimalFormat("###,##0.000000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 25:
                df = new DecimalFormat("###,##0.0000000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            case 26:
                df = new DecimalFormat("###,##0.00000000");
                df.setRoundingMode(RoundingMode.DOWN);
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
                break;
            default:
                break;
        }

    }


    //处理日期数据
    private void parseDate(BizObjectModel bizObjectModel, BizQueryColumnModel columnModel, BizQueryModel bizQuery) {
        List<BizQueryColumnModel> queryColumns = bizQuery.getQueryColumns();
        BizQueryColumnModel bizQueryColumnMode = null;
        for (BizQueryColumnModel bizQueryColumnModel : queryColumns) {
            String propertyCode = columnModel.getPropertyCode();
            String propertyCode1 = bizQueryColumnModel.getPropertyCode();
            if (StringUtils.isEmpty(propertyCode) || StringUtils.isEmpty(propertyCode1)) {
                continue;
            }
            boolean exits = bizQueryColumnModel.getPropertyType() == BizPropertyType.DATE && Objects.equals(propertyCode, propertyCode1);
            if (exits) {
                bizQueryColumnMode = bizQueryColumnModel;
            }
        }
        if (bizObjectModel.getData().get(columnModel.getPropertyCode()) == null) {
            return;
        }
        SimpleDateFormat df = null;
        if (bizQueryColumnMode == null) {
            df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.getData().get(columnModel.getPropertyCode())));
            return;
        }
        Integer displayFormat = bizQueryColumnMode.getDisplayFormat();
        if (displayFormat == null) {
            displayFormat = 0;
        }
        Object dt = bizObjectModel.getData().get(columnModel.getPropertyCode());
        Date date;
        if (!(dt instanceof Date)) {
            try {
                date = DateHelpUtils.parseDate(dt.toString());
            } catch (ParseException e) {
                log.warn(e.getMessage(), e);
                throw new RuntimeException(e.getMessage());
            }
        } else {
            date = (Date) dt;
        }
        switch (displayFormat) {
            case 1:
                df = new SimpleDateFormat("yyyy-MM-dd") {
                };
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(date));
                break;
            case 2:
            case 0:
                df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(date));
                break;
            case 3:
                df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(date));
                break;
            case 4:
                df = new SimpleDateFormat("yyyy-MM");
                bizObjectModel.put(columnModel.getPropertyCode(), df.format(date));
                break;
            default:
                break;
        }
    }


    @Override
    public List<String> queryBizObjectIds(BizObjectQueryModel bizObjectQueryModel) {
        final String schemaCode = bizObjectQueryModel.getSchemaCode();
        final String queryCode = bizObjectQueryModel.getQueryCode();
        final ClientType clientType = Objects.nonNull(bizObjectQueryModel.getClientType()) ? bizObjectQueryModel.getClientType() : ClientType.PC;
        if (StringUtils.isEmpty(schemaCode) && StringUtils.isEmpty(queryCode)) {
            throw new ServiceException(ErrCode.BIZ_QUERY_CODE_SCHEMACODE_NOT_EXIST, bizObjectQueryModel);
        }

        final BizQuery bizQuery = bizQueryService.getBySchemaCodeAndCode(schemaCode, queryCode);
        if (bizQuery == null) {
            log.debug("bizQuery is null. {}", queryCode);
            throw new ServiceException(ErrCode.BIZ_QUERY_NOT_EXIST, queryCode);
        }
        String queryId = bizQuery.getId();
        //排序信息
        List<BizQuerySort> sortList = bizQuerySortService.getListByQueryId(queryId);
        List<BizQuerySort> sorts = sortList.stream().filter(t -> Objects.equals(clientType, t.getClientType())).collect(Collectors.toList());
        final List<OrderImpl> orders = new ArrayList<>();
        if (CollectionUtils.isEmpty(sorts)) {
            //使用默认排序
            orders.add(new OrderImpl(DefaultPropertyType.CREATED_TIME.getCode(), Order.Dir.DESC));
        } else {
            orders.addAll(getOrders(sorts));
        }
        List<String> columnList = Lists.newArrayList();
        final List<String> bizObjectIds = bizObjectService.queryBizObjectIds(new BizObjectQueryObject() {
            @Override
            public String getSchemaCode() {
                return schemaCode;
            }

            @Override
            public FilterExpression getFilterExpression() {
                return bizObjectQueryModel.getFilterExpr();
            }

            @Override
            public List<String> getDisplayFields() {
                columnList.add(DefaultPropertyType.ID.getCode());
                return columnList;
            }

            @Override
            public Pageable getPageable() {
                return bizObjectQueryModel.getPageable();
            }

            @Override
            public Sortable getSortable() {
                return orders == null ? null : new SortableImpl(orders);
            }
        });

        return bizObjectIds;
    }

    /**
     * 处理查询列表最终需要查询的字段
     * @param schemaCode 模型编码
     * @param queryCode  查询列表编码
     * @param queryId    查询列表id
     * @param options    查询列表配置项
     * @return 需要查询的字段
     */
    private List<String> listQueryColumn(String schemaCode, String queryCode, String queryId, BizObjectQueryModel.Options options, ClientType clientType, Boolean display) {
        QueryDisplayType displayType = QueryDisplayType.DEFAULT;
        if (options != null) {
            displayType = options.getQueryDisplayType();
        }

        final List<String> columnList = Lists.newArrayList();
        switch (displayType) {
            case DEFAULT:
            case APPEND:
                //获取需要展示的列信息
                List<BizQueryColumn> columnTemplateList = bizQueryColumnService.getListByQueryId(queryId);
                //新版列表设计展示字段区分客户端
                List<BizQueryColumn> columnTemplates = columnTemplateList.stream().filter(t -> Objects.equals(clientType, t.getClientType())).collect(Collectors.toList());
                if (CollectionUtils.isEmpty(columnTemplates)) {
                    log.warn("查询列表信息展示列不能为空.");
                    throw new ServiceException(ErrCode.BIZ_QUERY_DISPLAY_COLUMNS_NOT_EMPTY, queryCode);
                }

                //处理列表脏数据
                final List<BizProperty> propertyList = bizPropertyService.getListBySchemaCode(schemaCode, null);
                final List<String> codes = getPropertyCodes(propertyList);
                if (display) {
                    columnList.addAll(codes);
                } else {
                    for (BizQueryColumn columnTemplate : columnTemplates) {
                        if (codes.contains(columnTemplate.getPropertyCode())) {
                            columnList.add(columnTemplate.getPropertyCode());
                        }

                    }
                }

                if (displayType == QueryDisplayType.APPEND) {
                    log.debug("APPEND mode.");
                    final List<String> appendColumns = options.getCustomDisplayColumns();

                    if (CollectionUtils.isNotEmpty(appendColumns)) {
                        if (log.isDebugEnabled()) {
                            log.debug("OVERRIDE columns = {}", JSON.toJSONString(appendColumns));
                        }
                        columnList.addAll(appendColumns);
                    }

                }
                break;

            case OVERRIDE:
                log.debug("OVERRIDE mode.");
                final List<String> overRideColumns = options.getCustomDisplayColumns();
                if (CollectionUtils.isEmpty(overRideColumns)) {
                    log.warn("查询列表显示模式为OVERRIDE时，显示字段不能为空.");
                    throw new ServiceException(ErrCode.BIZ_QUERY_COLUMNS_NOT_EMPTY_FOR_OVERRIDE, queryCode);
                }
                if (log.isDebugEnabled()) {
                    log.debug("OVERRIDE columns = {}", JSON.toJSONString(overRideColumns));
                }
                columnList.addAll(overRideColumns);
                break;
            default:
                log.warn("查询列表显示模式不能为空.");
                throw new ServiceException(ErrCode.BIZ_QUERY_DISPLAY_MODE_NOT_EMPTY, queryCode);
        }
        return columnList;
    }

    /**
     * 获取排序信息
     * @param sorts
     * @return
     */
    private List<OrderImpl> getOrders(List<BizQuerySort> sorts) {
        List<OrderImpl> orders = new ArrayList<>();
        sorts.forEach(sort -> {
            orders.add(new OrderImpl(sort.getPropertyCode(), SortDirectionType.ASC == sort.getDirection() ? Order.Dir.ASC : Order.Dir.DESC));
        });
        return orders.size() > 0 ? orders : null;
    }

    @Override
    public Boolean removeBizObjects(String userId, String schemaCode, List<String> objectIds) {

        // 树形模型删除校验
        if (bizObjectConverter.checkTreeExistChild(schemaCode, objectIds)) {
            throw new ServiceException(ErrCode.BIZ_OBJECT_TREE_MODEL_EXIST_CHILD);
        }
        for (Object obj : objectIds) {
            String objectId;
            if (obj instanceof String) {
                objectId = (String) obj;
            } else {
                objectId = obj.toString();
            }

            removeBizObject(userId, schemaCode, objectId);
        }
        return Boolean.TRUE;
    }

    @Override
    public List<CheckResultForRemoveBizObject> checkForRemoveBizObject(String schemaCode, List<String> objectIds, List<String> noDeleteIDs) {
        List<CheckResultForRemoveBizObject> list = new ArrayList<>(objectIds.size());
        List<BizPropertyModel> bizPropertyModelList = ModelUtil.toModel(bizPropertyService.getListByRelativeCode(schemaCode), BizPropertyModel.class);
        bizPropertyModelList = bizPropertyModelList.stream().filter(item -> item.getPropertyType() == BizPropertyType.WORK_SHEET || item.getPropertyType() == BizPropertyType.MULT_WORK_SHEET).collect(Collectors.toList());
        Map<String, List<BizPropertyModel>> bizPropertyMap = bizPropertyModelList.stream().collect(Collectors.groupingBy(BizPropertyModel::getSchemaCode));
        for (Object obj : objectIds) {
            CheckResultForRemoveBizObject checkResult = new CheckResultForRemoveBizObject();
            String objectId;
            if (obj instanceof String) {
                objectId = (String) obj;
            } else {
                objectId = obj.toString();
            }
            WorkflowInstanceModel instance = bizWorkflowInstanceService.getByObjectId(objectId);
            if (instance == null) {
                checkResult.setResultCode(CheckResultForRemoveBizObject.CheckResultCode.BIZ_DATA);
            } else {
                boolean isParentWf = false;
                boolean isChildWf = false;
                List<WorkflowInstanceModel> childInstances = bizWorkflowInstanceService.getWorkflowInstanceByParentId(instance.getId());
                if (CollectionUtils.isNotEmpty(childInstances)) {
                    checkResult.setResultCode(CheckResultForRemoveBizObject.CheckResultCode.PARENT_WF_DATA);
                    isParentWf = true;
                }
                if (StringUtils.isNotBlank(instance.getParentId())) {
                    checkResult.setResultCode(CheckResultForRemoveBizObject.CheckResultCode.CHILD_WF_DATA);
                    isChildWf = true;
                }
                if (!isParentWf && !isChildWf) {
                    if (instance.getState() == WorkflowInstanceStatus.DRAFT) {
                        checkResult.setResultCode(CheckResultForRemoveBizObject.CheckResultCode.WF_DRAFT_DATA);
                    } else {
                        checkResult.setResultCode(CheckResultForRemoveBizObject.CheckResultCode.ONLY_WF_DATA);
                    }
                }
            }
            checkResult.setObjectId(objectId);
            list.add(checkResult);
        }
        //遍历关联数据模型
        checkRelative(list, bizPropertyMap, objectIds);

        for (String id : noDeleteIDs) {
            CheckResultForRemoveBizObject checkResult = new CheckResultForRemoveBizObject();
            checkResult.setResultCode(CheckResultForRemoveBizObject.CheckResultCode.NO_DELETE_WF_DATA);
            checkResult.setObjectId(id);
            list.add(checkResult);
        }
        return list;
    }

    /**
     * 校验是否存在关联的业务数据
     * @param list
     * @param bizPropertyMap
     * @param objectIds
     */
    private void checkRelative(List<CheckResultForRemoveBizObject> list, Map<String, List<BizPropertyModel>> bizPropertyMap, List<String> objectIds) {
        int count = 0;
        CountDownLatch countDownLatch = new CountDownLatch(bizPropertyMap.size() > 10 ? 10 : bizPropertyMap.size());
        Map<String, String> concurrentHashMap = new ConcurrentHashMap<>();
        for (Map.Entry<String, List<BizPropertyModel>> map : bizPropertyMap.entrySet()) {
            //未避免关联模型过多导致性能问题，现只查前10个模型数据
            count++;
            if (count > 10) {
                break;
            }
            checkRelativePool.execute(() -> {
                try {
                    Set<String> relativeIds = new HashSet<>();
                    BizPropertyModel bizPropertyModel = ModelUtil.toModel(bizPropertyService.getByPropertyCode(map.getKey()), BizPropertyModel.class);
                    if (bizPropertyModel != null && BizPropertyType.CHILD_TABLE == bizPropertyModel.getPropertyType()) {
                        //子表关联
                        childTableRelativeCheck(map, objectIds, relativeIds, bizPropertyModel);
                    } else {
                        //非子表关联
                        mainTableRelativeCheck(map, objectIds, relativeIds);

                    }
                    //是否存在关联数据判断
                    if (CollectionUtils.isEmpty(relativeIds)) {
                        return;
                    }
                    BizSchema bizSchema = bizSchemaService.getBySchemaCode(bizPropertyModel != null ? bizPropertyModel.getSchemaCode() : map.getKey());
                    //组装map，key=关联Id，value=关联模型名称（拼接）
                    relativeIds.forEach(str -> {
                        String modelName = concurrentHashMap.get(str);
                        if (StringUtils.isBlank(modelName)) {
                            concurrentHashMap.put(str, bizSchema.getName());
                        } else {
                            concurrentHashMap.put(str, modelName + '、' + bizSchema.getName());
                        }
                    });
                } catch (Exception e) {
                    log.error("处理失败", e);
                } finally {
                    countDownLatch.countDown();
                }
            });
        }
        //等待线程执行完
        try {
            countDownLatch.await();
            //遍历删除数据赋值关联模型
            list.forEach(item -> {
                if (concurrentHashMap.containsKey(item.getObjectId())) {
                    item.setRelativeModel(concurrentHashMap.get(item.getObjectId()));
                }
            });
        } catch (InterruptedException e) {
            log.error("[data delete checkRelative] countDownLatch await error. ", e);
        }
    }

    private void childTableRelativeCheck(Map.Entry<String, List<BizPropertyModel>> map, List<String> objectIds, Set<String> relativeIds, BizPropertyModel bizPropertyModel) {
        BizObjectQueryObject bizObjectQueryObject = new BizObjectQueryObject() {
            @Override
            public String getSchemaCode() {
                return map.getKey();
            }

            @Override
            public FilterExpression getFilterExpression() {
                List<FilterExpression> filterExpressions = new ArrayList<>();
                map.getValue().forEach(t -> {
                    if (t.getPropertyType() == BizPropertyType.WORK_SHEET) {
                        filterExpressions.add(Q.it(t.getCode(), FilterExpression.Op.In, objectIds));
                    } else if (t.getPropertyType() == BizPropertyType.MULT_WORK_SHEET) {
                        String objectIdStr = String.join("|", objectIds);
                        filterExpressions.add(Q.it(t.getCode(), FilterExpression.Op.Reg, objectIdStr));
                    }
                });
                if (filterExpressions.size() == 1) {
                    return filterExpressions.get(0);
                } else {
                    return Q.or(filterExpressions);
                }
            }

            @Override
            public List<String> getDisplayFields() {
                HashSet<String> displayFields = Sets.newHashSet();
                map.getValue().forEach(t -> displayFields.add(t.getCode()));
                return new ArrayList<>(displayFields);
            }
        };
        //查询子表中是否存在关联数据
        List<Map<String, Object>> chilData = bizObjectConverter.findChildTableData(map.getKey(), bizObjectQueryObject);
        if (CollectionUtils.isNotEmpty(chilData)) {
            chilData.forEach(item ->
                    item.forEach((k, v) -> {
                        if (!Objects.isNull(v)) {
                            List<String> strList = Arrays.asList(String.valueOf(v).split(";"));
                            relativeIds.addAll(strList);
                        }
                    })
            );
        }
    }

    private void mainTableRelativeCheck(Map.Entry<String, List<BizPropertyModel>> map, List<String> objectIds, Set<String> relativeIds) {
        //数据查询
        Page<Map<String, Object>> page = bizObjectService.queryBizObjects(new BizObjectQueryObject() {
            @Override
            public String getSchemaCode() {
                return map.getKey();
            }

            @Override
            public FilterExpression getFilterExpression() {
                List<FilterExpression> filterExpressions = new ArrayList<>();
                map.getValue().forEach(t -> {
                    if (t.getPropertyType() == BizPropertyType.WORK_SHEET) {
                        filterExpressions.add(Q.it(t.getCode(), FilterExpression.Op.In, objectIds));
                    } else if (t.getPropertyType() == BizPropertyType.MULT_WORK_SHEET) {
                        String objectIdStr = String.join("|", objectIds);
                        filterExpressions.add(Q.it(t.getCode(), FilterExpression.Op.Reg, objectIdStr));
                    }
                });
                if (filterExpressions.size() == 1) {
                    return filterExpressions.get(0);
                } else {
                    return Q.or(filterExpressions);
                }
            }

            @Override
            public List<String> getDisplayFields() {
                HashSet<String> displayFields = Sets.newHashSet();
                map.getValue().forEach(t -> displayFields.add(t.getCode()));
                return new ArrayList<>(displayFields);
            }
        });
        if (page.getTotal() == 0) {
            return;
        }

        //将数据中所有关联的ID抽成set
        page.getContent().forEach(item ->
                item.forEach((k, v) -> {
                    if (!Objects.isNull(v)) {
                        List<String> strList = Arrays.asList(String.valueOf(v).split(";"));
                        relativeIds.addAll(strList);
                    }
                })
        );

    }

    @Override
    public long countBizObject(String userId, String schemaCode) {
        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(schemaCode)) {
            log.debug("用户Id或schemaCode为空.");
            throw new ServiceException(ErrCode.SYS_PARAMETER_EMPTY);
        }

        getUser(userId);
        try {
            return bizObjectService.countBizObject(schemaCode);
        } catch (Exception e) {
            log.error("countBizObject happen exception. ", e);
        }
        return 0L;
    }

    @Override
    public List<BizCommentModel> getComments(String workflowInstanceId, String tokenId) {
        log.info("加载审批意见. workflowInstanceId = {}, tokenId = {}", workflowInstanceId, tokenId);
        List<Comment> tempComments;

        if (StringUtils.isEmpty(tokenId)) {
            tempComments = bizObjectService.listWorkflowInstanceComments(workflowInstanceId);
        } else {
            tempComments = bizObjectService.listWorkflowInstanceComments(workflowInstanceId, tokenId);
            tempComments = tempComments.stream().filter(comment -> ActionType.APPROVAL == comment.getActionType()).collect(Collectors.toList());
        }

        if (log.isTraceEnabled()) {
            log.trace("comments = {}", JSONObject.toJSONString(tempComments));
        }

        if (CollectionUtils.isEmpty(tempComments)) {
            return Collections.EMPTY_LIST;
        }

        List<BizCommentModel> models = Lists.newArrayListWithExpectedSize(tempComments.size());

        for (Comment c : tempComments) {
            BizCommentModel model = new BizCommentModel();
            model.setId(c.getId());
            model.setContent(c.getContent());
            model.setWorkItemId(c.getWorkItemId());
            model.setCommentTime(c.getCreatedTime());
            model.setBizPropertyCode(c.getBizPropertyCode());
            model.setRelUsers(SelectionValue.toListMap(c.getRelUsers()));
            model.setResult(c.getResult());
            model.setWorkflowTokenId(c.getWorkflowTokenId());
            model.setCreater(SelectionValue.toMap(c.getCreater()));
            model.setTokenId(c.getTokenId());
            model.setActivityCode(c.getActivityCode());
            model.setActivityName(c.getActivityName());
            model.setActionType(c.getActionType());
            model.setResources(getAttachments(c.getAttachments()));
            model.setCreatedTime(c.getCreatedTime());
            models.add(model);
        }
        return models;
    }

    @Override
    public AttachmentModel getAttachmentByRefId(String refId) {
        Optional<Attachment> resource = bizObjectService.getAttachmentByRefId(refId);
        if (!resource.isPresent()) {
            return null;
        }
        AttachmentModel model = new AttachmentModel();
        model.setId(resource.get().getId());
        model.setRefId(resource.get().getRefId());
        model.setName(resource.get().getName());
        model.setFileExtension(resource.get().getFileExtension());
        model.setFileSize(resource.get().getFileSize());
        model.setMimeType(resource.get().getMimeType());
        return model;
    }

    @Override
    public AttachmentModel getFormCommentAttachmentByRefId(String attachmentId) {
        BizFormCommentAttachment resource = bizFormCommentService.getFormCommentAttachmentByRefId(attachmentId);
        if (resource == null) {
            return null;
        }
        AttachmentModel model = new AttachmentModel();
        model.setId(resource.getId());
        model.setRefId(resource.getRefId());
        model.setName(resource.getName());
        model.setFileExtension(resource.getFileExtension());
        model.setFileSize(resource.getFileSize());
        model.setMimeType(resource.getMimeType());
        return model;
    }

    @Override
    public Integer batchInsertAttachments(List<AttachmentModel> attachmentModelList) {
        return bizObjectService.insertAttachments(attachmentModelList);
    }

    /**
     * 附件对象转换
     * @param resources
     * @return
     */
    private List<AttachmentModel> getAttachments(List<? extends Attachment> resources) {
        List<AttachmentModel> rms = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(resources)) {
            for (Attachment r : resources) {
                AttachmentModel rm = new AttachmentModel();
                rm.setId(r.getId());
                rm.setRefId(r.getRefId());
                rm.setName(r.getName());
                rm.setFileExtension(r.getFileExtension());
                rm.setFileSize(r.getFileSize());
                rms.add(rm);
            }
        }
        return rms;
    }

    @Override
    public PairSettingModel savePairSetting(PairSettingModel pairSettingModel) {
        if (log.isTraceEnabled()) {
            log.trace("savePairSetting 参数:{}", JSON.toJSONString(pairSettingModel));
        }
        //数据已存在时，直接使用原有数据
        try {
            // 解析json数据
            parseDataFromValue(pairSettingModel);

            if (StringUtils.isNotBlank(pairSettingModel.getObjectId()) && StringUtils.isNotBlank(pairSettingModel.getFormCode())) {
                List<PairSetting> pairValueList = pairSettingService.findByObjectIdAndFormCode(pairSettingModel.getObjectId(), pairSettingModel.getFormCode());
                if (CollectionUtils.isNotEmpty(pairValueList)) {
                    return ModelUtil.toModel(pairValueList.get(0), PairSettingModel.class);
                }
            }
        } catch (Exception e) {
        }
        PairSetting pairSetting = pairSettingService.save(ModelUtil.toEntity(pairSettingModel, PairSetting.class));
        return ModelUtil.toModel(pairSetting, PairSettingModel.class);
    }

    private PairSettingModel parseDataFromValue(PairSettingModel pairSettingModel) {
        String pairValue = pairSettingModel.getPairValue();
        if (!JSONUtil.isJson(pairValue)) {
            return pairSettingModel;
        }
        cn.hutool.json.JSONObject pairValueJson = JSONUtil.parseObj(pairValue);
        String objectId = pairValueJson.getStr("objectId");
        if (objectId == null) {
            objectId = pairValueJson.getStr("id");
        }
        String formCode = pairValueJson.getStr("formCode");
        if (formCode == null) {
            formCode = pairValueJson.getStr("sheetCode");
        }
        String schemaCode = pairValueJson.getStr("schemaCode");
        String workflowInstanceId = pairValueJson.getStr("workflowInstanceId");
        pairSettingModel.setObjectId(objectId);
        pairSettingModel.setSchemaCode(schemaCode);
        pairSettingModel.setFormCode(formCode);
        pairSettingModel.setWorkflowInstanceId(workflowInstanceId);
        return pairSettingModel;
    }

    @Override
    public PairSettingModel findByPairCode(String pairCode) {
        if (log.isTraceEnabled()) {
            log.trace("findByPairCode 参数:{}", JSON.toJSONString(pairCode));
        }
        PairSetting pairSetting = pairSettingService.findByPairCode(pairCode);
        return ModelUtil.toModel(pairSetting, PairSettingModel.class);
    }

    @Override
    public List<PairSettingModel> findAllPairSetting() {
        List<PairSetting> list = pairSettingService.findAll();
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }
        return ModelUtil.toModel(list, PairSettingModel.class);
    }

    @Override
    public List<Map<String, Object>> getChildDatas(String parentId, String subSchemaCode) {
        return bizObjectConverter.fromDbChildTableValue(subSchemaCode, parentId, true);
    }

    /**
     * 批量生成短码
     * 该接口不考虑重复请求，一次请求生成一批短码
     * @param pairSettingModels
     * @return
     */
    @Override
    public List<PairSettingModel> savePairCodes(List<PairSettingModel> pairSettingModels) {
        if (org.springframework.util.CollectionUtils.isEmpty(pairSettingModels)) {
            return Collections.EMPTY_LIST;
        }
        List<PairSettingModel> modelList = Lists.newArrayListWithExpectedSize(pairSettingModels.size());
        for (PairSettingModel settingModel : pairSettingModels) {
            parseDataFromValue(settingModel);
            PairSetting pairSetting = pairSettingService.save(ModelUtil.toEntity(settingModel, PairSetting.class));
            modelList.add(ModelUtil.toModel(pairSetting, PairSettingModel.class));
        }
        return modelList;
    }

    @Override
    public Page<BizObjectModel> queryBizObjectItemDatas(String owner, String schemaCode, String sequenceStatus, Integer page, Integer size) {
        if (StringUtils.isEmpty(owner)) {
            return new PageImpl<>(0L, Lists.newArrayList());
        }
        //当没有模型编码的时候
        if (!StringUtils.isEmpty(schemaCode)) {
            List<FilterExpression> filterExprValues = new ArrayList<>();
            FilterExpression filterExprValue = FilterExpression.empty;
            //获取数据查询条件
            filterExprValues.add(Q.it(DefaultPropertyType.OWNER.getCode(), FilterExpression.Op.Eq, owner));
            if (!StringUtils.isEmpty(sequenceStatus)) {
                String[] sequences = sequenceStatus.split(";");
                filterExprValues.add(Q.it(DefaultPropertyType.SEQUENCE_STATUS.getCode(), FilterExpression.Op.In, Arrays.asList(sequences)));
            }
            if (filterExprValues.size() == 1) {
                filterExprValue = filterExprValues.get(0);
            } else if (filterExprValues.size() > 1) {
                filterExprValue = Q.and(filterExprValues);
            }

            BizObjectQueryModel bizObjectQueryModel = new BizObjectQueryModel();
            bizObjectQueryModel.setFilterExpr(filterExprValue);

            //分页信息
            PageableImpl pageable = new PageableImpl(page * size, size);
            bizObjectQueryModel.setPageable(pageable);
            bizObjectQueryModel.setSchemaCode(schemaCode);
            bizObjectQueryModel.setQueryCode(schemaCode);
            bizObjectQueryModel.setClientType(ClientType.PC);
            Page<BizObjectModel> bizObjectModelPage = queryTransferBizObjects(bizObjectQueryModel);


            return bizObjectModelPage;
        }
        return new PageImpl<>(0L, Lists.newArrayList());
    }


    @Override
    public String getTableName(String schemaCode) {
        return db.ddl().getTableName(schemaCode);
    }

    @Override
    public BizFormModel getTempAuthSchemaCodes(String sheetId) {
        return ModelUtil.toModel(bizFormService.getTempAuthSchemaCodesBySheetId(sheetId), BizFormModel.class);
    }

    @Override
    public Page<BizObjectModel> queryBizObjectSkipQuery(BizObjectQueryModel bizObjectQueryModel) {
        String schemaCode = bizObjectQueryModel.getSchemaCode();
        final Page<Map<String, Object>> queryData = bizObjectService.queryBizObjects(new BizObjectQueryObject() {
            @Override
            public String getSchemaCode() {
                return schemaCode;
            }

            @Override
            public FilterExpression getFilterExpression() {
                return bizObjectQueryModel.getFilterExpr();
            }

            @Override
            public List<String> getDisplayFields() {
                List<String> groupByFields = bizObjectQueryModel.getGroupByFields();
                if (CollectionUtils.isNotEmpty(groupByFields)) {
                    return groupByFields;
                }

                List<String> orderByFields = bizObjectQueryModel.getOrderByFields();

                //下拉框关联业务模型：：自定义排序时，指定查询列表信息
                if (CollectionUtils.isNotEmpty(orderByFields)) {
                    BizObjectQueryModel.Options options = bizObjectQueryModel.getOptions();
                    if (options != null && options.getQueryDisplayType() == QueryDisplayType.OVERRIDE) {
                        List<String> columnList = Lists.newArrayList();
                        List<String> customDisplayColumns = options.getCustomDisplayColumns();
                        columnList.addAll(customDisplayColumns);
                        return columnList;
                    }
                }

                BizObjectQueryModel.Options options = bizObjectQueryModel.getOptions();
                if (options != null) {
                    List<String> columnList = Lists.newArrayList();
                    List<String> customDisplayColumns = options.getCustomDisplayColumns();
                    columnList.addAll(customDisplayColumns);
                    if (!columnList.contains(DefaultPropertyType.ID.getCode())) {
                        columnList.add(DefaultPropertyType.ID.getCode());
                    }
                    if (!columnList.contains(DefaultPropertyType.NAME.getCode())) {
                        columnList.add(0, DefaultPropertyType.NAME.getCode());
                    }
                    if (!columnList.contains(DefaultPropertyType.OWNER.getCode())) {
                        columnList.add(DefaultPropertyType.OWNER.getCode());
                    }
                    if (!columnList.contains(DefaultPropertyType.CREATER.getCode())) {
                        columnList.add(DefaultPropertyType.CREATER.getCode());
                    }
                    if (!columnList.contains(DefaultPropertyType.CREATED_TIME.getCode())) {
                        columnList.add(DefaultPropertyType.CREATED_TIME.getCode());
                    }
                    if (!columnList.contains(DefaultPropertyType.MODIFIED_TIME.getCode())) {
                        columnList.add(DefaultPropertyType.MODIFIED_TIME.getCode());
                    }
                    if (!columnList.contains(DefaultPropertyType.SEQUENCE_STATUS.getCode())) {
                        columnList.add(DefaultPropertyType.SEQUENCE_STATUS.getCode());
                    }
                    return columnList;
                }
                return bizPropertyService.getListBySchemaCode(schemaCode, true).stream().map(p -> p.getCode()).collect(Collectors.toList());
            }

            @Override
            public Sortable getSortable() {
                ArrayList<OrderImpl> orders = Lists.newArrayList();

                List<String> orderByFields = bizObjectQueryModel.getOrderByFields();
                int orderType = bizObjectQueryModel.getOrderType();
                if (CollectionUtils.isNotEmpty(orderByFields)) {
                    orderByFields.forEach(fields -> {
                        orders.add(new OrderImpl(fields, orderType == 2 ? Order.Dir.DESC : Order.Dir.ASC));
                    });
                    return new SortableImpl(orders);
                }

                List<String> groupByFields = bizObjectQueryModel.getGroupByFields();
                if (CollectionUtils.isNotEmpty(groupByFields)) {
                    return new SortableImpl(orders);
                }

                orders.add(new OrderImpl(DefaultPropertyType.CREATED_TIME.getCode(), Order.Dir.ASC));
                return new SortableImpl(orders);
            }

            @Override
            public Pageable getPageable() {
                return bizObjectQueryModel.getPageable();
            }

            @Override
            public List<String> getGroupByFields() {
                return bizObjectQueryModel.getGroupByFields();
            }
        });
        //转换WEB-API需要的数据
        List<? extends Map<String, Object>> content = queryData.getContent();
        final List<BizObjectModel> bizObjectList = Lists.newArrayListWithExpectedSize(content.size());
        for (Map<String, Object> map : content) {
            final BizObjectModel bizObjectModel = new BizObjectModel(schemaCode, map, false);
            bizObjectList.add(bizObjectModel);
        }
        if (log.isTraceEnabled()) {
            log.trace("==================bizObjectList={}", JSON.toJSONString(bizObjectList));
        }
        return new PageImpl<BizObjectModel>(queryData.getTotal(), bizObjectList);
    }

    @Override
    public String saveBizFormComment(BizFormCommentModel bizFormCommentModel) {
        return bizFormCommentService.saveBizFormComment(bizFormCommentModel);
    }

    @Override
    public void deleteBizFormComment(String id, String userId) {
        bizFormCommentService.deleteBizFormComment(id, userId);
    }

    @Override
    public BizFormCommentModel getBizFormCommentModel(String id) {
        BizFormComment bizFormComment = bizFormCommentService.get(id);
        return ModelUtil.toModel(bizFormComment, BizFormCommentModel.class);
    }

    @Override
    public org.springframework.data.domain.Page<BizFormCommentModel> fromCommentList(String bizObjectId, String schemaCode, int page, int pageSize) {
        Sort sort = Sort.by(Sort.Direction.DESC, "createdTime");
        final org.springframework.data.domain.Pageable pageable = PageRequest.of(Math.max(page, 0), Math.min(pageSize, 200), sort);
        org.springframework.data.domain.Page<BizFormCommentModel> bizFormCommentPage = bizFormCommentService.queryBizFormCommentByBizObjectIdAndSchemaCode(bizObjectId, schemaCode, pageable);
        if (bizFormCommentPage == null || CollectionUtils.isEmpty(bizFormCommentPage.getContent())) {
            return bizFormCommentPage;
        }
        final org.springframework.data.domain.Pageable replyPageable = PageRequest.of(0, 2, sort);
        bizFormCommentPage.getContent().forEach(it -> {
            org.springframework.data.domain.Page<BizFormCommentModel> replyCommentPage = bizFormCommentService.queryReplyComment(it.getId(), bizObjectId, schemaCode, replyPageable);
            if (replyCommentPage == null || CollectionUtils.isEmpty(replyCommentPage.getContent())) {
                return;
            }
            it.setReplies(replyCommentPage.getContent());
            it.setReplyCount(replyCommentPage.getTotalElements());

        });
        return bizFormCommentPage;
    }

    @Override
    public org.springframework.data.domain.Page<BizFormCommentModel> replyCommentList(String commentId, int page, int pageSize) {
        Sort sort = Sort.by(Sort.Direction.DESC, "createdTime");
        final org.springframework.data.domain.Pageable pageable = PageRequest.of(Math.max(page, 0), Math.min(pageSize, 50), sort);
        BizFormComment bizFormComment = bizFormCommentService.get(commentId);
        if (bizFormComment == null) {
            log.warn("评论[{}]不存在", commentId);
            return org.springframework.data.domain.Page.empty(pageable);
        }
        return bizFormCommentService.queryReplyComment(commentId, bizFormComment.getBizObjectId(), bizFormComment.getSchemaCode(), pageable);
    }

    @Override
    public Integer countBizFormComment(String bizObjectId, String schemaCode) {
        return bizFormCommentService.countByBizObjectAndSchemaCode(bizObjectId, schemaCode);
    }

    @Override
    public org.springframework.data.domain.Page<SelectionValue> pullPerson(String keyword, String bizObjectId, String schemaCode, String wfInstanceId, org.springframework.data.domain.Pageable pageable) {
        return bizFormCommentService.pullPerson(keyword, bizObjectId, schemaCode, wfInstanceId, pageable);
    }

    @Override
    public Map<String, List<SelectionValue>> getSecondImportDatas(String code, String schemaCode, String corpId, Map<String, Object> map, OrgType orgType) {
        Map<String, List<SelectionValue>> selectionValueMap = new HashMap<>();
        BizProperty bizProperty = bizPropertyService.get(schemaCode, code);
        Boolean propertyEmpty = bizProperty.getPropertyEmpty();
        Object o = map.get(code);
        boolean empty = Objects.isNull(o);
        //TODO此处的propertyEmpty需要再次确认
        if (empty && propertyEmpty) {
            return Maps.newHashMap();
        }

        List<Map<String, Object>> selectionValues = (List<Map<String, Object>>) o;

        if ((CollectionUtils.isEmpty(selectionValues) || MapUtils.isEmpty(selectionValues.get(0)) || Objects.isNull(selectionValues.get(0).get("id")) || StringUtils.isBlank(selectionValues.get(0).get("id").toString())) && propertyEmpty) {
            return Maps.newHashMap();
        }
        List<SelectionValue> list = new ArrayList<>();
        if (CollectionUtils.isEmpty(selectionValues)) {
            selectionValueMap.put(code, null);
            return selectionValueMap;
        }
        for (Map<String, Object> selectionValue : selectionValues) {
            Object type = selectionValue.get("type");
            Integer unitType = Integer.valueOf(type.toString());
            String id = (String) selectionValue.get("id");
            Boolean boo = validateSelectionImportDatas(id, corpId, unitType, list, orgType);
            if (!boo) {
                return Maps.newHashMap();
            }
        }
        selectionValueMap.put(code, list);
        return selectionValueMap;
    }

    private Boolean validateSelectionImportDatas(String id, String corpId, Integer type, List<SelectionValue> list, OrgType orgType) {
        SelectionValue selectionValue = new SelectionValue();
        if (type == 3) {
            UserModel user = userFacade.get(id);
            if (user == null || UserStatus.DISABLE == user.getStatus()) {
                return Boolean.FALSE;
            }
            if (orgType != OrgType.MAIN) {
                if (StringUtils.isEmpty(user.getCorpId()) || !Objects.equals(corpId, user.getCorpId())) {
                    return Boolean.FALSE;
                }
            }
            selectionValue.setType(UnitType.USER);
            selectionValue.setId(id);
            selectionValue.setName(user.getName());
            list.add(selectionValue);
        } else if (type == 1) {
            DepartmentModel department = departmentFacade.get(id);
            if (department == null || Objects.equals(department.getDeleted(), true)) {
                return Boolean.FALSE;
            }
            if (orgType != OrgType.MAIN) {
                if (StringUtils.isEmpty(department.getCorpId()) || (!Objects.equals(corpId, department.getCorpId()))) {
                    return Boolean.FALSE;
                }
            }
            selectionValue.setType(UnitType.DEPARTMENT);
            selectionValue.setId(id);
            selectionValue.setName(department.getName());
            list.add(selectionValue);
        }
        return Boolean.TRUE;
    }

    @Override
    public Page<BizObjectModel> queryBizObjectsFilter(BizObjectQueryModel bizObjectQueryModel, int page, int size) {
        final String schemaCode = bizObjectQueryModel.getSchemaCode();
        final String queryCode = bizObjectQueryModel.getQueryCode();
        final ClientType clientType = Objects.nonNull(bizObjectQueryModel.getClientType()) ? bizObjectQueryModel.getClientType() : ClientType.PC;
        if (StringUtils.isEmpty(schemaCode) && StringUtils.isEmpty(queryCode)) {
            throw new ServiceException(ErrCode.BIZ_QUERY_CODE_SCHEMACODE_NOT_EXIST, bizObjectQueryModel);
        }

        final BizQuery bizQuery = bizQueryService.getBySchemaCodeAndCode(schemaCode, queryCode);
        if (bizQuery == null) {
            log.debug("bizQuery is null. {}", queryCode);
            throw new ServiceException(ErrCode.BIZ_QUERY_NOT_EXIST, queryCode);
        }
        String queryId = bizQuery.getId();

        //排序信息
        List<BizQuerySort> sortList = bizQuerySortService.getListByQueryId(queryId);
        List<BizQuerySort> sorts = sortList.stream().filter(t -> Objects.equals(clientType, t.getClientType())).collect(Collectors.toList());
        final List<OrderImpl> orders = new ArrayList<>();
        if (CollectionUtils.isEmpty(sorts)) {
            //使用默认排序
            orders.add(new OrderImpl(DefaultPropertyType.CREATED_TIME.getCode(), Order.Dir.DESC));
        } else {
            orders.addAll(getOrders(sorts));
        }

        List<BizProperty> properties = bizPropertyService.getListBySchemaCode(schemaCode, true);
        if (CollectionUtils.isEmpty(properties)) {
            return new PageImpl<>(0, Lists.newArrayList());
        }

        QueryRelationObject queryRelationObject = null;
        // 多模型查询判断 (树形模型不支持多模型)
        if (bizObjectQueryModel.getView() == null || ModelViewType.LIST == bizObjectQueryModel.getView().getType()) {
            queryRelationObject = buildRelationQueryObject(bizObjectQueryModel, queryId, schemaCode, clientType, sorts, properties);
        }
        // 树形控件作为查询条件 且 需要查询子数据时，构建关联查询 sprint33
        if (queryRelationObject == null) {
            queryRelationObject = buildRelationQueryObjectForTree(bizObjectQueryModel, queryId, clientType, sorts, properties);
        }
        List<String> columnList = new ArrayList<>(DEFAULT_CAPACITY);
        if (queryRelationObject == null) {
            List<String> propertyCodes = properties.stream().filter(Objects::nonNull).map(BizProperty::getCode).collect(Collectors.toList());
            List<String> columns = listQueryColumn(schemaCode, queryCode, queryId, bizObjectQueryModel.getOptions(), clientType, bizObjectQueryModel.getDisplay());
            columnList = columns.stream().filter(propertyCodes::contains).collect(Collectors.toList());
            // 补全必要展示字段
            columnList = completeSystemFields(schemaCode, bizObjectQueryModel.getQuotes(), columnList);
            if (log.isDebugEnabled()) {
                log.debug("==================columnList={}", columnList);
            }
        }
        columnList.addAll(bizObjectQueryModel.getGanttColumns());
        boolean showParticipant = showParticipant(queryId, clientType);
        Pair<QueryRelationObject, List<String>> pair = Pair.of(queryRelationObject, columnList);
        final Page<Map<String, Object>> queryData = bizObjectService.queryBizObjects(new BizObjectQueryObject() {

            @Override
            public String getSchemaCode() {
                return schemaCode;
            }

            @Override
            public FilterExpression getFilterExpression() {
                return bizObjectQueryModel.getFilterExpr();
            }

            @Override
            public List<String> getDisplayFields() {
                List<String> displayFields = pair.getRight();
                if (showParticipant) {
                    displayFields.add(DefaultPropertyType.WORKFLOW_INSTANCE_ID.getCode());
                }
                return displayFields;
            }

            @Override
            public Pageable getPageable() {
                return bizObjectQueryModel.getPageable();
            }

            @Override
            public Sortable getSortable() {
                ArrayList<OrderImpl> dynamicOrders = Lists.newArrayList();

                List<String> orderByFields = bizObjectQueryModel.getOrderByFields();
                int orderType = bizObjectQueryModel.getOrderType();
                if (CollectionUtils.isNotEmpty(orderByFields)) {
                    orderByFields.forEach(fields -> {
                        dynamicOrders.add(new OrderImpl(fields, orderType == 2 ? Order.Dir.DESC : Order.Dir.ASC));
                    });
                    return new SortableImpl(dynamicOrders);
                }

                return orders == null ? null : new SortableImpl(orders);
            }

            @Override
            public Map<String, Object> getQuotes() {
                return bizObjectQueryModel.getQuotes();
            }

            public QueryRelationObject getQueryRelationObject() {
                QueryRelationObject relationObject = pair.getLeft();
                if (relationObject != null) {
                    relationObject.setShowTotal(bizObjectQueryModel.getShowTotal());
                }
                return pair.getLeft();
            }

            public ModelViewQueryModel getViewType() {
                return bizObjectQueryModel.getView();
            }

            public FilterExpression getAuthFilterExpression() {
                return bizObjectQueryModel.getAuthFilter();
            }

            public FilterExpression getFilterExpressionExcludeAuth() {
                return bizObjectQueryModel.getFilterExcludeAuth();
            }

            public boolean showParticipant() {
                return showParticipant;
            }

            public boolean getChildPageEnable() {
                return bizObjectQueryModel.getChildPageEnable();
            }

            public boolean showTotal() {
                return bizObjectQueryModel.getShowTotal();
            }
        });

        //转换WEB-API需要的数据
        final List<BizObjectModel> bizObjectList = buildBizObjectModel(schemaCode, (List<Map<String, Object>>) queryData.getContent());

        if (log.isTraceEnabled()) {
            log.trace("==================queryBizObjectsFilter bizObjectQueryModel={} dataSize={}", JSON.toJSONString(bizObjectQueryModel), queryData.getTotal());
        }
        return new PageImpl<>(queryData.getTotal(), bizObjectList);
    }

    private List<String> completeSystemFields(String schemaCode, Map<String, Object> quotes, List<String> displayFields) {
        HashSet<String> displayFieldSet = Sets.newHashSet(DefaultPropertyType.ID.getCode(),
                DefaultPropertyType.NAME.getCode(),
                DefaultPropertyType.OWNER.getCode(),
                DefaultPropertyType.CREATER.getCode(),
                DefaultPropertyType.CREATED_TIME.getCode(),
                DefaultPropertyType.MODIFIED_TIME.getCode(),
                DefaultPropertyType.SEQUENCE_STATUS.getCode(),
                DefaultPropertyType.WORKFLOW_INSTANCE_ID.getCode());
        displayFieldSet.addAll(displayFields);

        //引用类型的关联表单的数据  必须返回
        if (MapUtils.isNotEmpty(quotes)) {
            Object o = quotes.get(schemaCode);
            if (Objects.nonNull(o)) {
                Map<String, List<QuoteModel>> map = (Map<String, List<QuoteModel>>) o;
                Set<String> workSheets = map.keySet();
                if (!workSheets.isEmpty()) {
                    displayFieldSet.addAll(workSheets);
                }
            }
        }
        return new ArrayList<>(displayFieldSet);
    }

    private List<BizObjectModel> buildBizObjectModel(String schemaCode, List<Map<String, Object>> data) {
        List<BizObjectModel> bizObjectList = Lists.newArrayListWithExpectedSize(data.size());
        for (Map<String, Object> map : data) {
            Object childrenObj = map.get(ModelConstant.TREE_MODEL_QUERY_CHILDREN_KEY);
            if (childrenObj != null) {
                map.put(ModelConstant.TREE_MODEL_QUERY_CHILDREN_KEY, buildBizObjectModel(schemaCode, (List<Map<String, Object>>) childrenObj));
            }
            final BizObjectModel bizObjectModel = new BizObjectModel(schemaCode, map, false);
            bizObjectList.add(bizObjectModel);
        }
        return bizObjectList;
    }

    /**
     * 保存列表数据批量修改记录
     * @param bizObjectBatchUpdateRecordModel
     */
    @Override
    public void saveBizObjectBatchUpdateRecord(BizObjectBatchUpdateRecordModel bizObjectBatchUpdateRecordModel) {
        batchUpdateRecordService.saveBizObjectBatchUpdateRecord(ModelUtil.toEntity(bizObjectBatchUpdateRecordModel, BizObjectBatchUpdateRecord.class));
    }

    /**
     * 查询列表数据批量修改记录
     * @param schemaCode
     * @param pageable
     * @return
     */
    @Override
    public org.springframework.data.domain.Page<BizObjectBatchUpdateRecordModel> queryBizObjectBatchUpdateRecord(String schemaCode, org.springframework.data.domain.Pageable pageable) {
        org.springframework.data.domain.Page<BizObjectBatchUpdateRecordModel> page = ModelUtil.toModel(batchUpdateRecordService.queryBizObjectBatchUpdateRecord(schemaCode, pageable), pageable, BizObjectBatchUpdateRecordModel.class);
        List<BizObjectBatchUpdateRecordModel> content = page.getContent();
        if (CollectionUtils.isEmpty(content)) {
            return page;
        }

        List<String> userIds = content.stream().map(BizObjectBatchUpdateRecordModel::getUserId).collect(Collectors.toList());
        List<UserModel> users = userFacade.listByIdList(userIds);
        if (CollectionUtils.isEmpty(users)) {
            return page;
        }

        Map<String, UserModel> userNameMap = users.stream().collect(Collectors.toMap(UserModel::getId, Function.identity(), (v1, v2) -> v1));
        for (BizObjectBatchUpdateRecordModel bizObjectBatchUpdateRecordModel : content) {
            UserModel user = userNameMap.get(bizObjectBatchUpdateRecordModel.getUserId());
            if (user != null) {
                bizObjectBatchUpdateRecordModel.setUserName(user.getName());
                bizObjectBatchUpdateRecordModel.setImgUrl(user.getImgUrl());
            }
        }

        return page;
    }

    @Override
    public void updateBizObject(String userId, BizObjectModel bizObject, BizObject objectExisted) {
        bizObjectService.updateBizObject(userId, bizObject, objectExisted, null);
    }

    @Override
    public void updateBizObject(String userId, BizObjectModel bizObject, BizObject objectExisted, BizObjectOptions options) {
        bizObjectService.updateBizObject(userId, bizObject, objectExisted, options);
    }

    @Override
    public String saveComment(BizCommentModel model) {
        return bizObjectService.saveComment(new CreateComment() {
            @Override
            public String getSchemaCode() {
                return model.getSchemaCode();
            }

            @Override
            public String getBizObjectId() {
                return model.getBizObjectId();
            }

            @Override
            public String getWorkflowTokenId() {
                return model.getWorkflowTokenId();
            }

            @Override
            public String getWorkflowInstanceId() {
                return model.getWorkflowInstanceId();
            }

            @Nullable
            @Override
            public String getWorkItemId() {
                return model.getWorkItemId();
            }

            @Override
            public Integer getTokenId() {
                return model.getTokenId();
            }

            @Override
            public String getActivityCode() {
                return model.getActivityCode();
            }

            @Override
            public String getActivityName() {
                return model.getActivityName();
            }

            @Override
            public ActionType getActionType() {
                return model.getActionType();
            }

            @Override
            public String getContent() {
                return model.getContent();
            }

            @Override
            @Nullable
            public FormActionType getResult() {
                return model.getResult();
            }

            @Override
            @Nullable
            public String getBizPropertyCode() {
                return model.getBizPropertyCode();
            }

            @Override
            @Nullable
            public List<SelectionValue> getRelUsers() {
                if (org.springframework.util.CollectionUtils.isEmpty(model.getRelUsers())) {
                    return Lists.newArrayList();
                }
                String relUser = JSON.toJSONString(model.getRelUsers());
                return JSON.parseObject(relUser, new TypeReference<List<SelectionValue>>() {
                });
            }

            @Override
            public String getCreater() {
                return model.getCreatedBy();
            }

            @Nullable
            @Override
            public List<Attachment> getAttachments() {
                if (org.springframework.util.CollectionUtils.isEmpty(model.getResources())) {
                    return Lists.newArrayList();
                }
                return model.getResources().stream().map(it -> new Attachment() {
                    @Override
                    public String getId() {
                        return it.getId();
                    }

                    @Override
                    public String getRefId() {
                        return it.getRefId();
                    }

                    @Override
                    public String getName() {
                        return it.getName();
                    }

                    @Override
                    public String getMimeType() {
                        return it.getMimeType();
                    }

                    @Override
                    public String getFileExtension() {
                        return it.getFileExtension();
                    }

                    @Override
                    public Integer getFileSize() {
                        return it.getFileSize();
                    }

                    @Override
                    public String getBase64ImageStr() {
                        return it.getBase64ImageStr();
                    }
                }).collect(Collectors.toList());
            }
        });
    }

    @Override
    public BizObject loadBizObject(String schemaCode, @Nullable String objectId, @Nullable Map<String, Object> data) {
        return bizObjectService.loadBizObject(schemaCode, objectId, data);
    }

    @Override
    public BizObject createAndGetBizObject(String userId, BizObjectModel bizObject) throws EngineRuntimeException, ServiceException {
        updateChildTableModifyProperties(userId, bizObject);
        return bizObjectService.createAndGetBizObject(userId, bizObject);
    }

    @Override
    public void updateBizObject(BizObject bizObject) throws EngineRuntimeException, ServiceException {
        bizObjectService.updateBizObject(bizObject, null);
    }

    @Override
    public void updateBizObject(BizObject bizObject, BizObjectOptions options) throws EngineRuntimeException, ServiceException {
        bizObjectService.updateBizObject(bizObject, options);
    }

    @Override
    public void deleteBizObject(String schemaCode, String objectId) {
        bizObjectService.deleteBizObject(schemaCode, objectId);
    }

    @Override
    public BizObject invokeBizRule(String schemaCode, String objectId, BizObject bizObject, String businessRuleCode) {
        return bizObjectService.invokeBizRule(schemaCode, objectId, bizObject, businessRuleCode);
    }

    @Override
    public Object invokeBizRule(String businessRuleCode, String schemaCode, Object object) {
        return bizObjectService.invokeBizRule(businessRuleCode, schemaCode, object);
    }

    @Override
    public void invokeMethod(BizObject bizObject, String bizSchemaMethodCode) {
        bizObjectService.invokeMethod(bizObject, bizSchemaMethodCode);
    }

    @Override
    public String genBizObjectId() {
        return bizObjectService.genBizObjectId();
    }

    @Override
    public BizObject createAndGetBizObject(BizObject bizObject) throws EngineRuntimeException, ServiceException {
        return bizObjectService.createAndGetBizObject(bizObject);
    }

    @Override
    public void modifiedBizObject(BizObject bizObject) throws EngineRuntimeException, ServiceException {
        bizObjectService.modifiedBizObject(bizObject, null);
    }

    @Override
    public void modifyBizObject(BizObject bizObject, Boolean isGet) {
        bizObjectService.modifyBizObject(bizObject, isGet, null);
    }

    @Override
    public Page doInvokeMethodForCollection(BizObjectQueryObject bizObjectQueryObject, BizMethodMappingModel bizMethodMappingModel) {
        BizMethodMapping bizMethodMapping = ModelUtil.toEntity(bizMethodMappingModel, BizMethodMapping.class);
        return bizObjectService.doInvokeMethodForCollection(bizObjectQueryObject, bizMethodMapping);
    }

    @Override
    public Page<Map<String, Object>> queryBizObjectsWithNoBind(BizObjectQueryObject bizObjectQueryObject) {
        return bizObjectService.queryBizObjectsWithNoBind(bizObjectQueryObject);
    }

    @Override
    public Page<Map<String, Object>> queryBizObjects(BizObjectQueryObject bizObjectQueryObject) {
        return bizObjectService.queryBizObjects(bizObjectQueryObject);
    }

    @Override
    public List<String> queryBizObjectIds(BizObjectQueryObject bizObjectQueryObject) {
        return bizObjectService.queryBizObjectIds(bizObjectQueryObject);
    }

    @Override
    public void deleteObjectWorkFlow(String objectId) {
        bizObjectService.deleteObjectWorkFlow(objectId);
    }

    @Override
    public long countBizObject(String schemaCode) {
        return bizObjectService.countBizObject(schemaCode);
    }

    @Override
    public void invokeMethod(String schemaCode, String objectId, String bizSchemaMethodCode) {
        bizObjectService.invokeMethod(schemaCode, objectId, bizSchemaMethodCode);
    }

    @Override
    public BizObject invokeMethod(BizObject bizObject, BizMethodMappingModel methodMappingModel) {
        BizMethodMapping bizMethodMapping = ModelUtil.toEntity(methodMappingModel, BizMethodMapping.class);
        return bizObjectService.invokeMethod(bizObject, bizMethodMapping);
    }

    @Override
    public Integer insertAttachments(List<AttachmentModel> attachmentModelList) {
        return bizObjectService.insertAttachments(attachmentModelList);
    }

    @Override
    public String createBizObject(BizObject bizObject) throws EngineRuntimeException, ServiceException {
        return bizObjectService.createBizObject(bizObject);
    }

    /**
     * 关联查询
     * @param queryRelationModel 关联查询对象
     * @return 查询列表分页
     */
    @Override
    public Page<BizObjectModel> relationQuery(BizObjectQueryRelationModel queryRelationModel) {
        if (CollectionUtils.isEmpty(queryRelationModel.getSchemaRelations())) {
            throw new ServiceException(ErrCode.UNKNOW_ERROR, "未指定关联模型");
        }
        if (CollectionUtils.isEmpty(queryRelationModel.getDisplayFields())) {
            throw new ServiceException(ErrCode.UNKNOW_ERROR, "未指定查询字段");
        }
        QueryRelationObject queryObject = new QueryRelationObject();
        //主模型编码
        queryObject.setSchemaCode(queryRelationModel.getSchemaCode());
        //关联模型
        queryObject.getSchemaRelationsMap().put(queryRelationModel.getSchemaCode(), queryRelationModel.getSchemaRelations());
        //显示字段
        queryObject.setDisplayFields(queryRelationModel.getDisplayFields());
        //分组字段
        queryObject.setGroupByFields(queryRelationModel.getGroupByFields());
        //排序字段
        queryObject.setOrderByFields(queryRelationModel.getOrderByFields());
        //查询条件
        queryObject.setFilterExpression(queryRelationModel.getFilters());
        //分页信息
        if (queryRelationModel.getPage() != null) {
            Pageable pageable = new PageableImpl(queryRelationModel.getPage() * queryRelationModel.getSize(), queryRelationModel.getSize());
            queryObject.setPageable(pageable);
        }
        Page<Map<String, Object>> pageResult = bizObjectLocalCacheService.relationQuery(queryObject);
        List<? extends Map<String, Object>> content = pageResult.getContent();
        List<BizObjectModel> bizObjectList = Lists.newArrayListWithExpectedSize(content.size());
        for (Map<String, Object> map : content) {
            BizObjectModel bizObjectModel = new BizObjectModel(queryObject.getSchemaCode(), map, false);
            bizObjectList.add(bizObjectModel);
        }
        if (log.isTraceEnabled()) {
            log.trace("==================bizObjectList={}", JSON.toJSONString(bizObjectList));
        }
        return new PageImpl<>(pageResult.getTotal(), bizObjectList);
    }

    /**
     * 构建多模型查询条件，为空则不是多模型查询
     * @param bizObjectQueryModel 查询信息
     * @param queryId             查询编号
     * @param schemaCode          模型编码
     * @param clientType          客户端
     * @param sorts               排序字段
     * @param properties          数据项
     * @return QueryRelationObject
     **/
    private QueryRelationObject buildRelationQueryObject(BizObjectQueryModel bizObjectQueryModel, String queryId, String schemaCode,
                                                         ClientType clientType, List<BizQuerySort> sorts, List<BizProperty> properties) {
        //1***判断是否需要关联查询
        BizQueryPresent bizQueryPresent = bizQueryPresentService.getByQueryIdAndClientType(queryId, clientType);
        if (bizQueryPresent == null) {
            return null;
        }
        String schemaRelationsJson = bizQueryPresent.getSchemaRelations();
        if (StringUtils.isBlank(schemaRelationsJson)) {
            return null;
        }
        BizQuerySchemaRelationModel bizQuerySchemaRelationModel = JSONUtil.toBean(schemaRelationsJson, BizQuerySchemaRelationModel.class);
        if (bizQuerySchemaRelationModel == null) {
            return null;
        }
        Map<String, String> aliasMapping = bizQuerySchemaRelationModel.getAliasMapping();
        Map<String, List<RelationObject>> schemaRelations = bizQuerySchemaRelationModel.getSchemaRelations();
        if (MapUtils.isEmpty(schemaRelations) || MapUtils.isEmpty(aliasMapping)) {
            return null;
        }
        // 过滤不存在的模型
        Set<String> schemaCodes = Sets.newHashSetWithExpectedSize(aliasMapping.size());
        aliasMapping.values().stream().filter(StringUtils::isNotBlank).forEach(it -> {
            String[] split = it.split("\\.");
            schemaCodes.add(split[0]);
            if (split.length > 1) {
                schemaCodes.add(split[1]);
            }
        });

        List<BizSchema> bizSchemas = bizSchemaService.findByCodeIn(Lists.newArrayList(schemaCodes), true, false);
        if (CollectionUtils.isEmpty(bizSchemas)) {
            log.error("[relation query] build query object, bizSchemas is null, return.");
            return null;
        }

        List<BizQueryColumn> bizQueryColumnList = bizQueryColumnService.getListByQueryId(queryId, clientType);
        //过滤数据项中不存在的字段
        List<BizQueryColumn> bizQueryColumns = bizQueryColumnService.filterBizQueryColumn(schemaCode, bizQueryColumnList, clientType, aliasMapping);

        if (bizSchemas.size() < schemaCodes.size()) {
            Set<String> bizSchemaCodes = bizSchemas.stream().map(BizSchema::getCode).collect(Collectors.toSet());
            schemaCodes.removeAll(bizSchemaCodes);
            Set<String> notExistAlias = Sets.newHashSetWithExpectedSize(schemaCodes.size());
            log.debug("[relation query] build query object, remove not exist model from schemaRelations, notExistAlias={}.", notExistAlias);
            aliasMapping.forEach((k, v) -> {
                String[] split = v.split("\\.");
                if (!schemaCodes.contains(split[0])) {
                    return;
                }
                if (split.length > 1 && !schemaCodes.contains(split[1])) {
                    return;
                }
                notExistAlias.add(k);
                schemaRelations.remove(v);
            });
            schemaRelations.forEach((k, v) -> {
                List<RelationObject> relationObjects = Lists.newArrayListWithExpectedSize(v.size());
                for (RelationObject r : v) {
                    HashSet<Object> onSchemaAlias = Sets.newHashSetWithExpectedSize(r.getOn().size() * 2);
                    onSchemaAlias.addAll(r.getOn().stream().map(RelationObject.On::getLeftSchemaAlias).collect(Collectors.toSet()));
                    onSchemaAlias.addAll(r.getOn().stream().map(RelationObject.On::getRightSchemaAlias).collect(Collectors.toSet()));
                    if (CollectionUtils.intersection(onSchemaAlias, notExistAlias).size() > 0) {
                        notExistAlias.add(r.getSchemaAlias());
                        continue;
                    }
                    relationObjects.add(r);
                }
                schemaRelations.put(k, relationObjects);
            });
            if (CollectionUtils.isEmpty(schemaRelations.get(schemaCode))) {
                log.error("[relation query] build query object, there is not relation model after remove not exist.");
                return null;
            }
            aliasMapping.keySet().removeIf(notExistAlias::contains);
            bizQueryColumns.removeIf(it -> !aliasMapping.containsKey(it.getSchemaAlias()));
        }


        List<BizQueryColumn> mainQueryColumns = bizQueryColumns.stream().filter(it -> Objects.equals(QueryLevelType.MAIN.getType(), it.getQueryLevel())).collect(Collectors.toList());

        //主表存在待显示的被关联字段
        boolean mainExistRelation = mainQueryColumns.stream().anyMatch(it -> !schemaCode.equals(it.getSchemaCode()));
        //子表存在待显示的被关联字段
        boolean childExistRelation = false;

        //判断子表字段是否选择显示了关联字段
        List<BizQueryColumn> mainChildQueryColumns = mainQueryColumns.stream().filter(it -> BizPropertyType.CHILD_TABLE == it.getPropertyType()).collect(Collectors.toList());
        for (BizQueryColumn childQueryColumn : mainChildQueryColumns) {
            childExistRelation = bizQueryColumns.stream().anyMatch(it ->
                    QueryLevelType.CHILD.getType().equals(it.getQueryLevel()) && childQueryColumn.getPropertyCode().equals(it.getSchemaCode()) && !childQueryColumn.getPropertyAlias().equals(it.getSchemaAlias()));
            if (childExistRelation) {
                break;
            }
        }

        if (!mainExistRelation && !childExistRelation) {
            return null;
        }

        //2***组装关联查询的条件
        QueryRelationObject queryObject = new QueryRelationObject();
        //主模型编码
        String schemaAlias = ModelConstant.MUlIT_MODEL_QUERY_MAIN_DEFAULT_ALIAS;
        queryObject.setSchemaCode(schemaCode);
        queryObject.setSchemaAlias(ModelConstant.MUlIT_MODEL_QUERY_MAIN_DEFAULT_ALIAS);
        //关联模型-关联关系
        queryObject.setSchemaRelationsMap(schemaRelations);
        queryObject.setAliasMapping(aliasMapping);
        //显示字段
        List<String> bizPropertyCodes = getPropertyCodes(properties);
        List<DisplayField> displayFields = Lists.newArrayListWithExpectedSize(bizQueryColumns.size());
        for (BizQueryColumn queryColumn : bizQueryColumns) {
            displayFields.add(DisplayField.builder()
                    .schemaCode(queryColumn.getSchemaCode()).propertyCode(queryColumn.getPropertyCode())
                    .schemaAlias(queryColumn.getSchemaAlias()).propertyAlias(queryColumn.getPropertyAlias())
                    .queryLevel(queryColumn.getQueryLevel())
                    .build());

        }
        handelDisplayField(displayFields, bizPropertyCodes, schemaCode, schemaAlias, bizObjectQueryModel.getOptions());

        queryObject.setDisplayFields(displayFields);
        //主表字段 propertyAlias -> [schemaCode, schemaAlias, propertyCode] 映射关系
        Map<String, List<String>> alias2codeMap = mainQueryColumns.stream().filter(it -> BizPropertyType.CHILD_TABLE != it.getPropertyType())
                .collect(Collectors.toMap(BizQueryColumn::getPropertyAlias,
                        v -> Arrays.asList(v.getSchemaCode(), v.getSchemaAlias(), v.getPropertyCode()), (k1, k2) -> k1));
        //展示字段中没有 查询条件中有的字段 为主模型字段
        alias2codeMap.put(null, Arrays.asList(schemaCode, ModelConstant.MUlIT_MODEL_QUERY_MAIN_DEFAULT_ALIAS, null));

        //排序字段
        queryObject.setOrderByFields(resolveSorts(sorts, schemaCode, queryObject.getSchemaAlias(), alias2codeMap));
        //分页信息
        queryObject.setPageable(bizObjectQueryModel.getPageable());
        //分组字段
//        queryObject.setGroupByFields();

        // 使用树形控件作为查询条件
        List<RelationObject> relationObjects = buildRelationQueryInfoForTree(queryObject.getSchemaAlias(), bizObjectQueryModel.getCodeToRelationSchemaCode(), alias2codeMap);
        if (CollectionUtils.isNotEmpty(relationObjects)) {
            queryObject.getSchemaRelationsMap().get(schemaCode).addAll(relationObjects);
        }
        //查询条件
        QueryRelationFilter queryRelationFilter = resolveFilter(bizObjectQueryModel.getFilterExpr(), alias2codeMap);
        queryObject.setFilterExpression(queryRelationFilter);
        queryObject.setShowTotal(bizObjectQueryModel.getShowTotal());

        return queryObject;
    }

    private List<RelationObject> buildRelationQueryInfoForTree(String mainSchemaAlias, Map<String, String> codeToRelationSchemaCodeMap, Map<String, List<String>> alias2codeMap) {
        if (MapUtils.isEmpty(codeToRelationSchemaCodeMap)) {
            return null;
        }
        List<RelationObject> relationObjects = new ArrayList<>();
        codeToRelationSchemaCodeMap.forEach((k, v) -> {
            String relationSchemaCode = v;
            RelationObject relationObject = new RelationObject();
            relationObject.setSchemaCode(relationSchemaCode);
            relationObject.setSchemaAlias(relationSchemaCode);
            relationObject.setJoinType(RelationObject.JoinType.LEFT);
            relationObject.setQueryLevel(QueryLevelType.MAIN.getType());
            RelationObject.On on = new RelationObject.On();
            on.setLeftSchemaAlias(relationSchemaCode);
            on.setLeftPropertyCode(DefaultPropertyType.ID.getCode());
            on.setRightSchemaAlias(mainSchemaAlias);
            on.setRightPropertyCode(k);
            relationObject.setOn(Collections.singletonList(on));
            relationObjects.add(relationObject);
            alias2codeMap.put(v.concat(".").concat(DefaultTreePropertyType.PATH.getCode()), Arrays.asList(v, v, DefaultTreePropertyType.PATH.getCode()));
        });
        return relationObjects;
    }

    /**
     * 构建多模型查询条件，为空则不是多模型查询 - 使用树形控件作为查询条件时使用
     * @param bizObjectQueryModel 查询信息
     * @param queryId             查询条件id
     * @param clientType          客户端
     * @param properties          数据项
     * @return QueryRelationObject
     **/
    private QueryRelationObject buildRelationQueryObjectForTree(BizObjectQueryModel bizObjectQueryModel, String queryId, ClientType clientType,
                                                                List<BizQuerySort> sorts, List<BizProperty> properties) {

        /* 1.拦截校验 */
        if (MapUtils.isEmpty(bizObjectQueryModel.getCodeToRelationSchemaCode())) {
            return null;
        }

        /* 2.构建多模型查询信息 */
        String schemaCode = bizObjectQueryModel.getSchemaCode();
        QueryRelationObject queryObject = new QueryRelationObject();
        //主模型编码
        String schemaAlias = ModelConstant.MUlIT_MODEL_QUERY_MAIN_DEFAULT_ALIAS;
        queryObject.setSchemaCode(schemaCode);
        queryObject.setSchemaAlias(schemaAlias);
        queryObject.getAliasMapping().put(schemaAlias, schemaCode);

        List<BizQueryColumn> queryColumns = bizQueryColumnService.getListByQueryId(queryId, clientType);
        Map<String, List<BizQueryColumn>> queryColumnMap = queryColumns.stream().filter(it -> !BooleanUtil.isTrue(it.getDeleted())).collect(Collectors.groupingBy(BizQueryColumn::getSchemaCode));

        //显示字段 - 主表  移除主表查询信息
        List<DisplayField> displayFields = new ArrayList<>();
        List<String> bizPropertyCodes = getPropertyCodes(properties);
        List<BizQueryColumn> mainQueryColumns = queryColumnMap.remove(schemaCode);
        if (CollectionUtils.isEmpty(mainQueryColumns)) {
            return null;
        }
        mainQueryColumns.stream().filter(it -> bizPropertyCodes.contains(it.getPropertyCode())).forEach(it -> {
            displayFields.add(DisplayField.builder()
                    .schemaCode(schemaCode).schemaAlias(schemaAlias).propertyCode(it.getPropertyCode()).queryLevel(QueryLevelType.MAIN.getType())
                    .build());
        });
        handelDisplayField(displayFields, bizPropertyCodes, schemaCode, schemaAlias, bizObjectQueryModel.getOptions());

        //显示字段 - 子表
        List<DisplayField> childDisplays = buildChildDisplayField(queryColumnMap, properties);
        if (CollectionUtils.isNotEmpty(childDisplays)) {
            displayFields.addAll(childDisplays);
        }
        queryObject.setDisplayFields(displayFields);
        Map<String, List<String>> alias2codeMap = new HashMap<>();
        List<RelationObject> relationObjects = buildRelationQueryInfoForTree(schemaAlias, bizObjectQueryModel.getCodeToRelationSchemaCode(), alias2codeMap);
        //构建查询条件
        alias2codeMap.put(null, Arrays.asList(schemaCode, schemaAlias, null));
        QueryRelationFilter queryRelationFilter = resolveFilter(bizObjectQueryModel.getFilterExpr(), alias2codeMap);
        queryObject.setFilterExpression(queryRelationFilter);

        //构建连表查询关联关系
        queryObject.getSchemaRelationsMap().put(schemaCode, relationObjects);
        //排序字段
        queryObject.setOrderByFields(resolveSorts(sorts, schemaCode, schemaAlias, alias2codeMap));
        //分组字段
//        queryObject.setGroupByFields();
        //分页信息
        queryObject.setPageable(bizObjectQueryModel.getPageable());
        queryObject.setShowTotal(bizObjectQueryModel.getShowTotal());
        return queryObject;
    }

    private void handelDisplayField(List<DisplayField> displayFields, List<String> bizPropertyCodes, String schemaCode, String schemaAlias, BizObjectQueryModel.Options options) {
        if (options == null) {
            return;
        }
        if (options.getQueryDisplayType() == QueryDisplayType.APPEND) {
            List<String> customDisplayColumns = options.getCustomDisplayColumns();
            if (CollectionUtils.isEmpty(customDisplayColumns)) {
                return;
            }
            List<String> displayPropertyCodes = displayFields.stream().map(DisplayField::getPropertyCode).collect(Collectors.toList());
            for (String displayColumn: customDisplayColumns) {
                if (bizPropertyCodes.contains(displayColumn) && !displayPropertyCodes.contains(displayColumn)) {
                    displayFields.add(DisplayField.builder()
                            .schemaCode(schemaCode).schemaAlias(schemaAlias).propertyCode(displayColumn).queryLevel(QueryLevelType.MAIN.getType())
                            .build());
                }
            }
        }
    }

    private List<DisplayField> buildChildDisplayField(Map<String, List<BizQueryColumn>> queryColumnMap, List<BizProperty> properties) {

        if (MapUtils.isEmpty(queryColumnMap)) {
            return null;
        }
        List<String> childSchemaCodes = properties.stream().filter(it -> BizPropertyType.CHILD_TABLE == it.getPropertyType()).map(BizProperty::getCode).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(childSchemaCodes)) {
            return null;
        }
        Map<String, List<BizProperty>> childPropertyMap = bizPropertyService.findMapBySchemaCodeIn(childSchemaCodes, true, false);
        if (MapUtils.isEmpty(childPropertyMap)) {
            return null;
        }
        List<DisplayField> displayFields = new ArrayList<>();
        queryColumnMap.forEach((k, v) -> {
            List<BizProperty> bizProperties = childPropertyMap.get(k);
            if (CollectionUtils.isEmpty(bizProperties)) {
                return;
            }
            List<String> propertyCodes = getPropertyCodes(bizProperties);
            v.stream().filter(c -> propertyCodes.contains(c.getPropertyCode())).forEach(c -> displayFields.add(DisplayField.builder()
                    .schemaCode(c.getSchemaCode()).propertyCode(c.getPropertyCode()).queryLevel(QueryLevelType.CHILD.getType())
                    .build()));
        });
        return displayFields;
    }

    private List<String> getPropertyCodes(List<BizProperty> properties) {
        return properties.stream().map(BizProperty::getCode).collect(Collectors.toList());
    }

    /**
     * 关联查询-排序条件
     * @param sorts       已组装好的查询条件
     * @param schemaCode  主模型编码
     * @param schemaAlias 主模型别名
     * @return List
     **/
    private List<OrderByField> resolveSorts(List<BizQuerySort> sorts, String schemaCode, String schemaAlias, Map<String, List<String>> alias2codeMap) {
        List<OrderByField> sortList = new ArrayList<>();
        if (CollectionUtils.isEmpty(sorts)) {
            sortList.add(new OrderByField(schemaCode, schemaAlias, DefaultPropertyType.CREATED_TIME.getCode(), Order.Dir.DESC));
        } else {
            sorts.forEach(it -> {
                List<String> info = alias2codeMap.get(it.getSchemaCode());
                sortList.add(new OrderByField(it.getSchemaCode(), (info == null ? schemaAlias : info.get(1)), it.getPropertyCode(),
                        SortDirectionType.ASC == it.getDirection() ? Order.Dir.ASC : Order.Dir.DESC));
            });
        }
        return sortList;
    }

    /**
     * 关联查询-过滤条件 从组装好的查询条件转换为关联查询条件
     * @param filter 已组装好的查询条件
     * @return QueryRelationFilter
     **/
    private QueryRelationFilter resolveFilter(FilterExpression filter, Map<String, List<String>> alias2codeMap) {
        if (filter == null) {
            return null;
        }
        if (filter instanceof FilterExpression.Item) {
            return resolveFilter((FilterExpression.Item) filter, alias2codeMap);
        } else if (filter instanceof FilterExpression.Or) {
            return resolveFilter((FilterExpression.Or) filter, alias2codeMap);
        } else if (filter instanceof FilterExpression.And) {
            return resolveFilter((FilterExpression.And) filter, alias2codeMap);
        } else {
            return null;
        }
    }

    private QueryRelationFilter resolveFilter(FilterExpression.And and, Map<String, List<String>> alias2codeMap) {
        List<QueryRelationFilter> result = new ArrayList<>();
        for (FilterExpression item : and.items) {
            if (item instanceof FilterExpression.Item) {
                result.add(resolveFilter((FilterExpression.Item) item, alias2codeMap));
            } else if (item instanceof FilterExpression.Or) {
                result.add(resolveFilter((FilterExpression.Or) item, alias2codeMap));
            } else if (item instanceof FilterExpression.And) {
                result.add(resolveFilter((FilterExpression.And) item, alias2codeMap));
            }
        }
        return new QueryRelationFilter.And(result);
    }

    private QueryRelationFilter resolveFilter(FilterExpression.Or or, Map<String, List<String>> alias2codeMap) {
        List<QueryRelationFilter> result = new ArrayList<>();
        for (FilterExpression item : or.items) {
            if (item instanceof FilterExpression.Item) {
                result.add(resolveFilter((FilterExpression.Item) item, alias2codeMap));
            } else if (item instanceof FilterExpression.Or) {
                result.add(resolveFilter((FilterExpression.Or) item, alias2codeMap));
            } else if (item instanceof FilterExpression.And) {
                result.add(resolveFilter((FilterExpression.And) item, alias2codeMap));
            }
        }
        return new QueryRelationFilter.Or(result);
    }

    private QueryRelationFilter resolveFilter(FilterExpression.Item item, Map<String, List<String>> alias2codeMap) {
        List<String> info = alias2codeMap.get(item.field);
        if (info == null) {
            info = alias2codeMap.get(null);
        }
        String schemaCode = info.get(0);
        String schemaAlias = info.get(1);
        String propertyCode = info.get(2) == null ? item.field : info.get(2);
        return new QueryRelationFilter.Item(schemaCode, schemaAlias, propertyCode, item.op, item.value);
    }

    @Override
    public Map<Boolean, Set<String>> checkDataExist(String schemaCode, List<String> ids) {
        if (StringUtils.isEmpty(schemaCode) || CollectionUtils.isEmpty(ids)) {
            return Collections.emptyMap();
        }
        return bizObjectConverter.checkDataExist(schemaCode, ids);
    }

    @Override
    public List<Map<String, Object>> batchCheckTreeCircularRef(List<Map<String, Object>> data, String schemaCode) {
        return bizObjectConverter.batchCheckTreeCircularRef(data, schemaCode);
    }

    @Override
    public List<String> getAllTreeParentIds(String schemaCode, String objectId) {
        return bizObjectConverter.getAllTreeParentIds(schemaCode, objectId);
    }

    @Override
    public boolean compareObject(Object oldValue, Object newValue, BizPropertyType propertyType) {
        return bizObjectService.compareObject(oldValue, newValue, propertyType);
    }

    @Override
    public Page<BizObjectModel> queryBizObjects(String userId, QueryDataModel queryData, Boolean isExport, Boolean isAuthFilter, Boolean isOwner) {
        boolean isMobile = queryData.getMobile();
        ClientType clientType = isMobile ? ClientType.APP : ClientType.PC;
        if (StringUtils.isBlank(queryData.getQueryCode())) {
            queryData.setQueryCode(getQueryCodeFromQuery(queryData.getSchemaCode(), isMobile));
        }
        String schemaCode = queryData.getSchemaCode();
        BizQueryModel bizQueryModel = bizQueryFacade.getBizQuery(schemaCode, queryData.getQueryCode(), clientType, null);
        BizObjectQueryModel bizObjectQueryModel = getBizObjectQuery(userId, queryData, bizQueryModel, isAuthFilter, isOwner);

        //分页信息
        int page = queryData.getPage() == null ? 0 : queryData.getPage();
        int size = queryData.getSize();
        PageableImpl pageable = new PageableImpl(isExport ? 0 : page * size, size);
        bizObjectQueryModel.setPageable(pageable);
        bizObjectQueryModel.setSchemaCode(queryData.getSchemaCode());
        bizObjectQueryModel.setQueryCode(queryData.getQueryCode());
        bizObjectQueryModel.setOptions(queryData.getOptions());
        bizObjectQueryModel.setClientType(clientType);
        bizObjectQueryModel.setDisplay(queryData.getDisplay());
        bizObjectQueryModel.setOrderByFields(queryData.getOrderByFields());
        bizObjectQueryModel.setOrderType(queryData.getOrderType());
        bizObjectQueryModel.setView(queryData.getView());
        bizObjectQueryModel.setCodeToRelationSchemaCode(queryData.getCodeToRelationSchemaCode());
        bizObjectQueryModel.setChildPageEnable(queryData.getChildPageEnable());

        BizObjectQueryModel bizObjectQuery = parseQueries(bizObjectQueryModel, schemaCode);

        //关联表单展示字段
        QueryDataModel.ReverseSchemaParam param = queryData.getReverseSchemaParam();
        if (param != null && queryData.getReverseViewFlag()) {
            BizObjectQueryModel.Options options = bizObjectQuery.getOptions();
            if (options != null && options.getQueryDisplayType() == QueryDisplayType.APPEND) {
                List<String> addColumnList = getRelevanceFormConfigColumn(param.getReverseSchemaCode(), param.getReverseSheetCode(), param.getReverseRelevanceFormCode());
                List<String> customDisplayColumns = bizObjectQuery.getOptions().getCustomDisplayColumns();
                if (CollectionUtils.isNotEmpty(customDisplayColumns)) {
                    customDisplayColumns.addAll(addColumnList);
                } else {
                    bizObjectQuery.getOptions().setCustomDisplayColumns(addColumnList);
                }
            }

        }
        //甘特图必查字段处理
        List<String> addGanttColums = buildGanttColums(bizQueryModel);
        bizObjectQuery.setGanttColumns(addGanttColums);

        Page<BizObjectModel> data = this.queryBizObjectsFilter(bizObjectQuery, page, size);
        if (CollectionUtils.isNotEmpty(data.getContent())) {
            List<BizQueryColumnModel> bizQueryColumnModels = bizQueryModel.getQueryColumns();
            parseAddressAndReference(data, bizQueryColumnModels, isExport, queryData.getQueryCode());
            if (isMobile) {
                //处理移动端用户图像
                disposeUserInfo(data);
            } else {
                //处理PC端选人
                parseSpecialProperties(data, isExport);
            }
            disposeGanttModel(data, bizQueryModel, bizObjectQuery.getView().isTreeModel());
        }
        //甘特图视图普通模型层级关系处理
        return data;
    }

    @Override
    public Page<BizObjectModel> queryBizObjectsForRelation(String userId, QueryDataModel queryData, Boolean isExport, Boolean isAuthFilter, Boolean isOwner) {
        Boolean isMobile = Objects.nonNull(queryData.getMobile()) ? queryData.getMobile() : Boolean.FALSE;
        ClientType clientType = isMobile ? ClientType.APP : ClientType.PC;
        if (StringUtils.isEmpty(queryData.getQueryCode())) {
            queryData.setQueryCode(getQueryCodeFromQuery(queryData.getSchemaCode(), isMobile));
        }
        BizQueryModel bizQueryModel = bizQueryFacade.getBizQuery(queryData.getSchemaCode(), queryData.getQueryCode(), clientType, null);
        //生成查询条件方法
        BizObjectQueryModel bizObjectQueryModel = getBizObjectQueryForRelation(userId, queryData, bizQueryModel, isAuthFilter, isOwner);
        //分页信息
        int page = queryData.getPage() == null ? 0 : queryData.getPage();
        int size = queryData.getSize();
        PageableImpl pageable = null;
        if (isExport) {
            pageable = new PageableImpl(0, size);
        } else {
            pageable = new PageableImpl(page * size, size);
        }

        bizObjectQueryModel.setPageable(pageable);
        bizObjectQueryModel.setSchemaCode(queryData.getSchemaCode());
        bizObjectQueryModel.setQueryCode(queryData.getQueryCode());
        bizObjectQueryModel.setOptions(queryData.getOptions());
        bizObjectQueryModel.setClientType(clientType);

        bizObjectQueryModel.setDisplay(queryData.getDisplay());
        Page<BizObjectModel> data = this.queryBizObjects(bizObjectQueryModel);
        if (CollectionUtils.isNotEmpty(data.getContent())) {
            List<BizQueryColumnModel> bizQueryColumnModels = bizQueryModel.getQueryColumns();
            parseAddressAndReference(data, bizQueryColumnModels, isExport, queryData.getQueryCode());
            if (isMobile) {
                //处理移动端用户图像
                disposeUserInfo(data);
            } else {
                //处理PC端选人
                parseSpecialProperties(data, isExport);
            }
        }
        return data;
    }

    @Override
    public String getQueryCodeFromQuery(String schemaCode, boolean isMobile) {
        List<BizQueryHeaderModel> bizQueryHeaders = Lists.newArrayList();
        //如果列表编码为空则获取默认第一个列表
        List<BizQueryHeaderModel> bizQueryHeadersTmp = bizQueryFacade.getBizQueryHeaders(schemaCode);
        List<BizQueryHeaderModel> bizQueryHeadersForApp = bizQueryHeadersTmp.stream().filter(t -> Objects.equals(t.getShowOnMobile(), Boolean.TRUE)).collect(Collectors.toList());
        List<BizQueryHeaderModel> bizQueryHeadersForPC = bizQueryHeadersTmp.stream().filter(t -> Objects.equals(t.getShowOnPc(), Boolean.TRUE)).collect(Collectors.toList());
        if (isMobile) {
            //处理历史数据
            if (CollectionUtils.isNotEmpty(bizQueryHeadersForApp)) {
                bizQueryHeaders.addAll(bizQueryHeadersForApp);
            }
        } else {
            bizQueryHeaders.addAll(bizQueryHeadersForPC);
        }
        return CollectionUtils.isNotEmpty(bizQueryHeaders) ? bizQueryHeaders.get(0).getCode() : bizQueryHeadersTmp.get(0).getCode();
    }

    public List<String> buildGanttColums(BizQueryModel bizQueryModel) {
        //甘特图必查字段校验
        List<String> addGanttColums = new ArrayList<>();
        if (bizQueryModel.getQueryPresentationType() == QueryPresentationType.GANTT && bizQueryModel.getQueryGanttModel() != null) {
            BizQueryGanttModel bizQueryGanttModel = bizQueryModel.getQueryGanttModel();
            boolean containStartTime = bizQueryModel.getQueryColumns().stream().anyMatch(colums -> colums.getPropertyCode().equals(bizQueryGanttModel.getStartTimePropertyCode()));
            boolean containEndTime = bizQueryModel.getQueryColumns().stream().anyMatch(colums -> colums.getPropertyCode().equals(bizQueryGanttModel.getEndTimePropertyCode()));
            boolean containProgress = bizQueryModel.getQueryColumns().stream().anyMatch(colums -> colums.getPropertyCode().equals(bizQueryGanttModel.getProgressPropertyCode()));
            boolean containLevel = bizQueryModel.getQueryColumns().stream().anyMatch(colums -> colums.getPropertyCode().equals(bizQueryGanttModel.getLevelPropertyCode()));
            boolean containPreDependency = bizQueryModel.getQueryColumns().stream().anyMatch(colums -> colums.getPropertyCode().equals(bizQueryGanttModel.getPreDependencyPropertyCode()));
            boolean containsMilepostCode = bizQueryModel.getQueryColumns().stream().anyMatch(colums -> colums.getPropertyCode().equals(bizQueryGanttModel.getMilepostCode()));
            if (!containStartTime && StringUtils.isNotEmpty(bizQueryGanttModel.getStartTimePropertyCode())) {
                addGanttColums.add(bizQueryGanttModel.getStartTimePropertyCode());
            }
            if (!containEndTime && StringUtils.isNotEmpty(bizQueryGanttModel.getEndTimePropertyCode())) {
                addGanttColums.add(bizQueryGanttModel.getEndTimePropertyCode());
            }
            if (!containProgress && StringUtils.isNotEmpty(bizQueryGanttModel.getProgressPropertyCode())) {
                addGanttColums.add(bizQueryGanttModel.getProgressPropertyCode());
            }
            if (!containLevel && StringUtils.isNotEmpty(bizQueryGanttModel.getLevelPropertyCode())) {
                addGanttColums.add(bizQueryGanttModel.getLevelPropertyCode());
            }
            if (!containPreDependency && StringUtils.isNotEmpty(bizQueryGanttModel.getPreDependencyPropertyCode())) {
                addGanttColums.add(bizQueryGanttModel.getPreDependencyPropertyCode());
            }

            if (!containsMilepostCode && StringUtils.isNotEmpty(bizQueryGanttModel.getMilepostCode())) {
                addGanttColums.add(bizQueryGanttModel.getMilepostCode());
            }
        }
        return addGanttColums;
    }

    @Override
    public BizObjectQueryModel getBizObjectQuery(String userId, QueryDataModel queryDataVO, BizQueryModel bizQueryModel, Boolean isAuthFilter, Boolean isOwner, boolean isRelation) {
//        if (isRelation) {
//            return this.getBizObjectQueryForRelation(userId, queryDataVO, bizQueryModel, isAuthFilter, isOwner);
//        } else {
//        }
        return this.getBizObjectQuery(userId, queryDataVO, bizQueryModel, isAuthFilter, isOwner);

    }

    private BizObjectQueryModel getBizObjectQuery(String userId, QueryDataModel queryDataVO, BizQueryModel bizQueryModel, Boolean isAuthFilter, Boolean isOwner) {

        //权限条件
        FilterExpression authFilter = isAuthFilter ? getAuthFilter(queryDataVO, userId, isOwner) : FilterExpression.empty;
        if (log.isDebugEnabled()) {
            log.debug(JSON.toJSONString(authFilter));
        }

        //生成查询条件
        FilterExpression filterExprs = getFiltersByQueryParam(queryDataVO, null, bizQueryModel.getQueryConditions());
        //生成选中的列表的查询条件
        FilterExpression checkedFilter = getCheckedFilters(queryDataVO.getObjectIds());
        //如果选中的ID不为空，加入到查询条件
        filterExprs = splicingFilterExpression(AND, filterExprs, checkedFilter);
        //自己草稿的数据
        FilterExpression statusFilters = this.filterSeqStatus(userId);
        //非草稿数据
        FilterExpression notDrafFilters = this.filterNotDraftSeqStatus();
        //模型不需要过滤草稿
        if (notFilterDraftSet.contains(queryDataVO.getSchemaCode())) {
            statusFilters = FilterExpression.empty;
            notDrafFilters = FilterExpression.empty;
        }
        //权限条件 = (权限 and 非草稿)
        authFilter = splicingFilterExpression(AND, authFilter, notDrafFilters);
        //我的草稿与权限条件拼成OR条件
        FilterExpression draftOrAuth = splicingFilterExpression(OR, authFilter, statusFilters);
        //最终查询条件（查询条件 and (权限条件 or 自己的草稿)
        BizObjectQueryModel bizObjectQueryObject = new BizObjectQueryModel();
        bizObjectQueryObject.setFilterExpr(splicingFilterExpression(AND, filterExprs, draftOrAuth));
        bizObjectQueryObject.setFilterExcludeAuth(splicingFilterExpression(AND, filterExprs, splicingFilterExpression(OR, statusFilters, notDrafFilters)));
        bizObjectQueryObject.setAuthFilter(authFilter);
        if (log.isDebugEnabled()) {
            log.debug("列表查询条件为{}", JSONUtil.toJsonStr(bizObjectQueryObject.getFilterExpr()));
        }
        //设置是否显示总数
        bizObjectQueryObject.setShowTotal(getShowTotal(queryDataVO, bizQueryModel));
        bizObjectQueryObject.setView(queryDataVO.getView());
        bizObjectQueryObject.setSchemaCode(bizQueryModel.getSchemaCode());
        bizObjectQueryObject.setClientType(Objects.nonNull(queryDataVO.getMobile()) && queryDataVO.getMobile() ? ClientType.APP : ClientType.PC);
        bizObjectQueryObject.setCodeToRelationSchemaCode(queryDataVO.getCodeToRelationSchemaCode());
        return bizObjectQueryObject;
    }

    private Boolean getShowTotal(QueryDataModel queryDataVO, BizQueryModel bizQueryModel) {
        if (queryDataVO.getShowTotal() != null) {
            return queryDataVO.getShowTotal();
        }
        String options = bizQueryModel.getOptions();
        if (StringUtils.isEmpty(options)) {
            return BizQueryOption.defaultOption().getPagingSetting().getShowTotal();
        }
        BizQueryOption bizQueryOption = ObjectMapperUtils.fromJSON(options, BizQueryOption.class);
        return bizQueryOption.getPagingSetting().getShowTotal();
    }

    /**
     * 拼接查询条件
     * @param op
     * @param first
     * @param second
     * @return
     */
    private FilterExpression splicingFilterExpression(String op, FilterExpression first, FilterExpression second) {
        //如果两个条件都是空，
        if (FilterExpression.empty.equals(first) && FilterExpression.empty.equals(second)) {
            return FilterExpression.empty;
        }
        //如果两个条件都不为空
        if (!FilterExpression.empty.equals(first) && !FilterExpression.empty.equals(second)) {
            return OR.equalsIgnoreCase(op) ? Q.or(first, second) : Q.and(first, second);
        }
        //其中一个为空，则返回不为空的
        return FilterExpression.empty.equals(first) ? second : first;
    }

    private FilterExpression getAuthFilter(QueryDataModel queryDataVO, String userId, boolean isOwner) {
        FilterExpression authFilter;
        //是否临时授权
        if (StringUtils.isNotEmpty(queryDataVO.getTempAuthObjectId()) || StringUtils.isNotEmpty(queryDataVO.getTempAuthPropertyCode())) {
            BizFormModel sourceSheet = this.getTempAuthSchemaCodes(queryDataVO.getTempAuthSheetId());
            if (sourceSheet == null) {
                authFilter = permissionManagementFacade.getQueryFilterExpression(userId, queryDataVO.getSchemaCode(), isOwner);
            } else {
                //跳转过来的源表单的临时授权当前访问的表单  与前端约定保存的表单数据为 SchemaCode_SheetCode
                if (sourceSheet.getTempAuthSchemaCodes().contains(queryDataVO.getSchemaCode() + "_")) {
                    authFilter = permissionManagementFacade.getQueryFilterExpression(userId, sourceSheet.getSchemaCode(), isOwner);
                } else {
                    authFilter = permissionManagementFacade.getQueryFilterExpression(userId, queryDataVO.getSchemaCode(), isOwner);
                }
            }
        } else {
            authFilter = permissionManagementFacade.getQueryFilterExpression(userId, queryDataVO.getSchemaCode(), isOwner);
        }
        return authFilter;
    }

    /**
     * 获取关联查询/反关联查询条件对象 TODO 统一入口
     * @param userId        用户id
     * @param queryDataVO   查询数据结果
     * @param bizQueryModel 列表详细信息
     * @return BizObjectQueryModel
     */
    private BizObjectQueryModel getBizObjectQueryForRelation(String userId, QueryDataModel queryDataVO, BizQueryModel bizQueryModel, Boolean isAuthFilter, Boolean isOwner) {
        List<FilterExpression> filterExpressions = new ArrayList<>();
        //权限条件
        FilterExpression authFilter = isAuthFilter ? getAuthFilter(queryDataVO, userId, isOwner) : FilterExpression.empty;
        //生成查询条件
        FilterExpression finalFilterExpr = getFiltersByQueryParam(queryDataVO, null, bizQueryModel.getQueryConditions());

        //生成选中的列表的查询条件
        FilterExpression checkedFilter = getCheckedFilters(queryDataVO.getObjectIds());
        if (!authFilter.equals(FilterExpression.empty)) {
            filterExpressions.add(authFilter);
        }
        if (!finalFilterExpr.equals(FilterExpression.empty)) {
            filterExpressions.add(finalFilterExpr);
        }
        if (!checkedFilter.equals(FilterExpression.empty)) {
            filterExpressions.add(checkedFilter);
        }

        FilterExpression seqStatus = Q.it(DefaultPropertyType.SEQUENCE_STATUS.getCode(), FilterExpression.Op.NotEq, WorkflowInstanceStatus.DRAFT.toString());
        filterExpressions.add(seqStatus);

        BizObjectQueryModel bizObjectQueryObject = new BizObjectQueryModel();
        FilterExpression filterExprTemplate;
        if (filterExpressions.size() > 1) {
            filterExprTemplate = Q.and(filterExpressions);
        } else {
            filterExprTemplate = filterExpressions.get(0);
        }
        if (log.isDebugEnabled()) {
            log.debug("获取的权限相关条件为{}", JSON.toJSONString(filterExprTemplate));
        }
        bizObjectQueryObject.setFilterExpr(filterExprTemplate);
        bizObjectQueryObject.setSchemaCode(bizQueryModel.getSchemaCode());
        bizObjectQueryObject.setClientType(Objects.nonNull(queryDataVO.getMobile()) && queryDataVO.getMobile() ? ClientType.APP : ClientType.PC);
        return bizObjectQueryObject;
    }

    @Override
    public Page<BizObjectModel> queryExportData(String userId, QueryDataModel queryData, Boolean isExport, Boolean isAuthFilter, List<BizPropertyModel> properties,
                                                List<BizQueryColumnModel> columns, Map<String, Map<String, Object>> childNumberFormat) {
        long start = System.currentTimeMillis();
        Boolean isMobile = Objects.nonNull(queryData.getMobile()) ? queryData.getMobile() : Boolean.FALSE;
        ClientType clientType = isMobile ? ClientType.APP : ClientType.PC;
        if (StringUtils.isEmpty(queryData.getQueryCode())) {
            queryData.setQueryCode(getQueryCodeFromQuery(queryData.getSchemaCode(), isMobile));
        }
        BizQueryModel bizQueryModel = bizQueryFacade.getBizQuery(queryData.getSchemaCode(), queryData.getQueryCode(), clientType, null);

        //分页信息
        int page = queryData.getPage() == null ? 0 : queryData.getPage();
        int size = queryData.getSize();
        PageableImpl pageable = new PageableImpl(page * size, size);
        //生成查询条件方法
        BizObjectQueryModel bizObjectQueryModel = this.getBizObjectQuery(userId, queryData, bizQueryModel, isAuthFilter, false);
        bizObjectQueryModel.setPageable(pageable);
        bizObjectQueryModel.setSchemaCode(queryData.getSchemaCode());
        bizObjectQueryModel.setQueryCode(queryData.getQueryCode());
        bizObjectQueryModel.setOptions(queryData.getOptions());
        bizObjectQueryModel.setClientType(clientType);
        Page<BizObjectModel> data = this.queryExportDatas(bizObjectQueryModel, properties, queryData.getMobile(), columns, userId, childNumberFormat);
        long end = System.currentTimeMillis();
        log.info("查询：--{}", end - start);
        if (CollectionUtils.isNotEmpty(data.getContent())) {
            List<BizQueryColumnModel> bizQueryColumnModels = bizQueryModel.getQueryColumns();
            if (isMobile) {
                //处理移动端用户图像
                disposeUserInfo(data);
            } else {
                //处理PC端选人
                parseSpecialProperties(data, false);
            }
        }
        long masche = System.currentTimeMillis();
        log.info("处理；--{}", masche - end);
        return data;
    }

    @Override
    public FileOperationResult secondImportData(FileOperationResult operationResult) {
        if (operationResult == null) {
            throw new ServiceException(ErrCode.BIZ_QUERY_DATA_EMPTY);
        }
        if (CollectionUtils.isEmpty(operationResult.getHeadColumns()) || CollectionUtils.isEmpty(operationResult.getSecondImportData())) {
            throw new ServiceException(ErrCode.BIZ_QUERY_DATA_EMPTY);
        }
//        UserModel user = organizationFacade.getUser(operationResult.getUserId());
        List<BizQueryColumnModel> headColumns = operationResult.getHeadColumns();
        List<Map<String, Object>> secondImportData = operationResult.getSecondImportData();
        //在内存中存入子表code和properties的关系，减少查询次数--Map<String,Object>
        //在内存中存入code和headColumn的关系
        Map<String, List<BizQueryChildColumnModel>> childMap = Maps.newHashMap();
//        Map<String, BizQueryColumnModel> mainMap = Maps.newHashMap();
        for (BizQueryColumnModel columnModel : headColumns) {
            if (BizPropertyType.CHILD_TABLE == columnModel.getPropertyType()) {
                List<BizQueryChildColumnModel> childColumns = columnModel.getChildColumns();
                childMap.put(columnModel.getPropertyCode(), childColumns);
            } else {
//                mainMap.put(columnModel.getPropertyCode(), columnModel);
            }
        }
        String schemaCode = headColumns.get(0).getSchemaCode();
        String workflowCode = obtainWorkflowCodeFromBizQuery(schemaCode, operationResult.getQueryCode());
        BizSchemaModel bizSchemaModel = bizSchemaFacade.getBySchemaCodeWithProperty(schemaCode, true);
        List<BizPropertyModel> properties = bizSchemaModel.getProperties();
        List<BizObjectModel> bizObjectModels = Lists.newArrayListWithExpectedSize(secondImportData.size());
        Map<String, List<String>> organizationIdMap = new HashMap<>();
        organizationIdMap.put("userIdList", new ArrayList<>());
        organizationIdMap.put("errorUserIdList", new ArrayList<>());
        organizationIdMap.put("deptIdList", new ArrayList<>());
        organizationIdMap.put("errorDeptIdList", new ArrayList<>());
        for (Map<String, Object> map : secondImportData) {
            if (MapUtils.isEmpty(map)) {
                continue;
            }
            for (BizQueryColumnModel columnModel : headColumns) {
                BizPropertyModel bizProperty = properties.stream().filter(t -> t.getCode().equals(columnModel.getPropertyCode())).findAny().orElse(null);
                if (bizProperty == null) {
                    continue;
                }
                BizPropertyType propertyType = bizProperty.getPropertyType();
                if (!BizPropertyType.selectionTypes.contains(propertyType) && BizPropertyType.CHILD_TABLE != propertyType) {
                    continue;
                }
                if (BizPropertyType.selectionTypes.contains(propertyType)) {
                    boolean flag = verifySelections(map, bizProperty, organizationIdMap);
                    if (!flag) {
                        operationResult.setErrCode(ErrCode.BIZ_QUERY_DATA_VALID_FAIL);
                        return operationResult;
                    }
                    continue;
                }
                String code = bizProperty.getCode();
                if (MapUtils.isEmpty(childMap) || CollectionUtils.isEmpty(childMap.get(code))) {
                    continue;
                }
                Object childValues = map.get(code);
                if (childValues == null) {
                    continue;
                }
                List<Map<String, Object>> childDataMapList = (List<Map<String, Object>>) childValues;
                List<Map<String, Object>> childList = Lists.newArrayListWithExpectedSize(childDataMapList.size());
                List<BizQueryChildColumnModel> childColumnModels = childMap.get(code);
                for (Map<String, Object> childDataMap : childDataMapList) {
                    for (BizQueryChildColumnModel bizQueryChildColumnModel : childColumnModels) {
                        String childPropertyCode = bizQueryChildColumnModel.getPropertyCode();
                        BizSchemaModel bizSchemaBySchemaCode = bizSchemaFacade.getBySchemaCodeWithProperty(code, true);
                        List<BizPropertyModel> childPropertyList = bizSchemaBySchemaCode.getProperties();
                        BizPropertyModel propertyModel = childPropertyList.stream().filter(item -> childPropertyCode.equals(item.getCode())).findAny().orElse(null);
                        if (propertyModel == null) {
                            continue;
                        }
                        if (BizPropertyType.selectionTypes.contains(bizQueryChildColumnModel.getPropertyType())) {
                            boolean flag = this.verifySelections(childDataMap, propertyModel, organizationIdMap);
                            if (!flag) {
                                operationResult.setErrCode(ErrCode.BIZ_QUERY_DATA_VALID_FAIL);
                                return operationResult;
                            }
                        }
                    }
                    childList.add(childDataMap);
                }
                map.put(code, childList);
            }
            Map<String, Object> importData = Maps.newHashMap();
            importData.putAll(map);
            Object owner = importData.get("owner");
            if (Objects.nonNull(owner)) {
                List<SelectionValue> owner1 = (List<SelectionValue>) owner;
                SelectionValue selectionValue = owner1.get(0);
                if (selectionValue != null && StringUtils.isNotEmpty(selectionValue.getId())) {
                    String id = selectionValue.getId();
                    importData.put("owner", id);
                }
            }
            BizObjectModel bizObject = new BizObjectModel(schemaCode, importData, false);
            bizObject.setSequenceStatus(SequenceStatus.COMPLETED.toString());
            if (StringUtils.isBlank(bizObject.getId())) {
                bizObject.setId(genId());
            }
            //流程发起权限校验
            boolean hasWorkflowPerm = true;
            String ownerId = StringUtils.isBlank(bizObject.getOwnerId()) ? operationResult.getUserId() : bizObject.getOwnerId();
            if (StringUtils.isNotBlank(workflowCode)) {
                hasWorkflowPerm = checkWorkflowPermission(ownerId, workflowCode);
            }

            if (hasWorkflowPerm) {
                bizObjectModels.add(bizObject);
            } else {
                log.debug("{}没有发起流程的权限", ownerId);
            }
        }

        if (CollectionUtils.isNotEmpty(bizObjectModels)) {
            if (StringUtils.isNotBlank(workflowCode)) {
                startWorkflow(bizObjectModels, workflowCode, operationResult.getUserId(), operationResult.getClient(), operationResult.getClientIp());
            } else {
                String queryField = operationResult.getQueryField();
                operationResult.setSuccessCount(bizObjectModels.size());
                log.info("==================批量更新========成功导入={}条,bizObjects={}", bizObjectModels.size(), JSONObject.toJSONString(bizObjectModels));
                this.addBizObjects(exeRuleFlag, operationResult.getUserId(), bizObjectModels, queryField);
            }
        } else {
            operationResult.setErrCode(ErrCode.BIZ_QUERY_DATA_VALID_FAIL);
        }
        return operationResult;
    }

    /**
     * 选中数据导出
     */
    private FilterExpression getCheckedFilters(List<String> objectIds) {
        FilterExpression checkedFilter = FilterExpression.empty;
        if (CollectionUtils.isEmpty(objectIds)) {
            return checkedFilter;
        }
        checkedFilter = Q.it(DefaultPropertyType.ID.getCode(), FilterExpression.Op.In, objectIds);
        return checkedFilter;
    }


    /**
     * 查看自己的草稿
     * @param userId 用户id
     */
    private FilterExpression filterSeqStatus(String userId) {
        FilterExpression.And eqDraft = Q.and(Q.it(DefaultPropertyType.CREATER.getCode(), FilterExpression.Op.Eq, userId),
                Q.it(DefaultPropertyType.SEQUENCE_STATUS.getCode(), FilterExpression.Op.Eq, SequenceStatus.DRAFT.toString()));
        return eqDraft;
    }

    /**
     * 非草稿
     */
    private FilterExpression filterNotDraftSeqStatus() {

        return Q.or(Q.it(DefaultPropertyType.SEQUENCE_STATUS.getCode(), FilterExpression.Op.Eq,null),
                Q.it(DefaultPropertyType.SEQUENCE_STATUS.getCode(), FilterExpression.Op.NotEq, SequenceStatus.DRAFT.toString()));
    }

    private Map<String, Object> filterQueryCondition(List<List<List<FilterModel>>> queryCondition, Set<String> propertyCodes, ModelViewQueryModel view) {

        boolean queryForTreeModel = view != null && ModelViewType.TREE == view.getType();
        Map<String, Object> result = Maps.newHashMapWithExpectedSize(10);
        if (queryForTreeModel) {
            view.setParentRefs(Lists.newArrayListWithExpectedSize(10));
        }
        if (CollectionUtils.isNotEmpty(queryCondition)) {
            Iterator<List<List<FilterModel>>> iterator = queryCondition.iterator();
            while (iterator.hasNext()) {
                List<List<FilterModel>> filterModelGroups = iterator.next();
                if (CollectionUtils.isNotEmpty(filterModelGroups)) {
                    filterModelGroups.stream().filter(CollectionUtils::isNotEmpty).forEach(filterModels -> {
                        Map<String, Object> resultPer = filterQueryCondition(filterModels, propertyCodes, queryForTreeModel);
                        // 树形模型处理 查询条件有 上级关联时 记录
                        if (queryForTreeModel && resultPer != null) {
                            Object parentRef = resultPer.get(DefaultTreePropertyType.PARENT_REF.getCode());
                            if (parentRef != null) {
                                view.getParentRefs().add((String) parentRef);
                            }
                        }
                    });
                    filterModelGroups.removeIf(CollectionUtils::isEmpty);
                }
                if (CollectionUtils.isEmpty(filterModelGroups)) {
                    iterator.remove();
                }
            }
        }
        // 树形模型处理
        if (queryForTreeModel) {
            if (CollectionUtils.isNotEmpty(queryCondition)) {
                //树形模型 存在查询条件时 存个标记
                view.setExistQueryCondition(true);
            }
            // 懒加载时，多查一层
            if (view.getDepth() > 0) {
                view.setDepth(view.getDepth() + 1);
            }
            if (CollectionUtils.isNotEmpty(view.getParentRefs())) {
                // TODO 上级关联暂时只支持第一个
                view.setParentRef(view.getParentRefs().get(0));
            }
            if (StringUtils.isBlank(view.getParentRef())) {
                //无查询条件 parentRef为空时 默认从根节点查询
                view.setParentRef(ModelViewQueryModel.TREE_QUERY_ROOT_PARENT_REF);
            }

        }
        return result;
    }

    private Map<String, Object> filterQueryCondition(List<FilterModel> filterModels, Set<String> propertyCodes, boolean queryForTreeModel) {
        if (CollectionUtils.isEmpty(filterModels)) {
            return null;
        }
        Map<String, Object> result = Maps.newHashMapWithExpectedSize(10);
        Iterator<FilterModel> iterator = filterModels.iterator();

        while (iterator.hasNext()) {
            FilterModel filterModel = iterator.next();
            if (!propertyCodes.contains(filterModel.getPropertyCode())) {
                log.debug("[filterQueryCondition] propertyCodes not contains, remove filterModel={}", filterModel);
                iterator.remove();
                continue;
            }
            QueryFilterType queryFilterType = filterModel.getQueryFilterType();
            if (queryFilterType == null && filterModel.getOperatorType() != null) {
                queryFilterType = QueryFilterType.getByIndex(filterModel.getOperatorType().getIndex());
            }
            if ((StringUtils.isBlank(filterModel.getPropertyValue()) || CollectionUtils.isEmpty(filterModel.getPropertyValueList()))
                    && (QueryFilterType.IsNull != queryFilterType && QueryFilterType.IsNotNull != queryFilterType)) {
                log.debug("[filterQueryCondition] remove filterModel={}", filterModel);
                iterator.remove();
                continue;
            }
            if (queryForTreeModel && DefaultTreePropertyType.PARENT_REF.getCode().equals(filterModel.getPropertyCode())) {
                result.put(DefaultTreePropertyType.PARENT_REF.getCode(), filterModel.getPropertyValue());
                iterator.remove();
            }
        }
        return result;
    }

    /**
     * 构建查询条件
     * @param queryDataModel  传入的查询条件
     * @param queryConditions 查询条件配置信息
     * @return 构建后的条件
     **/
    private FilterExpression getFiltersByQueryParam(QueryDataModel queryDataModel, List<BizProperty> bizProperties, List<BizQueryConditionModel> queryConditions) {
        if (bizProperties == null) {
            bizProperties = bizPropertyService.getListBySchemaCode(queryDataModel.getSchemaCode(), true);
        }
        if (CollectionUtils.isEmpty(bizProperties)) {
            return FilterExpression.empty;
        }
        List<List<List<FilterModel>>> queryCondition = queryDataModel.getQueryCondition();
        // 旧查询条件兼容
        if (queryDataModel.getQueryVersion() == null && CollectionUtils.isNotEmpty(queryDataModel.getFilters())) {
            queryCondition = Lists.newArrayListWithExpectedSize(1);
            List<List<FilterModel>> filterModelGroup = Lists.newArrayListWithExpectedSize(1);
            filterModelGroup.add(queryDataModel.getFilters());
            queryCondition.add(filterModelGroup);
        }
        Map<String, BizProperty> bizPropertyMap = bizProperties.stream().collect(Collectors.toMap(BizProperty::getCode, Function.identity(), (k1, k2) -> k1));
        // 查询条件过滤、树形模型处理
        filterQueryCondition(queryCondition, bizPropertyMap.keySet(), queryDataModel.getView());
        if (CollectionUtils.isEmpty(queryCondition)) {
            return FilterExpression.empty;
        }
        // 使用树形控件查询 带出子节点数据
        queryDataModel.setCodeToRelationSchemaCode(Maps.newHashMapWithExpectedSize(3));
        Map<String, BizQueryConditionModel> bizQueryConditionMap = CollectionUtils.isEmpty(queryConditions) ? Maps.newHashMapWithExpectedSize(1)
                : queryConditions.stream().collect(Collectors.toMap(BizQueryConditionModel::getPropertyCode, Function.identity(), (k1, k2) -> k1));
        List<FilterExpression> filterExpressionGroups = new ArrayList<>();
        queryCondition.forEach(group -> {
            List<FilterExpression> filterExpressions = new ArrayList<>();
            group.forEach(filterModelGroup -> {
                List<FilterExpression> filterExpressionInners = new ArrayList<>();
                filterModelGroup.forEach(filterModel -> {
                    FilterExpression filterExpression = buildFilter(filterModel, bizQueryConditionMap.get(filterModel.getPropertyCode()), bizPropertyMap.get(filterModel.getPropertyCode()), queryDataModel.getCodeToRelationSchemaCode());
                    if (filterExpression != null && FilterExpression.empty != filterExpression) {
                        filterExpressionInners.add(filterExpression);
                    }
                });
                if (filterExpressionInners.size() == 1) {
                    filterExpressions.add(filterExpressionInners.get(0));
                } else if (filterExpressionInners.size() > 1) {
                    filterExpressions.add(Q.and(filterExpressionInners));
                }
            });
            if (filterExpressions.size() == 1) {
                filterExpressionGroups.add(filterExpressions.get(0));
            } else if (filterExpressions.size() > 1) {
                filterExpressionGroups.add(Q.or(filterExpressions));
            }
        });
        if (filterExpressionGroups.size() == 1) {
            return filterExpressionGroups.get(0);
        } else if (filterExpressionGroups.size() > 1) {
            return Q.and(filterExpressionGroups);
        } else {
            return FilterExpression.empty;
        }
    }

    private FilterExpression.Op getOpByQueryFilterType(QueryFilterType queryFilterType) {
        if (QueryFilterType.Of == queryFilterType || QueryFilterType.NotOf == queryFilterType) {
            return null;
        } else {
            return FilterExpression.Op.valueOf(queryFilterType.name());
        }
    }

    /**
     * 构建单个查询条件
     * @param filterModel         传入的查询条件
     * @param queryConditionModel 查询条件配置信息
     * @param bizProperty         数据项信息
     * @return 构建后的条件
     **/
    private FilterExpression buildFilter(FilterModel filterModel, BizQueryConditionModel queryConditionModel, BizProperty bizProperty, Map<String, String> codeToRelationSchemaCode) {
        QueryFilterType queryFilterType = filterModel.getQueryFilterType();
        String propertyCode = filterModel.getPropertyCode();
        if (queryFilterType == null && filterModel.getOperatorType() != null) {
            queryFilterType = QueryFilterType.getByIndex(filterModel.getOperatorType().getIndex());
        }
        if (queryFilterType == null && filterModel.getOp() != null) {
            queryFilterType = QueryFilterType.valueOf(filterModel.getOp().name());
        }
        if (queryFilterType == null) {
            queryFilterType = QueryFilterType.getDefaultQueryFilterType(filterModel.getPropertyType(), bizProperty.getDefaultProperty());
        }
        if (QueryFilterType.IsNull == queryFilterType) {
            return Q.it(propertyCode, FilterExpression.Op.IsNull, null);
        }
        if (QueryFilterType.IsNotNull == queryFilterType) {
            return Q.it(propertyCode, FilterExpression.Op.IsNotNull, null);
        }
        if (StringUtils.isBlank(filterModel.getPropertyValue())) {
            return null;
        }

        List<String> valueList = filterModel.getPropertyValueList();
        if (CollectionUtils.isEmpty(valueList)) {
            return null;
        }
        List<String> propertyValueList = Lists.newArrayListWithExpectedSize(valueList.size());
        propertyValueList.addAll(valueList);
        BizPropertyType propertyType = filterModel.getPropertyType();
        FilterExpression.Op op = getOpByQueryFilterType(queryFilterType);
        if (BizPropertyType.selectionTypes.contains(propertyType)) {

            boolean isDefaultProperty = DefaultPropertyType.codes.contains(propertyCode);
            if (isDefaultProperty) {
                if ((QueryFilterType.Of == queryFilterType || QueryFilterType.NotOf == queryFilterType) && (Objects.equals(DefaultPropertyType.OWNER_DEPT_ID.getCode(), filterModel.getPropertyCode()) || Objects.equals(DefaultPropertyType.CREATED_DEPT_ID.getCode(), filterModel.getPropertyCode()))) {
                    List<DepartmentModel> departmentModels = organizationFacade.getDepartmentsChildList(propertyValueList, true);
                    if (CollectionUtils.isNotEmpty(departmentModels)) {
                        propertyValueList.addAll(departmentModels.stream().map(DepartmentModel::getId).collect(Collectors.toList()));
                        propertyValueList = new ArrayList<>(new HashSet<>(propertyValueList));
                    }
                    op = QueryFilterType.Of == queryFilterType ? FilterExpression.Op.In : FilterExpression.Op.NotIn;
                }
            } else {
                if (FilterExpression.Op.Eq == op || FilterExpression.Op.NotEq == op) {
                    FilterExpression.Op tempOp = op;
                    if (BizPropertyType.selectionMultiTypes.contains(propertyType)) {
                        tempOp = FilterExpression.Op.Eq == op ? FilterExpression.Op.Reg : FilterExpression.Op.NotLike;
                    }
                    final FilterExpression.Op finalTempOp = tempOp;
                    List<FilterExpression> filterTemplates = propertyValueList.stream().map(it -> Q.it(propertyCode,
                            finalTempOp, it)).collect(Collectors.toList());
                    // 人员/部门多选 等于时判断长度 TODO 多选优化
                    if (BizPropertyType.selectionMultiTypes.contains(propertyType)) {
                        int valueLength = propertyValueList.size() * 33 - 1;
                        if (BizPropertyType.SELECTION == propertyType) {
                            valueLength = propertyValueList.size() * 35 - 1;
                        }
                        if (filterTemplates.size() == 1) {
                            if (FilterExpression.Op.Eq == op) {
                                return Q.and(filterTemplates.get(0), Q.it(propertyCode, FilterExpression.Op.LengthEq, valueLength));
                            } else {
                                return Q.or(filterTemplates.get(0), Q.it(propertyCode, FilterExpression.Op.LengthNotEq, valueLength), Q.it(propertyCode, FilterExpression.Op.IsNull, ""));
                            }
                        } else {
                            if (FilterExpression.Op.Eq == op) {
                                return Q.and(Q.and(filterTemplates), Q.it(propertyCode, FilterExpression.Op.LengthEq, valueLength));
                            } else {
                                return Q.or(Q.or(filterTemplates), Q.it(propertyCode, FilterExpression.Op.LengthNotEq, valueLength), Q.it(propertyCode, FilterExpression.Op.IsNull, ""));
                            }
                        }
                    } else {
                        if (filterTemplates.size() == 1) {
                            if (FilterExpression.Op.Eq == op) {
                                return filterTemplates.get(0);
                            } else {
                                return Q.or(filterTemplates.get(0), Q.it(propertyCode, FilterExpression.Op.IsNull, ""));
                            }
                        } else {
                            if (FilterExpression.Op.Eq == op) {
                                return Q.and(filterTemplates);
                            } else {
                                return Q.or(Q.or(filterTemplates), Q.it(propertyCode, FilterExpression.Op.IsNull, ""));
                            }
                        }
                    }
                } else if (FilterExpression.Op.Like == op) {
                    //根据工单#116059修改人员部门包含多个，改为同时包含。
                    final FilterExpression.Op finalTempOp = op;
                    List<FilterExpression> filterTemplates = propertyValueList.stream().map(it -> Q.it(propertyCode, finalTempOp, it)).collect(Collectors.toList());
                    if (filterTemplates.size() == 1) {
                        return filterTemplates.get(0);
                    } else {
                        return Q.and(filterTemplates);
                    }
                }
            }
        } else if (BizPropertyType.MULT_WORK_SHEET == propertyType || BizPropertyType.CHECKBOX == propertyType || BizPropertyType.DROPDOWN_MULTI_BOX == propertyType) {

            if (FilterExpression.Op.Like == op || FilterExpression.Op.NotLike == op) {
                String regex = generateRegex(propertyValueList);
                if (FilterExpression.Op.Like == op) {
                    return Q.it(propertyCode, FilterExpression.Op.Reg, regex);
                } else {
                    return Q.or(Q.it(propertyCode, FilterExpression.Op.NotReg, regex), Q.it(propertyCode, FilterExpression.Op.IsNull, ""));
                }
            } else if (FilterExpression.Op.Eq == op || FilterExpression.Op.NotEq == op) {
                if (propertyValueList.size() == 1) {
                    if (FilterExpression.Op.Eq == op) {
                        return Q.it(propertyCode, op, propertyValueList.get(0));
                    } else {
                        return Q.or(Q.it(propertyCode, op, propertyValueList.get(0)), Q.it(propertyCode, FilterExpression.Op.IsNull, ""));
                    }
                } else {
                    // 多选使用正则匹配 1. 等于使用 like(and连接) 和 字符长度匹配 2. 不等于使用 not like(or连接) 和 字符长度不匹配
                    FilterExpression.Op tempOp = FilterExpression.Op.Eq == op ? FilterExpression.Op.Like : FilterExpression.Op.NotLike;
                    List<FilterExpression> filterTemplates = propertyValueList.stream().map(it -> Q.it(propertyCode, tempOp, it)).collect(Collectors.toList());
                    long valueLength = propertyValueList.stream().collect(Collectors.summarizingInt(String::length)).getSum() + propertyValueList.size() - 1;
                    if (FilterExpression.Op.Eq == op) {
                        return Q.and(Q.and(filterTemplates), Q.it(propertyCode, FilterExpression.Op.LengthEq, valueLength));
                    } else {
                        return Q.or(Q.or(filterTemplates), Q.it(propertyCode, FilterExpression.Op.LengthNotEq, valueLength),
                                Q.it(propertyCode, FilterExpression.Op.IsNull, ""));
                    }
                }
            }
        } else if (BizPropertyType.ADDRESS == propertyType) {
            return buildAddressEqFilter(propertyValueList.get(0), propertyCode, op);
        } else if (BizPropertyType.NUMERICAL == propertyType) {
            if (FilterExpression.Op.NotBetween == op) {
                return Q.or(Q.it(propertyCode, op, propertyValueList), Q.it(propertyCode, FilterExpression.Op.IsNull, ""));
            }
        } else if (BizPropertyType.DATE == propertyType) {
            String startTime = propertyValueList.get(0);
            if (FilterExpression.Op.Eq == op || FilterExpression.Op.NotEq == op) {
                String endTime = propertyValueList.get(1);
                if (!startTime.equals(endTime)) {
                    if (FilterExpression.Op.Eq == op) {
                        return Q.it(propertyCode, FilterExpression.Op.Between, Arrays.asList(startTime, endTime));
                    } else {
                        return Q.or(Q.it(propertyCode, FilterExpression.Op.NotBetween, Arrays.asList(startTime, endTime)),
                                Q.it(propertyCode, FilterExpression.Op.IsNull, ""));
                    }
                } else {
                    return Q.it(propertyCode, op, propertyValueList.get(0));
                }
            } else if (FilterExpression.Op.Between == op) {
                return Q.it(propertyCode, op, propertyValueList);
            } else if (FilterExpression.Op.NotBetween == op) {
                return Q.or(Q.it(propertyCode, op, propertyValueList), Q.it(propertyCode, FilterExpression.Op.IsNull, ""));
            } else {
                return Q.it(propertyCode, op, startTime);
            }
        } else if (BizPropertyType.WORK_SHEET == propertyType) {
            // 树形控件包含子数据处理
            String relativeCode = bizProperty.getRelativeCode();
            if (queryConditionModel != null && queryConditionModel.getIncludeSubData() && StringUtils.isNotBlank(relativeCode)) {
                BizObjectCreatedModel bizObjectCreatedModel = this.getBizObjectWithoutChild(relativeCode, propertyValueList.get(0));
                if (bizObjectCreatedModel != null && bizObjectCreatedModel.getData() != null) {
                    String path = (String) bizObjectCreatedModel.getData().get(DefaultTreePropertyType.PATH.getCode());
                    if (StringUtils.isNotBlank(path)) {
                        // 保存字段编码 和 关联模型编码 关系
                        codeToRelationSchemaCode.put(propertyCode, relativeCode);
                        return Q.it(relativeCode.concat(".").concat(DefaultTreePropertyType.PATH.getCode()), FilterExpression.Op.LLike, path);
                    }
                }
            }
        }
        if (op == null) {
            return null;
        }
        FilterExpression filterExpression;
        if (propertyValueList.size() == 1) {
            if (FilterExpression.Op.In == op) {
                op = FilterExpression.Op.Eq;
            } else if (FilterExpression.Op.NotIn == op) {
                op = FilterExpression.Op.NotEq;
            } else if (FilterExpression.Op.NotLike == op) {
                return Q.or(Q.it(propertyCode, op, propertyValueList.get(0)), Q.it(propertyCode, FilterExpression.Op.IsNull, ""));
            }
            filterExpression = Q.it(propertyCode, op, propertyValueList.get(0));
        } else {
            filterExpression = Q.it(propertyCode, op, propertyValueList);
        }
        return filterExpression;
    }

    private FilterExpression buildAddressEqFilter(String addressString, String propertyCode, FilterExpression.Op op) {
        List<String> addressCodes = new ArrayList<>();
        if (FilterExpression.Op.Eq == op || FilterExpression.Op.NotEq == op) {
            addressCodes = getAddressCodes(addressString);
            op = FilterExpression.Op.NotEq == op ? FilterExpression.Op.NotLike : FilterExpression.Op.Like;
        } else if (FilterExpression.Op.Like == op || FilterExpression.Op.NotLike == op) {
            //分词
            addressCodes = TokenizerUtils.parse(addressString, true);
        } else {
            return null;
        }
        if (CollectionUtils.isEmpty(addressCodes)) {
            return null;
        }
        FilterExpression.Op tempOp = op;
        List<FilterExpression> filterTemplates = addressCodes.stream().map(it -> Q.it(propertyCode, tempOp, it)).collect(Collectors.toList());
        if (filterTemplates.size() == 1) {
            return FilterExpression.Op.NotLike == op ? Q.or(filterTemplates.get(0), Q.it(propertyCode, FilterExpression.Op.IsNull, null)) : filterTemplates.get(0);
        } else {
            return FilterExpression.Op.NotLike == op ? Q.or(Q.or(filterTemplates), Q.it(propertyCode, FilterExpression.Op.IsNull, null)) : Q.and(filterTemplates);
        }
    }

    private List<String> getAddressCodes(String addressString) {
        if (!JSONUtil.isJson(addressString)) {
            return null;
        }
        List<String> addressCodes = Lists.newArrayListWithExpectedSize(3);
        Map<String, String> addressMap = JSON.parseObject(addressString, new TypeReference<Map<String, String>>() {
        });
        String provinceAdCode = addressMap.get("provinceAdcode");
        String cityAdCode = addressMap.get("cityAdcode");
        String districtAdCode = addressMap.get("districtAdcode");
        if (StringUtils.isNotBlank(provinceAdCode)) {
            addressCodes.add(provinceAdCode);
        }
        if (StringUtils.isNotBlank(cityAdCode)) {
            addressCodes.add(cityAdCode);
        }
        if (StringUtils.isNotBlank(districtAdCode)) {
            addressCodes.add(districtAdCode);
        }
        return addressCodes;
    }

    /**
     * 根据选项构建正则表达式查询条件
     */
    private String generateRegex(List<String> propertyValues) {
        String regex = StringUtils.join(propertyValues, ";|");
        if (propertyValues.size() > 1) {
            regex = regex.concat(";");
        }
        return regex;
    }

    private String generateRegexForMulti(Long spiltNumber) {
        return "^.{".concat(String.valueOf(spiltNumber)).concat("}$");
    }

    private void disposeGanttModel(Page<BizObjectModel> data, BizQueryModel bizQueryModel, boolean isTreeModel) {
        if (bizQueryModel.getQueryPresentationType() == QueryPresentationType.GANTT && !isTreeModel) {
            if (CollectionUtils.isEmpty(data.getContent())) {
                return;
            }
            BizQueryGanttModel queryGanttModel = bizQueryModel.getQueryGanttModel();
            List<BizObjectModel> bizObjectModelList = (List<BizObjectModel>) data.getContent();
            for (BizObjectModel bizObject : data.getContent()) {
                if (bizObject.get(queryGanttModel.getLevelPropertyCode()) != null) {
                    Map parentMap = (Map) bizObject.get(queryGanttModel.getLevelPropertyCode());
                    boolean isParent = bizObjectModelList.stream().anyMatch(model -> model.get("id").equals(parentMap.get("id")));
                    if (!isParent) {
                        bizObject.put(queryGanttModel.getLevelPropertyCode(), null);
                    }
                }
            }
        }

    }

    /**
     * 设置用户显示图像
     * @param data 分页数据
     */
    private void disposeUserInfo(Page<BizObjectModel> data) {
        if (CollectionUtils.isEmpty(data.getContent())) {
            return;
        }
        for (BizObjectModel bizObject : data.getContent()) {
            List<SelectionValue> selectionValues = (List<SelectionValue>) bizObject.get(DefaultPropertyType.CREATER.getCode());
            if (CollectionUtils.isNotEmpty(selectionValues)) {
                bizObject.put("imgUrl", selectionValues.get(0).getImgUrl());
                bizObject.put("originator", selectionValues.get(0).getName());
            }
        }
    }

    /**
     * 处理PC端选人控件展示
     * @param data 分页数据
     */
    private void parseSpecialProperties(Page<BizObjectModel> data, Boolean isExport) {
        //获选人数据项
        BizObjectModel objectModel = data.getContent().get(0);
        BizSchemaModel bizSchema = bizSchemaFacade.getBySchemaCodeWithProperty(objectModel.getSchemaCode(), null);
        if (isExport) {
            List<String> selectCodes = getPropertyCodeList(objectModel, BizPropertyType.selectionTypes);
            if (CollectionUtils.isEmpty(selectCodes)) {
                return;
            }
            if (log.isDebugEnabled()) {
                log.debug("需要处理的选人数据项为{}", JSON.toJSONString(selectCodes));
            }
            Set<String> dataCodes = null;
            for (String selectCode : selectCodes) {
                for (BizObjectModel bizObject : data.getContent()) {
                    if (CollectionUtils.isEmpty(dataCodes)) {
                        dataCodes = bizObject.getData().keySet();
                        if (log.isDebugEnabled()) {
                            log.debug("需要展示的数据项为{}", JSON.toJSONString(dataCodes));
                        }
                    }
                    if (!dataCodes.contains(selectCode)) {
                        if (log.isDebugEnabled()) {
                            log.debug("不需要展示的选人数据项为{}", selectCode);
                        }
                        break;
                    }
                    if (bizObject.get(selectCode) == null) {
                        continue;
                    }
                    List<SelectionValue> selectionValues = (List<SelectionValue>) bizObject.get(selectCode);
                    bizObject.put(selectCode, getUserNames(selectionValues));
                }
            }
            //处理单据状态
            if (data.getContent().get(0).getData().containsKey(DefaultPropertyType.SEQUENCE_STATUS.getCode())) {
                for (BizObjectModel bizObjectModel : data.getContent()) {
                    bizObjectModel.put(DefaultPropertyType.SEQUENCE_STATUS.getCode(), parseSequenceStatus(bizObjectModel));
                }
            }
            //获取逻辑型
            List<BizPropertyModel> logicalModels = bizSchema.getProperties().stream().filter(t -> t.getPropertyType() == BizPropertyType.LOGICAL).collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(logicalModels)) {
                List<String> logicals = logicalModels.stream().map(BizPropertyModel::getCode).collect(Collectors.toList());
                Set<String> logicalDataCodes = null;
                for (String logical : logicals) {
                    for (BizObjectModel bizObjectModel : data.getContent()) {
                        if (CollectionUtils.isEmpty(logicalDataCodes)) {
                            logicalDataCodes = bizObjectModel.getData().keySet();
                        }
                        if (!logicalDataCodes.contains(logical)) {
                            break;
                        }
                        if (bizObjectModel.get(logical) == null) {
                            continue;
                        }
                        bizObjectModel.put(logical, disposeLogic(bizObjectModel.getString(logical)));
                    }

                }
            }
        }

    }

    /**
     * 从列表数据中获取到用户名
     * @return Object
     */
    public Object getUserNames(List<SelectionValue> selectionValues) {
        if (selectionValues == null) {
            return "";
        }
        return selectionValues.stream().map(SelectionValue::getName).collect(Collectors.joining(", "));
    }

    /**
     * 处理审批意见
     */
    public Object disposeLogic(String logic) {
        switch (logic) {
            case "1":
                return "是";
            case "0":
                return "否";
            case "true":
                return "是";
            case "false":
                return "否";
            default:
                return "";
        }
    }

    private List<String> getRelevanceFormConfigColumn(String schemaCode, String sheetCode, String relevanceFormCode) {
        //当前表单
        BizFormModel bizFormModel = bizFormFacade.getBySchemaCodeAndFormCode(schemaCode, sheetCode);
        List<String> showField = BizFormUtil.getRelevanceFormShowField(bizFormModel, relevanceFormCode);
        String regex = ".";
        String transferRegex = "\\.";
        List<String> configColumnList = Collections.emptyList();
        if (CollectionUtils.isNotEmpty(showField)) {
            List<String> cg = new ArrayList<>();
            List<String> c1 = showField.stream().filter(t -> !t.contains(regex)).collect(Collectors.toList());
            List<String> c2 = showField.stream().filter(t -> t.contains(regex)).collect(Collectors.toList());
            List<String> c3 = c2.stream().map(s -> s.split(transferRegex)[0]).distinct().collect(Collectors.toList());
            cg.addAll(c1);
            cg.addAll(c3);
            configColumnList = cg;
        }
        return configColumnList;
    }

    private void parseAddressAndReference(Page<BizObjectModel> data, List<BizQueryColumnModel> columnModels, Boolean isExport, String queryCode) {
        if (CollectionUtils.isEmpty(columnModels) || CollectionUtils.isEmpty(data.getContent())) {
            return;
        }
        if (isExport) {
            //获取模板信息
            List<BizQueryColumnModel> collect = columnModels.stream().filter(t -> t.getPropertyType() == BizPropertyType.NUMERICAL).collect(Collectors.toList());
            Map<String, String> mapTemp = new HashMap<>();
            if (CollectionUtils.isNotEmpty(collect)) {
                BizQueryModel bizQuery = bizQueryFacade.getBizQuery(columnModels.get(0).getSchemaCode(), queryCode, ClientType.PC, null);
                String formCode = null;
                if (bizQuery != null && CollectionUtils.isNotEmpty(bizQuery.getQueryActions())) {
                    BizQueryActionModel bizQueryAction = bizQuery.getQueryActions().stream().filter(t -> t.getQueryActionRelativeType() != null && t.getQueryActionRelativeType() == QueryActionRelativeType.BIZSHEET && StringUtils.isNotEmpty(t.getRelativeCode())).findAny().orElse(null);
                    if (bizQueryAction != null) {
                        formCode = bizQueryAction.getRelativeCode();
                        log.debug("获取到的表单编码为{}", formCode);
                    }
                }
                if (StringUtils.isEmpty(formCode)) {
                    formCode = bizFormFacade.getFormCodeBySchemaCode(columnModels.get(0).getSchemaCode());
                }
                BizForm sheetModel = bizFormService.getBySchemaCodeAndFormCode(columnModels.get(0).getSchemaCode(), formCode);
                Map map = JSON.parseObject(sheetModel.getPublishedAttributesJson(), Map.class);
                for (BizQueryColumnModel bizQueryColumnModel : collect) {
                    if (map.containsKey(bizQueryColumnModel.getPropertyCode())) {
                        Map map1 = JSON.parseObject(map.get(bizQueryColumnModel.getPropertyCode()).toString(), Map.class);
                        Map map2 = JSON.parseObject(map1.get("options").toString(), Map.class);
                        mapTemp.put(bizQueryColumnModel.getPropertyCode(), map2.get("format").toString());
                    }
                }
                log.debug("获取到的表单编码为{}", formCode);

            }
            columnModels = columnModels.stream().filter(t -> t.getPropertyType() == BizPropertyType.ADDRESS || t.getPropertyType() == BizPropertyType.WORK_SHEET || t.getPropertyType() == BizPropertyType.NUMERICAL).collect(Collectors.toList());
            for (BizQueryColumnModel columnModel : columnModels) {
                for (BizObjectModel bizObjectModel : data.getContent()) {
                    if (columnModel.getPropertyType() == BizPropertyType.ADDRESS) {
                        String json = null;
                        if (bizObjectModel.get(columnModel.getPropertyCode()) != null) {
                            json = bizObjectModel.get(columnModel.getPropertyCode()).toString();
                        }
                        if ("{}".equals(json)) {
                            json = null;
                            bizObjectModel.put(columnModel.getPropertyCode(), null);
                            log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> >>>>>>>>>>>>>>NO.23>>>> json为null");
                        }
                        if (StringUtils.isNotEmpty(json)) {
                            Map map = JSONObject.parseObject(json, Map.class);
                            String name = "";
                            if (map.get("provinceName") != null) {
                                name = name + map.get("provinceName");
                            }
                            if (map.get("cityName") != null) {
                                name = name + map.get("cityName");
                            }
                            if (map.get("districtName") != null) {
                                name = name + map.get("districtName");
                            }
                            if (map.get("address") != null) {
                                name = name + map.get("address");
                            }
                            log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>NO.33>>>> {}", name);
                            bizObjectModel.put(columnModel.getPropertyCode(), name);
                        }
                    }
                    if (columnModel.getPropertyType() == BizPropertyType.WORK_SHEET) {
                        if (bizObjectModel.get(columnModel.getPropertyCode()) != null) {
                            Object o = bizObjectModel.get(columnModel.getPropertyCode());
                            //修改:
                            bizObjectModel.put(columnModel.getPropertyCode(), o == "null" ? null : o);
                            log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>NO.43>>>> {}", o);
                        }
                    }
                    if (columnModel.getPropertyType() == BizPropertyType.NUMERICAL) {
                        if (bizObjectModel.get(columnModel.getPropertyCode()) == null) {
                            continue;
                        }
                        DecimalFormat df = null;
                        switch (mapTemp.get(columnModel.getPropertyCode())) {

                            case "integer":
                                df = new DecimalFormat("#");
                                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.get(columnModel.getPropertyCode())));
                                break;
                            case "tenths":
                                df = new DecimalFormat("###,###.#");
                                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.get(columnModel.getPropertyCode())));
                                break;
                            case "percentile":
                                df = new DecimalFormat("###,###.##");
                                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.get(columnModel.getPropertyCode())));
                                break;
                            case "ratio":
                                df = new DecimalFormat("#%");
                                BigDecimal ratioResult = (BigDecimal) bizObjectModel.get(columnModel.getPropertyCode());
                                bizObjectModel.put(columnModel.getPropertyCode(), df.format(ratioResult.longValue() / 100.0));
                                break;
                            case "ratio.tenths":
                                df = new DecimalFormat("##.#" + "%");
                                BigDecimal ratioTenths = (BigDecimal) bizObjectModel.get(columnModel.getPropertyCode());
                                bizObjectModel.put(columnModel.getPropertyCode(), df.format(ratioTenths.longValue() / 100.0));
                                break;
                            case "ratio.percentile":
                                df = new DecimalFormat("##.##" + "%");
                                BigDecimal ratioPercentile = (BigDecimal) bizObjectModel.get(columnModel.getPropertyCode());
                                bizObjectModel.put(columnModel.getPropertyCode(), df.format(ratioPercentile.longValue() / 100.0));
                                break;
                            case "HK.percentile":
                                df = new DecimalFormat("HK$#,###.00");
                                bizObjectModel.put(columnModel.getPropertyCode(), df.format(bizObjectModel.get(columnModel.getPropertyCode())));
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }

    /**
     * api : 将引用数据项放入查询模型
     * <p>
     * bizObjectQueryModel 中 的Map<String, Object> quotes  里面有三层结构  第一层  key为 模型编码（可以为子表） value为对应该模型编码下的引用的数据项  第二层 key 为关联表单编码 value为该关联表单下的引用数据项 第三层  key为引用的数据项编码 value 为该数据项的几层引用
     * @param bizObjectQueryModel 查询条件
     * @param schemaCode          模型编码
     * @return 查询条件
     */
    private BizObjectQueryModel parseQueries(BizObjectQueryModel bizObjectQueryModel, String schemaCode) {
        Map<String, Map<String, List<QuoteModel>>> quoteModels = parseQuote(schemaCode);
        BizSchemaModel bizSchema = bizSchemaFacade.getBySchemaCodeWithProperty(schemaCode, true);
        List<BizPropertyModel> properties = bizSchema.getProperties();
        List<String> subSchemaCodes = properties.stream().filter(propertyModel -> propertyModel.getPropertyType() == BizPropertyType.CHILD_TABLE).map(BizPropertyModel::getCode).collect(Collectors.toList());
        Map<String, Map<String, Map<String, List<QuoteModel>>>> object = Maps.newHashMapWithExpectedSize(subSchemaCodes.size() + 1);
        object.put(schemaCode, quoteModels);
        if (CollectionUtils.isEmpty(subSchemaCodes)) {
            Map<String, Object> result = new HashMap<>(object);
            bizObjectQueryModel.setQuotes(result);
            return bizObjectQueryModel;
        }
        for (String sub : subSchemaCodes) {
            Map<String, Map<String, List<QuoteModel>>> quotes = parseQuote(sub);
            object.put(sub, quotes);
        }
        Map<String, Object> result = new HashMap<>(object);
        bizObjectQueryModel.setQuotes(result);
        return bizObjectQueryModel;
    }

    private Map<String, Map<String, List<QuoteModel>>> parseQuote(String schemaCode) {
        BizSchemaModel bizSchema = bizSchemaFacade.getBySchemaCodeWithProperty(schemaCode, true);
        List<BizPropertyModel> properties = bizSchema.getProperties();
        if (CollectionUtils.isEmpty(properties)) {
            throw new ServiceException(ErrCode.BIZ_PROPERTY_NOT_EXIST.getErrCode(), ErrCode.BIZ_PROPERTY_NOT_EXIST.getErrMsg(), "数据项为空");
        }
        List<BizPropertyModel> workSheets = properties.stream().filter(propertyModel -> propertyModel.getPropertyType() == BizPropertyType.WORK_SHEET).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(workSheets)) {
            return Maps.newHashMap();
        }
        Map<String, Map<String, List<QuoteModel>>> quotes = Maps.newHashMapWithExpectedSize(workSheets.size());
        for (BizPropertyModel bizPropertyModel : workSheets) {
            List<QuoteModel> quoteCodes = bizPropertyModel.getQuoteCodes();
            if (CollectionUtils.isEmpty(quoteCodes)) {
                continue;
            }
            String code = bizPropertyModel.getCode();
            Map<String, List<QuoteModel>> map = Maps.newHashMapWithExpectedSize(quoteCodes.size());
            for (QuoteModel quoteModel : quoteCodes) {
                List<QuoteModel> list = new ArrayList<>();
                quoteModel.setRelativePropertyCode(bizPropertyModel.getCode());
                list.add(quoteModel);
                parseQuoteModel(quoteModel, list);
                map.put(quoteModel.getCode(), list);
            }
            quotes.put(code, map);
        }
        return quotes;
    }

    /**
     * api : 计算每一个引用数据项所有的引用   并保持引用全过程
     * @param quoteModel 引用数据项信息
     * @param result     引用信息
     * @return 引用信息
     */
    private List<QuoteModel> parseQuoteModel(QuoteModel quoteModel, List<QuoteModel> result) {
        String relativeCode = quoteModel.getRelativeCode();
        String relativeSchemaCode = quoteModel.getRelativeSchemaCode();
        BizSchemaModel bizSchema = bizSchemaFacade.getBySchemaCodeWithProperty(relativeSchemaCode, true);
        List<BizPropertyModel> properties = bizSchema.getProperties();
        if (CollectionUtils.isEmpty(properties)) {
            throw new ServiceException(ErrCode.BIZ_PROPERTY_NOT_EXIST.getErrCode(), ErrCode.BIZ_PROPERTY_NOT_EXIST.getErrMsg(), "数据项为空");
        }
        List<BizPropertyModel> workSheets = properties.stream().filter(propertyModel -> propertyModel.getPropertyType() == BizPropertyType.WORK_SHEET).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(workSheets)) {
            if (CollectionUtils.isNotEmpty(result)) {
                List<QuoteModel> collect = result.stream().filter(quote -> quote.getCode().equals(quoteModel.getCode())).collect(Collectors.toList());
                if (CollectionUtils.isEmpty(collect)) {
                    result.add(quoteModel);
                    return result;
                }
                return result;
            }
            result.add(quoteModel);
            return result;
        }
        List<QuoteModel> quoteModels = Lists.newArrayListWithExpectedSize(workSheets.size());
        for (BizPropertyModel bizPropertyModel : workSheets) {
            List<QuoteModel> quoteCodes = bizPropertyModel.getQuoteCodes();
            if (CollectionUtils.isEmpty(quoteCodes)) {
                continue;
            }
            List<QuoteModel> quotes = quoteCodes.stream().filter(quote -> Objects.equals(quote.getCode(), relativeCode)).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(quotes)) {
                continue;
            }
            quotes.get(0).setRelativePropertyCode(bizPropertyModel.getCode());
            quoteModels.add(quotes.get(0));
        }
        if (CollectionUtils.isEmpty(quoteModels)) {
            if (CollectionUtils.isNotEmpty(result)) {
                List<QuoteModel> collect = result.stream().filter(quote -> quote.getCode().equals(quoteModel.getCode())).collect(Collectors.toList());
                if (CollectionUtils.isEmpty(collect)) {
                    result.add(quoteModel);
                    return result;
                }
                return result;
            }
        }
        result.add(quoteModels.get(0));
        parseQuoteModel(quoteModels.get(0), result);
        return result;

    }

    private boolean verifySelections(Map<String, Object> dataMap, BizPropertyModel bizPropertyModel, Map<String, List<String>> organizationIdMap) {
        if (MapUtils.isEmpty(dataMap)) {
            return false;
        }
        String propertyCode = bizPropertyModel.getCode();
        Object value = dataMap.get(propertyCode);
        if (value == null || StringUtils.isBlank(value.toString())) {
            return !bizPropertyModel.getPropertyEmpty();
        }
        List<Map<String, Object>> selectDataMapList = (List<Map<String, Object>>) value;
        BizPropertyType propertyType = bizPropertyModel.getPropertyType();
        switch (propertyType) {
            case STAFF_SELECTOR:
                if (selectDataMapList.size() > 1) {
                    return false;
                }
                if (!checkUser(selectDataMapList.get(0), organizationIdMap)) {
                    return false;
                }
                dataMap.put(propertyCode, Lists.newArrayList(parseSelectValue(selectDataMapList.get(0))));
                return true;
            case STAFF_MULTI_SELECTOR:
                if (selectDataMapList.stream().anyMatch(item -> !checkUser(item, organizationIdMap))) {
                    return false;
                }
                dataMap.put(propertyCode, selectDataMapList.stream().map(this::parseSelectValue).collect(Collectors.toList()));
                return true;
            case DEPARTMENT_SELECTOR:
                if (selectDataMapList.size() > 1) {
                    return false;
                }
                if (!checkDepartment(selectDataMapList.get(0), organizationIdMap)) {
                    return false;
                }
                dataMap.put(propertyCode, Lists.newArrayList(parseSelectValue(selectDataMapList.get(0))));
                return true;
            case DEPARTMENT_MULTI_SELECTOR:
                if (selectDataMapList.stream().anyMatch(item -> !checkDepartment(item, organizationIdMap))) {
                    return false;
                }
                dataMap.put(propertyCode, selectDataMapList.stream().map(this::parseSelectValue).collect(Collectors.toList()));
                return true;
            case SELECTION:
                List<SelectionValue> selectionValueList = new ArrayList<>();
                for (Map<String, Object> item : selectDataMapList) {
                    UnitType unitType = UnitType.get(Integer.parseInt(item.get("type").toString()));
                    if (UnitType.USER == unitType) {
                        if (!checkUser(item, organizationIdMap)) {
                            return false;
                        }
                        selectionValueList.add(parseSelectValue(item));
                    } else {
                        if (!checkDepartment(item, organizationIdMap)) {
                            return false;
                        }
                        selectionValueList.add(parseSelectValue(item));
                    }
                }
                dataMap.put(propertyCode, selectionValueList);
                return true;
            default:
                return false;
        }
    }

    private boolean checkUser(Map<String, Object> selectValueMap, Map<String, List<String>> organizationIdMap) {
        if (UnitType.USER.getIndex() != Integer.parseInt(selectValueMap.get("type").toString())) {
            return false;
        }
        String id = selectValueMap.get("id").toString();
        List<String> userIdList = organizationIdMap.get("userIdList");
        if (userIdList.contains(id)) {
            return true;
        }
        List<String> errorUserIdList = organizationIdMap.get("errorUserIdList");
        if (errorUserIdList.contains(id)) {
            return false;
        }
        UserModel userModel = organizationFacade.getUser(id);
        if (userModel != null && !Boolean.FALSE.equals(userModel.getEnabled())) {
            userIdList.add(id);
            return true;
        } else {
            errorUserIdList.add(id);
            return false;
        }
    }

    private boolean checkDepartment(Map<String, Object> selectValueMap, Map<String, List<String>> organizationIdMap) {
        if (UnitType.DEPARTMENT.getIndex() != Integer.parseInt(selectValueMap.get("type").toString())) {
            return false;
        }
        String id = selectValueMap.get("id").toString();
        List<String> deptIdList = organizationIdMap.get("deptIdList");
        if (deptIdList.contains(id)) {
            return true;
        }
        List<String> errorDeptIdList = organizationIdMap.get("errorDeptIdList");
        if (errorDeptIdList.contains(id)) {
            return false;
        }
        DepartmentModel departmentModel = organizationFacade.getDepartment(id);
        if (departmentModel != null && !Boolean.FALSE.equals(departmentModel.getEnabled())) {
            deptIdList.add(id);
            return true;
        } else {
            errorDeptIdList.add(id);
            return false;
        }
    }

    private SelectionValue parseSelectValue(Map<String, Object> map) {
        SelectionValue selectionValue = new SelectionValue();
        selectionValue.setId(map.get("id").toString());
        selectionValue.setType(UnitType.get(Integer.parseInt(map.get("type").toString())));
        selectionValue.setName(map.get("name").toString());
        return selectionValue;
    }

    @Override
    public void parseRelevanceData(String relevanceInfo, String userId, List<BizPropertyModel> bizPropertyModelList, Map<String, Object> data, String publishedAttributesJson) {
        if (CollectionUtils.isEmpty(bizPropertyModelList)) {
            return;
        }

        if (StringUtils.isEmpty(relevanceInfo)) {
            return;
        }

        List<BizPropertyModel> bizPropertyModels = bizPropertyModelList.stream()
                .filter(m -> m.getPublished() && m.getPropertyType() == BizPropertyType.WORK_SHEET).collect(Collectors.toList());

        if (CollectionUtils.isEmpty(bizPropertyModels)) {
            return;
        }
        Map<String, List<BizPropertyModel>> listMap = bizPropertyModels.stream().collect(Collectors.groupingBy(BizPropertyModel::getCode));
        Map<String, Object> map = JSONObject.parseObject(relevanceInfo, Map.class);

        for (Map.Entry<String, Object> entry : map.entrySet()) {
            List<BizPropertyModel> propertyModels = listMap.get(entry.getKey());
            if (CollectionUtils.isEmpty(propertyModels)) {
                continue;
            }
            String relativeCode = propertyModels.get(0).getRelativeCode();
            QueryDataModel queryData = new QueryDataModel();
            queryData.setPage(0);
            queryData.setSize(1);
            queryData.setSchemaCode(relativeCode);
            if (StringUtils.isEmpty(relativeCode)) {
                continue;
            }
            queryData.setMobile(false);
            if (entry.getValue() == null) {
                continue;
            }
            queryData.setObjectIds(ImmutableList.of(entry.getValue().toString()));
            //解析表单数据项映射
            List<String> queryCodes = bizFormJsonResolveService.getRelationMappingByCode(entry.getKey(), publishedAttributesJson);
            if (CollectionUtils.isNotEmpty(queryCodes)) {
                BizObjectQueryModel.Options options = new BizObjectQueryModel.Options();
                options.setCustomDisplayColumns(queryCodes);
                options.setQueryDisplayType(QueryDisplayType.APPEND);
                queryData.setOptions(options);
            }
            Page<BizObjectModel> bizObject = this.queryBizObjectsForRelation(userId, queryData, false, true, false);
            if (bizObject == null) {
                continue;
            }
            if (CollectionUtils.isEmpty(bizObject.getContent())) {
                continue;
            }
            data.put(entry.getKey(), bizObject.getContent().get(0).getData());
        }

    }

    /**
     * 判断父节点是否属于子孙节点
     * @param levelPropertyCode        模型的层级编码
     * @param bizObjectId              业务数据id
     * @param parentLevelPropertyValue 父级值
     * @param schemaCode               模型编码
     * @return
     */
    @Override
    public boolean checkParentNodeIsChildNode(String levelPropertyCode, String bizObjectId,
                                              String parentLevelPropertyValue, String schemaCode) {

        List<String> children = queryChildOfCurrentNode(levelPropertyCode, bizObjectId, schemaCode);
        if (children == null) return false;
        if (!CollectionUtils.isEmpty(children) && children.contains(parentLevelPropertyValue)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * 甘特图获取当前节点的子孙节点
     * @param levelPropertyCode 模型的层级编码
     * @param bizObjectId       业务数据id
     * @param schemaCode        模型编码
     * @return
     */
    public List<String> queryChildOfCurrentNode(String levelPropertyCode, String bizObjectId, String schemaCode) {
        //分页信息
        int page = 0;
        int size = Integer.MAX_VALUE;
        BizObjectQueryObject bizObjectQueryObject = new BizObjectQueryObject() {
            @Override
            public String getSchemaCode() {
                return schemaCode;
            }

            @Override
            public List<String> getDisplayFields() {
                return Arrays.asList(new String[]{DefaultPropertyType.ID.getCode(), levelPropertyCode});
            }

            @Override
            public FilterExpression getFilterExpression() {
                return FilterExpression.empty;

            }

            @Override
            public Pageable getPageable() {
                return new PageableImpl(page, size);
            }
        };

        Page<Map<String, Object>> data = bizObjectService.queryBizObjects(bizObjectQueryObject);

        List<? extends Map<String, Object>> content = data.getContent();
        if (CollectionUtils.isEmpty(content)) {
            return null;
        }
        List<String> children = findChildrenOfCurrentNode((List<Map<String, Object>>) content, new ArrayList<String>(), bizObjectId, levelPropertyCode);
        return children;
    }

    public static List<String> findChildrenOfCurrentNode(List<Map<String, Object>> list, List<String> children, String pid, String levelPropertyCode) {
        list.forEach(item -> {
            if (Objects.nonNull(item.get(levelPropertyCode))) {
                Map map = (Map) item.get(levelPropertyCode);
                String levelPropertyValue = (String) map.get("id");

                if (pid.equals(levelPropertyValue)) {
                    children.add((String) item.get(DefaultPropertyType.ID.getCode()));
                    findChildrenOfCurrentNode(list, children, (String) item.get(DefaultPropertyType.ID.getCode()), levelPropertyCode);
                }
            }
        });
        return children;

    }


    @Override
    public Boolean validContainsGanttChildNode(String levelPropertyCode, String schemaCode, List<String> ids) {
        //分页信息
        int page = 0;
        int size = Integer.MAX_VALUE;
        BizObjectQueryObject bizObjectQueryObject = new BizObjectQueryObject() {
            @Override
            public String getSchemaCode() {
                return schemaCode;
            }

            @Override
            public List<String> getDisplayFields() {
                return Arrays.asList(new String[]{DefaultPropertyType.ID.getCode(), levelPropertyCode});
            }

            @Override
            public FilterExpression getFilterExpression() {
                return FilterExpression.empty;

            }

            @Override
            public Pageable getPageable() {
                return new PageableImpl(page, size);
            }

        };

        Page<Map<String, Object>> data = this.queryBizObjects(bizObjectQueryObject);

        List<? extends Map<String, Object>> content = data.getContent();
        if (CollectionUtils.isEmpty(content)) {
            return null;
        }
        boolean flag = true;
        for (int i = 0; i < ids.size(); i++) {
            String bizObjectId = ids.get(i);
            List<String> children = this.findChildrenOfCurrentNode((List<Map<String, Object>>) content, new ArrayList<String>(), bizObjectId, levelPropertyCode);
            if (CollectionUtils.isNotEmpty(children)) {
                //判断是否有交集
                Collection<String> intersection = CollectionUtils.intersection(ids, children);
                flag = intersection.size() == children.size();
                if (!flag) {
                    break;
                }
            }
        }
        return flag;


    }

    private boolean showParticipant(String queryId, ClientType clientType) {
        BizQueryPresent queryPresent = bizQueryPresentService.getBizQueryPresentByQueryId(queryId, clientType);
        if (queryPresent == null || !JSONUtil.isJson(queryPresent.getOptions())) {
            return false;
        }
        cn.hutool.json.JSONObject options = JSONUtil.parseObj(queryPresent.getOptions());
        Boolean bool = options.getBool(ModelConstant.QUERY_COLUMN_PARTICIPANTS_SWITCH);
        if (bool == null) {
            return false;
        }
        return bool;
    }

    public Page<Map<String, Object>> queryChildBizObjects(String userId, QueryDataModel queryDataModel) {

        final String childSchemaCode = queryDataModel.getChildSchemaCode();
        final String bizObjectId = queryDataModel.getBizObjectId();
        List<BizProperty> propertyList = bizPropertyService.getListBySchemaCode(childSchemaCode, true);
        if (CollectionUtils.isEmpty(propertyList)) {
            log.debug("[bizObject child query] bizProperty is null");
            throw new ServiceException(ErrCode.BIZ_QUERY_DISPLAY_COLUMNS_NOT_EMPTY);
        }
        int page = queryDataModel.getPage() == null ? 0 : queryDataModel.getPage();
        int size = queryDataModel.getSize();
        PageableImpl pageable = new PageableImpl(page * size, size);

        FilterExpression filter = getFiltersByQueryParam(queryDataModel, propertyList, null);

        ArrayList<String> displayFields = new ArrayList<>(listChildQueryColumn(queryDataModel, propertyList, queryDataModel.getColumns()));
        final Page<Map<String, Object>> content = bizObjectLocalCacheService.queryChlid(new BizObjectQueryObject() {

            @Override
            public String getSchemaCode() {
                return childSchemaCode;
            }

            @Override
            public FilterExpression getFilterExpression() {
                FilterExpression.Item parentIdEq = Q.it(DefaultSubPropertyType.PARENT_ID.getCode(), FilterExpression.Op.Eq, bizObjectId);
                if (filter == null || filter.isEmpty()) {
                    return parentIdEq;
                }
                return Q.and(parentIdEq, filter);
            }

            @Override
            public List<String> getDisplayFields() {
                return displayFields;
            }

            @Override
            public Pageable getPageable() {
                return pageable;
            }

            @Override
            public Sortable getSortable() {
                return new SortableImpl(Collections.singletonList(new OrderImpl(DefaultSubPropertyType.SORT_KEY.getCode(), Order.Dir.ASC)));
            }
        });
        return content;
    }

    private Set<String> listChildQueryColumn(QueryDataModel queryDataModel, List<BizProperty> propertyList, List<BizQueryColumnModel> columns) {
        BizObjectQueryModel.Options options = queryDataModel.getOptions();
        QueryDisplayType displayType = QueryDisplayType.DEFAULT;
        if (options != null) {
            displayType = options.getQueryDisplayType();
        }
        final Set<String> columnList = Arrays.stream(DefaultSubPropertyType.values()).map(DefaultSubPropertyType::getCode).collect(Collectors.toSet());
        switch (displayType) {
            case DEFAULT:
            case APPEND:
                Set<String> propertyCodes = propertyList.stream().filter(it -> it.getPropertyType() != BizPropertyType.COMMENT).map(BizProperty::getCode).collect(Collectors.toSet());
                if (CollectionUtils.isEmpty(columns)) {
                    columnList.addAll(propertyCodes);
                } else {
                    Set<String> queryColumnCodes = columns.stream()
                            .map(BizQueryColumnModel::getPropertyCode)
                            .filter(propertyCodes::contains).collect(Collectors.toSet());
                    if (!queryColumnCodes.isEmpty()) {
                        columnList.addAll(queryColumnCodes);
                        log.debug("[bizObject child query] queryColumnCodes={}", JSON.toJSONString(queryColumnCodes));
                    }
                }
                break;

            case OVERRIDE:
                final List<String> overRideColumns = options.getCustomDisplayColumns();
                if (CollectionUtils.isEmpty(overRideColumns)) {
                    log.debug("[bizObject child query] overRideColumns is null");
                    throw new ServiceException(ErrCode.BIZ_QUERY_COLUMNS_NOT_EMPTY_FOR_OVERRIDE, "子表查询，覆盖模式，字段为空");
                }
                if (log.isDebugEnabled()) {
                    log.debug("[bizObject child query] overRideColumns={}", JSON.toJSONString(overRideColumns));
                }
                columnList.addAll(overRideColumns);
                break;
            default:
                throw new ServiceException(ErrCode.BIZ_QUERY_DISPLAY_MODE_NOT_EMPTY, "查询列表显示模式不能为空.");
        }
        return columnList;
    }


    public void parseQuoteDatas(String schemaCode, Map<String, Object> param) {
        bizObjectService.parseQuoteDatas(schemaCode, param);
    }

    @Override
    public BizObject initBizObject(String userId, BizObjectModel bizObjectModel) {
        updateChildTableModifyProperties(userId, bizObjectModel);
        return bizObjectService.initBizObject(userId, bizObjectModel);
    }

    /**
     * 从视图中获取绑定的流程编码
     * @param schemaCode
     * @param queryCode
     * @return
     */
    private String obtainWorkflowCodeFromBizQuery(String schemaCode, String queryCode) {
        if (StringUtils.isBlank(queryCode)) {
            return "";
        }

        BizQueryModel bizQueryModel = bizQueryFacade.getBizQueryModel(schemaCode, queryCode, ClientType.PC);
        if (bizQueryModel == null) {
            return "";
        }

        Optional<BizQueryActionModel> optional = bizQueryModel.getQueryActions().stream().filter(action -> action.getQueryActionType() == QueryActionType.ADD).findFirst();
        if (!optional.isPresent()) {
            return "";
        }

        BizQueryActionModel bizQueryActionModel = optional.get();
        if (bizQueryActionModel.getQueryActionRelativeType() != QueryActionRelativeType.WORKFLOW) {
            return "";
        }

        return bizQueryActionModel.getRelativeCode();
    }

    /**
     * 校验流程发起权限
     * @param userId
     * @param workflowCode
     */
    private boolean checkWorkflowPermission(String userId, String workflowCode) {
        UserModel userModel = userFacade.get(userId);
        if (userModel == null) {
            return false;
        }

        List<DepartmentModel> departmentModels = workflowManagementFacade.getDepartmentsByWorkflowPermission(userId, workflowCode, false);
        if (CollectionUtils.isEmpty(departmentModels)) {
            return false;
        }

        Set<String> deptIds = departmentModels.stream().map(DepartmentModel::getId).collect(Collectors.toSet());
        if (!deptIds.contains(userModel.getDepartmentId())) {
            return false;
        }

        return true;
    }

    /**
     * 发起流程
     * @param bizObjectModels
     * @param workflowCode
     * @param userId
     * @param client
     * @param clientIP
     */
    private void startWorkflow(List<BizObjectModel> bizObjectModels, String workflowCode, String userId, String client, String clientIP) {
        if (CollectionUtils.isEmpty(bizObjectModels) || StringUtils.isBlank(workflowCode)) {
            return;
        }

        for (BizObjectModel bizObjectModel : bizObjectModels) {
            bizObjectModel.setWorkflowInstanceId(UUID.randomUUID().toString().replace("-", ""));
            String ownerId = StringUtils.isBlank(bizObjectModel.getOwnerId()) ? userId : bizObjectModel.getOwnerId();
            bizWorkflowInstanceService.startWorkflowInstance(null, ownerId, null,
                    workflowCode, bizObjectModel, null, clientIP, client,
                    Boolean.TRUE, Boolean.FALSE, ProcessRunMode.MANUAL);
        }
    }


    @PreDestroy
    public void shutdown() {
        batchInsertPool.shutdown();
        checkRelativePool.shutdown();
    }
}

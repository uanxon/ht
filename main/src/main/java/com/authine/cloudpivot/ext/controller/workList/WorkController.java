package com.authine.cloudpivot.ext.controller.workList;

import com.alibaba.fastjson.JSONObject;
import com.authine.cloudpivot.engine.api.facade.BizObjectFacade;
import com.authine.cloudpivot.engine.api.model.organization.UserModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkflowInstanceModel;
import com.authine.cloudpivot.engine.enums.status.SequenceStatus;
import com.authine.cloudpivot.ext.Utils.CustomSchemaCode;
import com.authine.cloudpivot.ext.Utils.Utils;
import com.authine.cloudpivot.ext.service.CloudSqlService;
import com.authine.cloudpivot.web.api.controller.base.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


@Slf4j
@RestController
@RequestMapping("/public/WorkController/")
public class WorkController extends BaseController {

    @Autowired
    CloudSqlService sqlService;

    /**
     * 工作记录  审批流完成后 数据写到-工作记录基础表
     */
    @RequestMapping("workRecordFinish")
    public void workRecordFinish(String bizId) {
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.WorkRecord, bizId);
        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get(CustomSchemaCode.WorkRecordDetail);
        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String approval = getFileBaseInfoWorkFlowApproval(bizObject);
        BizObjectFacade bizObjectFacade = getBizObjectFacade();
        String workRecordBasicTableName = bizObjectFacade.getTableName("workRecordBasic");
        for (Map<String, Object> map : list) {
            try {
                String workItem = MapUtils.getString(map, "workItem", "");
                Map<String, Object> data = Utils.fileInfoBaseMap(map, approval);


                data = fileInfoBaseMap(map, now, approval);


                String workRecordBasicId = "";
                if ("ALO 档案借出（5年）".equals(workItem)) {
                    //ALO 档案借出（5年）:不管什么时候都是新增
                    BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.WorkRecordBasic, data, false);
                    model.setSequenceStatus(SequenceStatus.COMPLETED.name());
                    workRecordBasicId = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                }

                if ("ICC IC卡".equals(workItem) || "CH 衣架".equals(workItem)) {
                    //ICC IC卡   确定唯一性条件：公司代码，人员代码，启用时间，状态说明字段里第一个斜杠之前的所有文本
                    //CH 衣架：跟ICC IC卡逻辑一样
                    //如果能查的出，说明不唯一，需要修改
                    String companyNumber = MapUtils.getString(map, "companyNumber", "");
                    String workRecordNumber = MapUtils.getString(map, "workRecordNumber", "");
//                    Date startTimeTemp = (Date) map.get("start_time");//登记日期
//                    String startTime = startTimeTemp == null ? "" : DateFormatUtils.format(startTimeTemp, "yyyy-MM-dd");
                    LocalDateTime startTimeTemp = (LocalDateTime) map.get("start_time");
                    String startTime =  ObjectUtils.isEmpty(startTimeTemp) ? "" : startTimeTemp.toLocalDate().toString();

                    String workContent01 = MapUtils.getString(map, "workContent", "");
                    String workContent = StringUtils.isBlank(workContent01) ? "is null" : "'"+workContent01+"'";

                    String workContentString = workContent.split("\\\\")[0] + "\\\\\\\\%";
                    StringBuffer selectSql = new StringBuffer("select id from ").append(workRecordBasicTableName).append(" where companyNumber = '").append(companyNumber)
                            .append("' and workRecordNumber = '").append(workRecordNumber)
                            .append("' and start_time = '").append(startTime)
                            .append("' and replace(workContent,'\\\\','')   = replace( ").append(workContent).append(" ,'\\\\','')  " )
                            .append(" and sequenceStatus = 'COMPLETED'");
                    List<Map<String, Object>> list1 = sqlService.getList(selectSql.toString());
                    log.info(JSONObject.toJSONString(list1));
                    if(list1 != null && list1.size() >0){
                        Map<String, Object> stringObjectMap = list1.get(0);
                        String id = MapUtils.getString(stringObjectMap, "id");
                        data.put("id", id);
                        }
                    BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.WorkRecordBasic, data, false);
                    model.setSequenceStatus(SequenceStatus.COMPLETED.name());
                    workRecordBasicId = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);

                }

//                if ("CH 衣架".equals(workItem)) {
//                    //跟ICC IC卡逻辑一样
//                }

                if ("WCS 工作装尺寸".equals(workItem) || "LS 皮鞋".equals(workItem)
                        || "LB 皮带".equals(workItem) || "CS 公章".equals(workItem)
                        || "ESP 环安张贴".equals(workItem) || "LPSS 劳保装尺寸".equals(workItem)) {
                    //WCS 工作装尺寸 到 LPSS 劳保装尺寸：公司，代码、启用时间、说明（此时用说明中的全部文本作为查询条件）
                    String companyNumber = MapUtils.getString(map, "companyNumber", "");
                    String workRecordNumber = MapUtils.getString(map, "workRecordNumber", "");
//                    Date startTimeTemp = (Date) map.get("start_time");//登记日期
//                    String startTime = startTimeTemp == null ? "" : DateFormatUtils.format(startTimeTemp, "yyyy-MM-dd");
                    LocalDateTime startTimeTemp = (LocalDateTime) map.get("start_time");
                    String startTime =  ObjectUtils.isEmpty(startTimeTemp) ? "" : startTimeTemp.toLocalDate().toString();

                    String workContent = MapUtils.getString(map, "workContent", "");
                    String workContentReplace = workContent.replace("\\", "\\\\");
                    StringBuffer selectSql = new StringBuffer("select id from ").append(workRecordBasicTableName).append(" where companyNumber = '").append(companyNumber)
                            .append("' and workRecordNumber = '").append(workRecordNumber).append("' and start_time = '").append(startTime)
                            .append("' and workContent = '").append(workContentReplace).append("' and sequenceStatus = 'COMPLETED'");
                    List<Map<String, Object>> list1 = sqlService.getList(selectSql.toString());
                    log.info(JSONObject.toJSONString(list1));
                    if(list1 != null && list1.size() >0){
                        Map<String, Object> stringObjectMap = list1.get(0);
                        String id = MapUtils.getString(stringObjectMap, "id");
                        data.put("id", id);
                        BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.WorkRecordBasic, data, false);
                        model.setSequenceStatus(SequenceStatus.COMPLETED.name());
                        workRecordBasicId = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                    }
                }

                //todo: 未提供存储过程
                workRecordinishOrModifiyProcess(workRecordBasicId);
            } catch (Exception e) {
                log.info("原始表数据:{}", JSONObject.toJSONString(map));
                e.printStackTrace();
            }
        }
    }

    /**
     * -------工作记录
     * exec   SyncItemMangeInfo
     *
     * @itemname nvarchar(50),            ----事项类型名称
     * @comid nvarchar(100),            ----公司ID
     * @funcode nvarchar(50),            ----职能代码
     * @txtUnit nvarchar(50),            ----手填单位
     * @txtQty nvarchar(50),            ----手填数量
     * @code nvarchar(100),            ----代码
     * @name nvarchar(100),            ----名称
     * @remark nvarchar(100),            ----备注
     * @desc nvarchar(100),            ----说明
     * @mandeptid nvarchar(100),            ----管理部门id
     * @begdate datetime,                ----启用时间
     * @status nvarchar(20),            ----状态
     * @enddate datetime,                ----封闭时间
     * @workDesc nvarchar(200),            ----工作说明
     * @auditor nvarchar(50)            ----审批人 02.0100 张三
     */
    private void workRecordinishOrModifiyProcess(String workRecordBasicId) {
        if (workRecordBasicId == null) {
            log.info("数据为空，写入存储过程失败");
            return;
        }

        //编写sql
        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.WorkRecordBasic);
        StringBuilder sql = new StringBuilder("SELECT * from ")
                .append(tableName).append(" where id ='")
                .append(workRecordBasicId).append("';");
        //查询到入参
        Map<String, Object> map = sqlService.getMap(sql.toString());

        log.info("入参map={}", map);

        String workItem = MapUtils.getString(map, "workItem", "");//事项类型名称
        String company = MapUtils.getString(map, "company", "");//公司ID
        String eventFunctionFNumber = MapUtils.getString(map, "eventFunctionFNumber", "");//职能代码
        String txtUnit = "";//手填单位??
        String txtQty = "";//手填数量??
        String workRecordNumber = MapUtils.getString(map, "workRecordNumber", "");//代码
        String abbreviation = MapUtils.getString(map, "abbreviation", "");//名称
        String remarke = MapUtils.getString(map, "remarke", "");//备注
        String content = MapUtils.getString(map, "content", "");//说明
        String responsibleDeptFid = MapUtils.getString(map, "responsibleDeptFid", "");//管理部门id
//        Date startTimeTemp = (Date) map.get("start_time");//启用时间
//        String startTime = startTimeTemp == null ? "" : DateFormatUtils.format(startTimeTemp, "yyyy-MM-dd");
        LocalDateTime startTimeTemp = (LocalDateTime) map.get("start_time");
        String startTime =  ObjectUtils.isEmpty(startTimeTemp) ? "" : startTimeTemp.toLocalDate().toString();

        String status = MapUtils.getString(map, "status", "");//状态
        switch (status) {
            case "√":
                status = "yes";
                break;
            case "×":
                status = "no";
                break;
            case "/":
                status = "";
                break;
        }
//        Date closingTimeTemp = (Date) map.get("closingTime");//封闭时间
//        String closingTime = closingTimeTemp == null ? "" : DateFormatUtils.format(closingTimeTemp, "yyyy-MM-dd");
        LocalDateTime closingTimeTemp = (LocalDateTime) map.get("closingTime");
        String closingTime =  ObjectUtils.isEmpty(closingTimeTemp) ? "" : closingTimeTemp.toLocalDate().toString();

        String workContent = MapUtils.getString(map, "workContent", "");//工作说明
        String approval = MapUtils.getString(map, "approval", "");//审批人

        String execSql = String.format("exec [HG_LINK].[hg].[dbo].SyncItemMangeInfo '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'",
                workItem, company, eventFunctionFNumber, txtUnit, txtQty,
                workRecordNumber, abbreviation, remarke, content, responsibleDeptFid,
                startTime, status, closingTime, workContent, approval);
        log.info("\n==========准备调用[]存储过程:{}", execSql);
        sqlService.execute(CloudSqlService.htEas, execSql);
        log.info("\n=============[]存储过程执行完成");
    }


    @RequestMapping("workRecordToVoid")
    public void workRecordToVoid(String bizId) {
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.WorkRecordCancel, bizId);
        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get(CustomSchemaCode.WorkRecordDetailCancel);
        //获取审批人
        String approval = getFileBaseInfoWorkFlowApproval(bizObject);
        BizObjectFacade bizObjectFacade = getBizObjectFacade();
        String workRecordBasicTableName = bizObjectFacade.getTableName("workRecordBasic");
        for (Map<String, Object> map : list) {
            try {
                String workItem = MapUtils.getString(map, "workItem", "");
                Map<String, Object> data = Utils.fileInfoBaseMap(map, approval);

                if ("ALO 档案借出（5年）".equals(workItem)) {
                    //ALO 档案借出（5年）:不管什么时候都是新增
                    BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.WorkRecordBasic, data, false);
                    model.setSequenceStatus(SequenceStatus.COMPLETED.name());
                    getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                }

                if ("ICC IC卡".equals(workItem) || "CH 衣架".equals(workItem)) {
                    //ICC IC卡   确定唯一性条件：公司代码，人员代码，启用时间，状态说明字段里第一个斜杠之前的所有文本
                    //CH 衣架：跟ICC IC卡逻辑一样
                    //如果能查的出，说明不唯一，需要修改
                    String companyNumber = MapUtils.getString(map, "companyNumber", "");
                    String workRecordNumber = MapUtils.getString(map, "workRecordNumber", "");
//                    Date startTimeTemp = (Date) map.get("start_time");//登记日期
//                    String startTime = startTimeTemp == null ? "" : DateFormatUtils.format(startTimeTemp, "yyyy-MM-dd");
                    LocalDateTime startTimeTemp = (LocalDateTime) map.get("start_time");
                    String startTime =  ObjectUtils.isEmpty(startTimeTemp) ? "" : startTimeTemp.toLocalDate().toString();

                    String workContent = MapUtils.getString(map, "workContent", "");
                    String workContentString = workContent.split("\\\\")[0] + "\\\\\\\\%";
                    StringBuffer selectSql = new StringBuffer("select id from ").append(workRecordBasicTableName).append(" where companyNumber = '").append(companyNumber)
                            .append("' and workRecordNumber = '").append(workRecordNumber).append("' and start_time = '").append(startTime)
                            .append("' and workContent like '").append(workContentString).append("' and sequenceStatus = 'COMPLETED'");
                    List<Map<String, Object>> list1 = sqlService.getList(selectSql.toString());
                    log.info(JSONObject.toJSONString(list1));
                    for (Map<String, Object> stringObjectMap : list1) {
                        String id = MapUtils.getString(stringObjectMap, "id");
                        data.put("id", id);
                        BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.WorkRecordBasic, data, false);
                        model.setSequenceStatus(SequenceStatus.COMPLETED.name());
                        getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                    }
                }

//                if ("CH 衣架".equals(workItem)) {
//                    //跟ICC IC卡逻辑一样
//                }

                if ("WCS 工作装尺寸".equals(workItem) || "LS 皮鞋".equals(workItem)
                        || "LB 皮带".equals(workItem) || "CS 公章".equals(workItem)
                        || "ESP 环安张贴".equals(workItem) || "LPSS 劳保装尺寸".equals(workItem)) {
                    //WCS 工作装尺寸 到 LPSS 劳保装尺寸：公司，代码、启用时间、说明（此时用说明中的全部文本作为查询条件）
                    String companyNumber = MapUtils.getString(map, "companyNumber", "");
                    String workRecordNumber = MapUtils.getString(map, "workRecordNumber", "");
//                    Date startTimeTemp = (Date) map.get("start_time");//登记日期
//                    String startTime = startTimeTemp == null ? "" : DateFormatUtils.format(startTimeTemp, "yyyy-MM-dd");
                    LocalDateTime startTimeTemp = (LocalDateTime) map.get("start_time");
                    String startTime =  ObjectUtils.isEmpty(startTimeTemp) ? "" : startTimeTemp.toLocalDate().toString();

                    String workContent = MapUtils.getString(map, "workContent", "");
                    String workContentReplace = workContent.replace("\\", "\\\\");
                    StringBuffer selectSql = new StringBuffer("select id from ").append(workRecordBasicTableName).append(" where companyNumber = '").append(companyNumber)
                            .append("' and workRecordNumber = '").append(workRecordNumber).append("' and start_time = '").append(startTime)
                            .append("' and workContent = '").append(workContentReplace).append("' and sequenceStatus = 'COMPLETED'");
                    List<Map<String, Object>> list1 = sqlService.getList(selectSql.toString());
                    log.info(JSONObject.toJSONString(list1));
                    for (Map<String, Object> stringObjectMap : list1) {
                        String id = MapUtils.getString(stringObjectMap, "id");
                        data.put("id", id);
                        BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.WorkRecordBasic, data, false);
                        model.setSequenceStatus(SequenceStatus.COMPLETED.name());
                        getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                    }
                }

                //todo: 未提供存储过程
                //调用存储过程
                workRecordToVoidProcess(map);
            } catch (Exception e) {
                log.info("参数：{}", JSONObject.toJSONString(map));
                e.printStackTrace();
            }
        }
    }

    /**
     * -------工作记录 作废
     * exec     ItemMangeInfoDisable
     *
     * @param map
     * @itemname nvarchar(50),            ----事项类型名称
     * @comid nvarchar(100),            ----公司ID
     * @funcode nvarchar(50),            ----职能代码
     * @code nvarchar(100),            ----代码
     * @begdate datetime,                ----启用时间
     * @workDesc nvarchar(200),            ----工作说明
     * @auditor nvarchar(50)            ----审批人 02.0100 张三
     */
    private void workRecordToVoidProcess(Map<String, Object> map) {
        if (map == null) {
            log.info("数据为空，写入存储过程失败");
            return;
        }

        String workItem = MapUtils.getString(map, "workItem", "");//事项类型名称
        String company = MapUtils.getString(map, "company", "");//公司ID
        String eventFunctionFNumber = MapUtils.getString(map, "eventFunctionFNumber", "");//职能代码
        String workRecordNumber = MapUtils.getString(map, "workRecordNumber", "");//代码
//        Date startTimeTemp = (Date) map.get("start_time");//启用时间
//        String startTime = startTimeTemp == null ? "" : DateFormatUtils.format(startTimeTemp, "yyyy-MM-dd");
        LocalDateTime startTimeTemp = (LocalDateTime) map.get("start_time");
        String startTime =  ObjectUtils.isEmpty(startTimeTemp) ? "" : startTimeTemp.toLocalDate().toString();

        String content = MapUtils.getString(map, "content", "");//说明
        String approval = MapUtils.getString(map, "approval", "");//审批人

        String execSql = String.format("exec [HG_LINK].[hg].[dbo].ItemMangeInfoDisable '%s','%s','%s','%s','%s','%s','%s'",
                workItem, company, eventFunctionFNumber, workRecordNumber, startTime, content, approval);
        log.info("\n==========准备调用[]存储过程:{}", execSql);
        sqlService.execute(CloudSqlService.htEas, execSql);
        log.info("\n=============[]存储过程执行完成");
    }


    //工作事项只是基础表，没有流程
//    /**
//     * 工作事项  审批流完成后 数据写到-工作事项基础表
//     */
//    @RequestMapping("workItemFinish")
//    public void workItemFinish(String bizId) {
//        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.WorkItem, bizId);
//        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get(CustomSchemaCode.WorkItemDetail);
//        String approval = getFileBaseInfoWorkFlowApproval(bizObject, "Activity3");
//        for (Map<String, Object> map : list) {
//            try {
//                Map<String, Object> data = Utils.fileInfoBaseMap(map, approval);
//                // 判断是否是修改
//                String basicId = MapUtils.getString(map, "basicId", "");
//                if (StringUtils.isNotBlank(basicId)) {
//                    //基础信息表id
//                    data.put("id", basicId);
//                }
//                BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.WorkItemBasic, data, false);
//                model.setSequenceStatus(SequenceStatus.COMPLETED.name());
//                getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
//            } catch (Exception e) {
//                log.info("原始表数据:{}", JSONObject.toJSONString(map));
//                e.printStackTrace();
//            }
//        }
//    }
//
//
//    @RequestMapping("workItemToVoid")
//    public void workItemToVoid(String bizId) {
//        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.WorkItemCancel, bizId);
//        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get(CustomSchemaCode.WorkItemDetailCancel);
//        //获取审批人
//        String approval = getFileBaseInfoWorkFlowApproval(bizObject, "Activity3");
//        for (Map<String, Object> map : list) {
//            try {
//                Map<String, Object> data = Utils.fileInfoBaseMap(map, approval);
//                String basicId = MapUtils.getString(map, "basicId", "");
//                if (StringUtils.isNotBlank(basicId)) {
//                    //基础信息表id
//                    data.put("id", basicId);
//                }
//                BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.WorkItemBasic, data, false);
//                model.setSequenceStatus(SequenceStatus.CANCELED.name());
//                getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
//            } catch (Exception e) {
//                log.info("参数：{}", JSONObject.toJSONString(map));
//                e.printStackTrace();
//            }
//        }
//    }


    /**
     * 查询审批人,返回  员工号+姓名
     *
     * @param bizObject
     * @return
     */
    private String getFileBaseInfoWorkFlowApproval(BizObjectCreatedModel bizObject) {
        WorkflowInstanceModel instanceModel = getWorkflowInstanceFacade().getByObjectId(bizObject.getId());
        List<WorkItemModel> workItems = getWorkflowInstanceFacade().getWorkItems(instanceModel.getId(), true);
        final String finalActivityCode = "Activity15";
        Optional<WorkItemModel> first = workItems.stream().filter(a -> a.getActivityCode().equals(finalActivityCode)).findFirst();
        String participant = null;
        if (first.isPresent()) {
            WorkItemModel workItemModel = first.get();
            participant = workItemModel.getParticipant();
        }

        if (participant == null) {
            final String finalActivityCode2 = "Activity5";
            Optional<WorkItemModel> second = workItems.stream().filter(a -> a.getActivityCode().equals(finalActivityCode2)).findFirst();
            if (second.isPresent()) {
                WorkItemModel workItemModel = second.get();
                participant = workItemModel.getParticipant();
            }
        }


        if (participant == null) {
            participant = bizObject.getCreater().getId();
        }

        UserModel user = getOrganizationFacade().getUser(participant);
        return new StringBuilder(user.getEmployeeNo()).append("　").append(user.getName()).toString();
    }


    private Map<String,Object> fileInfoBaseMap(Map<String,Object> map,String auditDate,String auditer){
        Map<String, Object> data = new HashMap<>();
        //公司
        data.put("company",map.get("company"));
        //公司代码
        data.put("companyNumber",map.get("companyNumber"));
        //公司名称
        data.put("companyName",map.get("companyName"));
        //事项职能
        data.put("eventFunction",map.get("eventFunction"));
        //事项职能代码
        data.put("eventFunctionFNumber",map.get("eventFunctionFNumber"));
        //事项职能部门代码
        data.put("eventFunctionFNumber",map.get("eventFunctionFNumber"));

        //工作事项
        data.put("workItemBasic",map.get("workItemBasic"));
        //工作事项
        data.put("workItem",map.get("workItem"));
        //对应记录
        data.put("record",map.get("record"));
        //档案代码
        data.put("companyArchives",map.get("companyArchives"));
        //人员代码
        data.put("personnel",map.get("personnel"));

        //代码
        data.put("workRecordNumber",map.get("workRecordNumber"));
        //名称
        data.put("abbreviation",map.get("abbreviation"));
        //备注
        data.put("remarke",map.get("remarke"));
        //说明
        data.put("content",map.get("content"));
        //管理部门代码
        data.put("responsibleDeptFid",map.get("responsibleDeptFid"));
        //管理部门代码
        data.put("responsibleDeptFNumber",map.get("responsibleDeptFNumber"));
        //管理部门
        data.put("responsibleDept",map.get("responsibleDept"));

        //启用时间
        data.put("start_time",map.get("start_time"));
        //状态
        data.put("status",map.get("status"));
        //封闭时间
        data.put("closing_time",map.get("closing_time"));
        //状态说明
        data.put("workContent",map.get("workContent"));
        //状态说明
        data.put("workRecordBasic",map.get("workRecordBasic"));



        //审批人
        data.put("auditer",auditer);

        return data;
    }
}

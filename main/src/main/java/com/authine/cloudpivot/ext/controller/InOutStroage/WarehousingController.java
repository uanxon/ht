package com.authine.cloudpivot.ext.controller.InOutStroage;

import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.engine.enums.status.SequenceStatus;
import com.authine.cloudpivot.ext.Utils.CustomSchemaCode;
import com.authine.cloudpivot.ext.service.CloudSqlService;
import com.authine.cloudpivot.web.api.controller.base.BaseController;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName WarehousingController
 * @Description TODO
 * @Author fengjie
 * @CreatedTime 2023/9/12 15:23
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/public/warehousing")
public class WarehousingController extends BaseController {

    public static Map<String,String> warehousingMap = Maps.newHashMap();
    static {
        warehousingMap.put("purchaseIntostorage", "01 采购");
        warehousingMap.put("ProductInto", "02 生产");
        warehousingMap.put("OtherInto", "03 其他");
        warehousingMap.put("TransferInto", "04 调拨");
    }

    @Autowired
    CloudSqlService sqlService;

    /**
     * 获取入库批次号
     *
     * @return
     */
    @RequestMapping("getWarehousingNum")
    public String getWarehousingNum(@RequestParam String entryDate, @RequestParam String schemaCode) {
        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.WAREHOUSING_COUNTER);
        StringBuilder sql = new StringBuilder("select * from ").append(tableName)
                .append(" where entryDate = '").append(entryDate).append("'")
                .append(" and schemaCode = '").append(schemaCode).append("'")
                .append(" order by createdTime desc limit 1");
        Map<String, Object> map = sqlService.getMap(sql.toString());
        String id = MapUtils.getString(map, "id");
        Integer count = MapUtils.getInteger(map, "count", 0);
        Integer flag = count + 1;
        String format = String.format("%03d", flag);
        Map<String, Object> data = new HashMap<>();
        if (StringUtils.isNotBlank(id)) {
            data.put("id", id);
            data.put("count", flag);
        } else {
            data.put("entryDate", entryDate);
            data.put("schemaCode", schemaCode);
            data.put("schemaName", warehousingMap.get(schemaCode));
            data.put("count", 1);

        }

        BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.WAREHOUSING_COUNTER, data, false);
        model.setSequenceStatus(SequenceStatus.COMPLETED.name());
        getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
        return format;
    }
}

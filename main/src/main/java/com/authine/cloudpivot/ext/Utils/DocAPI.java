package com.authine.cloudpivot.ext.Utils;


import net.sf.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *  浩通发过来的docApi
 */
public class DocAPI {

    String jsonParam="";

    public JSONObject Vedio(String interfaceId, String jsonParam)
    {

        String result=executeInterface(interfaceId,jsonParam);
        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
        return json_test;
    }


    public  String  executeInterface(String interfaceId, String jsonParam)
    {
        String message="";
        try {

            URL url=new URL("http://ip:port/admin/services/AdminWebService?wsdl");
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("interfaceId=");
            sb.append(interfaceId+"&");
            sb.append("jsonParam=");
            sb.append(jsonParam+"&");
            out.write(sb.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;

    }


    public  JSONObject  getCopyFile(String token,String fileIds,String newParentId)
    {

        String url="http://47.104.86.126:188/app/file/copy-file";
        JSONObject obj = new JSONObject();
        String result=PostCopyFile(url,obj,token,fileIds,newParentId);

        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//       String code = json_test.getString("code");
        return json_test;
    }





    public static String PostCopyFile(String ADD_URL,JSONObject obj,String token,String fileIds,String newParentId) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            String token1=token;
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("system=");
            sb.append("web&");
            sb.append("token=");
            sb.append(token1+"&");
            sb.append("fileIds=");
            sb.append(fileIds+"&");
            sb.append("newParentId=");
            sb.append(newParentId);

            out.write(sb.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }




    public  JSONObject  getListFileAuth(String token,String fileId)
    {

        String url="http://47.104.86.126:188/app/file/list-file-auth";
        JSONObject obj = new JSONObject();
        String result=PostListFileAuth(url,obj,token,fileId);

        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//	       String code = json_test.getString("code");
        return json_test;
    }





    public static String PostListFileAuth(String ADD_URL,JSONObject obj,String token,String fileId) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            String token1=token;
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("system=");
            sb.append("web&");
            sb.append("token=");
            sb.append(token1+"&");
            sb.append("fileId=");
            sb.append(fileId);
            out.write(sb.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }




    public  JSONObject  getListUser(String token)
    {

        String url="http://47.104.86.126:188/app/file/list-all-user";
        JSONObject obj = new JSONObject();
        String result=PostListUser(url,obj,token);

        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//	       String code = json_test.getString("code");
        return json_test;
    }





    public static String PostListUser(String ADD_URL,JSONObject obj,String token) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            String token1=token;
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("system=");
            sb.append("web&");
            sb.append("token=");
            sb.append(token1+"&");
            sb.append("onlyUser=");
            sb.append("1");
            out.write(sb.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }

    public  JSONObject  getRename(String token,String  fileId,String name)
    {

        String url="http://47.104.86.126:188/app/file/rename-item";
        JSONObject obj = new JSONObject();
        String result=PostRename(url,obj,token,fileId,name);

        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//	       String code = json_test.getString("code");
        return json_test;
    }





    public static String PostRename(String ADD_URL,JSONObject obj,String token,String  fileId,String name) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            String token1=token;
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            // connection.setRequestProperty("Content-type","application/x-javascript->json");
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("system=");
            sb.append("web&");
            sb.append("token=");
            sb.append(token1+"&");
            sb.append("fileId=");
            sb.append(fileId+"&");
            sb.append("name=");
            sb.append(name);
            out.write(sb.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }



    public  JSONObject  getRemoveFileAuth(String token,String  authId)
    {

        String url="http://47.104.86.126:188/app/file/remove-file-auth";
        JSONObject obj = new JSONObject();
        String result=PostRemoveFileAuth(url,obj,token,authId);

        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//	       String code = json_test.getString("code");
        return json_test;
    }





    public static String PostRemoveFileAuth(String ADD_URL,JSONObject obj,String token,String authId) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            String token1=token;
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            // connection.setRequestProperty("Content-type","application/x-javascript->json");
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("system=");
            sb.append("web&");
            sb.append("token=");
            sb.append(token1+"&");
            sb.append("authId=");
            sb.append(authId);
            out.write(sb.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }



    public  JSONObject  getListUserAuth(String token,String  userId)
    {

        String url="http://47.104.86.126:188/app/file/list-user-auth";
        JSONObject obj = new JSONObject();
        String result=PostListUserAuth(url,obj,token,userId);

        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//	       String code = json_test.getString("code");
        return json_test;
    }





    public static String PostListUserAuth(String ADD_URL,JSONObject obj,String token,String userId) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            String token1=token;
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            // connection.setRequestProperty("Content-type","application/x-javascript->json");
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("system=");
            sb.append("web&");
            sb.append("token=");
            sb.append(token1+"&");
            sb.append("userId=");
            sb.append(userId);
            out.write(sb.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }



    public  JSONObject  getFileInfo(String token,String  fileId)
    {

        String url="http://47.104.86.126:188/app/file/get-fileinfo";
        JSONObject obj = new JSONObject();
        String result=PostFileInfo(url,obj,token,fileId);

        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//	       String code = json_test.getString("code");
        return json_test;
    }





    public static String PostFileInfo(String ADD_URL,JSONObject obj,String token,String fileId) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            String token1=token;
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            // connection.setRequestProperty("Content-type","application/x-javascript->json");
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("system=");
            sb.append("web&");
            sb.append("token=");
            sb.append(token1+"&");
            sb.append("fileIds=");
            sb.append(fileId);



            out.write(sb.toString());

//            out.write(obj.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }





    public  static JSONObject  getFullPath(String  fileId)
    {

        String url="http://47.104.86.126:188/app/file/get-fullpath";
        JSONObject obj = new JSONObject();
        String result=PostFullPath(url,obj,getToken(),fileId);

        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//	       String code = json_test.getString("code");
        return json_test;
    }





    public static String PostFullPath(String ADD_URL,JSONObject obj,String token,String fileId) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            String token1=token;
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            // connection.setRequestProperty("Content-type","application/x-javascript->json");
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("system=");
            sb.append("web&");
            sb.append("token=");
            sb.append(token1+"&");
            sb.append("fileId=");
            sb.append(fileId);



            out.write(sb.toString());

//            out.write(obj.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }







    public  String  getFolderId(String token,String fullpath)
    {

        String url="http://47.104.86.126:188/app/file/find-fileid-by-path";
        JSONObject obj = new JSONObject();
        String result=PostFolderId(url,obj,fullpath);

        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);

        String id = json_test.getString("fileId");
//	       String code = json_test.getString("code");
        return id;
    }





    public static String PostFolderId(String ADD_URL,JSONObject obj,String fullpath) {
        String message="";
        try {
            URL url=new URL(ADD_URL);

            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            // connection.setRequestProperty("Content-type","application/x-javascript->json");
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("system=");
            sb.append("web&");
            sb.append("token=");
            sb.append(getToken()+"&");
            sb.append("fullpath=");
            sb.append(fullpath);

            out.write(sb.toString());

//            out.write(obj.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }





    public  static JSONObject  getRoleAuth(String roleName,String userId,String auth,String  fileId)
    {

        String url="http://47.104.86.126:188/app/file/add-file-auth";
        JSONObject obj = new JSONObject();
        String result=PostRoleAuth(url,obj,userId,auth,fileId);

        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//	       String code = json_test.getString("code");
        return json_test;
    }





    public static String  PostRoleAuth(String ADD_URL,JSONObject obj,String userId,String auth,String fileId) {
        String message="";
        try {
            URL url=new URL(ADD_URL);

            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            // connection.setRequestProperty("Content-type","application/x-javascript->json");
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("system=");
            sb.append("web&");
            sb.append("token=");
            sb.append(getToken()+"&");
            sb.append("type=");
            sb.append("3&");
            sb.append("userId=");
            sb.append(userId+"&");
            sb.append("auth=");
            sb.append(auth+"&");
            sb.append("fileId=");
            sb.append(fileId);



            out.write(sb.toString());

//            out.write(obj.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }





    public static   JSONObject  getRole(String roleName)
    {

//		DocAPI api=new DocAPI();
//		String token =api.getToken();
//

//		String url="http://47.104.86.126:188/app/user/login";
        String url="http://47.104.86.126:188/app/file/find-roleid-by-name";
//		String url1="http://47.104.86.126:188/app/file/list-dir";
        JSONObject obj = new JSONObject();
        String result=PostRole(url,obj,roleName);

        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//	       String code = json_test.getString("code");
        return json_test;
    }





    public static String PostRole(String ADD_URL,JSONObject obj,String roleName) {
        String message="";
        try {
            URL url=new URL(ADD_URL);

            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            // connection.setRequestProperty("Content-type","application/x-javascript->json");
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
//            sb.append("username=");
//            sb.append("admin&");
//            sb.append("password=");
//            sb.append("123456&");
            sb.append("token=");
            sb.append(getToken()+"&");
            sb.append("roleName=");
//			sb.append(fileid+ "&");
            sb.append(roleName+"&");
            sb.append("system=");
            sb.append("web");
            out.write(sb.toString());

//            out.write(obj.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }




    public static  JSONObject  getFileID(String attribute)  {
        String url="http://47.104.86.126:188/app/file/find-file-by-attribute";
//		String url="http://47.104.86.126:188/app/file/list-dir";
//		String url1="http://47.104.86.126:188/app/file/list-dir";
        JSONObject obj = new JSONObject();
        String result=PostFileID(url,obj,getToken(),attribute);
        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//	       String code = json_test.getString("code");
        return json_test;
    }

    public static String PostFileID(String ADD_URL,JSONObject obj,String token,String attribute) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            String token1=token;
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            // connection.setRequestProperty("Content-type","application/x-javascript->json");
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
//            sb.append("username=");
//            sb.append("admin&");
//            sb.append("password=");
//            sb.append("123456&");
            sb.append("token=");
            sb.append(token1+"&");
            sb.append("attribute=");
            sb.append(attribute+ "&");
            sb.append("system=");
            sb.append("web");
            out.write(sb.toString());

//            out.write(obj.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }





    public static String  getToken(){
        String token;
/*        String url="http://47.104.86.126:188/app/user/login";
//		String url="http://47.104.86.126:188/app/file/list-dir";
//		String url1="http://47.104.86.126:188/app/file/list-dir";
        JSONObject obj = new JSONObject();
        obj.element("username", "test2");
        obj.element("password", "123456");
        obj.element("system","web");

        String result=Post(url,obj);
//	    String result1=Postone(url,obj);
        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
        System.out.println(json_test.get("data")) ;
        JSONObject json_data=JSONObject.fromObject(json_test.get("data"));
//	    System.out.println(json_data.get("token"));
//	    String  token="";
//	    String  name="";
        token=json_data.get("token").toString();
//	    name=json_data.get("name").toString();
//   test1的token*/
        token = "6724858270fe01ae420589f772510522585c35e893e29d5e1c84dd83cd15732a18b893";
        return token;
    }

//    String str =result.toString();
//    JSONObject json_test = JSONObject.fromObject(str);
////       String code = json_test.getString("code");
//	return json_test;

    public static String Post(String ADD_URL,JSONObject obj) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            // connection.setRequestProperty("Content-type","application/x-javascript->json");
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("username=");
            sb.append("test2&");
            sb.append("password=");
            sb.append("123456&");
//            sb.append("token=");
//            sb.append("06f125b31d278f1bafd715ead8e65d77b24ee2d24ba44bf2da6eeb9270f1262d186a1&");
//            sb.append("parentId=");
//            sb.append("18&");
            sb.append("system=");
            sb.append("web");
            out.write(sb.toString());

//            out.write(obj.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }



    public static  String getMove(String fileid,String destfullpath) {

        // DocAPI api=new DocAPI();
        // String token =api.getToken();
        //

        // String url="http://47.104.86.126:188/app/user/login";
        String url = "http://47.104.86.126:188/app/file/move-file";
        // String url1="http://47.104.86.126:188/app/file/list-dir";
        JSONObject obj = new JSONObject();
        String result = Postmove(url, obj, getToken(),fileid,destfullpath);

        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
        String code = json_test.getString("code");
//	       JSONArray array = json_test.getJSONArray("data");
//	        for(int i = 0; i < array.size(); i++)
//	        {
//	            JSONObject o = array.getJSONObject(i);
//	            System.out.println(o.get("rootname"));
//	            System.out.println(o.get("name"));
//	        }
//        JSONArray array = json_test.getJSONArray("code");
//	    System.out.println(json_test.get("code")) ;
        return code;
    }

    public static String Postmove(String ADD_URL, JSONObject obj, String token,String fileid,String destfullpath) {
        String message = "";
        try {
            URL url = new URL(ADD_URL);
            String token1 = token;
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            // connection.setRequestProperty("Content-type",
            // "application/x-javascript->json");
            connection.setRequestProperty("Content-type", "application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection
                    .getOutputStream(), "UTF-8");
            StringBuffer sb = new StringBuffer();
            // sb.append("username=");
            // sb.append("admin&");
            // sb.append("password=");
            // sb.append("123456&");
            sb.append("token=");
            sb.append(token1 + "&");
            sb.append("fileIds=");
            sb.append(fileid + "&");
            sb.append("destfullpath=");
            sb.append(destfullpath+ "&");
            sb.append("system=");
            sb.append("web");
            out.write(sb.toString());

            // out.write(obj.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    connection.getInputStream(), "UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message = sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }



    public static JSONObject getList(String parentId)
    {

        String url="http://47.104.86.126:188/app/file/list-dir";
        JSONObject obj = new JSONObject();
        String result=PostList(url,obj,getToken(),parentId);
        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//	       String code = json_test.getString("code");
        return json_test;
    }





    public static String PostList(String ADD_URL,JSONObject obj,String token,String parentId) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            String token1=token;
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("token=");
            sb.append(token1+"&");
            sb.append("parentId=");
//			sb.append(fileid+ "&");
            sb.append(parentId+"&");
            sb.append("system=");
            sb.append("web");
            out.write(sb.toString());

//            out.write(obj.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }



    public  String  getUpdate(String token,String fileid,String fileid2)
    {

//		DocAPI api=new DocAPI();
//		String token =api.getToken();
//

//		String url="http://47.104.86.126:188/app/user/login";
        String url="http://47.104.86.126:188/app/file/update-move-file";
//		String url1="http://47.104.86.126:188/app/file/list-dir";
        JSONObject obj = new JSONObject();
        String result=PostUpdate(url,obj,token,fileid,fileid2);

        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
        String code = json_test.getString("code");
        return code;
    }





    public static String PostUpdate(String ADD_URL,JSONObject obj,String token,String fileid,String fileid2) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            String token1=token;
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            // connection.setRequestProperty("Content-type","application/x-javascript->json");
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
//            sb.append("username=");
//            sb.append("admin&");
//            sb.append("password=");
//            sb.append("123456&");
            sb.append("token=");
            sb.append(token1+"&");
            sb.append("fileId=");
            sb.append(fileid+ "&");
            sb.append("destFileid=");//destFileid
            sb.append(fileid2+"&");
            sb.append("system=");
            sb.append("web");
            out.write(sb.toString());

//            out.write(obj.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }


    public  JSONObject  getRemove(String token,String fileIds)
    {

        String url="http://47.104.86.126:188/app/file/remove-file";
        JSONObject obj = new JSONObject();
        String result=PostRemove(url,obj,token,fileIds);
        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//	       String code = json_test.getString("code");
        return json_test;
    }





    public static String PostRemove(String ADD_URL,JSONObject obj,String token,String fileIds) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            String token1=token;
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("token=");
            sb.append(token1+"&");
            sb.append("fileIds=");
//			sb.append(fileid+ "&");
            sb.append(fileIds+"&");
            sb.append("system=");
            sb.append("web");
            out.write(sb.toString());

//            out.write(obj.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }

    public static JSONObject  getAddFolder(String parentId,String name,String remarks)
    {

        String url="http://47.104.86.126:188/app/file/add-folder";
        JSONObject obj = new JSONObject();
        String result=PostAddFolder(url,obj,getToken(),parentId,name,remarks);
        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//	       String code = json_test.getString("code");
        return json_test;
    }





    public static String PostAddFolder(String ADD_URL,JSONObject obj,String token,String parentId,String name,String remarks) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            String token1=token;
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("token=");
            sb.append(token1+"&");
            sb.append("parentId=");
            sb.append(parentId+"&");
            sb.append("name=");
            sb.append(name+"&");
            sb.append("remarks=");
            sb.append(remarks+"&");
            sb.append("system=");
            sb.append("web");
            out.write(sb.toString());

//            out.write(obj.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }




    public  JSONObject  getAddusertorole(String token,String uid,String rid  )
    {

        String url="http://47.104.86.126:188/app/file/add-user-to-role";
        JSONObject obj = new JSONObject();
        String result=PostAddusertorole(url,obj,token,uid,rid);
        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//	       String code = json_test.getString("code");
        return json_test;
    }





    public static String PostAddusertorole(String ADD_URL,JSONObject obj,String token,String uid,String rid) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            String token1=token;
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("token=");
            sb.append(token1+"&");
            sb.append("uid=");
            sb.append(uid+"&");
            sb.append("rid=");
            sb.append(rid+"&");
            sb.append("system=");
            sb.append("web");
            out.write(sb.toString());

//            out.write(obj.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }

    public  JSONObject  getfinduseridbyname(String token,String realname  )
    {

        String url="http://47.104.86.126:188/app/file/find-userid-by-name";
        JSONObject obj = new JSONObject();
        String result=Postfinduseridbyname(url,obj,token,realname  );
        String str =result.toString();
        JSONObject json_test = JSONObject.fromObject(str);
//	       String code = json_test.getString("code");
        return json_test;
    }





    public static String Postfinduseridbyname(String ADD_URL,JSONObject obj,String token,String realname ) {
        String message="";
        try {
            URL url=new URL(ADD_URL);
            String token1=token;
            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            connection.setRequestProperty("Content-type","application/json");
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            StringBuffer sb=new StringBuffer();
            sb.append("token=");
            sb.append(token1+"&");
            sb.append("realname =");
            sb.append(realname+"&");
//            sb.append("realname =");
//            sb.append(realname +"&");
            sb.append("system=");
            sb.append("web");
            out.write(sb.toString());

//            out.write(obj.toString());
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            String lines;
            StringBuffer sb1 = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb1.append(lines);
            }
            System.out.println(sb1);

            message=sb1.toString();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }


}
package com.authine.cloudpivot.ext.controller;

import com.authine.cloudpivot.engine.api.facade.BizObjectFacade;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.web.api.controller.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;



@RestController
@RequestMapping("/api/htCommon")
public class HtCommonController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(HtCommonController.class);

    /**
     * 更改表流程id生成规则
     *
     * @param objId
     * @return
     */
    @RequestMapping(value="/changeRuleBySequenceNo",method = {RequestMethod.POST ,RequestMethod.GET })
    @ApiOperation(value = "更改表流程id生成规则", notes = "更改表流程id生成规则")
    public void changeRuleBySequenceNo(String objId,String processCode,String storeCode) {
        log.info("获取参数:objId={},processCode={},storeCode", objId,processCode,storeCode);
        if (StringUtils.isBlank(objId) || StringUtils.isBlank(processCode) || StringUtils.isBlank(storeCode)) {
            log.info("参数不完整，直接结束");
            return;
        }
        BizObjectFacade bizObjectFacade = super.getBizObjectFacade();
        //获取业务数据
        BizObjectCreatedModel bizObject = bizObjectFacade.getBizObject(processCode, objId);

        if (bizObject == null) {
            log.info("业务对象不存在");
            return;
        }
        //单据号
        String sequenceNo = bizObject.getSequenceNo();
        String paramCode = (String) bizObject.get(storeCode);
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyyMMdd");
        String seqNo=simpleDateFormat.format(new Date());
        if(StringUtils.isNoneBlank(sequenceNo)){
            sequenceNo=seqNo+sequenceNo.substring(4,sequenceNo.length());
        }

        bizObject.put("billNo","PUI-"+paramCode+"-"+sequenceNo);

        getBizObjectFacade().saveBizObject("2c9280a26706a73a016706a93ccf002b", bizObject, Boolean.TRUE);
    }


}

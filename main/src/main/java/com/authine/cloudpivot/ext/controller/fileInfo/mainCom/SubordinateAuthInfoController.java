package com.authine.cloudpivot.ext.controller.fileInfo.mainCom;

import com.authine.cloudpivot.engine.api.model.organization.UserModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkflowInstanceModel;
import com.authine.cloudpivot.engine.enums.status.SequenceStatus;
import com.authine.cloudpivot.ext.Utils.CustomSchemaCode;
import com.authine.cloudpivot.ext.service.CloudSqlService;
import com.authine.cloudpivot.web.api.controller.base.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 *  总公司级档案下级公司授权信息
 **/
@RestController
@RequestMapping("/public/subordinateAuthInfo")
@Slf4j
public class SubordinateAuthInfoController extends BaseController {


    @Autowired
    CloudSqlService sqlService;

    /**
     *  总公司级档案下级公司授权信息  审批流完成后 数据写到基础表-总公司级档案下级公司授权
     */
    @Async
    @RequestMapping("empower")
    public void empower(String bizId){
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.SubordinateAuthInfo, bizId);

//        Object pushAllResult = bizObject.get("pushAllResult");


        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get("SubordinateAuthInfoSheet1");

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));


        //获取审批人
        String approvalType = (String) list.get(0).get("approvalType");
        String approval = getFileBaseInfoWorkFlowApproval(bizObject, approvalType);


        for (Map<String, Object> map : list) {
            String fileCode = (String) map.get("fileCode");

            try {

                //调用存储过程
                callProcess(map, approval);

            } catch (Exception e) {
                log.info("基础表-总公司档案下级公司  添加失败 fileCode={}", fileCode);
                log.info(e.getMessage(), e);
            }

        }
    }

    /**
     * 调用存储过程
     ------总公司级档案公司基础信息同步存储过程
     ------总公司级档案公司基础信息同步存储过程	----2023.03.22
     exec  SyncComFileInfo
     @TypeNumber     	nvarchar(50),       	----档案类型
     @ComNumber		nvarchar(20),		----公司
     @Number			nvarchar(200),		----档案代码
     @ComDeptNumber		nvarchar(50),		----公司主责部门代码
     @ComPosNumber		nvarchar(50),		----公司主责岗代码
     @Auth			nvarchar(20),		----权限     1  五角    2  三角    3 √
     @ComPerson		nvarchar(100),		----公司主责人
     @WorkDesc		nvarchar(200),		----工作说明
     @Auditor		nvarchar(100),		----审批人(与EAS登录账号保持一致   员工号+空格+姓名 例：02.0012 李四)
     @AuditDate		DATETIME		----审批时间

     * @param map
     * @param approval
     */
    private void callProcess(Map<String, Object> map, String approval) {

        if (ObjectUtils.isEmpty(map)) {
            return;
        }

        log.info("入参map={}", map);

        //调用存储过程
        String TypeNumber = MapUtils.getString(map, "archTypeNumber", "");//档案类型号
        String ComNumber = MapUtils.getString(map, "companyCompanyCode", "");//公司
        String Number = MapUtils.getString(map, "fileCode", "");//档案代码
        String ComDeptNumber = MapUtils.getString(map, "defaultmainDeptId", "");//主责部门代码
        String ComPosNumber = MapUtils.getString(map, "mainPositionCode", "");//主责主责岗代码

        String purview = MapUtils.getString(map, "purview", "");//权限      1  五角    2  三角    3 √
        String Auth = "";
        purview = purview == null ? "" : purview;
        switch (purview) {
            case "★":
                Auth = "1";
                break;
            case "▲":
                Auth = "2";
                break;
            case "√":
                Auth = "3";
                break;
        }

        String ComPerson = MapUtils.getString(map, "comResPerson", "");//公司主责人

        String WorkDesc = MapUtils.getString(map, "workdesc", "");//工作说明

        String AuditDate = DateFormatUtils.format(new Date(), "yyyy-MM-dd");//审批时间



        String execSql = String.format("exec [HG_LINK].[hg].[dbo].SyncComFileInfo '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'",
                TypeNumber, ComNumber, Number, ComDeptNumber, ComPosNumber,
                Auth, ComPerson, WorkDesc,
                approval, AuditDate);

        log.info("\n==========准备调用存储过程:{}", execSql);

        sqlService.execute(CloudSqlService.htEas, execSql);

        log.info("\n=============存储过程执行完成");

    }

    /**
     *  查询审批人,返回  员工号+姓名
     * @param bizObject
     * @param approvalType
     * @return
     */
    private String getFileBaseInfoWorkFlowApproval(BizObjectCreatedModel bizObject, String approvalType) {
        String mainDeptCode = (String) bizObject.get("mainDeptCode");
        String activityCode ="";
        if ("01".equals(approvalType)){
            String manageDeptCode = (String) bizObject.get("manageDeptCode");
            if (mainDeptCode.equals(manageDeptCode)){
                activityCode="Activity11";
            }else{
                activityCode="Activity13";

            }
        }else if("03".equals(approvalType)) {
            String frameDeptCode = (String) bizObject.get("frameDeptCode");
            if (mainDeptCode.equals(frameDeptCode)){
                activityCode="Activity16";
            }else{
                activityCode="Activity20";
            }
        }


        WorkflowInstanceModel instanceModel = getWorkflowInstanceFacade().getByObjectId(bizObject.getId());
        List<WorkItemModel> workItems = getWorkflowInstanceFacade().getWorkItems(instanceModel.getId(), true);
        final String finalActivityCode = activityCode;
        Optional<WorkItemModel> first = workItems.stream().filter(a -> a.getActivityCode().equals(finalActivityCode)).findFirst();
        String participant = null;
        if (first.isPresent()) {
            WorkItemModel workItemModel = first.get();
            participant = workItemModel.getParticipant();
        }

        if (participant == null) {
            participant = bizObject.getCreater().getId();
        }

        UserModel user = getOrganizationFacade().getUser(participant);


        return new StringBuilder(user.getEmployeeNo()).append(user.getName()).toString();
    }



    /**
     * 判断是否已存在
     * @param field
     * @param value
     * @param schemaCode
     * @return
     */
    private String existsBizObject(String field,String value,String schemaCode){

        String tableName = getBizObjectFacade().getTableName(schemaCode);

        StringBuilder sql = new StringBuilder("select id  from ").append(tableName)
                .append(" where ").append(field).append("='").append(value).append("'");

        Map<String, Object> map = sqlService.getMap(sql.toString());

        return (String) map.get("id");
    }

    /**
     *  转换成  基础表-总公司档案 的数据
     * @param map
     * @param auditDate
     * @return
     */
    private Map<String,Object> fileInfoBaseMap(Map<String,Object> map,String auditDate,String auditer){
        Map<String, Object> data = new HashMap<>();


        //关联档案
        data.put("relevFile",map.get("fileNumber"));
        //档案代码
        data.put("fileCode",map.get("fileCode"));
        //档案名称
        data.put("fileName",map.get("fileName"));
        //主责关联公司
        data.put("mainComGuanlian",map.get("maincom"));
        //主责公司代码
        data.put("mainCompanyCode",map.get("mainComYingshe"));
        //主责公司名称
        data.put("mainCompanyName",map.get("maincomName"));
        //档案类型关联
        data.put("relevFileType",map.get("typeName"));
        //档案类型
        data.put("fileType",map.get("archTypeNumber"));
        //对应记录
        data.put("dyjl",map.get("record"));
        //关联基础代码
        data.put("relevBaseCode",map.get("relevBaseCode"));
        //基础代码
        data.put("baseCode",map.get("baseCode"));
        //基础名称
        data.put("baseCodeName",map.get("name01"));
        //对应说明
        data.put("refDesc",map.get("refDes"));
        //管理职能
        data.put("manageFun",map.get("manageFun"));

        //关联管理部门
        data.put("manageFun",map.get("relevManageDept"));
        //管理部门代码
        data.put("manageDeptCode",map.get("manageDept"));
        //管理部门
        data.put("manageDeptName",map.get("manageDeptName"));
        //关联岗A
        data.put("relevPosA",map.get("relevPositionA"));
        //岗A代码
        data.put("posA",map.get("postionACode"));
        //岗A
        data.put("posAName",map.get("positionAName"));
        //关联岗B
        data.put("relevPosB",map.get("relevPositionB"));
        //岗B代码
        data.put("posB",map.get("postionBCode"));
        //岗B
        data.put("posBName",map.get("positionBName"));

        //备注
        data.put("remark",map.get("remarks"));
        //电子版
        data.put("elec",map.get("elec"));
        //说明
        data.put("explains",map.get("des"));
        //职能关联
        data.put("fun",map.get("zn"));
        //职能代码
        data.put("funCode",map.get("znCode"));
        //职能
        data.put("funName",map.get("znName"));

        //主责部门关联
        data.put("relevMainDept",map.get("MainDeptId"));
        //主责部门代码
        data.put("mainDeptCode",map.get("defaultmainDeptId"));
        //主责部门
        data.put("dept",map.get("MainDept"));
        //主责岗关联
        data.put("relevMainPosition",map.get("mainPositionId"));
        //主责岗代码
        data.put("mainPositionCode",map.get("mainPositionCode"));
        //主责岗
        data.put("mainPosition",map.get("mainPostionName"));

        //公司授权信息
        data.put("mainCompanyAuthInfo",map.get("purview"));
        //纸质数量
//        data.put("paperNum",map.get(""));
        //01授权
//        data.put("one",map.get("1"));
        //02授权
//        data.put("two",map.get("1"));
        //03授权
//        data.put("three",map.get("1"));
        //04授权
//        data.put("four",map.get("1"));
        //05授权
//        data.put("five",map.get("1"));
        //06授权
//        data.put("six",map.get("1"));
        //07授权
//        data.put("seven",map.get("1"));
        //08授权
//        data.put("eight",map.get("1"));
        //09授权
//        data.put("nine",map.get("1"));
        //10授权
//        data.put("ten",map.get("1"));
        //工作说明
        data.put("jobDesc",map.get("refDes"));
        //审批时间
        data.put("auditDate",auditDate);
        //审批人
        data.put("auditer",auditer);

        //序号
        data.put("serialNo",map.get("serialNo"));

        //主责人
        data.put("company",map.get("comResPerson"));

        return data;
    }
}

package com.authine.cloudpivot.ext.controller.fun;

        import cn.hutool.core.lang.Assert;
        import com.authine.cloudpivot.engine.api.model.bizmodel.BizPropertyModel;
        import com.authine.cloudpivot.engine.api.model.organization.UserModel;
        import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
        import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
        import com.authine.cloudpivot.engine.api.model.runtime.WorkflowInstanceModel;
        import com.authine.cloudpivot.engine.enums.status.SequenceStatus;
        import com.authine.cloudpivot.ext.Utils.CustomSchemaCode;
        import com.authine.cloudpivot.ext.service.CloudSqlService;
        import com.authine.cloudpivot.web.api.controller.base.BaseController;
        import com.authine.cloudpivot.web.api.view.ResponseResult;
        import lombok.extern.slf4j.Slf4j;
        import org.apache.commons.collections4.MapUtils;
        import org.apache.commons.lang3.StringUtils;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestBody;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RestController;

        import java.time.LocalDateTime;
        import java.time.format.DateTimeFormatter;
        import java.util.HashMap;
        import java.util.List;
        import java.util.Map;
        import java.util.Optional;
        import java.util.stream.Collectors;


/**
 * @Author yb
 * @Date 2022/12/28 17:03
 * @Description
 **/

@RestController
@RequestMapping("/public/corpfun")
@Slf4j
public class CorpFunController extends BaseController {

    @Autowired
    CloudSqlService sqlService;

    @RequestMapping("/validSubmit")
    public ResponseResult validSubmit(@RequestBody Map<String,Object> map){







        return getOkResponseResult("");
    }

    /**
     *  获取总公司职能列表数据
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("getCorpFunList")
    public List getFileInfoBaeList(Integer page, Integer size){

        String schemaCode = "corpFunBase";

        String tableName = getBizObjectFacade().getTableName(schemaCode);

        List<BizPropertyModel> list = getBizPropertyFacade().getListBySchemaCode(schemaCode,true);

        String codes = list.stream().filter(a-> !a.getDefaultProperty()).map(a -> a.getCode()).collect(Collectors.joining(","));

        StringBuilder sql = new StringBuilder("SELECT ").append(codes).append("  from ")
                .append(tableName);

        if (page != null &&  size != null) {
            if (page>0){
                page = (page-1)*size;
            }
            String limit = String.format(" limit %d,%d", page, size);
            sql.append(limit);
        }
        List<Map<String, Object>> mapList = sqlService.getList(sql.toString());

        return mapList;
    }

    /**
     *  总公司职能基础信息 新增 审批流完成后 数据写到基础表-总公司职能
     */
    @RequestMapping("addInfoCorpFunBase")
    public void addInfoCompanyFunBase(String bizId){
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.headFunAddNew, bizId);


        List<Map<String,Object>> list = (List<Map<String, Object>>) bizObject.get("Sheet1671004937120");

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));


        //获取审批人

        String approval = getFileBaseInfoWorkFlowApproval(bizObject );


        for (Map<String, Object> map : list) {
            String funNumber = (String) map.get("funNumber");

            try {

                Map<String, Object> data = fileInfoBaseMap(map, now, approval);
                //判断是否已存在
                String id = existsBizObject("funNumber",funNumber,CustomSchemaCode.corpFunBase);
                if (StringUtils.isNotEmpty(id)) {
                    data.put("id", id);
                }

                BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.corpFunBase, data, false);
                model.setSequenceStatus(SequenceStatus.COMPLETED.name());
                id = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                log.info("基础表-公司职能  添加成功 ");
                //调用存储过程
                callProcess(id);

            } catch (Exception e) {
                log.info("基础表-公司职能  添加失败 funNumber={}", funNumber);
                log.info(e.getMessage(), e);
            }

        }
    }

    /**
     *  总公司职能基础信息 新增 审批流完成后 数据写到基础表-总公司职能
     */
    @RequestMapping("toVoid")
    public void toVoid(String bizId){
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.headFunVoid, bizId);

        List<Map<String,Object>> list = (List<Map<String, Object>>) bizObject.get("SheetHeadFunVoid");

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));


        //获取审批人
        String approval = getFileBaseInfoWorkFlowApproval(bizObject );


        for (Map<String, Object> map : list) {
            String funNumber = (String) map.get("funNumber");

            try {

                Map<String, Object> data = fileInfoBaseMap(map, now, approval);
                //判断是否已存在
                String id = existsBizObject("funNumber",funNumber,CustomSchemaCode.corpFunBase);
                if (StringUtils.isNotEmpty(id)) {
                    data.put("id", id);
                }

                BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.corpFunBase, data, false);
                model.setSequenceStatus(SequenceStatus.CANCELED.name());
                id = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                log.info("基础表-公司职能  作废成功 ");
                //调用存储过程
                callProcessToVoid(id);

            } catch (Exception e) {
                log.info("基础表-公司职能  作废失败 funNumber={}", funNumber);
                log.info(e.getMessage(), e);
            }

        }
    }

    /**
     *  调用存储过程
     *  档案类型fid,档案类型,主责公司fid,主责公司,基础代码fid,基础代码,职能fid,职能,序号,档案代码,档案名称,备注,说明,电子版,主责部门fid,主责部门代码,主责岗代码fid,主责岗代码,权限,主责人,审批人(员工号+姓名),审批时间
     *  exec  SyncCorpFileInfo
     * TypeFID nvarchar(100),       ----档案类型fid
     * TypeNumber nvarchar(50),       ----档案类型
     * ComFID nvarchar(100),        ----主责公司fid
     * ComNumber nvarchar(20),        ----主责公司
     * BaseFID nvarchar(100),        ----基础代码fid
     * BaseNumber nvarchar(100),        ----基础代码
     * FunFID nvarchar(100),        ----职能fid  总公司职能
     * FunNumber nvarchar(50),        ----职能
     * Serial nvarchar(100),        ----序号
     * Number nvarchar(200),        ----档案代码
     * Name nvarchar(200),        ----档案名称
     * Remark nvarchar(200),        ----备注
     * Desc nvarchar(500),        ----说明
     * Elec nvarchar(10),        ----电子版      1   √     0    x     2  /
     * DeptFID nvarchar(100),        ----主责部门fid
     * DeptNumber nvarchar(50),        ----主责部门代码
     * PosFID nvarchar(100),        ----主责岗代码fid
     * PosNumber nvarchar(50),        ----主责岗代码
     * Auth nvarchar(20),        ----权限      1  五角    2  三角    3 √  ( 总公司档案, 电子版 1, 权限1;  电子版0,2, 权限3)
     * Person nvarchar(100),        ----主责人
     * Auditor nvarchar(100),        ----审批人(与EAS登录账号保持一致   员工号+空格+姓名 例：02.0012 李四)
     * AuditDate DATETIME            ----审批时间
     *
     *
     * @param bizId
     */
    private void callProcess(String bizId){

        if (StringUtils.isEmpty(bizId)) {
            return;
        }
        //编写sql
        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.corpFunBase);
        StringBuilder sql = new StringBuilder("SELECT funNumber,funName,englishName,memoryCode,funResDeptId,auditer,auditDate from ")
                .append(tableName).append(" where id ='")
                .append(bizId).append("';");
        //查询到入参
        Map<String, Object> map = sqlService.getMap(sql.toString());

        log.info("入参map={}",map);

        //调用存储过程


        String funNumber = MapUtils.getString(map,"funNumber","");
        String funName = MapUtils.getString(map,"funName","");
        String englishName = MapUtils.getString(map,"englishName","");
        String memoryCode = MapUtils.getString(map,"memoryCode","");
        String funResDeptNumber = MapUtils.getString(map,"funResDeptId","");
        String auditor = MapUtils.getString(map,"auditer","");



        log.info("funNumber={},auditor={},",funNumber,auditor);


        Assert.isFalse(StringUtils.isEmpty(funNumber),"{}不能为空",funNumber);
        Assert.isFalse(StringUtils.isEmpty(funName),"{}不能为空",funName);
        Assert.isFalse(StringUtils.isEmpty(englishName),"{}不能为空",englishName);
        Assert.isFalse(StringUtils.isEmpty(memoryCode),"{}不能为空",memoryCode);
        Assert.isFalse(StringUtils.isEmpty(funResDeptNumber),"{}不能为空",funResDeptNumber);
        Assert.isFalse(StringUtils.isEmpty(auditor),"{}不能为空",auditor);


        String execSql = String.format("exec [HG_LINK].[hg].[dbo].SyncCorpFunction  '%s','%s','%s','%s','%s','%s'",
                funNumber,funName,memoryCode,englishName,funResDeptNumber,auditor);

        log.info("\n==========准备调用存储过程:{}",execSql);

        sqlService.execute(CloudSqlService.htEas,execSql);

        log.info("\n=============存储过程执行完成");

    }
    /**
     *  调用存储过程
     *  exec   CorpFunctionVoid
     *    Number    nvarchar(50),      ----职能代码
     *    Audit r	nvarchar(50)     ----审批人
     *
     */
    private void callProcessToVoid(String bizId){

        if (StringUtils.isEmpty(bizId)) {
            return;
        }
        //编写sql
        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.corpFunBase);
        StringBuilder sql = new StringBuilder("SELECT  funNumber,auditer from ")
                .append(tableName).append(" where id ='")
                .append(bizId).append("';");
        //查询到入参
        Map<String, Object> map = sqlService.getMap(sql.toString());

        log.info("入参map={}",map);

        //调用存储过程

        String funNumber = MapUtils.getString(map,"funNumber","");
        String auditor = MapUtils.getString(map,"auditer","");

        Assert.isFalse(StringUtils.isEmpty(funNumber),"{}不能为空","funNumber");
        Assert.isFalse(StringUtils.isEmpty(auditor),"{}不能为空","auditor");

        String execSql = String.format("exec [HG_LINK].[hg].[dbo].CorpFunctionVoid '%s','%s'",
                funNumber, auditor);

        log.info("\n==========准备调用作废存储过程:{}",execSql);

        sqlService.execute(CloudSqlService.htEas,execSql);

        log.info("\n=============作废存储过程执行完成");

    }

    /**
     *  查询审批人,返回  员工号+姓名
     * @param bizObject
     * @return
     */
    private String getFileBaseInfoWorkFlowApproval(BizObjectCreatedModel bizObject ) {

        String activityCode="Activity15";
        WorkflowInstanceModel instanceModel = getWorkflowInstanceFacade().getByObjectId(bizObject.getId());
        List<WorkItemModel> workItems = getWorkflowInstanceFacade().getWorkItems(instanceModel.getId(), true);
        final String finalActivityCode = activityCode;
        Optional<WorkItemModel> first = workItems.stream().filter(a -> a.getActivityCode().equals(finalActivityCode)).findFirst();
        String participant = null;
        if (first.isPresent()) {
            WorkItemModel workItemModel = first.get();
            participant = workItemModel.getParticipant();
        }

        if (participant == null) {
            participant = bizObject.getCreater().getId();
        }

        UserModel user = getOrganizationFacade().getUser(participant);


        return new StringBuilder(user.getEmployeeNo()).append(user.getName()).toString();
    }



    /**
     * 判断是否已存在
     * @param funNumber
     * @param value
     * @param schemaCode
     * @return
     */
    private String existsBizObject(String funNumber,String value,String schemaCode){

        String tableName = getBizObjectFacade().getTableName(schemaCode);

        StringBuilder sql = new StringBuilder("select id  from ").append(tableName)
                .append(" where ").append(funNumber).append("='").append(value).append("'");

        Map<String, Object> map = sqlService.getMap(sql.toString());

        return (String) map.get("id");
    }

    /**
     *  转换成  基础表-总公司职能 的数据
     * @param map
     * @param auditDate
     * @return
     */
    private Map<String,Object> fileInfoBaseMap(Map<String,Object> map,String auditDate,String auditer){
        Map<String, Object> data = new HashMap<>();
        //职能代码
        data.put("funNumber",map.get("funNumber"));
        //职能名称
        data.put("funName",map.get("funName"));
        //英文名
        data.put("englishName",map.get("englishName"));
        //助记码
        data.put("memoryCode",map.get("memoryCode"));
        //职能总责部门id
        data.put("funResDeptId",map.get("funManageDeptId"));
        //职能总责部门代码
        data.put("funResDeptNumber",map.get("funManageDeptNumber"));
        //职能总责部门名称
        data.put("funResDeptName",map.get("funManageDept"));
        //审批人
        data.put("auditer",auditer);

        return data;
    }


}


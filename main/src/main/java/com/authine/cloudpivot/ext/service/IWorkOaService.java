package com.authine.cloudpivot.ext.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.authine.cloudpivot.engine.api.facade.BizWorkitemFacade;
import com.authine.cloudpivot.engine.api.model.dto.SimpleWorkDTO;
import com.authine.cloudpivot.engine.api.model.im.WorkRecordModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
import com.authine.cloudpivot.engine.enums.status.WorkRecordStatus;
import com.authine.cloudpivot.engine.enums.type.CorpRelatedType;
import com.authine.cloudpivot.engine.spi.service.IWorkService;
import com.authine.cloudpivot.ext.service.impl.OaWorkItemServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
public class IWorkOaService implements IWorkService {


    @Autowired
    OaWorkItemServiceImpl oaWorkItemService;
    @Autowired
    BizWorkitemFacade bizWorkitemFacade;

    @Override
    public String createWork(SimpleWorkDTO workDTO) {

        log.info("\n===================IWorkOaService.创建任务:workDTO={}\n", JSON.toJSONString(workDTO));

        //向oa发送待办任务逻辑

        WorkItemModel item = bizWorkitemFacade.get(workDTO.getBizTaskId());

        if (item == null) {
            log.info("发送待办失败,待办任务不存在,item.id={}",workDTO.getBizTaskId());
            return null;
        }
        return oaWorkItemService.sendTodo(item);
    }

    @Override
    public boolean updateStatus(String bizTaskId, String state) {
        //消待办， 完成,撤销,或删除
        log.info("\n===================IWorkOaService.修改状态:bizTaskId={},state={}\n", bizTaskId,state);

        //向oa发送消待办任务逻辑
//        if (WorkItemApprovalStatus.REJECT.name().equalsIgnoreCase(state) ||
//                WorkItemApprovalStatus.CANCELLED.name().equalsIgnoreCase(state) )
//        {
//
//        }else {
//
//        }
        return oaWorkItemService.setTodoDone(bizTaskId);

    }

    @Override
    public boolean updateFlowStatus(String bizFlowId, String bizTaskId, String state) {
        //消待办， 完成,撤销,或删除
        log.info("\n===================IWorkOaService.修改流程状态:bizFlowId={},bizTaskId={},state={}\n", bizFlowId,bizTaskId,state);
        //向oa发送消待办任务逻辑

        return updateStatus(bizTaskId,state);
    }

    @Override
    public boolean removeWork(WorkRecordModel workRecord) {
        //消待办，删除
        log.info("\n===================IWorkOaService.删除任务:workRecord={}\n", JSON.toJSONString(workRecord));
        //向oa发送消待办任务逻辑
        return oaWorkItemService.deleteToDo(workRecord);
    }

    @Override
    public String pushWorkMsg(WorkRecordModel workRecord) {
        log.info("\n===================IWorkOaService.发送消息通知:workRecord={}\n", JSON.toJSONString(workRecord));
        JSONObject bizParams = JSONObject.parseObject(workRecord.getBizParams());

        String workitemId = bizParams.getString("workitemId");
        SimpleWorkDTO workDTO = (new SimpleWorkDTO()).setContent(workRecord.getContent()).setTitle(workRecord.getTitle()).setBizTaskId(workitemId);

        String rst = this.createWork(workDTO);

        if ("0".equals(rst)){
            workRecord.setRecordId( UUID.randomUUID().toString().replaceAll("-", ""));
            workRecord.setWorkRecordStatus(WorkRecordStatus.FINISHED);
        }else if ("1".equals(rst)){
            //设置重试超过次数
            workRecord.setWorkRecordStatus(WorkRecordStatus.OUT_COUNT);
        }else {
            workRecord.setWorkRecordStatus(WorkRecordStatus.UNSEND);
        }

        return workRecord.getRecordId();
    }

    @Override
    public String initChannel() {
        return CorpRelatedType.DINGTALK.toString();
    }



}

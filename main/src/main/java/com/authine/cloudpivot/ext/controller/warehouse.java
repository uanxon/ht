package com.authine.cloudpivot.ext.controller;

import com.authine.cloudpivot.engine.api.facade.BizObjectFacade;
import com.authine.cloudpivot.engine.api.model.bizmodel.BizPropertyModel;
import com.authine.cloudpivot.engine.api.model.organization.UserModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.engine.api.model.runtime.SelectionValue;
import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkflowInstanceModel;
import com.authine.cloudpivot.engine.enums.status.DataRowStatus;
import com.authine.cloudpivot.engine.enums.status.SequenceStatus;
import com.authine.cloudpivot.ext.Utils.CustomSchemaCode;
import com.authine.cloudpivot.web.api.controller.base.BaseController;
import com.authine.cloudpivot.web.api.exception.PortalException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.authine.cloudpivot.ext.service.CloudSqlService;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
@Slf4j
@RestController
@RequestMapping("/public/WarehouseBase")
public class warehouse  extends BaseController {
    @Autowired
    CloudSqlService sqlService;

    /**
     *  获取库存列表数据
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("getRealTimeinventory")
    public List getFileInfoBaeList(Integer page, Integer size){

        String schemaCode = "warehouse";

        String tableName = getBizObjectFacade().getTableName(schemaCode);

        List<BizPropertyModel> list = getBizPropertyFacade().getListBySchemaCode(schemaCode,true);

        String codes = list.stream().filter(a-> !a.getDefaultProperty()).map(a -> a.getCode()).collect(Collectors.joining(","));

        StringBuilder sql = new StringBuilder("SELECT ").append(codes).append("  from ")
                .append(tableName);

        if (page != null &&  size != null) {
            if (page>0){
                page = (page-1)*size;
            }
            String limit = String.format(" limit %d,%d", page, size);
            sql.append(limit);
        }
        List<Map<String, Object>> mapList = sqlService.getList(sql.toString());

        return mapList;
    }

}

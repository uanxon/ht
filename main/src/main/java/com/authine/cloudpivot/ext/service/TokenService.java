package com.authine.cloudpivot.ext.service;

import jodd.introspector.Mapper;

/**
 * @Author hxd
 * @Date 2022/8/15 10:25
 * @Description 获取浩通门户token
 **/


public interface TokenService {

    /**
     *  获取Authorization , token_type+" "+access_token
     * @return
     */
    public String getAuthorization();


}



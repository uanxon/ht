package com.authine.cloudpivot.ext.controller;

import com.alibaba.fastjson.JSONObject;
import com.authine.cloudpivot.ext.service.IdaasOrgSyncService;
import com.authine.cloudpivot.web.api.controller.base.RuntimeController;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Description idaas数据推送
 * @Author fj
 * @Date 2022/05/16
 */
@RestController
@RequestMapping("/api/idaas/developer/scim/")
@Api(tags = "IDaaS推数据到云枢")
public class IdaasOrgSyncController extends RuntimeController {
    @Autowired
    IdaasOrgSyncService idaasOrgSyncService;

    @PostMapping("/organization")
    public Result organizationAdd(@RequestBody Map<String, Object> request) {
        try {
            JSONObject jsonObject = new JSONObject(request);
            idaasOrgSyncService.departmentAdd(jsonObject, "");
            return success();
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    @PutMapping("/organization")
    public Result organizationUpdate(@RequestBody Map<String, Object> request) {
        try {
            JSONObject jsonObject = new JSONObject(request);
            idaasOrgSyncService.departmentUpdate(jsonObject, "");
            return success();
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    @DeleteMapping("/organization")
    public Result organizationDelete(@RequestParam("id") String id) {
        try {
            idaasOrgSyncService.departmentDelete(id);
            return success();
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }


    @PostMapping("/account")
    public Result accountAdd(@RequestBody Map<String, Object> request) {
        try {
            JSONObject jsonObject = new JSONObject(request);
            idaasOrgSyncService.userAdd(jsonObject);
            return success();
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    @PutMapping("/account")
    public Result accountUpdate(@RequestBody Map<String, Object> request) {
        try {
            JSONObject jsonObject = new JSONObject(request);
            idaasOrgSyncService.userUpdate(jsonObject);
            return success();
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    @DeleteMapping("/account")
    public Result accountDelete(@RequestParam("id") String id) {
        try {
            idaasOrgSyncService.userDelete(id, getMainCorpId());
            return success();
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    private Result success() {
        return new Result(200, "数据同步成功");
    }

    private Result error(String msg) {
        return new Result(999, msg);
    }
}

@Getter
@Setter
class Result {
    private Integer code;
    private String message;

    public Result(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}


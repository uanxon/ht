package com.authine.cloudpivot.ext.controller.fileInfo.com;

import com.authine.cloudpivot.engine.api.model.organization.UserModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.engine.api.model.runtime.SelectionValue;
import com.authine.cloudpivot.engine.enums.type.UnitType;
import com.authine.cloudpivot.ext.Utils.AccountAuthorize;
import com.authine.cloudpivot.ext.Utils.DocAPI;
import com.authine.cloudpivot.ext.Utils.RoleAuth;
import com.authine.cloudpivot.ext.controller.FileCodeSortController;
import com.authine.cloudpivot.ext.service.CloudSqlService;
import com.authine.cloudpivot.ext.service.RoleAuthService;
import com.authine.cloudpivot.web.api.controller.runtime.WorkflowInstanceRuntimeController;
import com.authine.cloudpivot.web.api.view.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.axis.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @Author yb
 * @Date 2023/1/29 11:20
 * @Description
 * 公司级常规档案审批完，同步云盘操作
 **/
@RequestMapping("/public/CompanyArchiveYomi")
@RestController
@Slf4j

public class CompanyArchiveYomi extends WorkflowInstanceRuntimeController {
    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private CloudSqlService sqlService;

    @Autowired
    private RoleAuthService roleAuthService;

    @Autowired
    private FileCodeSortController fileCodeSortController;




    //公司级档案基础 新增
    @Async
    @GetMapping("ComArchive")
    public ResponseResult ComArchive(@RequestParam String bizId)
    {


        String desc = "";
        String attribute = "";
        String FolderAttribute = "";
        String prefix = "";
        String elec = "";
        String PosAName = "";
        String PosMainName= "";
        String fileid= "";
        String addfileid = "";
        String delId= "";
        String foldername = "";
        String AuthFileId = "";
        String PosBName = "";
        String PosADuty = "";
        String PosBDuty = "";
        String AppEmpId="";
        String RoleNumAss= "";//助理岗位02.01.006
        String RoleNameAss= "";//助理岗位”综管部助理“
        int SubItemORNot=0;
        String PosA = "";
        String PosB = "";
        String ArchType = "";
        String mainComCode = "";
        String MainDeptCode = "";
        String MainDeptName = "";
        String ArchFunCode = "";
        String manageDept = "";
        String PosMainDuty = "";
        String manageDeptSimpleName = "";
        String MainDeptAssDuty = "";       

        int SubmitPubSubItemFileNum=0;
        String FraMainPositionDuty="";
        String deptsimplename="";
        
        String archtype="";

        String FunMainDeptAssDuty="";
        String FunMainDeptAssDutyName="";

        DocAPI api = new DocAPI();
        String token = api.getToken();
        RoleAuth RoleAuth=new RoleAuth();

        Map<String, Object> objectMap = null;


        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject("companyFileBaseInfo",  bizId);

        List<Map<String, Object>> subSheet = (List<Map<String, Object>>) bizObject.getObject("sheetCompanyFile");

        String elec2 = (String) subSheet.get(0).get("elec");
        List<Map<String, Object>> code123123 = (List<Map<String, Object>>) subSheet.get(0).get("code");
        Map<String, Object> code123122223 = (Map<String, Object>) subSheet.get(0).get("code");


        if (CollectionUtils.isEmpty(subSheet)) {
            return getOkResponseResult("");
        }

        //获取申请人
        String createId = bizObject.getCreater().getId();
        UserModel user = getOrganizationFacade().getUser(createId);
        String AppEmp = user.getEmployeeNo();


        objectMap = subSheet.get(0);


        //获取表单上主责公司
        mainComCode = (String) objectMap.get("mainComYingshe");

        //获取表单上主责部门
        MainDeptCode = (String) objectMap.get("defaultmainDeptId");
        MainDeptName = (String) objectMap.get("MainDept");
        //获取表单上管理部门
        manageDept = (String) objectMap.get("manageDept");
        manageDeptSimpleName = (String) objectMap.get("manageDeptName");


        //获取表单上主责岗位
        PosMainDuty = (String) objectMap.get("mainPositionCode");
        PosMainName = (String) objectMap.get("mainPostionName");

        //获取表单上岗A
        PosA = (String) objectMap.get("postionACode");

        PosAName = (String) objectMap.get("positionAName");
        if (PosA != null) {
            PosADuty = PosA;
            PosAName = PosAName;
        }
//          PosAName=PosAName;

        //获取表单上岗B
        PosB = (String) objectMap.get("postionBCode");
        PosBName = (String) objectMap.get("positionBName");
        if (PosB != null) {
            PosBDuty = PosB;
            PosBName = PosBName;
        }
        // 获取主责部门助理角色
        MainDeptAssDuty = MainDeptCode + ".006";

        // 获取职能总责部门助理角色
        FunMainDeptAssDuty = (String) objectMap.get("FunMainDept") + ".006";
        String sqlFunMainDeptAssDutyCode = "select fname_l2 from [HG_LINK].[hg].dbo.T_org_position WHERE FNUMBER ='" + MainDeptAssDuty + "'";

        List<Map<String, Object>> list = sqlService.getList(CloudSqlService.htEas, sqlFunMainDeptAssDutyCode);
        try {
            FunMainDeptAssDutyName = (String) list.get(0).get("fname_l2");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        //职能代码
        ArchFunCode = (String) objectMap.get("znCode");
        // 获取档案类型号
        ArchType = (String) objectMap.get("fileTypeNumber");

        if (ArchType.substring(0, 2).equals("20")) {
            //上传岗：工作档案框架主责岗
            objectMap = subSheet.get(0);
            String CFCode = (String) objectMap.get("fileCode");//档案代码
//            attribute = attribute.replace(" ", "");//档案代码
            CFCode = CFCode.replace(" ", "");//档案代码
            String[] str=CFCode.split("-");
            String CFCode01 = str[0] + "-" + str[1] + "-" + str[2] + "-0";//工作档案框架
//            String CFCode01 = CFCode.substring(0, CFCode.lastIndexOf("-")) + "-0";//工作档案框架
            String sqlstr = "select b.fname_l2 FNAME,b.fnumber FNUMBER from [HG_LINK].[hg].dbo.ct_gen_comgeneralarchbase a inner join [HG_LINK].[hg].dbo.T_ORG_position b on a.CFMainDeptPICCodeI=b.fid  where a.fnumber = '" + CFCode01 + "'";
            List<Map<String, Object>> rs = sqlService.getList(CloudSqlService.htEas, sqlstr);
            try {
                String fnumber = (String) rs.get(0).get("FNUMBER");
                if (fnumber == null) {
                    MainDeptAssDuty = fnumber.substring(0, fnumber.lastIndexOf(".")) + ".006";
                    PosA = fnumber.substring(0, fnumber.lastIndexOf(".")) + ".006";
                } else {
                    MainDeptAssDuty = PosMainDuty.substring(0, PosMainDuty.lastIndexOf(".")) + ".006";
                    PosA = MainDeptAssDuty;
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            String sqlstr000 = "select fsimplename from [HG_LINK].[hg].dbo.t_org_admin  where fnumber = '" + PosADuty.substring(0, PosADuty.lastIndexOf(".")) + "'";
            List<Map<String, Object>> rs000 = sqlService.getList(CloudSqlService.htEas, sqlstr000);
            try {
                deptsimplename = (String) rs000.get(0).get("fsimplename");
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

            //获取公共区域对应岗位A文件夹里的文件List
            String SubmitAttribute= PosA+" PUB "+AppEmp;
            if (ArchType.substring(0, 2).equalsIgnoreCase("20"))
            {
                objectMap = subSheet.get(0);
                attribute = (String) objectMap.get("fileCode");
                attribute = attribute.replace(" ", "");//档案代码

                String CFCode = attribute.replace(" ", "");//档案代码

                String[] str=CFCode.split("-");
                String CFCode01 = str[0] + "-" + str[1] + "-" + str[2] + "-0";//工作档案框架


//                String CFCode01 =  CFCode.substring(0, CFCode.lastIndexOf("-"))+"-0";//工作档案框架
                String sqlstr = "select b.fname_l2 FNAME,b.fnumber FNUMBER from [HG_LINK].[hg].dbo.ct_gen_comgeneralarchbase a inner join [HG_LINK].[hg].dbo.T_ORG_position b on a.cfmaindeptpiccodei=b.fid  where a.fnumber = '"+CFCode01+ "'";
                List<Map<String, Object>> rs = sqlService.getList(CloudSqlService.htEas, sqlstr);
                try {

                    String value =  (String)rs.get(0).get("FNUMBER");

                    if(value!=null){
                        SubmitAttribute= value.substring(0, value.lastIndexOf("."))+".006"+" PUB "+AppEmp;
                        FraMainPositionDuty=value.substring(0, value.lastIndexOf("."))+".006";;

                    }else{
                        SubmitAttribute=PosMainDuty.substring(0, PosMainDuty.lastIndexOf("."))+".006"+" PUB "+AppEmp;
                        FraMainPositionDuty=PosMainDuty.substring(0, PosMainDuty.lastIndexOf("."))+".006";
                    }


                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                
                 String sqlstr000 = "select fsimplename from [HG_LINK].[hg].dbo.t_org_admin  where fnumber = '"+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."))+ "'";
                List<Map<String, Object>> rs000 = sqlService.getList(CloudSqlService.htEas, sqlstr000);
                try {
                    deptsimplename = (String) rs000.get(0).get("fsimplename");
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }




            }

            JSONObject SubmitPubFolderIDObj=api.getFileID( SubmitAttribute);
            int SubmitPubFolderNum=Integer.parseInt(SubmitPubFolderIDObj.getString("num"));
            if(SubmitPubFolderNum>0)
            {

                JSONArray SubmitPubFolderIDDataArray = SubmitPubFolderIDObj.getJSONArray("data");
                JSONObject SubmitPubFolderIDDataObj = SubmitPubFolderIDDataArray.getJSONObject(0);
                String SubmitPubFolderID = SubmitPubFolderIDDataObj.getString("id");
                JSONObject SubmitPubFolderListObj=api.getList( SubmitPubFolderID);
//          boolean SubmitPubFolderIsEmpty=SubmitPubFolderListObj.getJSONArray("data").isEmpty();
                JSONArray SubmitPubFolderListDataArray = SubmitPubFolderListObj.getJSONArray("data");//公共区域对应岗位A文件夹里的文件List
                int n = subSheet.size();
                for (int i = 0; i < n; i++) {
                    SubItemORNot=0;//分项数量
                    SubmitPubSubItemFileNum=0;

                    objectMap = subSheet.get(i);
                    // 获取档案类型号
                    ArchType = (String) objectMap.get("fileTypeNumber");


                    String Desc = (String) objectMap.get("des");





                    attribute = (String) objectMap.get("fileCode");
                    attribute = attribute.replace(" ", "");//档案代码

                    String OrgCode01=(String) objectMap.get("companyCode");



                    elec=(String) subSheet.get(i).get("elec");
                    if (elec.equalsIgnoreCase("×"))
                    {

                        if(ArchType.equalsIgnoreCase("15"))//HR
                        {
                            attribute=attribute+" "+ MainDeptCode;
                        }
                        if(ArchType.equalsIgnoreCase("13"))//CSD
                        {
                            attribute=attribute+" "+ PosA;;
                        }
                        if(ArchType.equalsIgnoreCase("12"))//F
                        {
                            attribute=attribute+" "+ PosA;;
                        }
                        if(ArchType.equalsIgnoreCase("14"))//HR
                        {
                            attribute=attribute+" "+ (String) objectMap.get("baseCode");//02.0208
                        }
                        if(ArchType.equalsIgnoreCase("16"))//OD
                        {
                            attribute=attribute+" "+ PosA.substring(0, PosA.indexOf("-"));//02.0208
                        }



                        JSONObject ExistFolderObj = api.getFileID(attribute);//
                        int FileNum=Integer.parseInt(ExistFolderObj.getString("num"));
                        if(FileNum==0){
                            continue;
                        }
                        if(FileNum!=0){

                            JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                            for(i=0;i<ExistFolderDataArray.size();i++){
                                JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(i);
                                String ExitFolderId = ExistFolderObj0.getString("id");
                                api.getRemove(token, ExitFolderId);}

                        }

                        continue;

                    }


//				if(!OrgCode01.equalsIgnoreCase("HT"))
//				{
//					continue;
//				}

                    String[] SubmitPubFileList=new String[200];//公共区域对应岗位A文件夹里的文件List
                    String[] SubmitPubSubItemFileList=new String[200];
                    String[] SubItemFileList01=new String[200];
                    int  IsNomalFile=0;
                    //遍历公共区域对应A岗位文件夹里的文件，带有分项的放入SubmitPubSubItemFileList，和EAS报批的文件名一致的放入SubItemFileList
                    for(int PubNum=0;PubNum<SubmitPubFolderListDataArray.size();PubNum++)
                    {
                        JSONObject SubmitPubFolderListNumDataArray = SubmitPubFolderListDataArray.getJSONObject(PubNum);
                        SubmitPubFileList[PubNum]=SubmitPubFolderListNumDataArray.getString("remarks");
                        String trans=SubmitPubFileList[PubNum].replace(',', '，');
                        SubmitPubFileList[PubNum]=trans;
                        String hsgd=attribute;
                        if(SubmitPubFileList[PubNum].equals(attribute)){
                            IsNomalFile=1;
                        }

                        if(trans.contains("，"))
                        {
                            SubmitPubSubItemFileList[SubmitPubSubItemFileNum]=trans;//带有分项的放入SubmitPubSubItemFileList
                            String trans01=trans.substring(0,trans.indexOf("，") );
                            if(trans01.equalsIgnoreCase(attribute))
                            {
                                SubItemFileList01[SubItemORNot]=trans;//分项文件List
                                SubItemORNot++;//分项数量

                            }
                            SubmitPubSubItemFileNum++;
//							 System.out.println(PubNum);
                        }
                    }
                    String[] SubItemFileList=new String[SubItemORNot];
                    for(int m=0;m<SubItemORNot;m++)
                    {
                        SubItemFileList[m]=SubItemFileList01[m];

                    }


                    Arrays.sort(SubItemFileList);
                    String OrgCode=(String) objectMap.get("companyCode");;//02
                    archtype = (String) objectMap.get("fileTypeNumber");
                    if (archtype != null) {
                        String strSql="";
                        List<Map<String, Object>> rs =null;
                        String strSql01="";
                        List<Map<String, Object>> rs01 =null;
                        String strSql02="";
                        List<Map<String, Object>> rs02 =null;
                        String strSql03="";
                        List<Map<String, Object>> rs03 =null;
                        String strSql04="";
                        List<Map<String, Object>> rs04 =null;
                        String strSql05="";
                        List<Map<String, Object>> rs05 =null;
                        String ParentFolderAttr="";
                        String ParentFolderId="";
                        String FolderAttr="";
                        String FolderName = "";
                        String MatArchiveType = "";
                        String MatCode="";

                        if (ArchType.substring(0, 2).equalsIgnoreCase("20")||ArchType.substring(0, 2).equalsIgnoreCase("17")) {
                            String MatType01= "";
                            String MatType02= "";
                            String MatType03= "";

                            String MaterialName01="";
                            String MaterialName02="";
                            String MaterialName03="";
                            String MaterialName04="";


                            desc = (String) objectMap.get("des");// 获取说明
                            MatCode=(String) objectMap.get("baseCode");;//基礎代碼
                            MatType01=MatCode.substring(0, 2);//08
                            MatType02=MatCode.substring(2, 4);//01
                            MatType03=MatCode.substring(4, 6);//18
                            MatArchiveType=attribute.substring(2, 4);//06

                            String abc=attribute.substring(0, attribute.lastIndexOf("-"))+"-0";//WD06-02-0801080086048338-0

//								strSql="SELECT B.FNUMBER   as FNUMBER FROM CT_GEN_ComGeneralArchBase A INNER JOIN  T_ORG_ADMIN B ON A.CFManagerDeptCodeI=B.FID where a.fnumber='"+ abc + "'";
//								rs= SQLExecutorFactory.getLocalInstance(ctx,strSql).executeSQL();
//								try {
//
//									if (rs.next())
//									{
//										FraMainPositionDuty= rs.getString("FNUMBER");
//
//
//									}
//
//
//								} catch (SQLException e) {
//									// TODO Auto-generated catch block
//									e.printStackTrace();
//								}
//

                            strSql01="SELECT FName_L2 as name FROM [HG_LINK].[hg].dbo.t_bd_materialgroup where fnumber='"+ MatType01 + "'";
                            rs01 = sqlService.getList(CloudSqlService.htEas, strSql01);
                            try {

                                String value =  (String)rs01.get(0).get("name");

                                if(value!=null){
                                    MaterialName01= (String)rs01.get(0).get("name");
                                    if (ArchType.substring(0, 2).equalsIgnoreCase("20"))
                                    {
                                        FolderAttr="WD"+MatArchiveType+"-"+OrgCode+"-"+MatType01+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                        ParentFolderAttr=FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."))+  " CWD "+MatArchiveType+" "+deptsimplename;
                                    }
                                    if (ArchType.substring(0, 2).equalsIgnoreCase("17"))
                                    {
                                        FolderAttr="AI"+"-"+OrgCode+"-"+MatType01+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                        ParentFolderAttr=FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."))+  " AI ";
                                    }
                                    JSONObject ParentFolderIdObj = api.getFileID(ParentFolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                                    JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                                    JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                                    ParentFolderId = ParentFolderIdObj0.getString("id");
                                    FolderName=MatType01+" "+MaterialName01;
                                    JSONObject FolderIdObj = api.getFileID(FolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                                    int SubFolderNum=Integer.parseInt(FolderIdObj.getString("num"));
                                    if(SubFolderNum==0){
                                        api.getAddFolder( ParentFolderId,MatType01+" "+MaterialName01, FolderAttr);
                                    }
                                    else{
                                        JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                        JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                                        ParentFolderId = FolderIdObj0.getString("id");
                                        api.getRename(token, ParentFolderId, FolderName);
                                    }
                                }

                                } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }




                            strSql02="SELECT FName_L2 as name FROM [HG_LINK].[hg].dbo.t_bd_materialgroup where fnumber='"+MatType01+ MatType02 + "'";
                            rs02 = sqlService.getList(CloudSqlService.htEas, strSql02);
                                try {

                                    String value =  (String)rs02.get(0).get("name");

                                    if(value!=null){
                                        MaterialName02= (String)rs02.get(0).get("name");
                                        if (ArchType.substring(0, 2).equalsIgnoreCase("20"))
                                        {
                                            ParentFolderAttr="WD"+MatArchiveType+"-"+OrgCode+"-"+MatType01+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                            FolderAttr="WD"+MatArchiveType+"-"+OrgCode+"-"+MatType01+MatType02+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                        }
                                        if (ArchType.substring(0, 2).equalsIgnoreCase("17"))
                                        {
                                            ParentFolderAttr="AI"+"-"+OrgCode+"-"+MatType01+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                            FolderAttr="AI"+"-"+OrgCode+"-"+MatType01+MatType02+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                        }
                                        JSONObject ParentFolderIdObj = api.getFileID(ParentFolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                                        JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                                        JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                                        ParentFolderId = ParentFolderIdObj0.getString("id");
                                        FolderName=MatType02+" "+MaterialName02;
                                        JSONObject FolderIdObj = api.getFileID(FolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                                        int SubFolderNum=Integer.parseInt(FolderIdObj.getString("num"));
                                        if(SubFolderNum==0){
                                            api.getAddFolder( ParentFolderId,FolderName, FolderAttr);
                                        }
                                        else{
                                            JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                            JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                                            ParentFolderId = FolderIdObj0.getString("id");
                                            api.getRename(token, ParentFolderId, FolderName);
                                        }
                                    }

                                } catch (Exception e02) {
                                    // TODO Auto-generated catch block
                                    e02.printStackTrace();
                                }



                            strSql03="SELECT FName_L2 as name FROM [HG_LINK].[hg].dbo.t_bd_materialgroup where fnumber='"+ MatType01+MatType02+MatType03 + "'";
                             rs03 = sqlService.getList(CloudSqlService.htEas, strSql03);
                                try {

                                    String value =  (String)rs03.get(0).get("name");

                                    if(value!=null){
                                        MaterialName03= (String)rs03.get(0).get("name");
                                        if (ArchType.substring(0, 2).equalsIgnoreCase("20"))
                                        {
                                            ParentFolderAttr="WD"+MatArchiveType+"-"+OrgCode+"-"+MatType01+MatType02+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                            FolderAttr="WD"+MatArchiveType+"-"+OrgCode+"-"+MatType01+MatType02+MatType03+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                        }
                                        if (ArchType.substring(0, 2).equalsIgnoreCase("17"))
                                        {
                                            ParentFolderAttr="AI"+"-"+OrgCode+"-"+MatType01+MatType02+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                            FolderAttr="AI"+"-"+OrgCode+"-"+MatType01+MatType02+MatType03+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                        }
                                        JSONObject ParentFolderIdObj = api.getFileID(ParentFolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                                        JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                                        JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                                        ParentFolderId = ParentFolderIdObj0.getString("id");
                                        FolderName=MatType03+" "+MaterialName03;
                                        JSONObject FolderIdObj = api.getFileID(FolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                                        int SubFolderNum=Integer.parseInt(FolderIdObj.getString("num"));
                                        if(SubFolderNum==0){
                                            api.getAddFolder( ParentFolderId,FolderName, FolderAttr);
                                        }
                                        else{
                                            JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                            JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                                            ParentFolderId = FolderIdObj0.getString("id");
                                            api.getRename(token, ParentFolderId, FolderName);
                                        }
                                    }
                                } catch (Exception e03) {
                                    // TODO Auto-generated catch block
                                    e03.printStackTrace();
                                }




                            strSql04="SELECT FName_L2 as name FROM [HG_LINK].[hg].dbo.t_bd_material where fnumber='"+ MatCode + "'";
                            rs04 = sqlService.getList(CloudSqlService.htEas, strSql04);
                                try {

                                    String value =  (String)rs04.get(0).get("name");

                                    if(value!=null){
                                        MaterialName04= (String)rs04.get(0).get("name");
                                        if (ArchType.substring(0, 2).equalsIgnoreCase("20"))
                                        {
                                            ParentFolderAttr="WD"+MatArchiveType+"-"+OrgCode+"-"+MatType01+MatType02+MatType03+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                            FolderAttr="WD"+MatArchiveType+"-"+OrgCode+"-"+MatCode+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                        }
                                        if (ArchType.substring(0, 2).equalsIgnoreCase("17"))
                                        {
                                            ParentFolderAttr="AI"+"-"+OrgCode+"-"+MatType01+MatType02+MatType03+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                            FolderAttr="AI"+"-"+OrgCode+"-"+MatCode+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                        }
                                        JSONObject ParentFolderIdObj = api.getFileID(ParentFolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                                        JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                                        JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                                        ParentFolderId = ParentFolderIdObj0.getString("id");
                                        FolderName=MatCode+" "+MaterialName04;
                                        FolderName=FolderName+"\\";
                                        FolderName=FolderName.substring(0,FolderName.indexOf("\\"));
                                        FolderName=FolderName+"≥";
                                        FolderName=FolderName.substring(0,FolderName.indexOf("≥"));
                                        FolderName=FolderName+"-";
                                        FolderName=FolderName.substring(0,FolderName.indexOf("-"));
//										FolderName=FolderName+" ";
//										FolderName=FolderName.substring(0,FolderName.lastIndexOf(" "));
//										FolderName=FolderName.replaceAll("\\\\", " ");



                                        JSONObject FolderIdObj = api.getFileID(FolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                                        int SubFolderNum=Integer.parseInt(FolderIdObj.getString("num"));
                                        if(SubFolderNum==0){
                                            api.getAddFolder( ParentFolderId,FolderName, FolderAttr);
                                        }
                                        else{
                                            JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                            JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                                            String FolderId = FolderIdObj0.getString("id");
                                            api.getRename(token, FolderId, FolderName);
                                            System.out.println("FolderName");

                                        }



//										String FrameCode="WD"+MatArchiveType+"-"+OrgCode+"-"+MatCode+"-0";
//										strSql05="SELECT FName_L2 as name FROM dbo.t_cma_comarchbase where fnumber='"+ FrameCode + "'";
//										rs05= SQLExecutorFactory.getLocalInstance(ctx,strSql05).executeSQL();
//											if (rs05.next())
//											{
//												FolderName= MatCode+" "+rs05.getString("name");
//												JSONObject FolderIdObj = api.getFileID(FolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
//												int SubFolderNum=Integer.parseInt(FolderIdObj.getString("num"));
//												FolderName=FolderName.replace("%", "");
//												if(SubFolderNum==0){
//													api.getAddFolder( ParentFolderId,FolderName, FolderAttr);
////													System.out.println("SubFolderNum");
//													}
//												else{
//													JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
//													JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
//													ParentFolderId = FolderIdObj0.getString("id");
//													api.getRename(token, ParentFolderId, FolderName);
//												}
//											}
                                    }
                                } catch (Exception e04) {
                                    // TODO Auto-generated catch block
                                    e04.printStackTrace();
                                }

                            if( SubItemORNot>0){
                                //带分项的文件
                                // 先加文件夹

                                prefix =attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length());//文件夹前缀
                                foldername = prefix + ' ' + (String) objectMap.get("fileName");//
                                String parentattribute =attribute.replace(" ", "").substring(0,attribute.replace(" ", "").lastIndexOf("-"))+ ' '+ FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                JSONObject FolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                                int SubFolderNum=Integer.parseInt(FolderIdObj.getString("num"));
                                if(SubFolderNum==0){
                                    continue;
                                }
                                JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                                String folederid = FolderIdObj0.getString("id");
                                String SubFolderAttribute =attribute+ ' '+   FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                JSONObject SubFolderIdObj = api.getFileID(SubFolderAttribute);// 分项文件夹是否存在
                                int num=Integer.parseInt(SubFolderIdObj.getString("num"));
                                if(num>0){
                                    JSONArray SubFolderIdDataArray = SubFolderIdObj.getJSONArray("data");
                                    JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(0);
                                    String SubFolderId = SubFolderIdObj0.getString("id");
                                    api.getRename(token, SubFolderId, foldername);
                                }
                                else{
                                    api.getAddFolder( folederid,foldername,attribute + ' '+   FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf(".")));
                                }
                                //查询分项文件对应文件夹的FolderId
                                String ExistFolderAttribute =attribute + ' '+    FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                JSONObject ExistFolderObj = api.getFileID(ExistFolderAttribute);//
                                JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                                JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(0);
                                String ExitFolderId = ExistFolderObj0.getString("id");
                                AuthFileId=ExitFolderId;
                                JSONObject ExitFolderFullPath = api.getFullPath(ExitFolderId);
                                String SubItemFolderDestFullpath = ExitFolderFullPath.getString("fullPath");
                                JSONObject ExitFolderListObj=api.getList( ExitFolderId);
                                JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                                int m=ExitFolderListDataArray.size();
                                for(int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++)
                                {
                                    JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                    String IsFolder=ExitFolderListDataObj.getString("fileType");
                                    if(!IsFolder.equals("1")){
                                        delId=ExitFolderListDataObj.getString("id");
                                        api.getRemove(token, delId);
                                        System.out.println(ExitFolderListDataObj.get("id"));}
                                }
                                //移动公共区域的分项文件到对应路径
                                for(int Sub=0;Sub<SubItemORNot;Sub++)
                                {
                                    String SubItemattribute = SubItemFileList[Sub].replace("，", ",");//02-A-01-16-02-14-01-01-02,01
                                    JSONObject SubItemFileIdObj=api.getFileID( SubItemattribute);
                                    JSONArray SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                    JSONObject SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                    String SubItemFileId = SubItemFileIdDataObj.getString("id");
//				AuthFileId=SubItemFileId;
                                    api.getMove( SubItemFileId,SubItemFolderDestFullpath);//
                                    if(IsNomalFile==1){//有分项也有正常文件时候，正常文件也传过去
                                        SubItemFileIdObj=api.getFileID( attribute);
                                        SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                        SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                        SubItemFileId = SubItemFileIdDataObj.getString("id");
                                        api.getMove( SubItemFileId,SubItemFolderDestFullpath);//
                                    }

                                    // 获取主责岗位角色及其上级直至董事长，并授权
                                }


                                // 获取主责岗位角色及其上级直至董事长，并授权
                                if (PosMainDuty != null) {
                                     
                                    PosMainName= PosMainName;
                                    RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosMainDuty, AuthFileId);
                                }
                                // 获取岗位B角色及其上级直至董事长，并授权
                                if (PosB != null)
                                {
                                    PosBDuty = PosB;
                                    PosBName=PosBName;
                                    RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosBDuty, AuthFileId);
                                }
                                // 获取岗位A角色及其上级直至董事长，并授权
                                if (PosA != null)
                                {
                                    PosADuty = PosA;
                                    PosAName=PosAName;
                                    RoleAuth.RoleAAuthDis(PosADuty, PosAName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosADuty, PosAName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosADuty, AuthFileId);
                                }


                                continue;
                            }
                            // 没有分项的情况
                            elec = (String) subSheet.get(i).get("elec");
                            attribute = attribute;// 从表单获取文件扩展属性==档案代码
                            prefix = attribute.substring(attribute.lastIndexOf("-")+1, attribute.length());
                            FolderAttribute = attribute + ' '+ FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));// 从表单获取文件夹扩展属性（=档案代码+主责部门）
                            addfileid = "";
                            desc=(String) objectMap.get("des");
                            String CAAttribute = "";
                            // 如果是框架则在文档系统中加对应的文件夹
                            if (desc != null) {
                                if (desc.length() == 2) {
                                    if (desc.substring(0, 2).equalsIgnoreCase("框架")) {
                                        // 添加文件夹到指定路径
                                        // 根据扩展属性获取将添加的框架的parentId
                                        String parentattribute = attribute.substring(0,attribute.lastIndexOf("-"))+ ' ' +FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));// 对应上级文件夹扩展属性
                                        attribute = attribute + ' '+ FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));
                                        JSONObject ParentFolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id
                                        int ParentFolderNum=Integer.parseInt(ParentFolderIdObj.getString("num"));
                                        if(ParentFolderNum==0){
                                            continue;
                                        }
                                        JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                                        JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                                        ParentFolderId = ParentFolderIdObj0.getString("id");
                                        FolderName = prefix + ' ' + (String) objectMap.get("fileName");// 从表单获取文件夹名称
                                        String FrameAttribute=attribute;
                                        JSONObject FrameAttributeObj=api.getFileID( FrameAttribute);
                                        String FrameNum=FrameAttributeObj.getString("num");
                                        int FrameNum01=Integer.parseInt(FrameNum);
                                        if(FrameNum01>0){
                                            JSONArray FrameAttributeDataArray = FrameAttributeObj.getJSONArray("data");
                                            JSONObject FrameAttributeDataObj = FrameAttributeDataArray.getJSONObject(0);
                                            String fileId = FrameAttributeDataObj.getString("id");
                                            api.getRename(token, fileId, FolderName);
                                        }
                                        else{
                                            api.getAddFolder(ParentFolderId, FolderName, FolderAttribute);}
                                    }
                                }
                            }

                            if (elec.equalsIgnoreCase("√")&&IsNomalFile==1) {
                                CAAttribute=attribute;
                                String	CALevelParAttribute=CAAttribute.substring(0, CAAttribute.lastIndexOf("-"))+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf("."));//上级文件夹扩展属性SD-02-142331024-9002-31 02.06.07.003
                                JSONObject CAParentFolderIdObj=api.getFileID( CALevelParAttribute);
                                int CAParentFolderNum=Integer.parseInt(CAParentFolderIdObj.getString("num"));
                                if(CAParentFolderNum==0){
                                    continue;
                                }
                                JSONArray CAParentFolderIdDataArray = CAParentFolderIdObj.getJSONArray("data");
                                JSONObject CAParentFolderIdDataObj = CAParentFolderIdDataArray.getJSONObject(0);
                                String CAParentFolderId = CAParentFolderIdDataObj.getString("id");

                                //查找文件对应文件夹
                                JSONObject ExistFolderIdObj=api.getFileID( CAAttribute+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf(".")));
                                int CAArrFolderNum=Integer.parseInt(ExistFolderIdObj.getString("num"));
                                JSONObject FolderIdObj=null;
                                if(CAArrFolderNum==0)
                                {
                                    //如果不存在文件对应文件夹则先添加对应文件夹
                                    FolderIdObj=api.getAddFolder( CAParentFolderId, attribute.substring(attribute.lastIndexOf("-")+1, attribute.length()) +" "+(String) objectMap.get("fileName"), CAAttribute+" "+FraMainPositionDuty.substring(0, FraMainPositionDuty.lastIndexOf(".")));
                                    //获取对应文件夹的FolderId，以便之后将文件放入该文件夹
                                    addfileid = FolderIdObj.getString("fileId");
                                }
                                //如果存在文件对应文件夹则先需要找到其FolderId
                                else{
                                    JSONArray  ExistFolderIdDataArray = ExistFolderIdObj.getJSONArray("data");
                                    JSONObject ExistFolderIdDataObj = ExistFolderIdDataArray.getJSONObject(0);
                                    addfileid = ExistFolderIdDataObj.getString("id");
                                    api.getRename(token, addfileid, attribute.substring(attribute.lastIndexOf("-")+1, attribute.length()) +" "+(String) objectMap.get("fileName"));
                                    JSONObject ExitFolderListObj=api.getList( addfileid);
                                    JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                                    for(int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++)
                                    {
                                        JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                        String IsFolder=ExitFolderListDataObj.getString("fileType");
                                        String filename=ExitFolderListDataObj.getString("fileName");
                                        if(!IsFolder.equals("1")&&SubItemORNot>0){

                                            delId=ExitFolderListDataObj.getString("id");
                                            api.getRemove(token, delId);
                                            System.out.println(ExitFolderListDataObj.get("id"));
                                        }
                                    }
                                }
                                JSONObject FolderPathObj = api.getFullPath( addfileid);
                                String CAdestfullpath = FolderPathObj.getString("fullPath");
                                JSONObject FileID = api.getFileID( CAAttribute);
                                int CAArrFileNum=Integer.parseInt(FileID.getString("num"));
                                if(CAArrFileNum==0){
                                    continue;
                                }

                                JSONArray FileIDDataArray = FileID.getJSONArray("data");
                                JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                                fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                                String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                                String NewFileName=FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));
                                if(FileIDDataArray.size()==1){
                                    api.getMove( fileid,CAdestfullpath);
                                }//有2个及以上相同扩展属性的文件时，A,B岗位未变更
                                else if(!addfileid.equals(Parentid2)) {

                                    JSONObject FolderPathObj01 = api.getFullPath( "1688009");
                                    String CAdestfullpath01 = FolderPathObj01.getString("fullPath");
                                    api.getMove( fileid2,CAdestfullpath01);//将老文件放入userFolder


//		                          		api.getMove( fileid,CAdestfullpath);
                                    JSONObject  ListFileAuth= api.getListFileAuth(token,fileid2);
                                    JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                                    int a=ListFileAuthDataArray.size();
                                    for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++)
                                    {
                                        JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                        String RemoveAuthPos= ListFileAuthDataObj.getString("name");
                                        String auth= ListFileAuthDataObj.getString("auth");
                                        JSONObject array1=api.getRole(RemoveAuthPos);
                                        String num=array1.getString("num");
                                        if(!num.equals("0")){
                                            JSONArray array = array1.getJSONArray("data");
                                            JSONObject array2 = array.getJSONObject(0);
                                            String userId = array2.getString("id");
                                            api.getRoleAuth( RemoveAuthPos, userId, auth, fileid);}
                                    }
                                    api.getRemove(token, fileid2);
//		            					api.getRename(token, fileid2, NewFileName);
//		            					String MOVEUpdate = api.getUpdate(token,fileid, fileid2);
                                }
                                //有2个及以上相同扩展属性的文件时，A,B岗位未变更
                                else if(FileIDDataArray.size()>1) {
//


                                    //将老文件放入userFolder
                                    JSONObject FolderPathObj01 = api.getFullPath( "1688009");
                                    String CAdestfullpath01 = FolderPathObj01.getString("fullPath");
                                    api.getMove( fileid2,CAdestfullpath01);//将老文件放入userFolder
                                    api.getRename(token, fileid2, "待删除");//先将老文件名改为"待删除"
                                    api.getMove( fileid,CAdestfullpath);//将新文件移动到对应文件夹，此时和老文件放在同一个文件夹
                                    JSONObject  ListFileAuth= api.getListFileAuth(token,fileid2);//获取老文件的权限
                                    JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                                    int a=ListFileAuthDataArray.size();
                                    for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++)
                                    {//将老文件的权限授予新文件

                                        JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                        String RemoveAuthPos= ListFileAuthDataObj.getString("name");
                                        String auth= ListFileAuthDataObj.getString("auth");
                                        JSONObject array1=api.getRole(RemoveAuthPos);
                                        String num=array1.getString("num");
                                        if(!num.equals("0"))
                                        {
                                            JSONArray array = array1.getJSONArray("data");
                                            JSONObject array2 = array.getJSONObject(0);
                                            String userId = array2.getString("id");
                                            api.getRoleAuth( RemoveAuthPos, userId, auth, fileid);
                                        }
                                    }
                                    api.getRemove(token, fileid2);//删除老文件
                                }
                                AuthFileId=fileid;
                                // 获取主责岗位角色及其上级直至董事长，并授权
                                if (PosMainDuty != null) {
                                     
                                    PosMainName= PosMainName;
                                    RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosMainDuty, AuthFileId);
                                }
                                // 获取岗位B角色及其上级直至董事长，并授权
                                if (PosB != null)
                                {
                                    PosBDuty = PosB;
                                    PosBName=PosBName;
                                    RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosBDuty, AuthFileId);
                                }
                                // 获取岗位A角色及其上级直至董事长，并授权
                                if (PosA != null)
                                {
                                    PosADuty = PosA;
                                    PosAName=PosAName;
                                    RoleAuth.RoleAAuthDis(PosADuty, PosAName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosADuty, PosAName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosADuty, AuthFileId);
                                }

                            }
                        }

                        if (ArchType.equalsIgnoreCase("15")) {
                            desc = (String) objectMap.get("des");// 获取说明
                            if( SubItemORNot>0){
                                //带分项的文件 // 先加文件夹
                                prefix =attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length());//文件夹前缀
                                foldername = prefix + ' ' + (String) objectMap.get("fileName");//
                                String parentattribute =attribute.replace(" ", "").substring(0,attribute.replace(" ", "").lastIndexOf("-"))+ ' '+ MainDeptCode;
                                JSONObject FolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                                int SubFolderNum=Integer.parseInt(FolderIdObj.getString("num"));
                                if(SubFolderNum==0){
                                    continue;
                                }
                                JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                                String folederid = FolderIdObj0.getString("id");
                                String SubFolderAttribute =attribute+ ' '+  MainDeptCode;
                                JSONObject SubFolderIdObj = api.getFileID(SubFolderAttribute);// 分项文件夹是否存在
                                int num=Integer.parseInt(SubFolderIdObj.getString("num"));
                                if(num>0){
                                    JSONArray SubFolderIdDataArray = SubFolderIdObj.getJSONArray("data");
                                    JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(0);
                                    String SubFolderId = SubFolderIdObj0.getString("id");
                                    api.getRename(token, SubFolderId, foldername);
                                }
                                else{
                                    api.getAddFolder( folederid,foldername,attribute + ' '+  MainDeptCode);
                                }
                                //查询分项文件对应文件夹的FolderId
                                String ExistFolderAttribute =attribute + ' '+   MainDeptCode;
                                JSONObject ExistFolderObj = api.getFileID(ExistFolderAttribute);//
                                JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                                JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(0);
                                String ExitFolderId = ExistFolderObj0.getString("id");
                                AuthFileId=ExitFolderId;
                                JSONObject ExitFolderFullPath = api.getFullPath( ExitFolderId);
                                String SubItemFolderDestFullpath = ExitFolderFullPath.getString("fullPath");
                                JSONObject ExitFolderListObj=api.getList( ExitFolderId);
                                JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                                int m=ExitFolderListDataArray.size();
                                for(int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++)
                                {
                                    JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                    String IsFolder=ExitFolderListDataObj.getString("fileType");
                                    if(!IsFolder.equals("1")&&SubItemORNot>0){
                                        delId=ExitFolderListDataObj.getString("id");
                                        api.getRemove(token, delId);
                                        System.out.println(ExitFolderListDataObj.get("id"));}
                                }
                                //移动公共区域的分项文件到对应路径
                                for(int Sub=0;Sub<SubItemORNot;Sub++)
                                {
                                    String SubItemattribute = SubItemFileList[Sub].replace("，", ",");//02-A-01-16-02-14-01-01-02,01
                                    JSONObject SubItemFileIdObj=api.getFileID( SubItemattribute);
                                    JSONArray SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                    JSONObject SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                    String SubItemFileId = SubItemFileIdDataObj.getString("id");
//								AuthFileId=SubItemFileId;
                                    api.getMove( SubItemFileId,SubItemFolderDestFullpath);//

                                    if(IsNomalFile==1){
                                        //有分项也有正常文件时候，正常文件也传过去
                                        SubItemFileIdObj=api.getFileID( attribute);
                                        SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                        SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                        SubItemFileId = SubItemFileIdDataObj.getString("id");
                                        api.getMove( SubItemFileId,SubItemFolderDestFullpath);//
                                    }
                                }


                                // 获取主责岗位角色及其上级直至董事长，并授权
                                if (PosMainDuty != null) {
                                     
                                    PosMainName= PosMainName;
                                    RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosMainDuty, AuthFileId);
                                }
                                // 获取岗位B角色及其上级直至董事长，并授权
                                if (PosB != null)
                                {
                                    PosBDuty = PosB;
                                    PosBName=PosBName;
                                    RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosBDuty, AuthFileId);
                                }
                                // 获取岗位A角色及其上级直至董事长，并授权
                                if (PosA != null)
                                {
                                    PosADuty = PosA;
                                    PosAName=PosAName;
                                    RoleAuth.RoleAAuthDis(PosADuty, PosAName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosADuty, PosAName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosADuty, AuthFileId);
                                }


                                continue;
                            }
                            // 没有分项的情况
                            elec = (String) subSheet.get(i).get("elec");
                            attribute =attribute.replace(" ", "");// 从表单获取文件扩展属性==档案代码
                            prefix = attribute.substring(attribute.lastIndexOf("-")+1, attribute.length());
                            FolderAttribute =attribute.replace(" ", "") + ' '+ MainDeptCode;// 从表单获取文件夹扩展属性（=档案代码+主责部门）
                            addfileid = "";
                            desc=(String) objectMap.get("des");
                            String CAAttribute = "";
                            // 如果是框架则在文档系统中加对应的文件夹
                            if (desc != null) {
                                if (desc.length() >= 2) {
                                    String asjhd=desc.substring(0, 2);
                                    if (desc.substring(0, 2).equalsIgnoreCase("框架")) {

                                        //√转为/，删除原文件(无论是否分项文件都给删掉）
                                        JSONObject ExistFileObj = api.getFileID(attribute);//
                                        int FileNum=Integer.parseInt(ExistFileObj.getString("num"));
                                        if(FileNum!=0){
                                            JSONArray ExistFileDataArray = ExistFileObj.getJSONArray("data");
                                            JSONObject ExistFileObj0 = ExistFileDataArray.getJSONObject(0);
                                            String ExitFileId = ExistFileObj0.getString("id");
                                            api.getRemove(token, ExitFileId);
                                        }
                                        else{
                                            String asdf=attribute.replace(" ", "")+" "+ MainDeptCode;
                                            JSONObject ExitFolderObj=api.getFileID( attribute.replace(" ", "")+" "+ MainDeptCode);
                                            FileNum=Integer.parseInt(ExitFolderObj.getString("num"));
                                            if(FileNum!=0){
                                                JSONArray ExitFolderDataArray = ExitFolderObj.getJSONArray("data");
                                                JSONObject ExitFolderObj0 = ExitFolderDataArray.getJSONObject(0);
                                                String ExitFolderId = ExitFolderObj0.getString("id");
                                                JSONObject ExitFolderListObj=api.getList( ExitFolderId);
//										    JSONObject ExitFolderListObj=api.getList( attribute.replace(" ", "")+" "+ MainDeptCode);
                                                JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                                                for(int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++)
                                                {
                                                    JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                                    String IsFolder=ExitFolderListDataObj.getString("fileType");
                                                    if(!IsFolder.equals("1")&&SubItemORNot>0){
                                                        delId=ExitFolderListDataObj.getString("id");
                                                        api.getRemove(token, delId);
                                                        System.out.println(ExitFolderListDataObj.get("id"));}
                                                }
                                            }
                                        }



                                        // 添加文件夹到指定路径
                                        // 根据扩展属性获取将添加的框架的parentId
                                        String parentattribute = attribute.substring(0,attribute.lastIndexOf("-"))+ ' ' + MainDeptCode;// 对应上级文件夹扩展属性
                                        attribute =attribute.replace(" ", "") + ' '+ MainDeptCode;
                                        JSONObject ParentFolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id
                                        int ParentFolderNum=Integer.parseInt(ParentFolderIdObj.getString("num"));
                                        if(ParentFolderNum==0){
                                            continue;
                                        }
                                        JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                                        JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                                        ParentFolderId = ParentFolderIdObj0.getString("id");
                                        foldername = prefix + ' ' + (String) objectMap.get("fileName");// 从表单获取文件夹名称
                                        String FrameAttribute=attribute;
                                        JSONObject FrameAttributeObj=api.getFileID( FrameAttribute);
                                        String FrameNum=FrameAttributeObj.getString("num");
                                        int FrameNum01=Integer.parseInt(FrameNum);
                                        if(FrameNum01>0){
                                            JSONArray FrameAttributeDataArray = FrameAttributeObj.getJSONArray("data");
                                            JSONObject FrameAttributeDataObj = FrameAttributeDataArray.getJSONObject(0);
                                            String fileId = FrameAttributeDataObj.getString("id");
                                            api.getRename(token, fileId, foldername);
                                        }
                                        else{
                                            api.getAddFolder(ParentFolderId, foldername, FolderAttribute);}
                                    }
                                }
                            }

                            if (elec.equalsIgnoreCase("√")&&SubItemORNot==0&&IsNomalFile==1) {
                                CAAttribute=attribute;
                                String	CALevelParAttribute=CAAttribute.substring(0, CAAttribute.lastIndexOf("-"))+" "+MainDeptCode;//上级文件夹扩展属性SD-02-142331024-9002-31 02.06.07.003
                                JSONObject CAParentFolderIdObj=api.getFileID( CALevelParAttribute);
                                int CAParentFolderNum=Integer.parseInt(CAParentFolderIdObj.getString("num"));
                                if(CAParentFolderNum==0){
                                    continue;
                                }
                                JSONArray CAParentFolderIdDataArray = CAParentFolderIdObj.getJSONArray("data");
                                JSONObject CAParentFolderIdDataObj = CAParentFolderIdDataArray.getJSONObject(0);
                                String CAParentFolderId = CAParentFolderIdDataObj.getString("id");

                                //查找文件对应文件夹
                                JSONObject ExistFolderIdObj=api.getFileID( CAAttribute+" "+MainDeptCode);
                                int CAArrFolderNum=Integer.parseInt(ExistFolderIdObj.getString("num"));
                                JSONObject FolderIdObj=null;
                                if(CAArrFolderNum==0)
                                {
                                    //如果不存在文件对应文件夹则先添加对应文件夹
                                    FolderIdObj=api.getAddFolder( CAParentFolderId,attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"), CAAttribute+" "+MainDeptCode);
                                    //获取对应文件夹的FolderId，以便之后将文件放入该文件夹
                                    addfileid = FolderIdObj.getString("fileId");
                                }
                                //如果存在文件对应文件夹则先需要找到其FolderId
                                else{
                                    JSONArray  ExistFolderIdDataArray = ExistFolderIdObj.getJSONArray("data");
                                    JSONObject ExistFolderIdDataObj = ExistFolderIdDataArray.getJSONObject(0);
                                    addfileid = ExistFolderIdDataObj.getString("id");
                                    api.getRename(token, addfileid,attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"));
                                    JSONObject ExitFolderListObj=api.getList( addfileid);
                                    JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                                    for(int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++)
                                    {
                                        JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                        String IsFolder=ExitFolderListDataObj.getString("fileType");
                                        String filename=ExitFolderListDataObj.getString("fileName");
                                        if(!IsFolder.equals("1")&&SubItemORNot>0){

                                            delId=ExitFolderListDataObj.getString("id");
                                            api.getRemove(token, delId);
                                            System.out.println(ExitFolderListDataObj.get("id"));
                                        }
                                    }
                                }
                                JSONObject FolderPathObj = api.getFullPath( addfileid);
                                String CAdestfullpath = FolderPathObj.getString("fullPath");



                                JSONObject FileID = api.getFileID( CAAttribute);
                                int CAArrFileNum=Integer.parseInt(FileID.getString("num"));
                                if(CAArrFileNum==0){
                                    continue;
                                }

                                JSONArray FileIDDataArray = FileID.getJSONArray("data");
                                JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                                fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                                String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                                String NewFileName=FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));
                                if(FileIDDataArray.size()==1){
                                    api.getMove( fileid,CAdestfullpath);
                                }//有2个及以上相同扩展属性的文件时，A,B岗位变更

                                else if(!addfileid.equals(Parentid2)) {
//                            	api.getRename(token, fileid2, "待删除");

                                    JSONObject FolderPathObj01 = api.getFullPath( "1688009");
                                    String CAdestfullpath01 = FolderPathObj01.getString("fullPath");
                                    api.getMove( fileid2,CAdestfullpath01);//将老文件放入userFolder
//
//                            	api.getMove( fileid,CAdestfullpath);

                                    JSONObject  ListFileAuth= api.getListFileAuth(token,fileid2);
                                    JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                                    int a=ListFileAuthDataArray.size();
                                    for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++)
                                    {

                                        JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                        String RemoveAuthPos= ListFileAuthDataObj.getString("name");
                                        String auth= ListFileAuthDataObj.getString("auth");
                                        JSONObject array1=api.getRole(RemoveAuthPos);
                                        String num=array1.getString("num");
                                        if(!num.equals("0")){
                                            JSONArray array = array1.getJSONArray("data");
                                            JSONObject array2 = array.getJSONObject(0);
                                            String userId = array2.getString("id");
                                            api.getRoleAuth( RemoveAuthPos, userId, auth, fileid);}
                                    }
//				               	api.getRename(token, fileid2, NewFileName);
//								String MOVEUpdate = api.getUpdate(token,fileid, fileid2);
                                    api.getRemove(token, fileid2);
                                }
                                //有2个及以上相同扩展属性的文件时，A,B岗位未变更
                                else if(FileIDDataArray.size()>1) {
//


                                    //将老文件放入userFolder
                                    JSONObject FolderPathObj01 = api.getFullPath( "1688009");
                                    String CAdestfullpath01 = FolderPathObj01.getString("fullPath");
                                    api.getMove( fileid2,CAdestfullpath01);//将老文件放入userFolder
                                    api.getRename(token, fileid2, "待删除");//先将老文件名改为"待删除"
                                    api.getMove( fileid,CAdestfullpath);//将新文件移动到对应文件夹，此时和老文件放在同一个文件夹
                                    JSONObject  ListFileAuth= api.getListFileAuth(token,fileid2);//获取老文件的权限
                                    JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                                    int a=ListFileAuthDataArray.size();
                                    for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++)
                                    {//将老文件的权限授予新文件

                                        JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                        String RemoveAuthPos= ListFileAuthDataObj.getString("name");
                                        String auth= ListFileAuthDataObj.getString("auth");
                                        JSONObject array1=api.getRole(RemoveAuthPos);
                                        String num=array1.getString("num");
                                        if(!num.equals("0"))
                                        {
                                            JSONArray array = array1.getJSONArray("data");
                                            JSONObject array2 = array.getJSONObject(0);
                                            String userId = array2.getString("id");
                                            api.getRoleAuth( RemoveAuthPos, userId, auth, fileid);
                                        }
                                    }
                                    api.getRemove(token, fileid2);//删除老文件
                                }
                                AuthFileId=fileid;

                                // 获取主责岗位角色及其上级直至董事长，并授权
                                if (PosMainDuty != null) {
                                    CAAttribute=attribute;
                                     
                                    PosMainName= PosMainName;
                                    RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    while(CAAttribute.contains("-"))	{
                                        CAAttribute=CAAttribute.substring(0, CAAttribute.lastIndexOf("-"));
                                        JSONObject AuthFileID = api.getFileID( CAAttribute);
                                        int CAArrAuthFileNum=Integer.parseInt(AuthFileID.getString("num"));
                                        if(CAArrAuthFileNum!=0){


                                            JSONArray AuthFileIDDataArray = AuthFileID.getJSONArray("data");
                                            JSONObject AuthFileIDDataObj = AuthFileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                            String authfileid = AuthFileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                            RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, authfileid);
                                            RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, authfileid);
                                        }
                                    }
                                }
                                // 获取岗位B角色及其上级直至董事长，并授权
                                if (PosB != null)
                                {
                                    CAAttribute=attribute;
                                    PosBDuty = PosB;
                                    PosBName=PosBName;
                                    RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    while(CAAttribute.contains("-"))	{
                                        CAAttribute=CAAttribute.substring(0, CAAttribute.lastIndexOf("-"));
                                        JSONObject AuthFileID = api.getFileID( CAAttribute);
                                        int CAArrAuthFileNum=Integer.parseInt(AuthFileID.getString("num"));
                                        if(CAArrAuthFileNum!=0){


                                            JSONArray AuthFileIDDataArray = AuthFileID.getJSONArray("data");
                                            JSONObject AuthFileIDDataObj = AuthFileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                            String authfileid = AuthFileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                            RoleAuth.RoleAuthDis(PosBDuty, PosBName, authfileid);
                                            RoleAuth.AssRoleAuthDis(PosBDuty, PosBName, authfileid);
                                        }
                                    }
                                }
                                // 获取岗位A角色及其上级直至董事长，并授权
                                if (PosA != null)
                                {
                                    CAAttribute=attribute;
                                    PosADuty = PosA;
                                    PosAName=PosAName;
                                    RoleAuth.RoleAAuthDis(PosADuty, PosAName, AuthFileId);
//		                        api.getRoleAuth( "02.01", "综合管理部","A", AuthFileId);
//		                        RoleAuth.RoleAAuthDis("02.01", "综合管理部", AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosADuty, PosAName, AuthFileId);
                                    while(CAAttribute.contains("-"))	{
                                        CAAttribute=CAAttribute.substring(0, CAAttribute.lastIndexOf("-"));
                                        JSONObject AuthFileID = api.getFileID( CAAttribute);
                                        int CAArrAuthFileNum=Integer.parseInt(AuthFileID.getString("num"));
                                        if(CAArrAuthFileNum!=0){


                                            JSONArray AuthFileIDDataArray = AuthFileID.getJSONArray("data");
                                            JSONObject AuthFileIDDataObj = AuthFileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                            String authfileid = AuthFileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                            RoleAuth.RoleAuthDis(PosADuty, PosAName, authfileid);
                                            RoleAuth.AssRoleAuthDis(PosADuty, PosAName, authfileid);


                                            RoleNumAss=PosADuty.substring(0, PosADuty.lastIndexOf("."))+".006";
                                            try {
                                                    String posname="";

                                                    String  SQL="select  fname_l2  from  [HG_LINK].[hg].dbo.T_ORG_Position where  fnumber='"+RoleNumAss+"' and fdeletedstatus=1 ";
                                                    List<Map<String, Object>> rsRoleNameAss = sqlService.getList(CloudSqlService.htEas, SQL);
                                                try {

                                                    String value =  (String)rsRoleNameAss.get(0).get("fname_l2");

                                                    if(value!=null){
                                                        RoleNameAss=(String)rsRoleNameAss.get(0).get("fname_l2");
                                                    }
                                                } catch (Exception e) {
                                                    // TODO Auto-generated catch block
                                                    e.printStackTrace();
                                                }
                                                JSONObject RolePosObj = api.getRole(RoleNumAss+" "+RoleNameAss);
                                                int RolePosNum=Integer.parseInt(RolePosObj.getString("num"));
                                                if(RolePosNum==0){
                                                    continue;
                                                }
                                                JSONArray RolePosDataArray = RolePosObj.getJSONArray("data");
                                                JSONObject RolePosDataObj = RolePosDataArray.getJSONObject(RolePosDataArray.size()-1);
                                                String RolePosId = RolePosDataObj.getString("id");


                                                api.getRoleAuth( RoleNameAss, RolePosId,"VPD", AuthFileId);
//										 RoleAuth.RoleAuthDis(RoleNumAss, RoleNameAss, authfileid);
                                                System.out.print("aaa");

                                            } catch (Exception e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }



                            }
                        }
                        //档案类型为SD
                        if (ArchType.equalsIgnoreCase("13")) {
                            desc = (String) objectMap.get("des");// 获取说明
                            String InfoCode=attribute;//SD-02-142320206-9004-01
                            String InfoCode01=InfoCode.substring(InfoCode.indexOf("-")+1, InfoCode.length());//02-142331024-9002-31-02
                            String InfoCode02=InfoCode01.substring(InfoCode01.indexOf("-")+1, InfoCode01.length());//142331024-9002-31-02
                            String InfoCode03=InfoCode02.substring(InfoCode02.indexOf("-")+1, InfoCode02.length());//9002-31-02
                            String InfoCodeRegion=InfoCode02.substring(0, InfoCode02.indexOf("-"));//142331024
                            String InfoCodeRegion01=InfoCode03.substring(0, InfoCode03.indexOf("-")).replace(" ", "");//9002
                            String ParentAttributeRegionNum="";
                            String SDParentAttribute="";
                            int RegionLevelCount=0;//"-"的个数
                            String SDAttribute="";
                            String InfoCodeCount=InfoCode;//SD-02-142331024-9002-31-02
                            elec = (String) subSheet.get(i).get("elec");
                            while (InfoCodeCount.indexOf("-") != -1) {
                                InfoCodeCount = InfoCodeCount.substring(InfoCodeCount.indexOf("-") + 1, InfoCodeCount.length());
                                RegionLevelCount++;
                            }



                            String gug=(String) subSheet.get(i).get("name01");
                            //一般关系方
                            String name01 =(String) subSheet.get(i).get("name01");//基礎代碼后的名稱
                            if(name01.contains("一般关系方")){
                                if(RegionLevelCount==5){
                                    InfoCode=(String) objectMap.get("baseCode");;//G-02-03
                                    String InfoName=(String) subSheet.get(i).get("name01");//02人资职能一般关系方

                                    SDAttribute="SD-"+OrgCode+"-"+InfoCode+" "+PosADuty;//
                                    SDParentAttribute ="SD-"+OrgCode+" "+PosADuty;//
                                    JSONObject SDParentFileID=api.getFileID( SDParentAttribute);
                                    int SDParentFileNum=Integer.parseInt(SDParentFileID.getString("num"));
                                    if(SDParentFileNum==0){
                                        continue;
                                    }
                                    JSONArray SDParentFileIdDataArray = SDParentFileID.getJSONArray("data");
                                    JSONObject SDParentFileIdDataObj = SDParentFileIdDataArray.getJSONObject(0);
                                    String SDParentFileId = SDParentFileIdDataObj.getString("id");
                                    api.getAddFolder( SDParentFileId,
                                            " "+InfoName, SDAttribute);
//							    api.getAddFolder( SDParentFileId, InfoName, SDAttribute);


                                    JSONObject SDParentFileID01=api.getFileID( SDAttribute);
                                    int Num=Integer.parseInt(SDParentFileID01.getString("num"));
                                    if(Num==0){
                                        continue;
                                    }
                                    JSONArray SDParentFileIdDataArray01 = SDParentFileID01.getJSONArray("data");

                                    JSONObject SDParentFileIdDataObj01 = SDParentFileIdDataArray01.getJSONObject(0);
                                    String SDParentFileId01 = SDParentFileIdDataObj01.getString("id");


                                    api.getAddFolder( SDParentFileId01,  attribute.substring(attribute.lastIndexOf("-")+1, attribute.length())+" "+(String) objectMap.get("fileName"), attribute+" "+PosADuty);

                                    if (elec.equalsIgnoreCase("/")){
                                        JSONObject SDFileID=api.getFileID( attribute+" "+PosADuty);
                                        prefix =attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length());//文件夹前缀
                                        foldername = prefix + ' ' + (String) objectMap.get("fileName");//需要添加的文件夹名称（01 报告 02公司行政职能资信）

                                        SDAttribute =attribute.replace(" ", "")+" "+PosADuty;
                                        String	SDLevelParAttribute=SDAttribute.substring(0, SDAttribute.lastIndexOf("-"))+" "+PosADuty;
                                        JSONObject SDParentFileIDG=api.getFileID( SDLevelParAttribute);
//						if(!SDParentFileID.containsValue("data")){continue;}
                                        JSONArray SDParentFileIdDataArrayG = SDParentFileIDG.getJSONArray("data");
                                        JSONObject SDParentFileIdDataObjG = SDParentFileIdDataArrayG.getJSONObject(0);
                                        String SDParentFileIdG = SDParentFileIdDataObjG.getString("id");

                                        JSONObject SDFolderID=api.getFileID( SDAttribute);
                                        int num=Integer.parseInt(SDFolderID.getString("num"));
                                        if(num>0){
                                            JSONArray SDFolderIdDataArray = SDFolderID.getJSONArray("data");
                                            JSONObject SDFolderIdObj0 = SDFolderIdDataArray.getJSONObject(0);
                                            String SDFolderId = SDFolderIdObj0.getString("id");
                                            api.getRename(token, SDFolderId, foldername);
                                        }
                                        else{
                                            api.getAddFolder( SDParentFileIdG,attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"), SDAttribute);
                                        }

                                        continue;
                                    }

                                    JSONObject SDFileID=api.getFileID( attribute);
                                    JSONArray SDFileIDDataArray = SDFileID.getJSONArray("data");
                                    JSONObject SDFileIDDataObj = SDFileIDDataArray.getJSONObject(0);

                                    String SDFileId = SDFileIDDataObj.getString("id");	//最早上传的老文件
                                    String Parentid2 = SDFileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                                    JSONObject FileIDPublicObj = SDFileIDDataArray.getJSONObject(SDFileIDDataArray.size() - 1);// 公共区新上传的文件id
                                    String fileidNEW = FileIDPublicObj.getString("id");//最新上传的文件的fileid

                                    JSONObject ExistFolderObj = api.getFileID(attribute+" "+PosADuty);//
                                    JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                                    JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(0);
                                    String ExitFolderId = ExistFolderObj0.getString("id");
                                    JSONObject ExitFolderFullPath = api.getFullPath( ExitFolderId);
                                    String SubItemFolderDestFullpath = ExitFolderFullPath.getString("fullPath");

                                    JSONArray FileIDDataArray = SDFileID.getJSONArray("data");
                                    int ajshg=FileIDDataArray.size();
                                    String NewFileName=FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));
                                    if(FileIDDataArray.size()==1){
                                        api.getMove( SDFileId,SubItemFolderDestFullpath);
                                        AuthFileId=SDFileId;


                                    }//有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的
                                    else if(!addfileid.equals(Parentid2)) {


                                        api.getMove( fileidNEW,SubItemFolderDestFullpath);

                                        JSONObject  ListFileAuth= api.getListFileAuth(token,SDFileId);
                                        JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                                        int a=ListFileAuthDataArray.size();
                                        for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++)
                                        {

                                            JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                            String RemoveAuthPos= ListFileAuthDataObj.getString("name");
                                            String auth= ListFileAuthDataObj.getString("auth");
                                            JSONObject array1=api.getRole(RemoveAuthPos);
                                            String num=array1.getString("num");
                                            if(!num.equals("0")){
                                                JSONArray array = array1.getJSONArray("data");
                                                JSONObject array2 = array.getJSONObject(0);
                                                String userId = array2.getString("id");
                                                api.getRoleAuth( RemoveAuthPos, userId, auth, fileidNEW);}
                                            System.out.print(num);
                                        }
                                        api.getRemove(token, SDFileId);
//										api.getRename(token, SDFileId, NewFileName);
//										String MOVEUpdate = api.getUpdate(token,fileidNEW, SDFileId);
                                    }
                                    //有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的
                                    else if(FileIDDataArray.size()>1) {
//										api.getRemove(token, fileid2);
//										api.getMove( fileid,destfullpath);

                                        api.getRename(token, SDFileId, NewFileName);
                                        String MOVEUpdate = api.getUpdate(token,fileidNEW, SDFileId);
                                        System.out.print(MOVEUpdate);
                                    }







                                    if (PosMainDuty != null) {
                                         
                                        PosMainName= PosMainName;
                                        RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                        RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                        RoleAuth.RoleDeptAss(token, PosMainDuty, AuthFileId);
                                    }
                                    // 获取岗位B角色及其上级直至董事长，并授权
                                    if (PosB != null)
                                    {
                                        PosBDuty = PosB;
                                        PosBName=PosBName;
                                        RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                        RoleAuth.AssRoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                        RoleAuth.RoleDeptAss(token, PosBDuty, AuthFileId);
                                    }
                                    // 获取岗位A角色及其上级直至董事长，并授权
                                    if (PosA != null)
                                    {
                                        PosADuty = PosA;
                                        PosAName=PosAName;
                                        RoleAuth.RoleAAuthDis(PosADuty, PosAName, AuthFileId);
                                        RoleAuth.AssRoleAuthDis(PosADuty, PosAName, AuthFileId);
                                        RoleAuth.RoleDeptAss(token, PosADuty, AuthFileId);
                                    }



                                    continue;
                                }

                            }

                            //岗位代码下面添加对应的地区文件夹
                            name01 =(String) subSheet.get(i).get("name01");//基礎代碼后的名稱
                            if(!name01.contains("一般关系方")){
                                int InfoCodeRegionLen=InfoCodeRegion.length();//142331024的长度
                                for(int s=0;s<InfoCodeRegionLen;s++){
                                    String RegionCode=InfoCodeRegion.substring(0, s+1);
                                    String sqlRegionCode="select fname_l2,fenglish from [HG_LINK].[hg].dbo.T_ARE_AREABASE WHERE FNUMBER ='"+ RegionCode + "'";
                                    List<Map<String, Object>> rsRegionCode = sqlService.getList(CloudSqlService.htEas, sqlRegionCode);
                                    try {

                                        String value =  (String)rsRegionCode.get(0).get("fname_l2");

                                        if(value!=null){
                                            String RegionName = (String)rsRegionCode.get(0).get("fname_l2");
                                            String RegionEnglishName = (String)rsRegionCode.get(0).get("fenglish");
                                            SDAttribute="SD-"+OrgCode+"-"+RegionCode+" "+PosADuty;//SD-02-1 02.06.07.003
                                            System.out.println("SDAttribute");
                                            JSONObject SDFileObj=api.getFileID( SDAttribute);
                                            //如已存在文件夹则跳过
                                            int num=Integer.parseInt(SDFileObj.getString("num"));
//								if(num==1)
//								{
//									continue;
//								}
                                            if(s==0){
                                                SDParentAttribute ="SD-"+OrgCode+" "+PosADuty;//SD-02 02.06.07.003
                                            }
                                            else {
                                                SDParentAttribute ="SD-"+OrgCode+"-"+ParentAttributeRegionNum+" "+PosADuty;//SD-02-1 02.06.07.003
                                            }
                                            //添加对应文件夹
                                            ParentAttributeRegionNum=RegionCode;
                                            JSONObject SDParentFileID=api.getFileID( SDParentAttribute);
                                            int SDParentFileNum=Integer.parseInt(SDParentFileID.getString("num"));
                                            if(SDParentFileNum==0){
                                                continue;
                                            }
                                            JSONArray SDParentFileIdDataArray = SDParentFileID.getJSONArray("data");
                                            JSONObject SDParentFileIdDataObj = SDParentFileIdDataArray.getJSONObject(0);
                                            String SDParentFileId = SDParentFileIdDataObj.getString("id");
                                            api.getAddFolder( SDParentFileId, RegionCode+" "+RegionEnglishName+" "+RegionName, SDAttribute);

                                        }


                                    } catch (Exception e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }

                                }


                                String RegionFolder="SD-"+OrgCode+"-"+InfoCodeRegion+" "+PosADuty;//获取SD-02-142331024的文件夹ID，将9002这个放在该文件夹下
                                JSONObject SDRegionFolderIDObj=api.getFileID( RegionFolder);
                                int SDRegionFolderNum=Integer.parseInt(SDRegionFolderIDObj.getString("num"));
                                if(SDRegionFolderNum==0){
                                    continue;
                                }
                                JSONArray SDRegionFolderIDDataArray = SDRegionFolderIDObj.getJSONArray("data");
                                JSONObject SDRegionFolderIDDataObj = SDRegionFolderIDDataArray.getJSONObject(0);
                                String SDRegionFolderID = SDRegionFolderIDDataObj.getString("id");

                                String RegionFolder01="SD-"+OrgCode+"-"+InfoCodeRegion+"-"+InfoCodeRegion01+" "+PosADuty;
                                JSONObject SDRegionFolderIDObj01=api.getFileID( RegionFolder01);
                                int SDRegionFolderNum01=Integer.parseInt(SDRegionFolderIDObj01.getString("num"));
                                String refdesc=(String) objectMap.get("refDes");
                                if(SDRegionFolderNum01==0){


                                    api.getAddFolder( SDRegionFolderID, InfoCodeRegion01+" "+refdesc, "SD-"+OrgCode+"-"+InfoCodeRegion+"-"+InfoCodeRegion01+" "+PosADuty);

                                }
                                else{
                                    JSONArray SDRegionFolderIDDataArray01 = SDRegionFolderIDObj01.getJSONArray("data");
                                    JSONObject SDRegionFolderIDDataObj01 = SDRegionFolderIDDataArray01.getJSONObject(0);
                                    String SDRegionFolderID01 = SDRegionFolderIDDataObj01.getString("id");
                                    api.getRename(token, SDRegionFolderID01, InfoCodeRegion01+" "+refdesc);

                                }
                            }
                            //带分项的文件
                            if ( SubItemORNot>0) {
                                //带分项的文件 先加文件夹

                                prefix =attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length());//文件夹前缀
                                foldername = prefix + ' ' + (String) objectMap.get("fileName");//
                                String parentattribute =attribute.replace(" ", "").substring(0,attribute.replace(" ", "").lastIndexOf("-"))+ ' '+ PosADuty;
                                JSONObject FolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                                int SubFolderNum=Integer.parseInt(FolderIdObj.getString("num"));
                                if(SubFolderNum==0){
                                    continue;
                                }
                                JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                                String folederid = FolderIdObj0.getString("id");
                                String SubFolderAttribute =attribute+ ' '+  PosADuty;
                                JSONObject SubFolderIdObj = api.getFileID(SubFolderAttribute);// 分项文件夹是否存在
                                int num=Integer.parseInt(SubFolderIdObj.getString("num"));
                                if(num>0){
                                    JSONArray SubFolderIdDataArray = SubFolderIdObj.getJSONArray("data");
                                    JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(0);
                                    String SubFolderId = SubFolderIdObj0.getString("id");
                                    api.getRename(token, SubFolderId, foldername);
                                }
                                else{
                                    api.getAddFolder( folederid,foldername,attribute + ' '+  PosADuty);
                                }
                                //查询分项文件对应文件夹的FolderId
                                String ExistFolderAttribute =attribute + ' '+   PosADuty;
                                JSONObject ExistFolderObj = api.getFileID(ExistFolderAttribute);//
                                JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                                JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(0);
                                String ExitFolderId = ExistFolderObj0.getString("id");
                                AuthFileId=ExitFolderId;
                                JSONObject ExitFolderFullPath = api.getFullPath( ExitFolderId);
                                String SubItemFolderDestFullpath = ExitFolderFullPath.getString("fullPath");
                                JSONObject ExitFolderListObj=api.getList( ExitFolderId);
                                JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                                int m=ExitFolderListDataArray.size();
                                for(int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++)
                                {
                                    JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                    String IsFolder=ExitFolderListDataObj.getString("fileType");
                                    if(!IsFolder.equals("1")&&SubItemORNot>0){
                                        delId=ExitFolderListDataObj.getString("id");
                                        api.getRemove(token, delId);
                                        System.out.println(ExitFolderListDataObj.get("id"));}
                                }
                                //移动公共区域的分项文件到对应路径
                                for(int Sub=0;Sub<SubItemORNot;Sub++)
                                {
                                    String SubItemattribute = SubItemFileList[Sub].replace("，", ",");//02-A-01-16-02-14-01-01-02,01
                                    JSONObject SubItemFileIdObj=api.getFileID( SubItemattribute);
                                    JSONArray SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                    JSONObject SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                    String SubItemFileId = SubItemFileIdDataObj.getString("id");
//	AuthFileId=SubItemFileId;
                                    api.getMove( SubItemFileId,SubItemFolderDestFullpath);//

                                    if(IsNomalFile==1){//有分项也有正常文件时候，正常文件也传过去
                                        SubItemFileIdObj=api.getFileID( attribute);
                                        SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                        SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                        SubItemFileId = SubItemFileIdDataObj.getString("id");
                                        api.getMove( SubItemFileId,SubItemFolderDestFullpath);//
                                    }
                                }


                                // 获取主责岗位角色及其上级直至董事长，并授权
                                if (PosMainDuty != null) {
                                     
                                    PosMainName= PosMainName;
                                    RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosMainDuty, AuthFileId);
                                }
                                // 获取岗位B角色及其上级直至董事长，并授权
                                if (PosB != null)
                                {
                                    PosBDuty = PosB;
                                    PosBName=PosBName;
                                    RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosBDuty, AuthFileId);
                                }
                                // 获取岗位A角色及其上级直至董事长，并授权
                                if (PosA != null)
                                {
                                    PosADuty = PosA;
                                    PosAName=PosAName;
                                    RoleAuth.RoleAAuthDis(PosADuty, PosAName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosADuty, PosAName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosADuty, AuthFileId);
                                }


                                continue;
                            }
                            InfoCodeCount=InfoCode.substring(InfoCode.indexOf("-")+1, InfoCode.length());//02-142331024-9002-31-02
                            InfoCodeCount=InfoCodeCount.substring(InfoCodeCount.indexOf("-")+1, InfoCodeCount.length());//142331024-9002-31-02
                            InfoCodeCount=InfoCodeCount.substring(InfoCodeCount.indexOf("-")+1, InfoCodeCount.length());//9002-31-02
                            if(RegionLevelCount>2){
                                // 如果是框架则在文档系统中加对应的文件夹
                                if (desc != null) {
                                    if (desc.length() >= 2) {
                                        if (desc.substring(0, 2).equalsIgnoreCase("框架")) {

                                            //√转为/，删除原文件
                                            JSONObject ExistFileObj = api.getFileID(attribute.replace(" ", ""));//
                                            int FileNum=Integer.parseInt(ExistFileObj.getString("num"));
                                            if(FileNum!=0){//如有文件先删除
                                                JSONArray ExistFileDataArray = ExistFileObj.getJSONArray("data");
                                                JSONObject ExistFileObj0 = ExistFileDataArray.getJSONObject(0);
                                                String ExitFileId = ExistFileObj0.getString("id");
                                                api.getRemove(token, ExitFileId);
                                            }
                                            else{//如无文件，有可能是分项，找到分项的ParentID，需要先删除其下面的分项文件
                                                String asdf=attribute.replace(" ", "")+" "+PosADuty;
                                                JSONObject ExitFolderObj=api.getFileID( attribute.replace(" ", "")+" "+PosADuty);

//											    JSONObject ExitFolderObj=api.getFileID( "02.02.001 PUB 02.0001");
                                                FileNum=Integer.parseInt(ExitFolderObj.getString("num"));
                                                if(FileNum!=0){

                                                    JSONArray ExitFolderDataArray = ExitFolderObj.getJSONArray("data");
                                                    JSONObject ExitFolderObj0 = ExitFolderDataArray.getJSONObject(0);
                                                    String ExitFolderId = ExitFolderObj0.getString("id");//分项文件夹ID


                                                    JSONObject ExitFolderListObj=api.getList( ExitFolderId);
                                                    JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                                                    for(int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++)
                                                    {
                                                        JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                                        String IsFolder=ExitFolderListDataObj.getString("fileType");
                                                        if(!IsFolder.equals("1")&&SubItemORNot>0){
                                                            delId=ExitFolderListDataObj.getString("id");
                                                            api.getRemove(token, delId);
                                                            System.out.println(ExitFolderListDataObj.get("id"));}
                                                    }
                                                }
                                            }



                                            prefix =attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length());//文件夹前缀
                                            foldername = prefix + ' ' + (String) objectMap.get("fileName");//需要添加的文件夹名称（01 报告 02公司行政职能资信）

                                            SDAttribute =attribute.replace(" ", "")+" "+PosADuty;
                                            String	SDLevelParAttribute=SDAttribute.substring(0, SDAttribute.lastIndexOf("-"))+" "+PosADuty;
                                            JSONObject SDParentFileID=api.getFileID( SDLevelParAttribute);
//						if(!SDParentFileID.containsValue("data")){continue;}
                                            JSONArray SDParentFileIdDataArray = SDParentFileID.getJSONArray("data");
                                            JSONObject SDParentFileIdDataObj = SDParentFileIdDataArray.getJSONObject(0);
                                            String SDParentFileId = SDParentFileIdDataObj.getString("id");

                                            JSONObject SDFolderID=api.getFileID( SDAttribute);
                                            int num=Integer.parseInt(SDFolderID.getString("num"));
                                            if(num>0){
                                                JSONArray SDFolderIdDataArray = SDFolderID.getJSONArray("data");
                                                JSONObject SDFolderIdObj0 = SDFolderIdDataArray.getJSONObject(0);
                                                String SDFolderId = SDFolderIdObj0.getString("id");
                                                api.getRename(token, SDFolderId, foldername);
                                            }
                                            else{
                                                api.getAddFolder( SDParentFileId,attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"), SDAttribute);
                                            }
                                            InfoCodeCount=InfoCodeCount.substring(InfoCodeCount.indexOf("-")+1, InfoCodeCount.length());
                                            System.out.println(InfoCodeCount);
                                        }
                                    }
                                }
                                if (elec.equalsIgnoreCase("√")&&SubItemORNot==0&&IsNomalFile==1) {//不是分项的文件
                                    SDAttribute=attribute.replace(" ", "");
                                    String	SDLevelParAttribute=SDAttribute.substring(0, SDAttribute.lastIndexOf("-"))+" "+PosADuty;//上级文件夹扩展属性SD-02-142331024-9002-31 02.06.07.003
                                    JSONObject SDParentFolderID=api.getFileID( SDLevelParAttribute);
                                    int SDParentFolderNum=Integer.parseInt(SDParentFolderID.getString("num"));
                                    if(SDParentFolderNum==0){
                                        continue;
                                    }
                                    JSONArray SDParentFolderIdDataArray = SDParentFolderID.getJSONArray("data");
                                    JSONObject SDParentFolderIdDataObj = SDParentFolderIdDataArray.getJSONObject(0);
                                    String SDParentFolderId = SDParentFolderIdDataObj.getString("id");
                                    //查找文件对应文件夹
                                    JSONObject ExistFolderIdObj=api.getFileID( SDAttribute+" "+PosADuty);
                                    int SDArrFolderNum=Integer.parseInt(ExistFolderIdObj.getString("num"));
                                    //如果不存在文件对应文件夹则先添加对应文件夹
                                    if(SDArrFolderNum==0)
                                    {
                                        JSONObject SDFolderIdObj=api.getAddFolder( SDParentFolderId,attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"), SDAttribute+" "+PosADuty);
                                        //获取对应文件夹的FolderId，以便之后将文件放入该文件夹
                                        addfileid = SDFolderIdObj.getString("fileId");
                                    }
                                    //如果存在文件对应文件夹则先需要找到其FolderId
                                    else{
                                        JSONArray  ExistFolderIdDataArray = ExistFolderIdObj.getJSONArray("data");
                                        JSONObject ExistFolderIdDataObj = ExistFolderIdDataArray.getJSONObject(0);
                                        addfileid = ExistFolderIdDataObj.getString("id");
                                        api.getRename(token, addfileid,attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"));
                                        JSONObject ExitFolderListObj=api.getList( addfileid);
                                        JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                                        for(int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++)
                                        {
                                            JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                            String IsFolder=ExitFolderListDataObj.getString("fileType");
                                            String filename=ExitFolderListDataObj.getString("fileName");
                                            if(!IsFolder.equals("1")&&SubItemORNot>0){

                                                delId=ExitFolderListDataObj.getString("id");
                                                api.getRemove(token, delId);
                                                System.out.println(ExitFolderListDataObj.get("id"));
                                            }
                                        }
                                    }
                                    JSONObject FolderPathObj = api.getFullPath( addfileid);
                                    String destfullpath = FolderPathObj.getString("fullPath");
                                    JSONObject FileID = api.getFileID( SDAttribute);
                                    int SDArrFileNum=Integer.parseInt(FileID.getString("num"));
                                    if(SDArrFileNum==0){
                                        continue;
                                    }
                                    JSONArray FileIDDataArray = FileID.getJSONArray("data");
                                    JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                    String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                    String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                                    JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                                    fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                                    String NewFileName=FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));;
                                    if(FileIDDataArray.size()==1){
                                        api.getMove( fileid,destfullpath);
                                    }//有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的

                                    else if(!addfileid.equals(Parentid2)) {

                                        api.getMove( fileid,destfullpath);
//										api.getMove( fileid,destfullpath);

                                        JSONObject  ListFileAuth= api.getListFileAuth(token,fileid2);
                                        JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                                        int a=ListFileAuthDataArray.size();
                                        for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++)
                                        {

                                            JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                            String RemoveAuthPos= ListFileAuthDataObj.getString("name");
                                            String auth= ListFileAuthDataObj.getString("auth");
                                            JSONObject array1=api.getRole(RemoveAuthPos);
                                            String num=array1.getString("num");
                                            if(!num.equals("0")){
                                                JSONArray array = array1.getJSONArray("data");
                                                JSONObject array2 = array.getJSONObject(0);
                                                String userId = array2.getString("id");
                                                api.getRoleAuth( RemoveAuthPos, userId, auth, fileid);}
                                        }
                                        api.getRemove(token, fileid2);
//										api.getRename(token, fileid2, NewFileName);
//										String MOVEUpdate = api.getUpdate(token,fileid, fileid2);


                                    }
                                    //有2个及以上相同扩展属性的文件时，A,B岗位未变更
                                    else if(FileIDDataArray.size()>1) {

                                        //将老文件放入userFolder
                                        JSONObject FolderPathObj01 = api.getFullPath( "1688009");
                                        String CAdestfullpath01 = FolderPathObj01.getString("fullPath");
                                        api.getMove( fileid2,CAdestfullpath01);//将老文件放入userFolder
//										api.getRename(token, fileid2, "待删除");//先将老文件名改为"待删除"



                                        api.getMove( fileid,destfullpath);//将新文件移动到对应文件夹，此时和老文件放在同一个文件夹
                                        JSONObject  ListFileAuth= api.getListFileAuth(token,fileid2);//获取老文件的权限
                                        JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                                        int a=ListFileAuthDataArray.size();
                                        for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++)
                                        {//将老文件的权限授予新文件

                                            JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                            String RemoveAuthPos= ListFileAuthDataObj.getString("name");
                                            String auth= ListFileAuthDataObj.getString("auth");
                                            JSONObject array1=api.getRole(RemoveAuthPos);
                                            String num=array1.getString("num");
                                            if(!num.equals("0"))
                                            {
                                                JSONArray array = array1.getJSONArray("data");
                                                JSONObject array2 = array.getJSONObject(0);
                                                String userId = array2.getString("id");
                                                api.getRoleAuth( RemoveAuthPos, userId, auth, fileid);
                                            }
                                        }
                                        api.getRemove(token, fileid2);//删除老文件
                                    }
                                    AuthFileId=fileid;
                                    // 获取主责岗位角色及其上级直至董事长，并授权
                                    if (PosMainDuty != null) {
                                         
                                        PosMainName= PosMainName;
                                        RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                        RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                        RoleAuth.RoleDeptAss(token, PosMainDuty, AuthFileId);

                                        while(SDAttribute.contains("-"))	{
                                            SDAttribute=SDAttribute.substring(0, SDAttribute.lastIndexOf("-"));
                                            JSONObject AuthFileID = api.getFileID( SDAttribute);
                                            int SDArrAuthFileNum=Integer.parseInt(AuthFileID.getString("num"));
                                            if(SDArrAuthFileNum!=0){


                                                JSONArray AuthFileIDDataArray = AuthFileID.getJSONArray("data");
                                                JSONObject AuthFileIDDataObj = AuthFileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                                String authfileid = AuthFileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                                RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, authfileid);
                                                RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, authfileid);
                                            }
                                        }
                                    }
                                    // 获取岗位B角色及其上级直至董事长，并授权
                                    if (PosB != null)
                                    {
                                        PosBDuty = PosB;
                                        PosBName=PosBName;
                                        RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                        RoleAuth.AssRoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                        while(SDAttribute.contains("-"))	{
                                            SDAttribute=SDAttribute.substring(0, SDAttribute.lastIndexOf("-"));
                                            JSONObject AuthFileID = api.getFileID( SDAttribute);
                                            int SDArrAuthFileNum=Integer.parseInt(AuthFileID.getString("num"));
                                            if(SDArrAuthFileNum!=0){


                                                JSONArray AuthFileIDDataArray = AuthFileID.getJSONArray("data");
                                                JSONObject AuthFileIDDataObj = AuthFileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                                String authfileid = AuthFileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                                RoleAuth.RoleAuthDis(PosBDuty, PosBName, authfileid);
                                                RoleAuth.AssRoleAuthDis(PosBDuty, PosBName, authfileid);
                                            }
                                        }
                                    }
                                    // 获取岗位A角色及其上级直至董事长，并授权
                                    if (PosA != null)
                                    {
                                        PosADuty = PosA;
                                        PosAName=PosAName;
                                        RoleAuth.RoleAAuthDis(PosADuty, PosAName, AuthFileId);
                                        RoleAuth.AssRoleAuthDis(PosADuty, PosAName, AuthFileId);

                                        while(SDAttribute.contains("-"))	{
                                            SDAttribute=SDAttribute.substring(0, SDAttribute.lastIndexOf("-"));
                                            JSONObject AuthFileID = api.getFileID( SDAttribute);
                                            int SDArrAuthFileNum=Integer.parseInt(AuthFileID.getString("num"));
                                            if(SDArrAuthFileNum!=0){


                                                JSONArray AuthFileIDDataArray = AuthFileID.getJSONArray("data");
                                                JSONObject AuthFileIDDataObj = AuthFileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                                String authfileid = AuthFileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                                RoleAuth.RoleAuthDis(PosADuty, PosAName, authfileid);
                                                RoleAuth.AssRoleAuthDis(PosADuty, PosAName, authfileid);
                                            }
                                        }
                                    }
                                }
                            }
                        }



                        //档案类型为F
                        if (ArchType.equalsIgnoreCase("12")) {
                            desc = (String) objectMap.get("des");// 获取说明
                            if (PosA != null) {
                                PosADuty = PosA;
                                PosAName = PosAName;
                            }
                            String InfoCode=attribute;//F-01060301-01
                            String InfoCode01=InfoCode.substring(InfoCode.indexOf("-")+1, InfoCode.length());//01060301-01
                            String InfoCode02=InfoCode01.substring(0, InfoCode01.indexOf("-"));//01060301
                            String ParentAttributeFNum="";
                            String FParentAttribute="";
                            String FAttribute="";
                            String trans=attribute;
                            elec = (String) subSheet.get(i).get("elec");
                            int count=0;
                            int level=0;//"-"的个数,level=2时才需要判断文件夹属性是否带leaf
                            String FIsLeaf="1";
                            String parentattribute="";
                            String	FLevelParAttribute="";
                            System.out.println(FLevelParAttribute);
                            while(trans.contains("-"))
                            {
                                trans=trans.substring(trans.indexOf("-")+1, trans.length());
                                level++;
                            }
                            if(level==2)//level=2时需要加对应的场地文件夹
                            {
                                int InfoCodeFLen=InfoCode02.length();//01060301的长度
                                for(int s=0;s<InfoCodeFLen;s++){
                                    String FCode=InfoCode02.substring(0, s+1);
                                    String sqlFCode="select fname_l2 ,isnull(fisleaf,'0')  fisleaf from [HG_LINK].[hg].dbo.T_man_siteinfo WHERE FNUMBER ='"+ FCode + "'";
                                    List<Map<String, Object>> rsFCode = sqlService.getList(CloudSqlService.htEas, sqlFCode);
                                    try {

                                        String value =  (String)rsFCode.get(0).get("fname_l2");

                                        if(value!=null){
                                            String FName = (String)rsFCode.get(0).get("fname_l2");
                                            FIsLeaf =  (String)rsFCode.get(0).get("fisleaf");
                                            if(count==0)
                                            {
                                                FParentAttribute ="F-"+OrgCode+" "+PosADuty;//
                                            }
                                            else
                                            {
                                                FParentAttribute ="F-"+ParentAttributeFNum+" "+PosADuty;//
                                            }
                                            FAttribute="F"+"-"+FCode+" "+PosADuty;//F-01 02.09.004
                                            JSONObject FFileIdObj=api.getFileID( FAttribute);
                                            int num=Integer.parseInt(FFileIdObj.getString("num"));

//								if(num==1)
//								{
//									continue;
//								}
                                            //添加对应文件夹
                                            JSONObject FParentFileID=api.getFileID( FParentAttribute);
                                            int FParentFileNum=Integer.parseInt(FParentFileID.getString("num"));
                                            if(FParentFileNum==0){
                                                continue;
                                            }
                                            JSONArray FParentFileIdDataArray = FParentFileID.getJSONArray("data");
//								JSONObject SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(SubItemFileIdDataArray.size()-1);
//								JSONObject SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(SubItemFileIdDataArray.size()-1);
                                            JSONObject FParentFileIdDataObj = FParentFileIdDataArray.getJSONObject(FParentFileIdDataArray.size()-1);
                                            String FParentFileId = FParentFileIdDataObj.getString("id");


                                            if(num==0){
                                                JSONObject FolderAddObj=api.getAddFolder( FParentFileId, FCode+" "+FName, FAttribute);
                                            }
                                            if(num>0){
                                                JSONArray FolderIdDataArray = FFileIdObj.getJSONArray("data");
                                                JSONObject FolderIdDataObj01 = FolderIdDataArray.getJSONObject(FolderIdDataArray.size()-1);
                                                String parentId=FolderIdDataObj01.getString("parentId");
                                                if(!parentId.equals(FParentFileId)){

                                                    api.getAddFolder( FParentFileId, FCode+" "+FName, FAttribute);
                                                }
                                                else{
                                                    JSONObject FolderIdDataObj = FolderIdDataArray.getJSONObject(FolderIdDataArray.size()-1);
//										JSONObject FolderIdDataObj = FolderIdDataArray.getJSONObject(0);
                                                    String FolderId = FolderIdDataObj.getString("id");
                                                    api.getRename(token, FolderId,FCode+" "+FName);
                                                }
                                            }
                                            JSONObject FolderAddObj=api.getFileID(  FAttribute);





                                            String FolderId="";
                                            if(FolderAddObj.containsValue("fileId"))
                                            {
                                                FolderId=FolderAddObj.getString("fileId");
                                            }
                                            else
                                            {
                                                JSONObject	FolderIdObj = api.getFileID(FAttribute);
                                                JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                                JSONObject FolderIdDataObj = FolderIdDataArray.getJSONObject(FolderIdDataArray.size()-1);
                                                FolderId = FolderIdDataObj.getString("id");
                                            }
                                            if(FIsLeaf.equals("0"))
                                            {
                                                FFileIdObj=api.getFileID( FAttribute+" leaf");
                                                num=Integer.parseInt(FFileIdObj.getString("num"));
                                                if(num==0){
                                                    api.getAddFolder( FolderId, FCode+" "+FName, FAttribute+" leaf");
                                                }
                                                if(num>0){
                                                    FFileIdObj=api.getFileID( FAttribute+" leaf");
                                                    JSONArray FolderIdDataArray = FFileIdObj.getJSONArray("data");


                                                    JSONObject FolderIdDataObj01 = FolderIdDataArray.getJSONObject(FolderIdDataArray.size()-1);
                                                    String parentId=FolderIdDataObj01.getString("parentId");
                                                    if(!parentId.equals(FParentFileId)){

                                                        api.getAddFolder( FolderId, FCode+" "+FName, FAttribute+" leaf");}

                                                    else{


                                                    }
                                                    JSONObject FolderIdDataObj = FolderIdDataArray.getJSONObject(FolderIdDataArray.size()-1);
                                                    FolderId = FolderIdDataObj.getString("id");
                                                    api.getRename(token, FolderId,FCode+" "+FName);
                                                }


                                            }
                                            ParentAttributeFNum=FCode;
                                            count++;
                                        }


                                    } catch (Exception e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                }
                            }
                            if(level==2)
                            {
                                String sqlFCode01="select fname_l2 ,isnull(fisleaf,'0')  fisleaf from [HG_LINK].[hg].dbo.T_man_siteinfo WHERE FNUMBER ='"+ InfoCode02 + "'";
                                List<Map<String, Object>> rsFCode01 = sqlService.getList(CloudSqlService.htEas, sqlFCode01);
                                try {

                                    String value =  (String)rsFCode01.get(0).get("fisleaf");

                                    if(value!=null){
                                        FIsLeaf = (String)rsFCode01.get(0).get("fisleaf");

                                    }
                                } catch (Exception e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }


                            }
                            //带分项的文件
                            if ( SubItemORNot>0) {
                                //带分项的文件
                                // 先加文件夹
                                prefix =attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length());//文件夹前缀
                                foldername = prefix + ' ' + (String) objectMap.get("fileName");//需要添加的文件夹名称（01 报告 02公司行政职能资信）
                                if(FIsLeaf.equals("0")&&level==2)
                                {
                                    parentattribute=attribute.substring(0,attribute.replace(" ", "").lastIndexOf("-"))+ ' '+PosADuty+" leaf";
                                }
                                else{
                                    parentattribute =attribute.replace(" ", "").substring(0,attribute.replace(" ", "").lastIndexOf("-"))+ ' '+PosADuty;
                                }
                                JSONObject FolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                                int FolderNum=Integer.parseInt(FolderIdObj.getString("num"));
                                if(FolderNum==0){
                                    continue;
                                }
                                JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                                String folederid = FolderIdObj0.getString("id");

                                String SubFolderAttribute =attribute.replace(" ", "")+ ' '+ PosADuty;
                                JSONObject SubFolderIdObj = api.getFileID(SubFolderAttribute);// 分项文件夹是否存在

                                int num=Integer.parseInt(SubFolderIdObj.getString("num"));
                                if(num>0){
                                    JSONArray SubFolderIdDataArray = SubFolderIdObj.getJSONArray("data");
                                    JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(0);
                                    String SubFolderId = SubFolderIdObj0.getString("id");
                                    api.getRename(token, SubFolderId, foldername);
                                }
                                else{
                                    api.getAddFolder( folederid,foldername,attribute + ' '+  PosADuty);
                                }
                                //查询分项文件对应文件夹的FolderId
                                String ExistFolderAttribute =attribute + ' '+   PosADuty;
                                JSONObject ExistFolderObj = api.getFileID(ExistFolderAttribute);//
                                JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                                JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(0);
                                String ExitFolderId = ExistFolderObj0.getString("id");
                                AuthFileId=ExitFolderId;
                                JSONObject ExitFolderFullPath = api.getFullPath( ExitFolderId);
                                String SubItemFolderDestFullpath = ExitFolderFullPath.getString("fullPath");
                                JSONObject ExitFolderListObj=api.getList( ExitFolderId);
                                JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                                int m=ExitFolderListDataArray.size();
                                for(int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++)
                                {
                                    JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                    String IsFolder=ExitFolderListDataObj.getString("fileType");
                                    if(!IsFolder.equals("1")&&SubItemORNot>0){
                                        delId=ExitFolderListDataObj.getString("id");
                                        api.getRemove(token, delId);
                                        System.out.println(ExitFolderListDataObj.get("id"));}
                                }
                                //移动公共区域的分项文件到对应路径
                                for(int Sub=0;Sub<SubItemORNot;Sub++)
                                {
                                    String SubItemattribute = SubItemFileList[Sub].replace("，", ",");//02-A-01-16-02-14-01-01-02,01
                                    JSONObject SubItemFileIdObj=api.getFileID( SubItemattribute);
                                    JSONArray SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                    JSONObject SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                    String SubItemFileId = SubItemFileIdDataObj.getString("id");
//							AuthFileId=SubItemFileId;
                                    api.getMove( SubItemFileId,SubItemFolderDestFullpath);//
                                    if(IsNomalFile==1){//有分项也有正常文件时候，正常文件也传过去
                                        SubItemFileIdObj=api.getFileID( attribute);
                                        SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                        SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                        SubItemFileId = SubItemFileIdDataObj.getString("id");
                                        api.getMove( SubItemFileId,SubItemFolderDestFullpath);//
                                    }
                                }
                                if (PosMainDuty != null) {
                                     
                                    PosMainName= PosMainName;
                                    RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosMainDuty, AuthFileId);
                                }
                                // 获取岗位B角色及其上级直至董事长，并授权
                                if (PosB != null)
                                {
                                    PosBDuty = PosB;
                                    PosBName=PosBName;
                                    RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosBDuty, AuthFileId);
                                }
                                // 获取岗位A角色及其上级直至董事长，并授权
                                if (PosA != null)
                                {
                                    PosADuty = PosA;
                                    PosAName=PosAName;
                                    RoleAuth.RoleAAuthDis(PosADuty, PosAName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosADuty, PosAName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosADuty, AuthFileId);
                                }


                                continue;
                            }
//						InfoCodeCount=InfoCode.substring(InfoCode.indexOf("-")+1, InfoCode.length());//02-142331024-9002-31-02
//						InfoCodeCount=InfoCodeCount.substring(InfoCodeCount.indexOf("-")+1, InfoCodeCount.length());//142331024-9002-31-02
//						InfoCodeCount=InfoCodeCount.substring(InfoCodeCount.indexOf("-")+1, InfoCodeCount.length());//9002-31-02

                            // 如果是框架则在文档系统中加对应的文件夹
                            if (desc != null) {
                                if (desc.length() >= 2) {
                                    if (desc.substring(0, 2).equalsIgnoreCase("框架")) {

                                        //√转为/，删除原文件
                                        JSONObject ExistFileObj = api.getFileID(attribute.replace(" ", ""));//
                                        int FileNum=Integer.parseInt(ExistFileObj.getString("num"));
                                        if(FileNum!=0){
                                            JSONArray ExistFileDataArray = ExistFileObj.getJSONArray("data");
                                            JSONObject ExistFileObj0 = ExistFileDataArray.getJSONObject(0);
                                            String ExitFileId = ExistFileObj0.getString("id");
                                            api.getRemove(token, ExitFileId);
                                        }
                                        else{
                                            String asdf=attribute.replace(" ", "")+" "+PosADuty;
                                            JSONObject ExitFolderObj=api.getFileID( attribute.replace(" ", "")+" "+PosADuty);
                                            FileNum=Integer.parseInt(ExitFolderObj.getString("num"));
                                            if(FileNum!=0){



                                                JSONArray ExitFolderDataArray = ExitFolderObj.getJSONArray("data");
                                                JSONObject ExitFolderObj0 = ExitFolderDataArray.getJSONObject(0);
                                                String ExitFolderId = ExitFolderObj0.getString("id");
                                                JSONObject ExitFolderListObj=api.getList( ExitFolderId);

//											    JSONObject ExitFolderListObj=api.getList( attribute.replace(" ", "")+" "+PosADuty);
                                                JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                                                for(int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++)
                                                {

                                                    JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                                    String IsFolder=ExitFolderListDataObj.getString("fileType");
                                                    if(!IsFolder.equals("1")&&SubItemORNot>0){
                                                        delId=ExitFolderListDataObj.getString("id");
                                                        api.getRemove(token, delId);
                                                        System.out.println(ExitFolderListDataObj.get("id"));}
                                                }
                                            }
                                        }

                                        FAttribute =attribute.replace(" ", "")+" "+PosADuty;
                                        if(FIsLeaf.equals("0")&&level==2)
                                        {
                                            FLevelParAttribute=FAttribute.substring(0, FAttribute.lastIndexOf("-"))+" "+PosADuty+" leaf";
                                        }
                                        else
                                        {
                                            FLevelParAttribute=FAttribute.substring(0, FAttribute.lastIndexOf("-"))+" "+PosADuty;
                                        }
                                        JSONObject FParentFileID=api.getFileID( FLevelParAttribute);
//						if(!SDParentFileID.containsValue("data")){continue;}
                                        JSONArray FParentFileIdDataArray = FParentFileID.getJSONArray("data");
                                        JSONObject FParentFileIdDataObj = FParentFileIdDataArray.getJSONObject(FParentFileIdDataArray.size()-1);
                                        String FParentFileId = FParentFileIdDataObj.getString("id");


                                        JSONObject SubFolderIdObj = api.getFileID(FAttribute);//
                                        int num=Integer.parseInt(SubFolderIdObj.getString("num"));
                                        if(num>0){
                                            JSONArray SubFolderIdDataArray = SubFolderIdObj.getJSONArray("data");
                                            JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(SubFolderIdDataArray.size()-1);
                                            String SubFolderId = SubFolderIdObj0.getString("id");
                                            api.getRename(token, SubFolderId, attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"));
                                        }
                                        else{
                                            api.getAddFolder( FParentFileId,attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"), FAttribute);
                                        }




                                    }
                                }
                            }
                            if (elec.equalsIgnoreCase("√")&&SubItemORNot==0&&IsNomalFile==1) {//不是分项的文件
                                FAttribute=attribute;
                                if(FIsLeaf.equals("0")&&level==2){
                                    FLevelParAttribute=FAttribute.substring(0, FAttribute.lastIndexOf("-"))+" "+PosADuty+" leaf";

                                }
                                else{
                                    FLevelParAttribute=FAttribute.substring(0, FAttribute.lastIndexOf("-"))+" "+PosADuty;
                                }
//								FLevelParAttribute=FAttribute.substring(0, FAttribute.lastIndexOf("-"))+" "+PosADuty;//上级文件夹扩展属性SD-02-142331024-9002-31 02.06.07.003
                                JSONObject FParentFolderID=api.getFileID( FLevelParAttribute);
                                int FParentFolderNum=Integer.parseInt(FParentFolderID.getString("num"));
                                if(FParentFolderNum==0){
                                    continue;
                                }
                                JSONArray FParentFolderIdDataArray = FParentFolderID.getJSONArray("data");
                                JSONObject FParentFolderIdDataObj = FParentFolderIdDataArray.getJSONObject(FParentFolderIdDataArray.size()-1);
                                String FParentFolderId = FParentFolderIdDataObj.getString("id");
                                //查找文件对应文件夹
                                JSONObject ExistFolderIdObj=api.getFileID( FAttribute+" "+PosADuty);
                                int FArrFolderNum=Integer.parseInt(ExistFolderIdObj.getString("num"));
                                //如果不存在文件对应文件夹则先添加对应文件夹
                                if(FArrFolderNum==0)
                                {
                                    JSONObject FFolderIdObj=api.getAddFolder( FParentFolderId,attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"), FAttribute+" "+PosADuty);
                                    //获取对应文件夹的FolderId，以便之后将文件放入该文件夹
                                    addfileid = FFolderIdObj.getString("fileId");
                                }
                                //如果存在文件对应文件夹则先需要找到其FolderId
                                ExistFolderIdObj=api.getFileID( FAttribute+" "+PosADuty);
                                FArrFolderNum=Integer.parseInt(ExistFolderIdObj.getString("num"));
                                if(FArrFolderNum>0)
                                {
                                    JSONArray  ExistFolderIdDataArray = ExistFolderIdObj.getJSONArray("data");
                                    JSONObject ExistFolderIdDataObj01 = ExistFolderIdDataArray.getJSONObject(ExistFolderIdDataArray.size()-1);
                                    String ExistFolderId = ExistFolderIdDataObj01.getString("parentId");

                                    if(!FParentFolderId.equals(ExistFolderId)){
                                        JSONObject FFolderIdObj=api.getAddFolder( FParentFolderId,attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"), FAttribute+" "+PosADuty);
                                        addfileid = FFolderIdObj.getString("fileId");
                                    }
                                    else{
                                        JSONObject ExistFolderIdDataObj = ExistFolderIdDataArray.getJSONObject(ExistFolderIdDataArray.size()-1);
                                        addfileid = ExistFolderIdDataObj.getString("id");
                                        api.getRename(token, addfileid,attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.length()) +" "+(String) objectMap.get("fileName"));
                                        JSONObject ExitFolderListObj=api.getList( addfileid);
                                        JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                                        for(int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++)
                                        {
                                            JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                            String IsFolder=ExitFolderListDataObj.getString("fileType");
                                            String filename=ExitFolderListDataObj.getString("fileName");
                                            if(!IsFolder.equals("1")&&SubItemORNot>0){

                                                delId=ExitFolderListDataObj.getString("id");
                                                api.getRemove(token, delId);
                                                System.out.println(ExitFolderListDataObj.get("id"));
                                            }
                                        }
                                    }
                                    JSONObject FolderPathObj = api.getFullPath( addfileid);
                                    String destfullpath = FolderPathObj.getString("fullPath");
                                    JSONObject FileID = api.getFileID( FAttribute);
                                    int FArrFileNum=Integer.parseInt(FileID.getString("num"));
                                    if(FArrFileNum==0){
                                        continue;
                                    }
                                    JSONArray FileIDDataArray = FileID.getJSONArray("data");
                                    JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                    String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                    String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                                    JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                                    fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                                    String NewFileName=FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));;
                                    if(FileIDDataArray.size()==1){
                                        api.getMove( fileid,destfullpath);
                                    }//如果岗位A发生变化，先将新文件放入新的文件夹下，再获取原文件的权限赋予新的文件

                                    else if(!addfileid.equals(Parentid2)) {


                                        api.getMove( fileid,destfullpath);

                                        JSONObject  ListFileAuth= api.getListFileAuth(token,fileid2);
                                        JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                                        int a=ListFileAuthDataArray.size();
                                        for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++)
                                        {

                                            JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                            String RemoveAuthPos= ListFileAuthDataObj.getString("name");
                                            String auth= ListFileAuthDataObj.getString("auth");
                                            JSONObject array1=api.getRole(RemoveAuthPos);
                                            String num=array1.getString("num");
                                            if(!num.equals("0")){
                                                JSONArray array = array1.getJSONArray("data");
                                                JSONObject array2 = array.getJSONObject(array.size()-1);
                                                String userId = array2.getString("id");
                                                api.getRoleAuth( RemoveAuthPos, userId, auth, fileid);}
                                        }

                                        api.getRemove(token, fileid2);
//    								api.getRename(token, fileid2, NewFileName);
//    								String MOVEUpdate = api.getUpdate(token,fileid, fileid2);

                                    }
                                    //有2个及以上相同扩展属性的文件时，A,B岗位未变更
                                    else if(FileIDDataArray.size()>1) {

                                        //将老文件放入userFolder
                                        JSONObject FolderPathObj01 = api.getFullPath( "1688009");
                                        String CAdestfullpath01 = FolderPathObj01.getString("fullPath");
                                        api.getMove( fileid2,CAdestfullpath01);//将老文件放入userFolder
//    								api.getRename(token, fileid2, "待删除");//先将老文件名改为"待删除"


                                        api.getMove( fileid,destfullpath);//将新文件移动到对应文件夹，此时和老文件放在同一个文件夹
                                        JSONObject  ListFileAuth= api.getListFileAuth(token,fileid2);//获取老文件的权限
                                        JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                                        int a=ListFileAuthDataArray.size();
                                        for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++)
                                        {//将老文件的权限授予新文件

                                            JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                            String RemoveAuthPos= ListFileAuthDataObj.getString("name");
                                            String auth= ListFileAuthDataObj.getString("auth");
                                            JSONObject array1=api.getRole(RemoveAuthPos);
                                            String num=array1.getString("num");
                                            if(!num.equals("0"))
                                            {
                                                JSONArray array = array1.getJSONArray("data");
                                                JSONObject array2 = array.getJSONObject(array.size()-1);
                                                String userId = array2.getString("id");
                                                api.getRoleAuth( RemoveAuthPos, userId, auth, fileid);
                                            }
                                        }
                                        api.getRemove(token, fileid2);//删除老文件
                                    }

                                    AuthFileId=fileid;
                                    // 获取主责岗位角色及其上级直至董事长，并授权
                                    if (PosMainDuty != null) {
                                         
                                        PosMainName= PosMainName;
                                        RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                        RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                        while(FAttribute.contains("-"))	{
                                            FAttribute=FAttribute.substring(0, FAttribute.lastIndexOf("-"));
                                            JSONObject AuthFileID = api.getFileID( FAttribute);
                                            int FArrAuthFileNum=Integer.parseInt(AuthFileID.getString("num"));
                                            if(FArrAuthFileNum!=0){


                                                JSONArray AuthFileIDDataArray = AuthFileID.getJSONArray("data");
                                                JSONObject AuthFileIDDataObj = AuthFileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                                String authfileid = AuthFileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                                RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, authfileid);
                                                RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, authfileid);
                                            }
                                        }

                                    }
                                    // 获取岗位B角色及其上级直至董事长，并授权
                                    if (PosB != null)
                                    {
                                        PosBDuty = PosB;
                                        PosBName=PosBName;
                                        RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                        RoleAuth.AssRoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                        while(FAttribute.contains("-"))	{
                                            FAttribute=FAttribute.substring(0, FAttribute.lastIndexOf("-"));
                                            JSONObject AuthFileID = api.getFileID( FAttribute);
                                            int FArrAuthFileNum=Integer.parseInt(AuthFileID.getString("num"));
                                            if(FArrAuthFileNum!=0){


                                                JSONArray AuthFileIDDataArray = AuthFileID.getJSONArray("data");
                                                JSONObject AuthFileIDDataObj = AuthFileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                                String authfileid = AuthFileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                                RoleAuth.RoleAuthDis(PosBDuty, PosBName, authfileid);
                                                RoleAuth.AssRoleAuthDis(PosBDuty, PosBName, authfileid);
                                            }
                                        }
                                    }
                                    // 获取岗位A角色及其上级直至董事长，并授权
                                    if (PosA != null)
                                    {
                                        PosADuty = PosA;
                                        PosAName=PosAName;
                                        RoleAuth.RoleAAuthDis(PosADuty, PosAName, AuthFileId);
                                        RoleAuth.AssRoleAuthDis(PosADuty, PosAName, AuthFileId);

                                        while(FAttribute.contains("-"))	{
                                            FAttribute=FAttribute.substring(0, FAttribute.lastIndexOf("-"));
                                            JSONObject AuthFileID = api.getFileID( FAttribute);
                                            int FArrAuthFileNum=Integer.parseInt(AuthFileID.getString("num"));
                                            if(FArrAuthFileNum!=0){


                                                JSONArray AuthFileIDDataArray = AuthFileID.getJSONArray("data");
                                                JSONObject AuthFileIDDataObj = AuthFileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                                String authfileid = AuthFileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                                RoleAuth.RoleAuthDis(PosADuty, PosAName, authfileid);
                                                RoleAuth.AssRoleAuthDis(PosADuty, PosAName, authfileid);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //档案类型为HR
                        if (ArchType.equalsIgnoreCase("14")) {
                            desc = (String) objectMap.get("des");// 获取说明
                            String InfoCode=(String) objectMap.get("baseCode");;//02.0208
                            String HRParentAttribute="";
                            String HRAttribute=attribute;
                            String trans=attribute;
                            elec = (String) subSheet.get(i).get("elec");
                            int level=0;//"-"的个数,level=2时和>2时HRParentAttribute不一样
                            while(trans.contains("-"))
                            {
                                trans=trans.substring(trans.indexOf("-")+1, trans.length());
                                level++;
                            }

                            if(level>1)
                            {
                                //有2/3个"-",
                                strSql="SELECT a.FNumber as personnumber, a.FName_L2 as personname,c.FNumber as positionnumber,c.FName_L2 as positionname FROM [HG_LINK].[hg].dbo.T_BD_Person AS a INNER JOIN [HG_LINK].[hg].dbo.T_POS_PositionConfigBase AS b ON b.FEmpID = a.FID  inner join [HG_LINK].[hg].dbo.T_ORG_Position c on c.FID=b.FPositionID where a.fnumber='"+ InfoCode + "'and b.FMainPosition='1' ";
                                rs = sqlService.getList(CloudSqlService.htEas, strSql);
                                try {

                                    String value =  (String)rs.get(0).get("positionnumber");

                                    if(value!=null){
                                        String  positionnumber=   (String)rs.get(0).get("positionnumber");
                                        HRParentAttribute="HR "+InfoCode;
                                        System.out.println(HRParentAttribute);
                                        //加对应框架
                                        prefix=attribute.substring(attribute.indexOf("-")+1,attribute.replace(" ", "").length());//02.0208-06-04
                                        prefix=prefix.substring(prefix.indexOf("-")+1, prefix.length());//06-04
                                        prefix=prefix.substring(0, 2);//06
                                        strSql01="SELECT a.Fname_l2 as HRARCHTYPE from [HG_LINK].[hg].dbo.CT_GRO_HRARCHTYPE AS a where a.fnumber='"+ prefix + "' ";
                                        rs01 = sqlService.getList(CloudSqlService.htEas, strSql01);
                                        try {

                                            value =  (String)rs01.get(0).get("HRARCHTYPE");

                                            if(value!=null){
                                                String  hrarchtype=  (String)rs01.get(0).get("HRARCHTYPE");
                                                JSONObject FolderIdObj = api.getFileID(HRParentAttribute);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                                                int FolderNum=Integer.parseInt(FolderIdObj.getString("num"));
                                                if(FolderNum==0){
                                                    continue;
                                                }
                                                JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                                JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                                                String folederid = FolderIdObj0.getString("id");
                                                //api.getAddFolder( folederid,prefix+" "+hrarchtype,OrgCode + "-"+ InfoCode+"-"+prefix+" "+InfoCode);
                                            }

                                        } catch (Exception e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }


                                    }

                                } catch (Exception e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }


                            }
                            if(level>2)
                            {
                                HRParentAttribute=attribute.substring(0,attribute.replace(" ", "").lastIndexOf("-"))+ ' '+InfoCode;
                            }
                            //带分项的文件
                            if ( SubItemORNot>0) {
                                //带分项的文件先加文件夹
                                prefix =attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length());//文件夹前缀
                                foldername = prefix + ' ' + (String) objectMap.get("fileName");//需要添加的文件夹名称（01 报告 02公司行政职能资信）
                                //带分项的文件 // 先加文件夹
                                String parentattribute =attribute.replace(" ", "").substring(0,attribute.replace(" ", "").lastIndexOf("-"))+ ' '+ InfoCode;
                                JSONObject FolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                                int SubFolderNum=Integer.parseInt(FolderIdObj.getString("num"));
                                if(SubFolderNum==0){
                                    continue;
                                }
                                JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                                String folederid = FolderIdObj0.getString("id");
                                String SubFolderAttribute =attribute+ ' '+  InfoCode;
                                JSONObject SubFolderIdObj = api.getFileID(SubFolderAttribute);// 分项文件夹是否存在
                                int num=Integer.parseInt(SubFolderIdObj.getString("num"));
                                if(num>0){
                                    JSONArray SubFolderIdDataArray = SubFolderIdObj.getJSONArray("data");
                                    JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(0);
                                    String SubFolderId = SubFolderIdObj0.getString("id");
                                    api.getRename(token, SubFolderId, foldername);
                                }
                                else{
                                    api.getAddFolder( folederid,foldername,attribute + ' '+ InfoCode);
                                }
                                //查询分项文件对应文件夹的FolderId
                                String ExistFolderAttribute =attribute + ' '+   InfoCode;
                                JSONObject ExistFolderObj = api.getFileID(ExistFolderAttribute);//
                                JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                                JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(0);
                                String ExitFolderId = ExistFolderObj0.getString("id");
                                AuthFileId=ExitFolderId;
                                JSONObject ExitFolderFullPath = api.getFullPath( ExitFolderId);
                                String SubItemFolderDestFullpath = ExitFolderFullPath.getString("fullPath");
                                JSONObject ExitFolderListObj=api.getList( ExitFolderId);
                                JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                                int m=ExitFolderListDataArray.size();
                                for(int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++)
                                {
                                    JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                    String IsFolder=ExitFolderListDataObj.getString("fileType");
                                    if(!IsFolder.equals("1")&&SubItemORNot>0){
                                        delId=ExitFolderListDataObj.getString("id");
                                        api.getRemove(token, delId);
                                        System.out.println(ExitFolderListDataObj.get("id"));}
                                }
                                //移动公共区域的分项文件到对应路径
                                for(int Sub=0;Sub<SubItemORNot;Sub++)
                                {
                                    String SubItemattribute = SubItemFileList[Sub].replace("，", ",");//02-A-01-16-02-14-01-01-02,01
                                    JSONObject SubItemFileIdObj=api.getFileID( SubItemattribute);
                                    JSONArray SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                    JSONObject SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                    String SubItemFileId = SubItemFileIdDataObj.getString("id");
//							AuthFileId=SubItemFileId;
                                    api.getMove( SubItemFileId,SubItemFolderDestFullpath);//
                                    if(IsNomalFile==1){//有分项也有正常文件时候，正常文件也传过去
                                        SubItemFileIdObj=api.getFileID( attribute);
                                        SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                        SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                        SubItemFileId = SubItemFileIdDataObj.getString("id");
                                        api.getMove( SubItemFileId,SubItemFolderDestFullpath);//
                                    }

                                    // 获取主责岗位角色及其上级直至董事长，并授权
                                }


                                // 获取主责岗位角色及其上级直至董事长，并授权
                                if (PosMainDuty != null) {
                                     
                                    PosMainName= PosMainName;
                                    RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosMainDuty, AuthFileId);
                                }
                                // 获取岗位B角色及其上级直至董事长，并授权
                                if (PosB != null)
                                {
                                    PosBDuty = PosB;
                                    PosBName=PosBName;
                                    RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosBDuty, AuthFileId);
                                }
                                // 获取岗位A角色及其上级直至董事长，并授权
                                if (PosA != null)
                                {
                                    PosADuty = PosA;
                                    PosAName=PosAName;
                                    RoleAuth.RoleAAuthDis(PosADuty, PosAName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosADuty, PosAName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosADuty, AuthFileId);
                                }


                                continue;
                            }
                            // 如果是框架则在文档系统中加对应的文件夹
                            if (desc != null) {
                                if (desc.length() >= 2) {
                                    if (desc.substring(0, 2).equalsIgnoreCase("框架")) {

                                        //√转为/，删除原文件
                                        JSONObject ExistFileObj = api.getFileID(attribute.replace(" ", ""));//
                                        int FileNum=Integer.parseInt(ExistFileObj.getString("num"));
                                        if(FileNum!=0){
                                            JSONArray ExistFileDataArray = ExistFileObj.getJSONArray("data");
                                            JSONObject ExistFileObj0 = ExistFileDataArray.getJSONObject(0);
                                            String ExitFileId = ExistFileObj0.getString("id");
                                            api.getRemove(token, ExitFileId);
                                        }
                                        else{
                                            String asdf=attribute.replace(" ", "")+" "+InfoCode;
                                            JSONObject ExitFolderObj=api.getFileID( attribute.replace(" ", "")+" "+InfoCode);
                                            FileNum=Integer.parseInt(ExitFolderObj.getString("num"));
                                            if(FileNum!=0){

                                                JSONArray ExitFolderDataArray = ExitFolderObj.getJSONArray("data");
                                                JSONObject ExitFolderObj0 = ExitFolderDataArray.getJSONObject(0);
                                                String ExitFolderId = ExitFolderObj0.getString("id");
                                                JSONObject ExitFolderListObj=api.getList( ExitFolderId);

//											    JSONObject ExitFolderListObj=api.getList( attribute.replace(" ", "")+" "+InfoCode);
                                                JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                                                for(int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++)
                                                {
                                                    JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                                    String IsFolder=ExitFolderListDataObj.getString("fileType");
                                                    if(!IsFolder.equals("1")&&SubItemORNot>0){
                                                        delId=ExitFolderListDataObj.getString("id");
                                                        api.getRemove(token, delId);
                                                        System.out.println(ExitFolderListDataObj.get("id"));}
                                                }
                                            }
                                        }

                                        JSONObject FParentFileID=api.getFileID( HRParentAttribute);
//						if(!SDParentFileID.containsValue("data")){continue;}
                                        JSONArray FParentFileIdDataArray = FParentFileID.getJSONArray("data");
                                        JSONObject FParentFileIdDataObj = FParentFileIdDataArray.getJSONObject(0);
                                        String FParentFileId = FParentFileIdDataObj.getString("id");
                                        JSONObject SubFolderIdObj = api.getFileID(attribute.replace(" ", "")+" "+InfoCode);//
                                        int num=Integer.parseInt(SubFolderIdObj.getString("num"));
                                        if(num>0){
                                            JSONArray SubFolderIdDataArray = SubFolderIdObj.getJSONArray("data");
                                            JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(0);
                                            String SubFolderId = SubFolderIdObj0.getString("id");
                                            api.getRename(token, SubFolderId, attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"));
                                        }
                                        else{
                                            api.getAddFolder( FParentFileId,attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"),attribute.replace(" ", "")+" "+InfoCode);
                                        }


                                    }
                                }
                            }
                            if (elec.equalsIgnoreCase("√")&&SubItemORNot==0&&IsNomalFile==1) {
                                //不是分项的文件
                                HRAttribute=attribute;
//                                FLevelParAttribute=FAttribute.substring(0, FAttribute.lastIndexOf("-"))+" "+PosADuty;//上级文件夹扩展属性SD-02-142331024-9002-31 02.06.07.003
                                JSONObject FParentFolderID=api.getFileID( HRParentAttribute);
                                int FParentFolderNum=Integer.parseInt(FParentFolderID.getString("num"));
                                if(FParentFolderNum==0){
                                    continue;
                                }
                                JSONArray FParentFolderIdDataArray = FParentFolderID.getJSONArray("data");
                                JSONObject FParentFolderIdDataObj = FParentFolderIdDataArray.getJSONObject(0);
                                String FParentFolderId = FParentFolderIdDataObj.getString("id");
                                //查找文件对应文件夹
                                JSONObject ExistFolderIdObj=api.getFileID( HRAttribute+" "+InfoCode);
                                int FArrFolderNum=Integer.parseInt(ExistFolderIdObj.getString("num"));
                                //如果不存在文件对应文件夹则先添加对应文件夹
                                if(FArrFolderNum==0)
                                {
                                    JSONObject FFolderIdObj=api.getAddFolder( FParentFolderId,attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"),HRAttribute+" "+InfoCode);
                                    //获取对应文件夹的FolderId，以便之后将文件放入该文件夹
                                    addfileid = FFolderIdObj.getString("fileId");
                                }
                                //如果存在文件对应文件夹则先需要找到其FolderId
                                else{
                                    JSONArray  ExistFolderIdDataArray = ExistFolderIdObj.getJSONArray("data");
                                    JSONObject ExistFolderIdDataObj = ExistFolderIdDataArray.getJSONObject(0);
                                    addfileid = ExistFolderIdDataObj.getString("id");
                                    api.getRename(token, addfileid,attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"));
                                    JSONObject ExitFolderListObj=api.getList( addfileid);
                                    JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                                    for(int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++)
                                    {
                                        JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                        String IsFolder=ExitFolderListDataObj.getString("fileType");
                                        String filename=ExitFolderListDataObj.getString("fileName");
                                        if(!IsFolder.equals("1")&&SubItemORNot>0){

                                            delId=ExitFolderListDataObj.getString("id");
                                            api.getRemove(token, delId);
                                            System.out.println(ExitFolderListDataObj.get("id"));
                                        }
                                    }
                                }
                                JSONObject FolderPathObj = api.getFullPath( addfileid);
                                String destfullpath = FolderPathObj.getString("fullPath");
                                JSONObject FileID = api.getFileID(  HRAttribute);
                                int HRArrFileNum=Integer.parseInt(FileID.getString("num"));
                                if(HRArrFileNum==0){
                                    continue;
                                }
                                JSONArray FileIDDataArray = FileID.getJSONArray("data");
                                JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                                JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                                fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                                String NewFileName=FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));

                                if(FileIDDataArray.size()==1){
                                    api.getMove( fileid,destfullpath);
                                }//有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的
                                else if(!addfileid.equals(Parentid2)) {


                                    api.getMove( fileid,destfullpath);

                                    JSONObject  ListFileAuth= api.getListFileAuth(token,fileid2);
                                    JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                                    int a=ListFileAuthDataArray.size();
                                    for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++)
                                    {

                                        JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                        String RemoveAuthPos= ListFileAuthDataObj.getString("name");
                                        String auth= ListFileAuthDataObj.getString("auth");
                                        JSONObject array1=api.getRole(RemoveAuthPos);
                                        String num=array1.getString("num");
                                        if(!num.equals("0")){
                                            JSONArray array = array1.getJSONArray("data");
                                            JSONObject array2 = array.getJSONObject(0);
                                            String userId = array2.getString("id");
                                            api.getRoleAuth( RemoveAuthPos, userId, auth, fileid);}
                                    }


//    								api.getRename(token, fileid2, NewFileName);
//    								String MOVEUpdate = api.getUpdate(token,fileid, fileid2);
                                    api.getRemove(token, fileid2);
                                }
                                //有2个及以上相同扩展属性的文件时，A,B岗位未变更
                                else if(FileIDDataArray.size()>1) {

                                    //将老文件放入userFolder
                                    JSONObject FolderPathObj01 = api.getFullPath( "1688009");
                                    String CAdestfullpath01 = FolderPathObj01.getString("fullPath");
                                    api.getMove( fileid2,CAdestfullpath01);//将老文件放入userFolder
//    								api.getRename(token, fileid2, "待删除");//先将老文件名改为"待删除"


                                    api.getMove( fileid,destfullpath);//将新文件移动到对应文件夹，此时和老文件放在同一个文件夹
                                    JSONObject  ListFileAuth= api.getListFileAuth(token,fileid2);//获取老文件的权限
                                    JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                                    int a=ListFileAuthDataArray.size();
                                    for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++)
                                    {//将老文件的权限授予新文件

                                        JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                        String RemoveAuthPos= ListFileAuthDataObj.getString("name");
                                        String auth= ListFileAuthDataObj.getString("auth");
                                        JSONObject array1=api.getRole(RemoveAuthPos);
                                        String num=array1.getString("num");
                                        if(!num.equals("0"))
                                        {
                                            JSONArray array = array1.getJSONArray("data");
                                            JSONObject array2 = array.getJSONObject(0);
                                            String userId = array2.getString("id");
                                            api.getRoleAuth( RemoveAuthPos, userId, auth, fileid);
                                        }
                                    }
                                    api.getRemove(token, fileid2);//删除老文件
                                }

                                AuthFileId=fileid;
                                // 获取主责岗位角色及其上级直至董事长，并授权
                                if (PosMainDuty != null) {
                                     
                                    PosMainName= PosMainName;
                                    RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    while(HRAttribute.contains("-"))	{
//
                                        JSONObject AuthFileID = api.getFileID( HRAttribute);
                                        int HRArrAuthFileNum=Integer.parseInt(AuthFileID.getString("num"));
                                        if(HRArrAuthFileNum!=0){


                                            JSONArray AuthFileIDDataArray = AuthFileID.getJSONArray("data");
                                            JSONObject AuthFileIDDataObj = AuthFileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                            String authfileid = AuthFileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                            RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, authfileid);

                                        }
                                        HRAttribute=HRAttribute.substring(0, HRAttribute.lastIndexOf("-"));
                                    }

                                }
                                // 获取岗位B角色及其上级直至董事长，并授权
                                if (PosB != null)
                                {
                                    PosBDuty = PosB;
                                    PosBName=PosBName;
                                    RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    while(HRAttribute.contains("-"))	{
//                                        HRAttribute=HRAttribute.substring(0, HRAttribute.lastIndexOf("-"));
                                        JSONObject AuthFileID = api.getFileID( HRAttribute);
                                        int HRArrAuthFileNum=Integer.parseInt(AuthFileID.getString("num"));
                                        if(HRArrAuthFileNum!=0){


                                            JSONArray AuthFileIDDataArray = AuthFileID.getJSONArray("data");
                                            JSONObject AuthFileIDDataObj = AuthFileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                            String authfileid = AuthFileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                            RoleAuth.RoleAuthDis(PosBDuty, PosBName, authfileid);
                                        }
                                        HRAttribute=HRAttribute.substring(0, HRAttribute.lastIndexOf("-"));
                                    }
                                }
                                // 获取岗位A角色及其上级直至董事长，并授权
                                if (PosA != null)
                                {
                                    PosADuty = PosA;
                                    PosAName=PosAName;
                                    RoleAuth.RoleAAuthDis(PosADuty, PosAName, AuthFileId);
                                    while(HRAttribute.contains("-"))	{
//                                        HRAttribute=HRAttribute.substring(0, HRAttribute.lastIndexOf("-"));
                                        JSONObject AuthFileID = api.getFileID( HRAttribute);
                                        int HRArrAuthFileNum=Integer.parseInt(AuthFileID.getString("num"));
                                        if(HRArrAuthFileNum!=0){


                                            JSONArray AuthFileIDDataArray = AuthFileID.getJSONArray("data");
                                            JSONObject AuthFileIDDataObj = AuthFileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                            String authfileid = AuthFileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                            RoleAuth.RoleAuthDis(PosADuty, PosAName, authfileid);
                                        }
                                        HRAttribute=HRAttribute.substring(0, HRAttribute.lastIndexOf("-"));
                                    }
                                }
                            }
                        }





                        if (ArchType.equalsIgnoreCase("16")) {

                            FolderAttr=	"OD-"+OrgCode+" "+OrgCode;

                            int countlevel=1;
                            String Sreial01="";//201607
                            String Sreial02="";//002
                            String Sreial03="";//01
                            JSONObject FolderIdObj;
                            JSONArray FolderIdDataArray;
                            String FolderId="";
                            JSONObject FolderIdDataObj;
                            String FileAttr=attribute;
                            String Sreial=(String) objectMap.get("serialNo");
                            while(Sreial.contains("-"))
                            {
                                Sreial=Sreial.substring(Sreial.indexOf("-")+1, Sreial.length());
                                countlevel++;

                            }
                            if(countlevel>1){
                                FolderIdObj=api.getFileID( FolderAttr);
                                FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                FolderIdDataObj = FolderIdDataArray.getJSONObject(0);
                                FolderId = FolderIdDataObj.getString("id");
                            }

                            Sreial=(String) objectMap.get("serialNo");
                            if(countlevel==1){
                                ParentFolderAttr=	attribute.substring(0, attribute.lastIndexOf("-"))+" "+OrgCode;
                                JSONObject ParentFolderIdObj=api.getFileID( ParentFolderAttr);
                                JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                                JSONObject ParentFolderIdDataObj = ParentFolderIdDataArray.getJSONObject(0);
                                String FParentFileId = ParentFolderIdDataObj.getString("id");

                                FolderAttr=	attribute+" "+OrgCode;
                                FolderIdObj=api.getFileID( FolderAttr);
                                int FileNum=Integer.parseInt(FolderIdObj.getString("num"));
                                if(FileNum>0){
                                    JSONArray SubFolderIdDataArray = FolderIdObj.getJSONArray("data");
                                    JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(0);
                                    String SubFolderId = SubFolderIdObj0.getString("id");
                                    api.getRename(token, SubFolderId, attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"));
                                }
                                else{
                                    api.getAddFolder( FParentFileId,attribute.replace(" ", "").substring(attribute.lastIndexOf("-")+1,attribute.replace(" ", "").length()) +" "+(String) objectMap.get("fileName"),attribute.replace(" ", "")+" "+OrgCode);
                                }

                                FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                FolderIdDataObj = FolderIdDataArray.getJSONObject(0);

                                //获取对应文件夹的FolderId，以便之后将文件放入该文件夹
                                addfileid = FolderIdDataObj.getString("id");



                                JSONObject FolderPathObj = api.getFullPath( addfileid);
                                String CAdestfullpath = FolderPathObj.getString("fullPath");
                                JSONObject FileID = api.getFileID( FileAttr);
                                int CAArrFileNum=Integer.parseInt(FileID.getString("num"));
                                if(CAArrFileNum==0){
                                    continue;
                                }

                                JSONArray FileIDDataArray = FileID.getJSONArray("data");
                                JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                                fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                                String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                                String NewFileName=FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));
                                if(FileIDDataArray.size()==1){
                                    api.getMove( fileid,CAdestfullpath);
                                }//有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的
                                else if(!addfileid.equals(Parentid2)) {


                                    api.getMove( fileid,CAdestfullpath);

                                    JSONObject  ListFileAuth= api.getListFileAuth(token,fileid2);
                                    JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                                    int a=ListFileAuthDataArray.size();
                                    for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++)
                                    {

                                        JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                        String RemoveAuthPos= ListFileAuthDataObj.getString("name");
                                        String auth= ListFileAuthDataObj.getString("auth");
                                        JSONObject array1=api.getRole(RemoveAuthPos);
                                        String num=array1.getString("num");
                                        if(!num.equals("0")){
                                            JSONArray array = array1.getJSONArray("data");
                                            JSONObject array2 = array.getJSONObject(0);
                                            String userId = array2.getString("id");
                                            api.getRoleAuth( RemoveAuthPos, userId, auth, fileid);}
                                    }
                                    api.getRemove(token, fileid2);
//    					api.getRename(token, fileid2, NewFileName);
//    					String MOVEUpdate = api.getUpdate(token,fileid, fileid2);
                                }
                                //有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的
                                else if(FileIDDataArray.size()>1) {
//    					api.getRemove(token, fileid2);
//    					api.getMove( fileid,destfullpath);

                                    api.getRename(token, fileid2, NewFileName);
                                    String MOVEUpdate = api.getUpdate(token,fileid, fileid2);
                                    System.out.print(MOVEUpdate);
                                }
                                JSONObject FileID01 = api.getFileID( FileAttr);
                                JSONArray FileIDDataArray01 = FileID01.getJSONArray("data");
                                JSONObject FileIDDataObj01 = FileIDDataArray01.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                String fileid3 = FileIDDataObj01.getString("id");
                                AuthFileId=fileid3;
                                // 获取主责岗位角色及其上级直至董事长，并授权
                                if (PosMainDuty != null) {
                                     
                                    PosMainName= PosMainName;
                                    RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosMainDuty, AuthFileId);
                                }
                                // 获取岗位B角色及其上级直至董事长，并授权
                                if (PosB != null)
                                {
                                    PosBDuty = PosB;
                                    PosBName=PosBName;
                                    RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosBDuty, AuthFileId);
                                }
                                // 获取岗位A角色及其上级直至董事长，并授权
                                if (PosA != null)
                                {
                                    PosADuty = PosA;
                                    PosAName=PosAName;
                                    RoleAuth.RoleAAuthDis(PosADuty, PosAName, AuthFileId);
                                    RoleAuth.AssRoleAuthDis(PosADuty, PosAName, AuthFileId);
                                    RoleAuth.RoleDeptAss(token, PosADuty, AuthFileId);
                                }
                                continue;
                            }

                            Sreial01=Sreial.substring(0, Sreial.indexOf("-"));
                            Sreial02=Sreial.substring(7, 10);


                            String asd="OD-"+OrgCode+"-"+Sreial01.substring(0, 4)+" "+OrgCode;
                            api.getAddFolder( FolderId, Sreial01.substring(0, 4)+"年",asd);
                            JSONObject FolderIdObj01=api.getFileID( asd);
                            JSONArray FolderIdDataArray01 = FolderIdObj01.getJSONArray("data");
                            JSONObject FolderIdDataObj01 = FolderIdDataArray01.getJSONObject(0);
                            String FolderId01 = FolderIdDataObj01.getString("id");


                            String asd01="OD-"+OrgCode+"-"+Sreial01+" "+OrgCode;
                            api.getAddFolder( FolderId01, Sreial01.substring(4, 6)+"月",asd01);
                            JSONObject FolderIdObj02=api.getFileID( asd01);
                            JSONArray FolderIdDataArray02 = FolderIdObj02.getJSONArray("data");
                            JSONObject FolderIdDataObj02 = FolderIdDataArray02.getJSONObject(0);
                            String FolderId02 = FolderIdDataObj02.getString("id");

                            String asd02="OD-"+OrgCode+"-"+Sreial01+"-"+Sreial02+" "+OrgCode;
                            api.getAddFolder( FolderId02, Sreial02,asd02);
                            JSONObject FolderIdObj03=api.getFileID( asd02);
                            JSONArray FolderIdDataArray03 = FolderIdObj03.getJSONArray("data");
                            JSONObject FolderIdDataObj03 = FolderIdDataArray03.getJSONObject(0);
                            String FolderId03 = FolderIdDataObj03.getString("id");





                            if(countlevel==3){
                                Sreial03=Sreial.substring(11, 13);
                                api.getAddFolder( FolderId03, Sreial03,attribute+" "+OrgCode);


                            }

                            ParentFolderAttr=	attribute+" "+OrgCode;
                            JSONObject ParentFolderIdObj=api.getFileID( ParentFolderAttr);
                            JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                            JSONObject ParentFolderIdDataObj = ParentFolderIdDataArray.getJSONObject(0);
                            String FParentFileId = ParentFolderIdDataObj.getString("id");
                            JSONObject FolderPathObj = api.getFullPath( FParentFileId);
                            String CAdestfullpath = FolderPathObj.getString("fullPath");
                            JSONObject FileID = api.getFileID( FileAttr);
                            int CAArrFileNum=Integer.parseInt(FileID.getString("num"));
                            if(CAArrFileNum==0){
                                continue;
                            }

                            JSONArray FileIDDataArray = FileID.getJSONArray("data");
                            JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                            String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                            JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                            fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                            String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                            String NewFileName=FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));
                            if(FileIDDataArray.size()==1){
                                api.getMove( fileid,CAdestfullpath);
                            }//有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的
                            else if(!addfileid.equals(Parentid2)) {


                                api.getMove( fileid,CAdestfullpath);

                                JSONObject  ListFileAuth= api.getListFileAuth(token,fileid2);
                                JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                                int a=ListFileAuthDataArray.size();
                                for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++)
                                {

                                    JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                    String RemoveAuthPos= ListFileAuthDataObj.getString("name");
                                    String auth= ListFileAuthDataObj.getString("auth");
                                    JSONObject array1=api.getRole(RemoveAuthPos);
                                    String num=array1.getString("num");
                                    if(!num.equals("0")){
                                        JSONArray array = array1.getJSONArray("data");
                                        JSONObject array2 = array.getJSONObject(0);
                                        String userId = array2.getString("id");
                                        api.getRoleAuth( RemoveAuthPos, userId, auth, fileid);}
                                }
                                api.getRemove(token, fileid2);
//					api.getRename(token, fileid2, NewFileName);
//					String MOVEUpdate = api.getUpdate(token,fileid, fileid2);
                            }
                            //有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的
                            else if(FileIDDataArray.size()>1) {
//					api.getRemove(token, fileid2);
//					api.getMove( fileid,destfullpath);

                                api.getRename(token, fileid2, NewFileName);
                                String MOVEUpdate = api.getUpdate(token,fileid, fileid2);
                                System.out.print(MOVEUpdate);
                            }
                            JSONObject FileID01 = api.getFileID( FileAttr);
                            JSONArray FileIDDataArray01 = FileID01.getJSONArray("data");
                            JSONObject FileIDDataObj01 = FileIDDataArray01.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                            String fileid3 = FileIDDataObj01.getString("id");
                            AuthFileId=fileid3;
                            // 获取主责岗位角色及其上级直至董事长，并授权
                            if (PosMainDuty != null) {
                                 
                                PosMainName= PosMainName;
                                RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                                RoleAuth.RoleDeptAss(token, PosMainDuty, AuthFileId);
                            }
                            // 获取岗位B角色及其上级直至董事长，并授权
                            if (PosB != null)
                            {
                                PosBDuty = PosB;
                                PosBName=PosBName;
                                RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                RoleAuth.AssRoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                RoleAuth.RoleDeptAss(token, PosBDuty, AuthFileId);
                            }
                            // 获取岗位A角色及其上级直至董事长，并授权
                            if (PosA != null)
                            {
                                PosADuty = PosA;
                                RoleAuth.RoleAAuthDis(PosADuty, PosAName, AuthFileId);
                                RoleAuth.AssRoleAuthDis(PosADuty, PosAName, AuthFileId);
                                RoleAuth.RoleDeptAss(token, PosADuty, AuthFileId);
                            }
                        }



                    }
                }
            }



        return getOkResponseResult("成功");


    }

    //公司级档案部门授权信息
    @Async
    @GetMapping("ComArchiveAuth")
    public ResponseResult ComArchiveAuth(@RequestParam String bizId){
        String FolderAttribute = "";
        String FolderAttr = "";
        String prefix = "";
        String elec = "";
        String PosMainName= "";
        String PosAName= "";
        String PosBName= "";
        String fileid= "";
        String ParentFolderAttr="";
        String addfileid = "";
        String delId= "";
        String foldername = "";
        String AuthFileId = "";
        String AppEmpId="";
        int SubItemORNot=0;
        String FunMainDeptAssDuty;
        String FunMainDeptAssDutyName="";
        String FunMainDeptLeaderDutyName="";
        int SubmitPubSubItemFileNum=0;
        JSONObject FrameAttributeObj;
        String PosMainDuty="";
        String PosADuty="";
        String PosBDuty="";
        String AuthReqDeptDuty;
        String MainDeptAssDutyName="";
        DocAPI api = new DocAPI();
        String temp="";
        String token = api.getToken();
        RoleAuth RoleAuth=new RoleAuth();
        String desc = "";
        String attribute = "";
        String parentattribute= "";
        int ChaNum;
        String ArchType="";
        String OTTFileName;
        String MainDeptDuty="";
        String AuthReqDeptAssDuty="";
        String AuthReqDeptLeaderDuty="";
        String  shouwei="";
        String userId006 ="";
        String userId001 ="";
        String auth="";
        String mainDeptNumber="";
        String mainDeptName="";


        Map<String, Object> objectMap = null;

        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject("comDepartmentAuthInfo", bizId);

        List<Map<String, Object>> subSheet = (List<Map<String, Object>>) bizObject.getObject("comDepartmentAuthInfoSheet1");


        //获取表单上主责部门
        mainDeptNumber = (String) subSheet.get(0).get("mainDepartmentNumber");
        mainDeptName=(String) subSheet.get(0).get("mainDepartmentName");

        String fisleaf="";


        //获取表单上授权需求部门
        AuthReqDeptDuty=(String) subSheet.get(0).get("authDeptNumber");

        //授权需求部门如果是中间组织则选取对应.10的部门
        String sqlDeptIsLeaf="select cast(fisleaf as nvarchar(5)) as  fisleaf from [HG_LINK].[hg].dbo.T_org_admin WHERE FNUMBER ='"+ AuthReqDeptDuty + "'";
        List<Map<String, Object>> rsDeptIsLeaf = sqlService.getList(CloudSqlService.htEas, sqlDeptIsLeaf);
        try {

            fisleaf =  (String)rsDeptIsLeaf.get(0).get("fisleaf");

            if(fisleaf.equals("0")){
                AuthReqDeptDuty=AuthReqDeptDuty+".10";

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int n = subSheet.size();
        // 获取授权需求部门助理角色
        AuthReqDeptAssDuty=AuthReqDeptDuty+".006";
        AuthReqDeptLeaderDuty=AuthReqDeptDuty+".001";
        String sqlFunMainDeptAssDutyCode="select fname_l2 from [HG_LINK].[hg].dbo.T_org_position WHERE FNUMBER ='"+ AuthReqDeptAssDuty + "'";
        List<Map<String, Object>> rsFunMainDeptAssDutyCode = sqlService.getList(CloudSqlService.htEas, sqlFunMainDeptAssDutyCode);
        try {

            FunMainDeptAssDutyName =  (String)rsFunMainDeptAssDutyCode.get(0).get("fname_l2");
            JSONObject array1=api.getRole( AuthReqDeptAssDuty+" "+ FunMainDeptAssDutyName);
            String num=array1.getString("num");
            if(!num.equals("0")){
                JSONArray array = array1.getJSONArray("data");
                JSONObject array2 = array.getJSONObject(0);
                userId006 = array2.getString("id");
            }


        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        String sqlFunMainDeptLeaserDutyCode="select fname_l2 from [HG_LINK].[hg].dbo.T_org_position WHERE FNUMBER ='"+ AuthReqDeptLeaderDuty + "'";
        List<Map<String, Object>> rsFunMainDeptLeaderDutyCode = sqlService.getList(CloudSqlService.htEas, sqlFunMainDeptLeaserDutyCode);
        try {

            FunMainDeptLeaderDutyName =  (String)rsFunMainDeptLeaderDutyCode.get(0).get("fname_l2");
            JSONObject array2=api.getRole( AuthReqDeptLeaderDuty+" "+ FunMainDeptLeaderDutyName);
            String num2=array2.getString("num");
            if(!num2.equals("0")){
                JSONArray array3 = array2.getJSONArray("data");
                JSONObject array4 = array3.getJSONObject(0);
                userId001 = array4.getString("id");
            }


        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        for (int i = 0; i < n; i++) {
            objectMap = subSheet.get(i);
            // 获取档案类型号
            ArchType = (String) objectMap.get("archTypeNumber");
            //获取授权信息
            String authId=(String) objectMap.get("purview");
//            int authNum=Integer.parseInt(authId);
            if("▲".equals(authId)){ auth="VPD";}
            else if("√".equals(authId)){auth="VP";}
            else if("▲".equals(authId)){ auth="VPD";}
            else { auth="BLANK";};

            attribute=(String) objectMap.get("fileCode");//档案代码
            attribute = attribute.replace(" ", "");//档案代码
            FrameAttributeObj=api.getFileID(  attribute);
            String FrameNum=FrameAttributeObj.getString("num");

            int FrameNum01=Integer.parseInt(FrameNum);
//            if(FrameNum01==0){
//                continue;
//            }
            if(FrameNum01>0){
                JSONArray FrameAttributeDataArray = FrameAttributeObj.getJSONArray("data");
                JSONObject FrameAttributeDataObj = FrameAttributeDataArray.getJSONObject(0);
                fileid = FrameAttributeDataObj.getString("id");
                if(!"BLANK".equals(auth)){
                    api.getRoleAuth( AuthReqDeptAssDuty+" "+FunMainDeptAssDutyName, userId006, auth, fileid);
                    api.getRoleAuth(AuthReqDeptLeaderDuty+" "+FunMainDeptLeaderDutyName, userId001, auth, fileid);
                }
                if("BLANK".equals(auth)){
//					 RoleAuth.RoleAuthClear( token,AuthReqDeptAssDuty+" "+FunMainDeptAssDutyName,fileid);
//					 RoleAuth.RoleAuthClear( token,AuthReqDeptLeaderDuty+" "+FunMainDeptLeaderDutyName,fileid);
                    RoleAuth.RoleDeptAuthClear(token, AuthReqDeptDuty, fileid);
                    System.out.print("5");
                }

            }
            if(FrameNum01==0){//分项
                String baseCode=(String) objectMap.get("baseCode");
                attribute=baseCode.replace(" ", "")+" "+mainDeptNumber;
                FrameAttributeObj=api.getFileID( attribute);
                String FrameNum02=FrameAttributeObj.getString("num");
                int FrameNum03=Integer.parseInt(FrameNum02);
                if(FrameNum03>0){
                    JSONArray FrameAttributeDataArray = FrameAttributeObj.getJSONArray("data");
                    JSONObject FrameAttributeDataObj = FrameAttributeDataArray.getJSONObject(0);
                    fileid = FrameAttributeDataObj.getString("id");
                    if(!"BLANK".equals(auth)){
                        api.getRoleAuth(AuthReqDeptAssDuty+" "+FunMainDeptAssDutyName, userId006, auth, fileid);
                        api.getRoleAuth( AuthReqDeptLeaderDuty+" "+FunMainDeptLeaderDutyName, userId001, auth, fileid);
                    }
                    if("BLANK".equals(auth)){
                        RoleAuth.RoleDeptAuthClear(token, AuthReqDeptDuty, fileid);
                    }

                }
            }





        }



        return getOkResponseResult("成功");
    }




    //公司级档案作废
//    @Async
    @GetMapping("ComArchiveVoid")
    private ResponseResult  ComArchiveVoid(@RequestParam String bizId){
        // TODO Auto-generated method stub
        String elec="";
        DocAPI api=new DocAPI();
        String token =api.getToken();
        String mainDeptNumber="";
        String mainDeptName="";


        Map<String, Object> objectMap = null;

        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject("companyFileBaseInfoVoid",  bizId);

        List<Map<String, Object>> subSheet = (List<Map<String, Object>>) bizObject.getObject("sheetCompanyFileVoid");


        //获取表单上主责部门
        mainDeptNumber = (String) subSheet.get(0).get("defaultmainDeptId");
        mainDeptName=(String) subSheet.get(0).get("MainDept");
            int n = subSheet.size();
            for(int i=0;i<n;i++)
            {
                //获取档案代码

                elec=(String) subSheet.get(i).get("elec");
                String ArchCode=(String)  subSheet.get(i).get("fileCode");//档案代码
//                attribute = attribute.replace(" ", "");//档案代码
                ArchCode = ArchCode.replace(" ", "");//档案代码
                //档案类型
                String ArchType=(String)  subSheet.get(i).get("fileTypeNumber");
                //获取表单上岗A
                String PosAduty = (String)  subSheet.get(i).get("postionACode");
                if(elec.equalsIgnoreCase("√") ||elec.equalsIgnoreCase("×")){
                    //根据扩展属性（档案代码）查询文件id
                    String attribute=ArchCode;
                    if(ArchType.equalsIgnoreCase("15"))//HR
                    {
                        attribute=ArchCode+" "+ mainDeptNumber;
                    }
                    if(ArchType.equalsIgnoreCase("13"))//CSD
                    {
                        attribute=ArchCode+" "+ PosAduty;
                    }
                    if(ArchType.equalsIgnoreCase("12"))//F
                    {
                        attribute=ArchCode+" "+ PosAduty;
                    }
                    if(ArchType.equalsIgnoreCase("14"))//HR
                    {
                        String baseCode=(String)  subSheet.get(i).get("baseCode");
                        attribute=ArchCode+" "+ baseCode;//02.0208
                    }
                    if(ArchType.equalsIgnoreCase("16"))//OD
                    {
                        attribute=ArchCode+" "+ PosAduty.substring(0, PosAduty.indexOf("-"));//02.0208
                    }
                    //删除文件夹下所有文件（不包含下面的文件夹）
                    JSONObject ExistFolderObj = api.getFileID(attribute);//
                    int FileNum=Integer.parseInt(ExistFolderObj.getString("num"));
                    if(FileNum==0){
                        continue;
                    }
                    if(FileNum!=0){

                        JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                        for(i=0;i<ExistFolderDataArray.size();i++){
                            JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(i);
                            String ExitFolderId = ExistFolderObj0.getString("id");
                            api.getRemove(token, ExitFolderId);}

                    }
                }
                if(elec.equalsIgnoreCase("/")){
                    String attribute="";
                    if(ArchType.equalsIgnoreCase("15")){
                        attribute=ArchCode+ ' '+ mainDeptNumber;
                    }
                    if(ArchType.equalsIgnoreCase("13")){
                        attribute=ArchCode+ ' '+ PosAduty;;
                    }
                    if(ArchType.equalsIgnoreCase("12"))
                    {
                        attribute=ArchCode+" "+ PosAduty;;
                    }
                    if(ArchType.equalsIgnoreCase("14"))
                    {String baseCode=(String)  subSheet.get(i).get("baseCode");
                        attribute=ArchCode+" "+ baseCode;//02.0208
                    }
                    if(ArchType.equalsIgnoreCase("16"))
                    {
                        attribute=ArchCode+" "+ PosAduty.substring(0, PosAduty.indexOf("-"));//02.0208
                    }
                    JSONObject FileID=api.getFileID(attribute);
                    String num = FileID.getString("num");

                    if(num.equalsIgnoreCase("0")){continue;}
                    if(!num.equalsIgnoreCase("0")){
                        JSONArray filearray = FileID.getJSONArray("data");
                        JSONObject filearray1 = filearray.getJSONObject(filearray.size()-1);
                        String fileId = filearray1.getString("id");
                        JSONObject listfile = api.getList(fileId);
                        api.getRemove(token, fileId);

                    }
                }
            }

        return getOkResponseResult("成功");
    }

}

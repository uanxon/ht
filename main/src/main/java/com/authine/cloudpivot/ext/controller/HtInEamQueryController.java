package com.authine.cloudpivot.ext.controller;

import com.alibaba.fastjson.JSON;
import com.authine.cloudpivot.engine.api.facade.BizObjectFacade;
import com.authine.cloudpivot.engine.api.model.bizquery.BizQueryModel;
import com.authine.cloudpivot.engine.api.model.organization.UserModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectQueryModel;
import com.authine.cloudpivot.engine.api.model.runtime.SelectionValue;
import com.authine.cloudpivot.engine.component.query.api.FilterExpression;
import com.authine.cloudpivot.engine.component.query.api.Page;
import com.authine.cloudpivot.engine.component.query.api.helper.PageableImpl;
import com.authine.cloudpivot.engine.enums.status.SequenceStatus;
import com.authine.cloudpivot.engine.enums.type.ClientType;
import com.authine.cloudpivot.engine.enums.type.QueryDisplayType;
import com.authine.cloudpivot.web.api.controller.base.BaseController;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.*;


@RestController
@RequestMapping("/api/htInEamQuery")
public class HtInEamQueryController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(HtInEamQueryController.class);

    /**
     * 对外接口，查询PersonnelFileCheck表单数据
     *
     * @param billNo
     * @return
     */
    @RequestMapping(value="/getPersonnelFileCheckByParam",method = {RequestMethod.POST ,RequestMethod.GET })
    @ApiOperation(value = "对外接口，查询PersonnelFileCheck表单数据", notes = "对外接口，查询PersonnelFileCheck表单数据")
    public List<Map<String,Object>> getPersonnelFileCheckByParam(String billNo) {
        log.info("获取参数:obillNo={}", billNo);

        BizObjectQueryModel queryModel = new BizObjectQueryModel();
        PageableImpl pageable = new PageableImpl(0,100000) ;
        queryModel.setSchemaCode("PersonnelFileCheck");
        queryModel.setQueryCode("PersonnelFileCheck");
        queryModel.setClientType(ClientType.PC);
        queryModel.setPageable(pageable);
        BizObjectQueryModel.Options options = new BizObjectQueryModel.Options();
        options.setQueryDisplayType(QueryDisplayType.APPEND);
        FilterExpression.Item item = new FilterExpression.Item( "sequenceStatus", FilterExpression.Op.Eq, SequenceStatus.COMPLETED.toString());
        queryModel.setFilterExpr(item);
        Page<BizObjectModel> bizObjectModelPage = getBizObjectFacade().queryBizObjects(queryModel);
        log.info("-------------查询PersonnelFileCheck表单总数据量为:{}",bizObjectModelPage.getTotal());
        log.info("-------------查询PersonnelFileCheck表单内容为:{}",JSON.toJSONString(bizObjectModelPage.getContent()));
        List<Map<String,Object>> data=new ArrayList<>();
        bizObjectModelPage.getContent().forEach(bizObjectModel -> {
            Map<String,Object> mapAll=new HashMap<>();
            mapAll.put("billNo",bizObjectModel.getString("billNo"));
            //存储子表内容
            List<Map<String,Object>> sheetMaps=new ArrayList<>();
            //获取子表内容
            List<Map<String,Object>> sheetData= (List<Map<String, Object>>) bizObjectModel.get("Sheet1667958361925");
            if(CollectionUtils.isNotEmpty(sheetData)){
                for (Map<String, Object> sheetDatum : sheetData) {
                    if(sheetDatum!=null && sheetDatum.size()>0){
                        Map<String,Object> sheetMap=new HashMap<>();
                        if(sheetDatum.get("ShortText1667964938278")!=null && sheetDatum.containsKey("ShortText1667964938278")){
                            sheetMap.put("companyCode",sheetDatum.get("ShortText1667964938278").toString());
                        }
                        if(sheetDatum.get("hrDeptPosCode")!=null && sheetDatum.containsKey("hrDeptPosCode")){
                            sheetMap.put("hrDeptPosCode",sheetDatum.get("hrDeptPosCode").toString());
                        }
                        if(sheetDatum.get("hrAssPerson")!=null && sheetDatum.containsKey("hrAssPerson")){
                            //多人选择
                            List<SelectionValue> person= (List<SelectionValue>) sheetDatum.get("hrAssPerson");
                            if(CollectionUtils.isNotEmpty(person)){
                                List<Map<String,String>> personData=new ArrayList<>();
                                for (SelectionValue selectionValue : person) {
                                    Map<String,String> personMap=new HashMap<>();
                                    String id=selectionValue.getId();
                                    UserModel userModel = getOrganizationFacade().getUser(id);
                                    if(userModel!=null){
                                        personMap.put("id",id);
                                        personMap.put("employeeNo",userModel.getEmployeeNo());
                                        personMap.put("name",userModel.getUsername());
                                        personData.add(personMap);
                                    }
                                }
                                sheetMap.put("hrAssPerson",personData);

                            }
                        }
                        sheetMaps.add(sheetMap);

                    }
                }

            }
            mapAll.put("sheetData",sheetMaps);
            data.add(mapAll);
        });
        return data;
    }


}

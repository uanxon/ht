package com.authine.cloudpivot.ext.controller.cancelProcess;

import com.authine.cloudpivot.engine.api.facade.BizWorkitemFacade;
import com.authine.cloudpivot.engine.api.facade.WorkflowInstanceFacade;
import com.authine.cloudpivot.engine.api.model.organization.UserModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
import com.authine.cloudpivot.engine.domain.organization.User;
import com.authine.cloudpivot.ext.controller.cancelProcess.vo.CancelProcessVo;
import com.authine.cloudpivot.web.api.view.PageVO;
import com.authine.cloudpivot.web.api.view.ResponseResult;
import com.authine.cloudpivot.web.api.view.runtime.WorkItemSearchVO;
import com.authine.cloudpivot.web.api.view.runtime.WorkItemVO;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/public/extProcess")
@Slf4j
public class CancelProcessController {
    @Autowired
    BizWorkitemFacade bizWorkitemFacade;
    @Autowired
    WorkflowInstanceFacade workflowInstanceFacade;

    @SneakyThrows
    @PostMapping("/cancelProcess")
    @ResponseBody
    //并行节点是同一个审批人取消其他节点审批
    public void cancelProcess(@RequestBody CancelProcessVo cancelProcessVo ) {
        if (ObjectUtils.isEmpty(cancelProcessVo)){
            return;
        }
        if (StringUtils.isEmpty(cancelProcessVo.getActiveCode())){
            return;
        }
        if (CollectionUtils.isEmpty(cancelProcessVo.getUser())){
            return;
        }
        List<String> list = Arrays.asList(cancelProcessVo.getActiveCode().split(","));
        List<WorkItemModel> listByParticipantAndInstanceId = bizWorkitemFacade.getListByParticipantAndInstanceId(MapUtils.getString(cancelProcessVo.getUser().get(0),"id"),cancelProcessVo.getWorkflowInstanceId());
        if (CollectionUtils.isEmpty(listByParticipantAndInstanceId)){
            return;
        }
        listByParticipantAndInstanceId.stream().forEach(aa ->{
            String activityCode = aa.getActivityCode();
            if (list.contains(activityCode)){
                 workflowInstanceFacade.cancelActivity(MapUtils.getString(cancelProcessVo.getUser().get(0),"id"),cancelProcessVo.getWorkflowInstanceId(),activityCode);
            }
        });
    }

    @SneakyThrows
    @PostMapping("/dataMerge")
    @ResponseBody
    //子表合并数据
    public Map<String,Object> dataMerge(@RequestBody Map<String,List<Map<String,Object>>> maps) {
        Map<String,Object> map = new HashMap<>();
        map.put("jcdm","");
        map.put("dadm","");
        if (ObjectUtils.isEmpty(maps)){
            return map;
        }
        List<Map<String, Object>> list = maps.get("zb");
        if (CollectionUtils.isEmpty(list)){
            return map;
        }
        String jcdm = list.stream().map(p -> MapUtils.getString(p, "baseCode")).collect(Collectors.joining(","));
        String dadm = list.stream().map(p -> MapUtils.getString(p, "fileCode")).collect(Collectors.joining(","));
        String substring = jcdm.length() > 199 ? jcdm.substring(0, 199) : jcdm.substring(0, jcdm.length());
        String substring1 = dadm.length() > 199 ? dadm.substring(0, 199) : dadm.substring(0, dadm.length());
        if (!",".equals(substring)){
            map.put("jcdm",substring);
        }
        if (!",".equals(substring1)){
            map.put("dadm",substring1);
        }
        return map;
    }

}



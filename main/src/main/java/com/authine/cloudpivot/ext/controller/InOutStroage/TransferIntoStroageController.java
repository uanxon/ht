package com.authine.cloudpivot.ext.controller.InOutStroage;

        import com.alibaba.fastjson.JSON;
        import com.authine.cloudpivot.engine.api.model.organization.UserModel;
        import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
        import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
        import com.authine.cloudpivot.engine.api.model.runtime.WorkflowInstanceModel;
        import com.authine.cloudpivot.engine.enums.status.SequenceStatus;
        import com.authine.cloudpivot.ext.Utils.CustomSchemaCode;
        import com.authine.cloudpivot.ext.Utils.Utils;
        import com.authine.cloudpivot.ext.service.CloudSqlService;
        import com.authine.cloudpivot.web.api.controller.base.BaseController;
        import lombok.extern.slf4j.Slf4j;
        import org.apache.commons.collections4.MapUtils;
        import org.apache.commons.lang3.StringUtils;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.util.ObjectUtils;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RestController;

        import java.math.BigDecimal;
        import java.time.LocalDateTime;
        import java.time.format.DateTimeFormatter;
        import java.util.*;

/**
 * 调拨
 **/
@RestController
@RequestMapping("/public/TransferIntoStroageController")
@Slf4j
public class TransferIntoStroageController extends BaseController {
    @Autowired
    CloudSqlService sqlService;

    /**
     * 调拨  审批流完成后 数据写到-库存/预库存 基础表     */
    @RequestMapping("finish")
    public void finish(String bizId) {
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.TransferInto, bizId);

        //Sheet1665369415055  子表
        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get("Sheet1665369415058");
//        String type = (String) bizObject.get("type");


        Date nowTime = Calendar.getInstance().getTime();
        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

//        String approval = getFileBaseInfoWorkFlowApproval(bizObject);
        String approval = "";

        for (Map<String, Object> map : list) {

//            Date entryDate = (Date) map.get("entryDate");//入库日期
            //两个日期相等，返回0
            //entryDate < nowTime，返回负值
            //entryDate > nowTime，返回正值
//            int i = entryDate.compareTo(nowTime);
            BigDecimal zero= BigDecimal.valueOf(0);
            BigDecimal quantityFromSubSheet= BigDecimal.valueOf(0);
            BigDecimal quantity = BigDecimal.valueOf(0);
            BigDecimal auxiliaryQuantityFromSubSheet= BigDecimal.valueOf(0);
            BigDecimal auxiliaryQuantity = BigDecimal.valueOf(0);
            BigDecimal amountFromSubSheet= BigDecimal.valueOf(0);
            BigDecimal amount = BigDecimal.valueOf(0);
            BigDecimal actualAmountFromSubSheet= BigDecimal.valueOf(0);
            BigDecimal actualAmount = BigDecimal.valueOf(0);
            BigDecimal taxFromSubSheet= BigDecimal.valueOf(0);
            BigDecimal tax = BigDecimal.valueOf(0);
            BigDecimal totalFromSubSheet= BigDecimal.valueOf(0);
            BigDecimal total = BigDecimal.valueOf(0);


            try {
                    log.info("------------------------开始执行调出公司、仓库减库存---------------------");
                    //调出公司、仓库减库存
                    Map<String, Object> mapFromBase = existsBizObject(map,bizObject);//mapFromBase是基础表的数据
                    Map<String, Object> data = new HashMap<>();
                    String idFromBase = (String) mapFromBase.get("id");
                    log.info("----------------------------------减库存数据："+ JSON.toJSONString(mapFromBase));

                    if (mapFromBase != null & mapFromBase.size() > 0) {
                        data.put("id", idFromBase);
                        //此时代表通过查询条件，在基础表的查询结果为有值
                        //调出仓库库存累减
                        //数量 quantity
                        quantityFromSubSheet = (BigDecimal) mapFromBase.get("quantity");//基础表的【数量】
                        quantity = zero.add((BigDecimal) map.get("quantity"));//子表的【数量】
                        //辅助数量 auxiliaryQuantity
                        auxiliaryQuantityFromSubSheet = (BigDecimal) mapFromBase.get("auxiliaryQuantity");//基础表的【辅助数量】
                        auxiliaryQuantity = zero.add((BigDecimal) map.get("diaoboAssQuantity"));//子表的【辅助数量】
                        //金额 amount
                        amountFromSubSheet = (BigDecimal) mapFromBase.get("amount");//基础表的【金额】
                        amount = zero.add((BigDecimal) map.get("amount"));//子表的【金额】
                        //现值 actualAmount
                        actualAmountFromSubSheet = (BigDecimal) mapFromBase.get("actualAmount");//基础表的【现值】
                        actualAmount = zero.add((BigDecimal) map.get("actualAmount"));//子表的【现值】
                        //税额 tax
                        taxFromSubSheet = (BigDecimal) mapFromBase.get("tax");//基础表的【税额】
                        tax = zero.add((BigDecimal) map.get("tax"));//子表的【税额】
                        //价税合计 total
                        totalFromSubSheet = (BigDecimal) mapFromBase.get("total");//基础表的【价税合计】
                        total = zero.add((BigDecimal) map.get("total"));//子表的【价税合计】

                        //执行累减逻辑
                        data.put("quantity", quantityFromSubSheet.subtract(quantity).toString());//数量
                        data.put("auxiliaryQuantity", auxiliaryQuantityFromSubSheet.subtract(auxiliaryQuantity).toString());//辅助数量
                        data.put("amount", amountFromSubSheet.subtract(amount).toString());//金额
                        data.put("actualAmount", actualAmountFromSubSheet.subtract(actualAmount).toString());//现值
                        data.put("tax", taxFromSubSheet.subtract(tax).toString());//税额
                        data.put("total", totalFromSubSheet.subtract(total).toString());//价税合计

                        //将子表中数据处理，随后更新至【库存 基础表】
                        BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.warehouse, data, false);
                        model.setSequenceStatus(SequenceStatus.COMPLETED.name());
                        String id = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                        log.info("基础表-庫存基础更新成功:{}", id);
                    }


                //调入公司、仓库增库存
                log.info("------------------------开始执行调入公司、仓库加库存---------------------");
                Map<String, Object> mapFromBase01 = existsBizObject01(map,bizObject);//mapFromBase01是基础表的数据
                Map<String, Object> data01 = new HashMap<>();
                String idFromBase01 = (String) mapFromBase01.get("id");
                log.info("------------------------加库存数据："+JSON.toJSONString(mapFromBase01));
                if (mapFromBase01 != null & mapFromBase01.size() > 0) {
                    data01.put("id", idFromBase01);
                    //此时代表通过查询条件，在基础表的查询结果为有值
                    //调入仓库库存累加
                    //数量 quantity
                    quantityFromSubSheet = (BigDecimal) mapFromBase01.get("quantity");//基础表的【数量】
                    quantity = zero.add((BigDecimal) map.get("quantity"));//子表的【数量】
                    //辅助数量 auxiliaryQuantity
                    auxiliaryQuantityFromSubSheet = (BigDecimal) mapFromBase01.get("auxiliaryQuantity");//基础表的【辅助数量】
                    auxiliaryQuantity = zero.add((BigDecimal) map.get("diaoboAssQuantity"));//子表的【辅助数量】
                    //金额 amount
                    amountFromSubSheet = (BigDecimal) mapFromBase01.get("amount");//基础表的【金额】
                    amount = zero.add((BigDecimal) map.get("amount"));//子表的【金额】
                    //现值 actualAmount
                    actualAmountFromSubSheet = (BigDecimal) mapFromBase01.get("actualAmount");//基础表的【现值】
                    actualAmount = zero.add((BigDecimal) map.get("actualAmount"));//子表的【现值】
                    //税额 tax
                    taxFromSubSheet = (BigDecimal) mapFromBase01.get("tax");//基础表的【税额】
                    tax = zero.add((BigDecimal) map.get("tax"));//子表的【税额】
                    //价税合计 total
                    totalFromSubSheet = (BigDecimal) mapFromBase01.get("total");//基础表的【价税合计】
                    total = zero.add((BigDecimal) map.get("total"));//子表的【价税合计】

                    //执行累加逻辑
                    data01.put("quantity", quantityFromSubSheet.add(quantity).toString());//数量
                    data01.put("auxiliaryQuantity", auxiliaryQuantityFromSubSheet.add(auxiliaryQuantity).toString());//辅助数量
                    data01.put("amount", amountFromSubSheet.add(amount).toString());//金额
                    data01.put("actualAmount", actualAmountFromSubSheet.add(actualAmount).toString());//现值
                    data01.put("tax", taxFromSubSheet.add(tax).toString());//税额
                    data01.put("total", totalFromSubSheet.add(total).toString());//价税合计

                    //将子表中数据处理，随后更新至【库存 基础表】
                    BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.warehouse, data01, false);
                    model.setSequenceStatus(SequenceStatus.COMPLETED.name());
                    String id = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                    log.info("基础表-庫存基础更新成功:{}", id);


                }
                if (mapFromBase01 == null || mapFromBase01.size() == 0) {
                    //此时代表通过查询条件，在基础表的查询结果为空
                    //不累加也不累减，直接将子表的数据插入基础表
                    data01 = fileInfoBaseMap(map, now, approval, bizObject);
                    //将子表中数据处理，随后更新至【库存 基础表】
                    BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.warehouse, data01, false);
                    model.setSequenceStatus(SequenceStatus.COMPLETED.name());
                    String id = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                    log.info("基础表-庫存基础更新成功:{}", id);

                }

                //调用存储过程


            } catch (Exception e) {
//                String matCode = (String) map.get("code");
//                String storageSpace = (String) map.get("storageSpace");
//
//                log.info("基础表-人员信息基础操作失败 matCode={},storageSpace={}", matCode, storageSpace);
//                log.info(e.getMessage(), e);
            }

        }
    }


    /**
     * 查询审批人,返回  员工号+姓名
     *
     * @param bizObject
     * @return
     */
    private String getFileBaseInfoWorkFlowApproval(BizObjectCreatedModel bizObject) {
        WorkflowInstanceModel instanceModel = getWorkflowInstanceFacade().getByObjectId(bizObject.getId());
        List<WorkItemModel> workItems = getWorkflowInstanceFacade().getWorkItems(instanceModel.getId(), true);
        final String finalActivityCode = "Activity9";
        Optional<WorkItemModel> first = workItems.stream().filter(a -> a.getActivityCode().equals(finalActivityCode)).findFirst();
        String participant = null;
        if (first.isPresent()) {
            WorkItemModel workItemModel = first.get();
            participant = workItemModel.getParticipant();
        }

        if (participant == null) {
            final String finalActivityCode2 = "Activity9";
            Optional<WorkItemModel> second = workItems.stream().filter(a -> a.getActivityCode().equals(finalActivityCode2)).findFirst();
            if (second.isPresent()) {
                WorkItemModel workItemModel = second.get();
                participant = workItemModel.getParticipant();
            }
        }


        if (participant == null) {
            participant = bizObject.getCreater().getId();
        }

        UserModel user = getOrganizationFacade().getUser(participant);


        return new StringBuilder(user.getEmployeeNo()).append("　").append(user.getName()).toString();
    }


    /**
     * 从主表的子表中获取数据，作为基础表的查询条件（调出公司、仓库等），用于减库存
     * @param data 审批表子表数据
     * @param bizObject  审批表主表数据
     * @return
     */
    private Map<String, Object> existsBizObject(Map data, BizObjectCreatedModel bizObject) {

        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.warehouse);

        String companyNumber = (String) bizObject.get("companycode");//获取主表数据 调出公司代码



//        String exchangeRateStr = data.get("exchangeRate") == null ? "0" : data.get("exchangeRate").toString();//获取子表数据 汇率
//        BigDecimal exchangeRate = "0E-8".equals(exchangeRateStr) ? new BigDecimal("0") : new BigDecimal(exchangeRateStr).setScale(6, BigDecimal.ROUND_HALF_UP);

        String warehouseNumberHidden = (String) bizObject.get("warehouseNumberHidden");//获取主表数据 调出仓库编码

        //TODO: 完善查询条件
        //data是审批表子表的每行数据，获取查询条件ShortText1669194395575
        String supplierCodeTxt = MapUtils.getString(data,"ShortText1669194395575","");//获取子表数据 供應商代碼
        String supplierCode = StringUtils.isBlank(supplierCodeTxt) ? "is null" : "= '"+supplierCodeTxt+"'";
        String supplierNameTxt = MapUtils.getString(data,"supplierName","");//获取子表数据 供應商名稱
        String supplierName = StringUtils.isBlank(supplierNameTxt) ? "is null" : "= '"+supplierNameTxt+"'";

        String storageSpaceTxt = MapUtils.getString(data,"storageSpace","");//获取子表数据 调出仓位
        String storageSpace = StringUtils.isBlank(storageSpaceTxt) ? "is null" : "= '"+storageSpaceTxt+"'";
        String code = (String) data.get("code");//获取子表数据 编码

        String modelTxt = MapUtils.getString(data,"model","");//获取子表数据 型号
//        String model = StringUtils.isBlank(modelTxt) ? "is null" : "'"+modelTxt+"'";
        String model = StringUtils.isBlank(modelTxt) ? "(model is null or model = '')" : "   replace(replace('"+modelTxt+"','/','') ,'\\\\','')  = replace(replace( model ,'/','' ),'\\\\','') ";

        String batchTxt = MapUtils.getString(data,"batch","");//获取子表数据 批次
        String batch = StringUtils.isBlank(batchTxt) ? "is null" : "= '"+batchTxt+"'";

        String abstractTxt = MapUtils.getString(data,"abstract","");//获取子表数据 摘要
        String abstractT = StringUtils.isBlank(abstractTxt) ? "(abstract is null or abstract = '')" : "   replace(replace('"+abstractTxt+"','/','') ,'\\\\','')  = replace(replace( abstract ,'/','' ),'\\\\','') ";

        String currencyHiddenTxt = MapUtils.getString(data,"currency","");//获取子表数据 币别
        String currencyHidden = StringUtils.isBlank(currencyHiddenTxt) ? "is null" : "= '"+currencyHiddenTxt+"'";

        String exchangeRateTxt = MapUtils.getString(data,"exchangeRate","");//获取子表数据 汇率
        String exchangeRate = StringUtils.isBlank(exchangeRateTxt) ? "is null" : "= '"+exchangeRateTxt+"'";

        Date productionDate = (Date) data.get("productionDate");//获取子表数据 生产日期
        String productionDateString = ObjectUtils.isEmpty(productionDate) ? "is null" : "= '"+productionDate.toString()+"'";

        Date expiryDate = (Date) data.get("expiryDate");//获取子表数据 到期日期
        String expiryDateString = ObjectUtils.isEmpty(expiryDate) ? "is null" : "= '"+expiryDate.toString()+"'";

        String measuringUnit = (String) data.get("measuringUnit");//获取子表数据 计量单位
        String auxiliaryUnitTxt = MapUtils.getString(data,"auxiliaryUnit","");//获取子表数据 辅助计量单位
        String auxiliaryUnit = StringUtils.isBlank(auxiliaryUnitTxt) ? "is null" : "= '"+auxiliaryUnitTxt+"'";

        //第一个入参data.get("数值类型的数据项编码")，第二个入参sql查询字段
        //例如传入data.get("unitPrice"), "price"，获得的返回值是：price = '1' 或者 price is null
        String priceSql = Utils.getPrettyNumberSqlString(data.get("unitPrice"), "price");//获取子表数据 单价
        String actualPriceSql = Utils.getPrettyNumberSqlString(data.get("actualPrice"), "actualPrice");//获取子表数据 现价


        String taxRateStr = data.get("taxRate") == null ? "0" : data.get("taxRate").toString();//获取子表数据 税率
//        BigDecimal taxRate = "0E-8".equals(taxRateStr) || "0".equals(taxRateStr) ? new BigDecimal("0") : new BigDecimal(taxRateStr).setScale(0, BigDecimal.ROUND_HALF_UP);
        String taxRateString = "0".equals(taxRateStr) ? "is null" : "= '"+new BigDecimal(taxRateStr).setScale(0, BigDecimal.ROUND_HALF_UP)+"'";



        String singleTaxStr = data.get("singleTax") == null ? "0" : data.get("singleTax").toString();//获取子表数据 单税
        BigDecimal singleTax = "0E-8".equals(singleTaxStr) ? new BigDecimal("0") : new BigDecimal(singleTaxStr).setScale(6, BigDecimal.ROUND_HALF_UP);

        String manufactureTxt = MapUtils.getString(data,"manufacture","");//获取子表数据 制造单位
        String manufacture = StringUtils.isBlank(manufactureTxt) ? "is null" : "= '"+manufactureTxt+"'";
        Date deadline = (Date) data.get("deadline");//获取子表数据 保修截止期
        String deadlineString = ObjectUtils.isEmpty(deadline) ? "is null" : "= '"+deadline.toString()+"'";

        String warrantyBillTxt = MapUtils.getString(data,"warrantyBillTxt","");//获取子表数据 保修单
        String warrantyBill = StringUtils.isBlank(warrantyBillTxt) ? "is null" : "= '"+warrantyBillTxt+"'";


        StringBuilder sql = new StringBuilder("select *  from ").append(tableName)
                .append(" where companyCode='").append(companyNumber).append("'")
//                .append(" and Supplier ").append(supplierCode)
                .append(" and supplierName ").append(supplierName)
//                .append(" and currency ").append(currencyHidden)
//                .append(" and currencyRate").append(exchangeRate)
                .append(" and warehouseCode='").append(warehouseNumberHidden).append("'")
//                .append(" and abstract ").append(abstractT)
                .append(" and  ").append(abstractT)
                .append(" and storageSpace ").append(storageSpace)
                .append(" and code='").append(code).append("'")
                .append(" and  ").append(model)
//
//                .append(" and  replace(replace(model,'/','') ,'\\\\','')  = replace(replace( ").append(model).append(" ,'/','') ,'\\\\','') " )
                .append(" and batch ").append(batch)
                .append(" and productionDate ").append(productionDateString)
                .append(" and expiryDate ").append(expiryDateString)
                .append(" and measuringUnit='").append(measuringUnit).append("'")
                .append(" and auxiliaryUnit ").append(auxiliaryUnit)
                .append(" and ").append(priceSql)
                .append(" and ").append(actualPriceSql)
                .append(" and taxRate ").append(taxRateString).append("")
//                .append(" and singleTax='").append(singleTax).append("'")
                .append(" and manufacture ").append(manufacture)
                .append(" and deadline ").append(deadlineString)
                .append(" and WarrantyBill ").append(warrantyBill);

        Map<String, Object> map = sqlService.getMap(sql.toString());

        return map;
    }

    /**
     * 从主表的子表中获取数据，作为基础表的查询条件（调入公司、仓库等），用于增库存
     * @param data 审批表子表数据
     * @param bizObject  审批表主表数据
     * @return
     */
    private Map<String, Object> existsBizObject01(Map data, BizObjectCreatedModel bizObject) {

        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.warehouse);

        String companyNumber = (String) bizObject.get("toCompanyCode");//获取主表数据 调入公司代码


//        String exchangeRateStr = data.get("exchangeRate") == null ? "0" : data.get("exchangeRate").toString();//获取子表数据 汇率
//        BigDecimal exchangeRate = "0E-8".equals(exchangeRateStr) ? new BigDecimal("0") : new BigDecimal(exchangeRateStr).setScale(6, BigDecimal.ROUND_HALF_UP);

        String warehouseNumberHidden = (String) bizObject.get("toWarehouseNumberHidden");//获取主表数据 调入仓库编码

        //TODO: 完善查询条件
        //data是审批表子表的每行数据，获取查询条件
        String storageSpaceTxt = MapUtils.getString(data,"toStorageSpace","");//获取子表数据 调入仓位
        String storageSpace = StringUtils.isBlank(storageSpaceTxt) ? "is null" : "= '"+storageSpaceTxt+"'";
        String code = (String) data.get("code");//获取子表数据 编码

        String modelTxt = MapUtils.getString(data,"model","");//获取子表数据 型号
//        String model = StringUtils.isBlank(modelTxt) ? "is null" : "'"+modelTxt+"'";
        String model = StringUtils.isBlank(modelTxt) ? "(model is null or model = '')" : "   replace(replace('"+modelTxt+"','/','') ,'\\\\','')  = replace(replace( model ,'/','' ),'\\\\','') ";


        String batchTxt = MapUtils.getString(data,"batch","");//获取子表数据 批次
        String batch = StringUtils.isBlank(batchTxt) ? "is null" : "= '"+batchTxt+"'";

        String supplierCodeTxt = MapUtils.getString(data,"ShortText1669194395575","");//获取子表数据 供應商代碼
        String supplierCode = StringUtils.isBlank(supplierCodeTxt) ? "is null" : "= '"+supplierCodeTxt+"'";
        String supplierNameTxt = MapUtils.getString(data,"supplierName","");//获取子表数据 供應商名稱
        String supplierName = StringUtils.isBlank(supplierNameTxt) ? "is null" : "= '"+supplierNameTxt+"'";

        String abstractTxt = MapUtils.getString(data,"abstract","");//获取子表数据 摘要
        String abstractT = StringUtils.isBlank(abstractTxt) ? "(abstract is null or abstract = '')" : "   replace(replace('"+abstractTxt+"','/','') ,'\\\\','')  = replace(replace( abstract ,'/','' ),'\\\\','') ";

        String currencyHiddenTxt = MapUtils.getString(data,"currency","");//获取子表数据 币别
        String currencyHidden = StringUtils.isBlank(currencyHiddenTxt) ? "is null" : "= '"+currencyHiddenTxt+"'";

        String exchangeRateTxt = MapUtils.getString(data,"exchangeRate","");//获取子表数据 汇率
        String exchangeRate = StringUtils.isBlank(exchangeRateTxt) ? "is null" : "= '"+exchangeRateTxt+"'";

        Date productionDate = (Date) data.get("productionDate");//获取子表数据 生产日期
        String productionDateString = ObjectUtils.isEmpty(productionDate) ? "is null" : "= '"+productionDate.toString()+"'";

        Date expiryDate = (Date) data.get("expiryDate");//获取子表数据 到期日期
        String expiryDateString = ObjectUtils.isEmpty(expiryDate) ? "is null" : "= '"+expiryDate.toString()+"'";

        String measuringUnit = (String) data.get("measuringUnit");//获取子表数据 计量单位
        String auxiliaryUnitTxt = MapUtils.getString(data,"auxiliaryUnit","");//获取子表数据 辅助计量单位
        String auxiliaryUnit = StringUtils.isBlank(auxiliaryUnitTxt) ? "is null" : "= '"+auxiliaryUnitTxt+"'";

        //第一个入参data.get("数值类型的数据项编码")，第二个入参sql查询字段
        //例如传入data.get("unitPrice"), "price"，获得的返回值是：price = '1' 或者 price is null
        String priceSql = Utils.getPrettyNumberSqlString(data.get("unitPrice"), "price");//获取子表数据 单价
        String actualPriceSql = Utils.getPrettyNumberSqlString(data.get("actualPrice"), "actualPrice");//获取子表数据 现价


        String taxRateStr = data.get("taxRate") == null ? "0" : data.get("taxRate").toString();//获取子表数据 税率
//        BigDecimal taxRate = "0E-8".equals(taxRateStr) || "0".equals(taxRateStr) ? new BigDecimal("0") : new BigDecimal(taxRateStr).setScale(0, BigDecimal.ROUND_HALF_UP);
        String taxRateString = "0".equals(taxRateStr) ? "is null" : "= '"+new BigDecimal(taxRateStr).setScale(0, BigDecimal.ROUND_HALF_UP)+"'";



        String singleTaxStr = data.get("singleTax") == null ? "0" : data.get("singleTax").toString();//获取子表数据 单税
        BigDecimal singleTax = "0E-8".equals(singleTaxStr) ? new BigDecimal("0") : new BigDecimal(singleTaxStr).setScale(6, BigDecimal.ROUND_HALF_UP);

        String manufactureTxt = MapUtils.getString(data,"manufacture","");//获取子表数据 制造单位
        String manufacture = StringUtils.isBlank(manufactureTxt) ? "is null" : "= '"+manufactureTxt+"'";
        Date deadline = (Date) data.get("deadline");//获取子表数据 保修截止期
        String deadlineString = ObjectUtils.isEmpty(deadline) ? "is null" : "= '"+deadline.toString()+"'";

        String warrantyBillTxt = MapUtils.getString(data,"warrantyBillTxt","");//获取子表数据 保修单
        String warrantyBill = StringUtils.isBlank(warrantyBillTxt) ? "is null" : "= '"+warrantyBillTxt+"'";


        StringBuilder sql = new StringBuilder("select *  from ").append(tableName)
                .append(" where companyCode='").append(companyNumber).append("'")
//                .append(" and Supplier ").append(supplierCode)
                .append(" and supplierName ").append(supplierName)
//                .append(" and currency ").append(currencyHidden)
//                .append(" and currencyRate").append(exchangeRate)
                .append(" and warehouseCode='").append(warehouseNumberHidden).append("'")
//                .append(" and abstract ").append(abstractT)
                .append(" and  ").append(abstractT)
                .append(" and storageSpace ").append(storageSpace)
                .append(" and code='").append(code).append("'")
                .append(" and  ").append(model)
//
//                .append(" and  replace(replace(model,'/','') ,'\\\\','')  = replace(replace( ").append(model).append(" ,'/','') ,'\\\\','') " )
                .append(" and batch ").append(batch)
                .append(" and productionDate ").append(productionDateString)
                .append(" and expiryDate ").append(expiryDateString)
                .append(" and measuringUnit='").append(measuringUnit).append("'")
                .append(" and auxiliaryUnit ").append(auxiliaryUnit)
                .append(" and ").append(priceSql)
                .append(" and ").append(actualPriceSql)
                .append(" and taxRate ").append(taxRateString).append("")
//                .append(" and singleTax='").append(singleTax).append("'")
                .append(" and manufacture ").append(manufacture)
                .append(" and deadline ").append(deadlineString)
                .append(" and WarrantyBill ").append(warrantyBill);

        Map<String, Object> map = sqlService.getMap(sql.toString());

        return map;
    }

    /**
     * 转换成  库存 的数据
     *
     * @param map
     * @param auditDate
     * @param bizObject
     * @return
     */
    private Map<String, Object> fileInfoBaseMap(Map<String, Object> map, String auditDate, String auditer, BizObjectCreatedModel bizObject) {
        Map<String, Object> data = new HashMap<>();
        //TODO: 将子表数据放到data中，后续作为新增基础表的数据使用，待补充


        Map<String,Object> company = (Map<String, Object>) bizObject.get("toCompany");
        String companyId = (String) company.get("id");
        //公司id
        data.put("company", companyId);
        //公司代码
        data.put("companyCode", bizObject.get("toCompanyCode"));
        //公司名称
        data.put("companyName", bizObject.get("toCompanyName"));

        Map<String,Object> warehouse = (Map<String, Object>) bizObject.get("toWarehouseNumber");
        String warehouseId = (String) warehouse.get("id");
        //仓库id
        data.put("warehouse", warehouseId);
        //仓库代码
        data.put("warehouseCode", bizObject.get("toWarehouseNumberHidden"));
        //仓库名称
        data.put("wasrhouse", bizObject.get("toWarehouseName"));

        //仓位
        data.put("storageSpace", map.get("toStorageSpace"));

//        Map<String,Object> Supplier = (Map<String, Object>) bizObject.get("Supplier");
//        String SupplierId = (String) Supplier.get("id");

        //供应商id
        data.put("supplierGuanlian", map.get("supplierGuanlian"));
        //供应商代码
        data.put("Supplier", map.get("ShortText1669194395575"));
        //供应商名称
        data.put("supplierName", map.get("supplierName"));


        //物料id
        data.put("material", map.get("RelevanceMaterial"));
        //物料代码
        data.put("Code", map.get("code"));
        //物料名称
        data.put("materialName", map.get("materialName"));

        //型号示例
        data.put("modelExample", map.get("modelExample"));
        //型号
        data.put("model", map.get("model"));
        //摘要
        data.put("abstract", map.get("abstract"));

        //币别
        data.put("currency", map.get("currency"));
        //汇率
        data.put("currencyRate", map.get("exchangeRate"));

        //批次
        data.put("batch", map.get("batch"));
        //生产日期
        data.put("productionDate", map.get("productionDate"));
        //到期日期
        data.put("expiryDate", map.get("expiryDate"));

        //计量单位
        data.put("measuringUnit", map.get("measuringUnit"));
        //数量
        data.put("quantity", map.get("quantity"));
        //辅助计量单位
        data.put("auxiliaryUnit", map.get("auxiliaryUnit"));
        //辅助数量
        data.put("auxiliaryQuantity", map.get("diaoboAssQuantity"));

        //单价
        data.put("price", map.get("unitPrice"));
        //现价
        data.put("actualPrice", map.get("actualPrice"));
        //税率
        data.put("taxRate", map.get("taxRate"));

        //金额
        data.put("amount", map.get("amount"));
        //现值
        data.put("actualAmount", map.get("actualAmount"));
        //税额
        data.put("tax", map.get("tax"));

        //单税
        data.put("singleTax", map.get("singleTax"));
        //含税单价
        data.put("priceInclTax", map.get("priceInclTax"));
        //价税合计
        data.put("total", map.get("total"));

        //制造单位
        data.put("manufacture", map.get("manufacture"));
        //保修截止期
        data.put("deadline", map.get("deadline"));
        //保修单
        data.put("WarrantyBill", map.get("warrantyBillTxt"));











        return data;
    }

}




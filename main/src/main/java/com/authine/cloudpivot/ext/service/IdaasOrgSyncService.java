package com.authine.cloudpivot.ext.service;

import com.alibaba.fastjson.JSONObject;

/**
 * @Description
 * @Author fj
 * @Date 2022/05/16
 */
public interface IdaasOrgSyncService {

    void userAdd(JSONObject requestBody) throws Exception;

    void userUpdate(JSONObject requestBody) throws Exception;

    void userDelete(String id,String mainCorpId);

    void departmentAdd(JSONObject requestBody, String clientId) throws Exception;

    void departmentUpdate(JSONObject requestBody, String clientId) throws Exception;

    void departmentDelete(String id);
}

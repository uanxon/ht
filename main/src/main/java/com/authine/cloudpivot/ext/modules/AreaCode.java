package com.authine.cloudpivot.ext.modules;

import com.authine.cloudpivot.engine.enums.status.SequenceStatus;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum AreaCode {
    DaZHou("大洲", "DaZhou"),
    GuoJia("国家", "GuoJia"),
    QuYu("区域", "QuYu"),
    ShengFen("省份", "ShengFen"),
    ChengShi("城市", "ChengShi"),
    QuXian("区县", "QuXian");

    private String name;
    private String code;

    private AreaCode(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public String getCode() {
        return code;
    }

    public static String getCode(String name) {
        AreaCode[] var1 = values();
        int var2 = var1.length;

        for (int var3 = 0; var3 < var2; ++var3) {
            AreaCode status = var1[var3];
            if (status.getName().equals(name)) {
                return status.code;
            }
        }

        return null;
    }


//    DRAFT("草稿", 0),
//    PROCESSING("进行中", 1),
//    CANCELLED("已取消", 2),
//    COMPLETED("已完成", 3),
//    CANCELED("已作废", 4);
//
//    private final String name;
//    private final int index;
//
//    private AreaCode(String name, int index) {
//        this.name = name;
//        this.index = index;
//    }

}
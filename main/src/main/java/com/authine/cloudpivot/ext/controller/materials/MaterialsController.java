package com.authine.cloudpivot.ext.controller.materials;

import com.authine.cloudpivot.engine.api.model.organization.UserModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.engine.api.model.runtime.CommonCommentModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkflowInstanceModel;
import com.authine.cloudpivot.engine.enums.status.SequenceStatus;
import com.authine.cloudpivot.ext.Utils.CustomSchemaCode;
import com.authine.cloudpivot.ext.Utils.Utils;
import com.authine.cloudpivot.ext.service.CloudSqlService;
import com.authine.cloudpivot.web.api.controller.base.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @author fengjie
 * @version 1.0.0
 * @ClassName MaterialsController
 * @Description 物料信息记录
 * @createTime 2023/2/2 8:43
 */
@RestController
@RequestMapping("/public/materials")
@Slf4j
public class MaterialsController extends BaseController {
    @Autowired
    CloudSqlService sqlService;

    /**
     * 【总公司物料记录 基础信息】审批完成后，数据同步到【总公司物料信息记录 基础表】
     */
    @RequestMapping("finish")
    public void finish(String bizId) {
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.HEAD_OFFICE_MATERIALS_BA, bizId);

        //相关档案明细表
        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get(CustomSchemaCode.HEAD_OFFICE_MATERIALS_DETAIL_BA);

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        String approval = getFileBaseInfoWorkFlowApproval(bizObject);

        for (Map<String, Object> map : list) {
            try {
                //将子表中数据处理，随后更新至【总公司物料信息记录 基础表】
//                Map<String, Object> data = fileInfoBaseMap(map, now, approval);
                Map<String, Object> data = Utils.fileInfoBaseMap(map, approval);
                //判断是否已存在
                String id = existsBizObject(data);
                if (StringUtils.isNotEmpty(id)) {
                    data.put("id", id);
                }
                BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.HEAD_OFFICE_MATERIALS, data, false);
                model.setSequenceStatus(SequenceStatus.COMPLETED.name());
                id = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                log.info("总公司物料信息记录 基础表数据新增成功");

                //调用存储过程
                callProcess(id);
            } catch (Exception e) {
                log.info("总公司物料信息记录 基础表数据维护失败，报错信息：{}", e.getMessage());
                log.info("总公司物料信息记录 基础表数据操作失败 物料编码={}", map.get("fullCodeNumber"));
            }
        }
    }

    /**
     * 更新总公司物料信息-规范信息
     */
    @RequestMapping("updateHeadOfficeStandard")
    public void updateHeadOfficeStandard(String bizId) {
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.HEAD_OFFICE_MATERIALS_ST, bizId);

        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get(CustomSchemaCode.HEAD_OFFICE_MATERIALS_DETAIL_ST);

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String approval = getFileBaseInfoWorkFlowApproval(bizObject);
        String tableName = this.getBizObjectFacade().getTableName(CustomSchemaCode.HEAD_OFFICE_MATERIALS);

        for (Map<String, Object> map : list) {
            try {
                //修改规范信息
                String fullCodeNumber = MapUtils.getString(map, "fullCodeNumber", "");
                String headOfficeStandard = MapUtils.getString(map, "headOfficeStandard", "");

                StringBuilder sql = new StringBuilder();
                sql.append("update ").append(tableName)
                        .append(" set headOfficeStandard = '").append(headOfficeStandard).append("'")
                        .append(",auditDate = '").append(now).append("'")
                        .append(",approver = '").append(approval).append("'")
                        .append(" where fullCodeNumber = '").append(fullCodeNumber).append("'");
                sqlService.update(sql.toString());
                log.info("总公司物料信息记录 规范信息数据修改成功");

                //调用存储过程
                callProcessToStandard(fullCodeNumber, headOfficeStandard);
            } catch (Exception e) {
                log.info("总公司物料信息记录 规范信息数据维护失败，报错信息：{}", e.getMessage());
                log.info("总公司物料信息记录 规范信息操作失败 物料编码={}", map.get("fullCodeNumber"));
            }
        }
    }

    /**
     * 更新总公司物料信息-采购信息
     */
    @RequestMapping("updateHeadOfficePurchase")
    public void updateHeadOfficePurchase(String bizId) {
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.HEAD_OFFICE_MATERIALS_PU, bizId);

        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get(CustomSchemaCode.HEAD_OFFICE_MATERIALS_DETAIL_PU);

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String approval = getFileBaseInfoWorkFlowApproval(bizObject);
        String tableName = this.getBizObjectFacade().getTableName(CustomSchemaCode.HEAD_OFFICE_MATERIALS);

        for (Map<String, Object> map : list) {
            try {
                //修改采购信息
                String fullCodeNumber = MapUtils.getString(map, "fullCodeNumber", "");
                String headOfficePurchase = MapUtils.getString(map, "headOfficePurchase", "");

                StringBuilder sql = new StringBuilder();
                sql.append("update ").append(tableName)
                        .append(" set headOfficePurchase = '").append(headOfficePurchase).append("'")
                        .append(",auditDate = '").append(now).append("'")
                        .append(",approver = '").append(approval).append("'")
                        .append(" where fullCodeNumber = '").append(fullCodeNumber).append("'");
                sqlService.update(sql.toString());
                log.info("总公司物料信息记录 采购信息数据修改成功");

                //调用存储过程
                callProcessToPurchase(fullCodeNumber, headOfficePurchase);
            } catch (Exception e) {
                log.info("总公司物料信息记录 采购信息数据维护失败，报错信息：{}", e.getMessage());
                log.info("总公司物料信息记录 采购信息操作失败 物料编码={}", map.get("fullCodeNumber"));
            }
        }
    }

    /**
     * 更新总公司物料信息-销售信息
     */
    @RequestMapping("updateHeadOfficeSale")
    public void updateHeadOfficeSale(String bizId) {
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.HEAD_OFFICE_MATERIALS_SA, bizId);


        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get(CustomSchemaCode.HEAD_OFFICE_MATERIALS_DETAIL_SA);

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String approval = getFileBaseInfoWorkFlowApproval(bizObject);
        String tableName = this.getBizObjectFacade().getTableName(CustomSchemaCode.HEAD_OFFICE_MATERIALS);

        for (Map<String, Object> map : list) {
            try {
                //修改采购信息
                String fullCodeNumber = MapUtils.getString(map, "fullCodeNumber", "");
                String headOfficeSales = MapUtils.getString(map, "headOfficeSales", "");

                StringBuilder sql = new StringBuilder();
                sql.append("update ").append(tableName)
                        .append(" set headOfficeSales = '").append(headOfficeSales).append("'")
                        .append(",auditDate = '").append(now).append("'")
                        .append(",approver = '").append(approval).append("'")
                        .append(" where fullCodeNumber = '").append(fullCodeNumber).append("'");
                sqlService.update(sql.toString());
                log.info("总公司物料信息记录 销售信息数据修改成功");

                //调用存储过程
                callProcessToSale(fullCodeNumber, headOfficeSales);
            } catch (Exception e) {
                log.info("总公司物料信息记录 销售信息数据维护失败，报错信息：{}", e.getMessage());
                log.info("总公司物料信息记录 销售信息操作失败 物料编码={}", map.get("fullCodeNumber"));
            }
        }
    }

    @RequestMapping("toVoid")
    public void toVoid(String bizId) {
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.HEAD_OFFICE_MATERIALS_VO, bizId);
        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get(CustomSchemaCode.HEAD_OFFICE_MATERIALS_DETAIL_VO);

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        //获取审批人
        String approval = getFileBaseInfoWorkFlowApproval(bizObject);

        for (Map<String, Object> map : list) {
            try {
                //获取数据
                Map<String, Object> data = fileInfoBaseMap(map, now, approval);
                //判断是否已存在
                String id = existsBizObject(data);
                if (StringUtils.isEmpty(id)) {
                    //判断是否已存在，如果不存在，放弃这条数据，直接跳过此次循环
                    continue;
                } else if (StringUtils.isNotEmpty(id)) {
                    data.put("id", id);
                }

                BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.HEAD_OFFICE_MATERIALS, data, false);
                model.setSequenceStatus(SequenceStatus.CANCELED.name());
                id = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                log.info("总公司物料信息记录 基础表作废操作成功");
                //调用存储过程
                callProcessToVoid(id);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                log.info("总公司物料信息记录 基础表作废操作失败 物料编码={}", map.get("fullCodeNumber"));
            }
        }
    }


    /**
     * 调用存储过程
     * ------总公司物料
     * exec   SyncCorpMaterial
     */
    private void callProcess(String bizId) {

        if (StringUtils.isEmpty(bizId)) {
            return;
        }
        //编写sql
        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.HEAD_OFFICE_MATERIALS);
        StringBuilder sql = new StringBuilder("SELECT * from ")
                .append(tableName).append(" where id ='")
                .append(bizId).append("';");
        //查询到入参
        Map<String, Object> map = sqlService.getMap(sql.toString());

        log.info("入参map={}", map);

        //调用存储过程
        String fullCodeNumber = MapUtils.getString(map, "fullCodeNumber", "");//物料编码
        String sinopecName = MapUtils.getString(map, "sinopecName", "");//中石化名称
        String materialName = MapUtils.getString(map, "materialName", "");//物料名称
        String materialAlias = MapUtils.getString(map, "materialAlias", "");//别名
        String materialForeignName = MapUtils.getString(map, "materialForeignName", "");//外文名称
        String modelExample = MapUtils.getString(map, "modelExample", "");//型号示例
        String mnemonicCode = MapUtils.getString(map, "mnemonicCode", "");//助记码
        Integer unitPricePrecision = MapUtils.getInteger(map, "unitPricePrecision", 0);//单价精度
        String baseMeasureUnitName = MapUtils.getString(map, "baseMeasureUnitName", "");//基本计量单位名称
        String assistMeasureUnitName = MapUtils.getString(map, "assistMeasureUnitName", "");//辅助计量单位名称
        String materialStatus = MapUtils.getString(map, "materialStatus", "");//状态
        String statusNumber;
        switch (materialStatus) {
            case "未核准":
                statusNumber = "0";
                break;
            case "禁用":
                statusNumber = "1";
                break;
            case "核准":
                statusNumber = "2";
                break;
            default:
                statusNumber = "1";
                break;
        }
        String company = MapUtils.getString(map, "company", "");//主责公司FID
        String responsibleDeptFid = MapUtils.getString(map, "responsibleDeptFid", "");//主责部门FID
        String postNumber = MapUtils.getString(map, "postNumber", "");//主责岗位代码
        String responsiblePersonName = MapUtils.getString(map, "responsiblePersonName", "");//主责人
        String image = MapUtils.getString(map, "image", "");//图片
        String approver = MapUtils.getString(map, "approver", "");//审批人


        String execSql = String.format("exec [HG_LINK].[hg].[dbo].SyncCorpMaterial '%s','%s','%s','%s','%s','%s','%s','%d','%s','%s','%s','%s','%s','%s','%s','%s','%s'",
                fullCodeNumber,sinopecName, materialName, materialAlias, materialForeignName, modelExample, mnemonicCode, unitPricePrecision,
                baseMeasureUnitName, assistMeasureUnitName, statusNumber, company, responsibleDeptFid, postNumber, responsiblePersonName, image, approver);

        log.info("\n==========准备调用存储过程:{}", execSql);

        sqlService.execute(CloudSqlService.htEas, execSql);

        log.info("\n=============存储过程执行完成");

    }

    /**
     * 调用存储过程
     * ------总公司物料信息-规范信息
     * exec   SyncCorpMatStandard
     */
    private void callProcessToStandard(String fullCodeNumber, String headOfficeStandard) {
        String execSql = String.format("exec [HG_LINK].[hg].[dbo].SyncCorpMatStandard '%s','%s'",
                fullCodeNumber, headOfficeStandard);

        log.info("\n==========准备调用存储过程:{}", execSql);
        sqlService.execute(CloudSqlService.htEas, execSql);
        log.info("\n=============存储过程执行完成");
    }

    /**
     * 调用存储过程
     * ------总公司物料信息-采购信息
     * exec   SyncCorpMatPurchase
     */
    private void callProcessToPurchase(String fullCodeNumber, String headOfficePurchase) {
        String execSql = String.format("exec [HG_LINK].[hg].[dbo].SyncCorpMatPurchase '%s','%s'",
                fullCodeNumber, headOfficePurchase);

        log.info("\n==========准备调用存储过程:{}", execSql);
        sqlService.execute(CloudSqlService.htEas, execSql);
        log.info("\n=============存储过程执行完成");
    }

    /**
     * 调用存储过程
     * ------总公司物料信息-销售信息
     * exec   SyncCorpMatSale
     */
    private void callProcessToSale(String fullCodeNumber, String headOfficeSales) {
        String execSql = String.format("exec [HG_LINK].[hg].[dbo].SyncCorpMatSale '%s','%s'",
                fullCodeNumber, headOfficeSales);

        log.info("\n==========准备调用存储过程:{}", execSql);
        sqlService.execute(CloudSqlService.htEas, execSql);
        log.info("\n=============存储过程执行完成");
    }

    /**
     * 调用作废存储过程
     * --------总公司物料信息记录 作废
     * exec    CorpMaterialDisAble
     */
    private void callProcessToVoid(String bizId) {

        if (StringUtils.isEmpty(bizId)) {
            return;
        }
        //编写sql
        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.HEAD_OFFICE_MATERIALS);
        StringBuilder sql = new StringBuilder("SELECT * from ")
                .append(tableName).append(" where id ='")
                .append(bizId).append("';");
        //查询到入参
        Map<String, Object> map = sqlService.getMap(sql.toString());

        log.info("入参map={}", map);

        //调用存储过程
        String fullCodeNumber = MapUtils.getString(map, "fullCodeNumber", "");//公司fid

        String execSql = String.format("exec [HG_LINK].[hg].[dbo].CorpMaterialDisAble '%s'",
                fullCodeNumber);

        log.info("\n==========准备调用作废存储过程:{}", execSql);

        sqlService.execute(CloudSqlService.htEas, execSql);

        log.info("\n=============作废存储过程执行完成");

    }

    /**
     * 查询审批人,返回  员工号+姓名
     *
     * @param bizObject
     * @return
     */
    private String getFileBaseInfoWorkFlowApproval(BizObjectCreatedModel bizObject) {
        WorkflowInstanceModel instanceModel = getWorkflowInstanceFacade().getByObjectId(bizObject.getId());
        List<WorkItemModel> workItems = getWorkflowInstanceFacade().getWorkItems(instanceModel.getId(), true);
        final String finalActivityCode = "Activity25";
        Optional<WorkItemModel> first = workItems.stream().filter(a -> a.getActivityCode().equals(finalActivityCode)).findFirst();
        String participant = null;
        if (first.isPresent()) {
            WorkItemModel workItemModel = first.get();
            participant = workItemModel.getParticipant();
        }
        if (CustomSchemaCode.HEAD_OFFICE_MATERIALS_DETAIL_PU.equals(bizObject.getSchemaCode()) || CustomSchemaCode.HEAD_OFFICE_MATERIALS_DETAIL_SA.equals(bizObject.getSchemaCode())) {
            if (participant == null) {
                final String finalActivityCode2 = "Activity22";
                Optional<WorkItemModel> second = workItems.stream().filter(a -> a.getActivityCode().equals(finalActivityCode2)).findFirst();
                if (second.isPresent()) {
                    WorkItemModel workItemModel = second.get();
                    participant = workItemModel.getParticipant();
                }
            }
        }
        if (participant == null) {
            final String finalActivityCode2 = "Activity21";
            Optional<WorkItemModel> second = workItems.stream().filter(a -> a.getActivityCode().equals(finalActivityCode2)).findFirst();
            if (second.isPresent()) {
                WorkItemModel workItemModel = second.get();
                participant = workItemModel.getParticipant();
            }
        }

        if (participant == null) {
            final String finalActivityCode2 = "Activity18";
            Optional<WorkItemModel> second = workItems.stream().filter(a -> a.getActivityCode().equals(finalActivityCode2)).findFirst();
            if (second.isPresent()) {
                WorkItemModel workItemModel = second.get();
                participant = workItemModel.getParticipant();
            }
        }

        if (participant == null) {
            participant = bizObject.getCreater().getId();
        }

        UserModel user = getOrganizationFacade().getUser(participant);


        return new StringBuilder(user.getEmployeeNo()).append("　").append(user.getName()).toString();
    }


    /**
     * 判断是否已存在
     *
     * @return
     */
    private String existsBizObject(Map data) {

        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.HEAD_OFFICE_MATERIALS);

        StringBuilder sql = new StringBuilder("select id  from ").append(tableName)
                .append(" where fullCodeNumber='").append(data.get("fullCodeNumber")).append("'")
                .append(";");
        Map<String, Object> map = sqlService.getMap(sql.toString());
        return (String) map.get("id");
    }

    /**
     * 转换成  基础表-总公司档案 的数据
     *
     * @param map
     * @param auditDate
     * @return
     */
    private Map<String, Object> fileInfoBaseMap(Map<String, Object> map, String auditDate, String auditer) {
        Map<String, Object> data = new HashMap<>();
        List<String> keys = Arrays.asList("fullCodeNumber", "sinopecName", "materialName", "materialAlias", "materialForeignName", "modelExample", "mnemonicCode",
                "unitPricePrecision", "unitPrecision", "assistUnitPrecision", "materialStatus", "companyNumber", "responsibleDeptFNumber",
                "responsibleDeptName","postNumber","postName","responsiblePersonName","image", "baseMeasureUnitName", "assistMeasureUnitName");
        // 子表id
        data.put("childTableID", map.get("id"));
        keys.forEach(key -> data.put(key, map.get(key)));
        List<String> relationKeys = Arrays.asList("baseMeasureUnit", "assistMeasureUnit", "company", "responsibleDeptFid", "relevancePost");
        relationKeys.forEach(key -> {
            Map<String,Object> relationMap = (Map<String, Object>) map.get(key);
            data.put(key, relationMap.get("id"));
        });
        //审批时间和审批人
        data.put("auditDate", auditDate);
        data.put("approver", auditer);
        return data;
    }
}

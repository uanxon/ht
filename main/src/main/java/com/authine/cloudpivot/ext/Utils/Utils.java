package com.authine.cloudpivot.ext.Utils;

import cn.hutool.core.map.MapBuilder;
import com.authine.cloudpivot.engine.api.facade.WorkflowInstanceFacade;
import com.authine.cloudpivot.engine.api.model.organization.UserModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkflowInstanceModel;
import org.apache.commons.collections4.MapUtils;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.*;

/**
 * @Author hxd
 * @Date 2022/8/9 9:01
 * @Description
 **/
public class Utils {


//    public static void main(String[] args) {
//        RestTemplate restTemplate = new RestTemplate();
//
//        //=======
//        MapBuilder<String, Object> mapBuilder = MapBuilder.create(new HashMap<String, Object>())
//                .put("corpId", "ding319f3fbb53f6c2cc35c2f4657eb6378f")
//                .put("username", "admin")
//                .put("password", "aozhe1234")
//                .put("url", "http://112.74.108.173/api/login?redirect_uri=http%3A%2F%2F112.74.108.173%2Fapi%2Foauth%2Fauthorize%3Fclient_id%3Dapi%26response_type%3Dcode%26scope%3Dread%26redirect_uri%3Dhttp%3A%2F%2F112.74.108.173%2Foauth");
//
//
//        Map result = restTemplate.postForObject("http://112.74.108.173/api/login/Authentication/get_code", mapBuilder.build(), Map.class);
//        System.out.println(result);
//        String code = MapUtils.getString(result, "code");
//        System.out.println(code);
//        mapBuilder.map().clear();
//        mapBuilder.put("url","http://112.74.108.173/api")
//         .put("code",code)
//         .put("client_secret","c31b32364ce19ca8fcd150a417ecce58")
//         .put("client_id","api")
//         .put("redirect_uri", "http://112.74.108.173/oauth");
//
//        result = restTemplate.getForObject("http://112.74.108.173/api/login/Authentication/get_token", Map.class, mapBuilder.build());
//
//        System.out.println(result);
//    }


    public static Map<String, Object> fileInfoBaseMap(Map<String, Object> map, String approval) {
        //审批人,审批时间
        map.put("approval", approval);
        map.put("flow_time", new Date());
        map.remove("rowStatus");
        map.remove("id");
        return map;
    }

    public static BigDecimal getPrettyNumber(String dec) {
        BigDecimal decimal = new BigDecimal(BigDecimal.valueOf(Double.parseDouble(dec)).stripTrailingZeros().toPlainString());
        return decimal;
    }

    /**
     * 第一个入参data.get("数值类型的数据项编码")，第二个入参sql查询字段
     * 例如传入data.get("unitPrice"), "price"，获得的返回值是：price = '1' 或者 price is null
     *
     * @param object 例如传入：data.get("quantity")
     * @param key  sql的查询字段
     * @return  例如：price = '0' 或者 price is null
     */
    public static String getPrettyNumberSqlString(Object object, String key) {
        BigDecimal bigDecimal = (BigDecimal) object;
        StringBuffer sql = new StringBuffer();
        if (bigDecimal == null) {
            //查询条件应该是 price is null
            sql.append(" ").append(key).append(" is null ");
        } else {
            //当data.get("unitPrice")是0时会取出0E-8，如下处理
            String s = bigDecimal.stripTrailingZeros().toString();
            sql.append(" ").append(key).append(" = '").append(s).append("' ");
        }
        return sql.toString();
    }


}

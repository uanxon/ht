package com.authine.cloudpivot.ext.controller;

import com.authine.cloudpivot.engine.api.model.organization.UserModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.engine.api.model.runtime.SelectionValue;
import com.authine.cloudpivot.engine.enums.type.UnitType;
import com.authine.cloudpivot.ext.Utils.AccountAuthorize;
import com.authine.cloudpivot.ext.Utils.CustomSchemaCode;
import com.authine.cloudpivot.ext.Utils.DocAPI;
import com.authine.cloudpivot.ext.Utils.RoleAuth;
import com.authine.cloudpivot.ext.service.CloudSqlService;
import com.authine.cloudpivot.ext.service.RoleAuthService;
import com.authine.cloudpivot.web.api.controller.runtime.WorkflowInstanceRuntimeController;
import com.authine.cloudpivot.web.api.view.ResponseResult;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.axis.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @Author hxd
 * @Date 2022/8/8 11:20
 * @Description
 **/
@RequestMapping("/api/custom")
@RestController
@Slf4j
@Component
public class  AttributeController extends WorkflowInstanceRuntimeController {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private CloudSqlService sqlService;

    @Autowired
    private RoleAuthService roleAuthService;

    @Autowired
    private FileCodeSortController fileCodeSortController;
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @RequestMapping("validAttribute")
    public ResponseResult validAttribute(@RequestBody Map<String, Object> map) {

        List<String> ids = (List<String>) map.get("ids");

        if (CollectionUtils.isEmpty(ids)) {
            return getOkResponseResult("无ids参数");
        }

        String url = "http://47.104.86.126:188/app/file/find-file-by-attribute";
        String token = "c7176c48be36edad213c13032364dcbb20760fcd14bd372d726602495279f6fd18b033";

        StringBuilder resp = new StringBuilder();


        for (int i = 0; i < ids.size(); i++) {

            String id = ids.get(i);
            if (StringUtils.isEmpty(id)) {
                continue;
            }

            Map<String, Object> result = getAttribute(url, token, id);
            int code = MapUtils.getIntValue(result, "code", -1);
            int num = MapUtils.getIntValue(result, "num", 0);
            if (code == 0 && num > 0) {
                resp.append(String.format("第%d行,属性已存在,id=%s,\n", i + 1, id));
            } else if (code != 0) {
                String msg = MapUtils.getString(result, "msg", "");
                resp.append(String.format("第%d行,调用校验接口失败:%s,\n", i + 1, msg));
            }
        }

        if (resp.length() == 0) {
            return getOkResponseResult("无重复");
        }

        return getErrResponseResult(-1L, resp.toString());
    }

    private Map<String, Object> getAttribute(String url, String token, String attribute) {
        RestTemplate restTemplate = new RestTemplate();


        String format = String.format("token=%s&attribute=%s&system=web", token, attribute);

        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity entity = new HttpEntity(format, headers);

        Map<String, Object> result = restTemplate.postForObject(url, entity, Map.class);
        log.info("{},响应结果:{}", attribute, result);
        return result;
    }


    @PostMapping("validSubmit")
    public ResponseResult validSubmit(@RequestBody Map<String, Object> map) {

        //校验参数
        List<Map<String, Object>> list = (List<Map<String, Object>>) map.get("list");
        StringBuilder validList = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> obj = list.get(i);

            if (!obj.containsKey("MainDeptId")) {
                validList.append(String.format("第%d行,主责部门id 不能为空,", i + 1));
            }
        }
        if (validList.length() > 0) {
            return getErrResponseResult(-1L, validList.toString());
        }
        String appEmp = (String) map.get("employeeNo");
        if (StringUtils.isEmpty(appEmp)) {
            return getErrResponseResult(-1L, "工号不能为空");
        }


        //校验是否上传档案文件
//        ResponseResult responseResult = fileIsExist(list, appEmp);
//
//        if (!"0".equals(responseResult.getErrcode())) {
//            return responseResult;
//        }

        return getOkResponseResult("成功");
    }

    @PostMapping("getQEO")
    public ResponseResult<Map<String, Object>> getQEO(@RequestBody Map<String, Object> param) {
        String companyNumber = MapUtils.getString(param, "companyNumber", "");
        String archivesFNumber = MapUtils.getString(param, "archivesFNumber", "");
        String tableName = this.getBizObjectFacade().getTableName(CustomSchemaCode.WORK_RELATED_FILES_RECORD);
        List<String> numberList = Arrays.asList(archivesFNumber.split(","));

        String sql = "select id,fileQ,fileE,fileO from " + tableName + " where companyNumber = :companyNumber and archivesFNumber in (:numberList)";
        Map<String, Object> sqlParam = Maps.newHashMap();
        sqlParam.put("companyNumber", companyNumber);
        sqlParam.put("numberList", numberList);
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql, sqlParam);

        Map<String, Object> resultMap = Maps.newHashMap();
        resultMap.put("Q", 0);
        resultMap.put("E", 0);
        resultMap.put("O", 0);
        if (CollectionUtils.isNotEmpty(list)) {
            for (Map<String, Object> map : list) {
                if ("√".equals(MapUtils.getString(map, "fileQ", ""))) {
                    resultMap.put("Q", 1);
                }
                if ("√".equals(MapUtils.getString(map, "fileE", ""))) {
                    resultMap.put("E", 1);
                }
                if ("√".equals(MapUtils.getString(map, "fileO", ""))) {
                    resultMap.put("O", 1);
                }
            }
        }
        return this.getOkResponseResult(resultMap, "成功");
    }

    public static int getCharacterPosition(String string, int i, String character) {
        //这里是获取"-"符号的位置
        // Matcher slashMatcher = Pattern.compile("/").matcher(string);
        java.util.regex.Matcher slashMatcher = Pattern.compile(character).matcher(string);
        int mIdx = 0;
        while (slashMatcher.find()) {
            mIdx++;
            //当"/"符号第三次出现的位置
            if (mIdx == i) {
                break;
            }
        }
        return slashMatcher.start();
    }

    public static String md5(String plainText) {
        byte[] secretBytes = null;
        try {
            secretBytes = MessageDigest.getInstance("md5").digest(
                    plainText.getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("没有这个md5算法！");
        }
        String md5code = new BigInteger(1, secretBytes).toString(16);
        for (int i = 0; i < 32 - md5code.length(); i++) {
            md5code = "0" + md5code;
        }
        return md5code;
    }


    @GetMapping("finish")
    public ResponseResult finish(String id) {
        String FolderAttribute = "";
        String FolderAttr = "";
        String mainComCode = "";

        String prefix = "";
        String elec = "";
        String PosMainName = "";
        String PosAName = "";
        String PosBName = "";
        String fileid = "";
        String ParentFolderAttr = "";
        String addfileid = "";
        String delId = "";
        String foldername = "";
        String AuthFileId = "";
        String AppEmpId = "";
        int SubItemORNot = 0;
        String FunMainDeptAssDuty;
        String FunMainDeptAssDutyName = "";
        int SubmitPubSubItemFileNum = 0;
        String PosMainDuty = "";
        String PosA = "";
        String PosB = "";
        String PosADuty = "";
        String PosBDuty = "";
        String MainDeptCode = "";
        String MainDeptName = "";
        String manageDept = "";
        String manageDeptSimpleName = "";
        String MainDeptAssDuty;
        String MainDeptAssDutyName = "";
        DocAPI api = new DocAPI();
        String temp = "";
        String gfd = "1";
//      String token=versionInfo.getString("token");
        String token = api.getToken();

        RoleAuth RoleAuth = new RoleAuth();
        String desc = "";
        String attribute = "";
        String parentattribute = "";
        int ChaNum;
        String ArchType = "";
        String OTTFileName;
        String shouwei = "";
        String ArchFunCode = "";
        String deptsimplename = "";
        Map<String, Object> objectMap = null;


        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject("test11", id);

        List<Map<String, Object>> subSheet = (List<Map<String, Object>>) bizObject.getObject("Sheet1658798318323");

        String elec2 = (String) subSheet.get(0).get("elec");
        List<Map<String, Object>> code123123 = (List<Map<String, Object>>) subSheet.get(0).get("code");
        Map<String, Object> code123122223 = (Map<String, Object>) subSheet.get(0).get("code");


        if (CollectionUtils.isEmpty(subSheet)) {
            return getOkResponseResult("");
        }


        //排序
        //subSheet.sort("filecode");

        //获取申请人
        String createId = bizObject.getCreater().getId();
        UserModel user = getOrganizationFacade().getUser(createId);
        String AppEmp = user.getEmployeeNo();


        objectMap = subSheet.get(0);


        //获取表单上主责公司
        mainComCode = (String) objectMap.get("mainComYingshe");

        //获取表单上主责部门
        MainDeptCode = (String) objectMap.get("defaultmainDeptId");
        MainDeptName = (String) objectMap.get("MainDept");
        //获取表单上管理部门
        manageDept = (String) objectMap.get("manaDeptId");
        manageDeptSimpleName = (String) objectMap.get("manaDeptName");


        //获取表单上主责岗位
        PosMainDuty = (String) objectMap.get("mainPositionCode");
        PosMainName = (String) objectMap.get("mainPostionName");

        //获取表单上岗A
        PosA = (String) objectMap.get("PositionACode");
        PosAName = (String) objectMap.get("positionAname");
//          PosAName=PosAName;

        //获取表单上岗B
        PosB = (String) objectMap.get("positionBCode");
        PosBName = (String) objectMap.get("postionBname");
        // 获取主责部门助理角色
        MainDeptAssDuty = MainDeptCode + ".006";

        // 获取职能总责部门助理角色
        FunMainDeptAssDuty = (String) objectMap.get("FunMainDept") + ".006";
        String sqlFunMainDeptAssDutyCode = "select fname_l2 from [HG_LINK].[hg].dbo.T_org_position WHERE FNUMBER ='" + MainDeptAssDuty + "'";

        List<Map<String, Object>> list = sqlService.getList(CloudSqlService.htEas, sqlFunMainDeptAssDutyCode);
        try {
            FunMainDeptAssDutyName = (String) list.get(0).get("fname_l2");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        //职能代码
        ArchFunCode = (String) objectMap.get("znCode");
        // 获取档案类型号
        ArchType = (String) objectMap.get("archTypeNumber");

        // 档案类型为06、11、19上传是“管理部门-岗A”
        if (ArchType.equalsIgnoreCase("06") || ArchType.equalsIgnoreCase("11") || ArchType.equalsIgnoreCase("19")) {
            MainDeptAssDuty = PosA;
            PosADuty = PosA;
        }
        if (ArchType.equalsIgnoreCase("23")) {
            MainDeptAssDuty = "02.12.006";
            PosADuty = "02.12.006";
            AppEmp = "02.0063";
        }
        if (ArchType.substring(0, 2).equals("18")) {
            //上传岗：工作档案框架主责岗
            objectMap = subSheet.get(0);
            String CFCode = (String) objectMap.get("fileCode");//档案代码
            String CFCode01 = CFCode.substring(0, CFCode.lastIndexOf("-")) + "-0";//工作档案框架
            String sqlstr = "select b.fname_l2 FNAME,b.fnumber FNUMBER from t_fil_fileinfo a inner join [HG_LINK].[hg].dbo.T_ORG_position b on a.fpiccodeid=b.fid  where a.fnumber = '" + CFCode01 + "'";
            List<Map<String, Object>> rs = sqlService.getList(CloudSqlService.htEas, sqlstr);
            try {
                String fnumber = (String) rs.get(0).get("FNUMBER");
                if (fnumber == null) {
                    MainDeptAssDuty = fnumber.substring(0, fnumber.lastIndexOf(".")) + ".006";
                    PosADuty = fnumber.substring(0, fnumber.lastIndexOf(".")) + ".006";
                } else {
                    MainDeptAssDuty = PosMainDuty.substring(0, PosMainDuty.lastIndexOf(".")) + ".006";
                    PosADuty = MainDeptAssDuty;
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            String sqlstr000 = "select fsimplename from [HG_LINK].[hg].dbo.t_org_admin  where fnumber = '" + PosADuty.substring(0, PosADuty.lastIndexOf(".")) + "'";
            List<Map<String, Object>> rs000 = sqlService.getList(CloudSqlService.htEas, sqlstr000);
            try {
                deptsimplename = (String) rs000.get(0).get("fsimplename");
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        //获取公共区域对应岗文件夹里的文件List
        String SubmitAttribute = MainDeptAssDuty + " PUB " + AppEmp;
        JSONObject SubmitPubFolderIDObj = api.getFileID(SubmitAttribute);
        int SubmitPubFolderNum = Integer.parseInt(SubmitPubFolderIDObj.getString("num"));


        if (ArchType.equalsIgnoreCase("23")) {

            //获取监控后台token
            //先给监控后台授权
            AccountAuthorize obj = new AccountAuthorize();
            String FirstAuthorize = obj.testFirstAuthorize("system", "192.168.11.43:5443", "WINPC_V1");
            JSONObject FirstAuthorizeObj = JSONObject.fromObject(FirstAuthorize);
            String realm = FirstAuthorizeObj.getString("realm");
            String randomKey = FirstAuthorizeObj.getString("randomKey");
            String encryptType = FirstAuthorizeObj.getString("encryptType");
            String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlrrLOJnJE6MfIk82R2EbmBHv9cq6RaWKehuN5y8WiRfxDhwG2wE4ZFjjs831GcrZz8SZKcW/OcWlTs88JqleSbyDSAw+hxLSyCN9cKVO0rP7APYjS5XAus/77ftA4EO+2SKm+AKksx+ajm4vH2MfbXJOE2c9Ypd9nKYhONVH/pxauCtQmntt4gwX0+TnrAhPcQ2cUHkcLdAWYDFUUsabJFQr4JMuZ0C9L2N/lhj9koc2OLYNdiXUfxb0dXLrMhnO4Dbi/slW1PgQAolMXm4o161zFHI3KptlqkBrKfUiaZqcAyYOi3cNV8BrhtM2zRJ5qbyUhn0zmEzmkZZJvk+b4wIDAQAB";
            String password = "ht123456";
            String username = "system";
            String Vediotoken = " ";
            temp = md5(password);
            temp = md5(username + temp);
            temp = md5(temp);
            temp = md5(username + ":" + realm + ":" + temp);
            String signature = md5(temp + ":" + randomKey);
            int aa = 0;
            String VedioRoleAuthList = "A";
            //
            String SecondAuthorize = obj.testSecondAuthorize("C8:D9:D2:16:AF:F6", signature, username, randomKey, publicKey, encryptType, "192.168.11.43", "WINPC_V1", "0");
            JSONObject SecondAuthorizeObj = JSONObject.fromObject(SecondAuthorize);

            Vediotoken = SecondAuthorizeObj.getString("token");
            //
            ////String KeepAlive=  obj.KeepAlive( Vediotoken,"300");


            String PosFid = "";
            String PosName = "";
            String PosDuty = "";
            String RolePosAName = "";
            String RolePosId = "";
            //给授权主责岗PosMainDuty、上级直至董事长授权
            String sql = "select fid,  fname_l2 ,FNUMBER from  [HG_LINK].[hg].dbo.T_ORG_Position where  fnumber='" + PosMainDuty + "'";
            List<Map<String, Object>> rs = sqlService.getList(CloudSqlService.htEas, sql);
            try {
                for (Map<String, Object> m : rs) {
                    PosFid = (String) m.get("fid");
                    PosName = (String) m.get("fname_l2");
                    PosDuty = (String) m.get("FNUMBER");
                    if (PosFid != null && !PosName.equalsIgnoreCase("董事长")) {
                        //找岗位上级直至董事长
                        sql = "select B.fname_l2 as fname_l2,B.FNUMBER as FNUMBER ,B.FID from  [HG_LINK].[hg].dbo.T_ORG_PositionHierarchy A inner join [HG_LINK].[hg].dbo.T_ORG_POSITION B ON A.FPARENTID=B.FID where  A.fchildid='" + PosFid + "'";
                        rs = sqlService.getList(CloudSqlService.htEas, sql);

                        //查询该岗位下的所有监控权限
                        VedioRoleAuthList = "";
//							String sqlRoleAuthList01="select   FP_1 ,fnumber from  T_FIL_DeptFileBae   where  FNumber  like  'MT%' and  FOrgDeptID=(select  fid  from   T_ORG_Admin where  FNumber='02.12'   )";
                        String sqlRoleAuthList01 = "select * from (select  CASE  RIGHT('" + PosDuty + "',3)  WHEN  '001' THEN    FP_1    WHEN  '002'  THEN  FP_2  WHEN  '003'  THEN  FP_3 WHEN  '004'  THEN  FP_4 WHEN  '005'  THEN  FP_5 WHEN  '006'  THEN  FP_6 WHEN  '007'  THEN  FP_7 WHEN  '008'  THEN  FP_8 WHEN  '009'  THEN  FP_9 WHEN  '010'  THEN  FP_10 END as auth , FNumber from  [HG_LINK].[hg].dbo.T_FIL_DeptFileBae    where  FNumber  like  'MT%'  and  FOrgDeptID=(select  FAdminOrgUnitID  from   [HG_LINK].[hg].dbo.T_ORG_Position  where  FNumber='" + PosDuty + "'   )	) t1 where auth in ('99','8','9') 	";
                        List<Map<String, Object>> rsRoleAuthList01 = sqlService.getList(CloudSqlService.htEas, sqlRoleAuthList01);
                        try {
                            for (Map<String, Object> mm : rsRoleAuthList01) {
                                String fnumber = (String) mm.get("fnumber");
                                if (fnumber != null) {
                                    VedioRoleAuthList = VedioRoleAuthList + "," + fnumber;
                                }
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }


                        VedioRoleAuthList = VedioRoleAuthList + ' ';
                        VedioRoleAuthList = VedioRoleAuthList.substring(1, VedioRoleAuthList.length());
                        obj.UpdateRole(Vediotoken, PosDuty, "实时预览", "", "test");
                        obj.UpdateRole(Vediotoken, PosDuty, "实时预览", "", "test ");
                        obj.UpdateRole(Vediotoken, PosDuty, "实时预览", "", VedioRoleAuthList);
                        obj.UpdateRole(Vediotoken, PosDuty, "实时预览", "", VedioRoleAuthList + " ");


                        //查询该岗位+C下的所有监控权限
                        VedioRoleAuthList = "";
                        String sqlRoleAuthList02 = "select * from (select  CASE  RIGHT('" + PosDuty + "',3)  WHEN  '001' THEN    FP_1    WHEN  '002'  THEN  FP_2  WHEN  '003'  THEN  FP_3 WHEN  '004'  THEN  FP_4 WHEN  '005'  THEN  FP_5 WHEN  '006'  THEN  FP_6 WHEN  '007'  THEN  FP_7 WHEN  '008'  THEN  FP_8 WHEN  '009'  THEN  FP_9 WHEN  '010'  THEN  FP_10 END as auth , FNumber from  [HG_LINK].[hg].dbo.T_FIL_DeptFileBae    where  FNumber  like  'MT%'  and  FOrgDeptID=(select  FAdminOrgUnitID  from   [HG_LINK].[hg].dbo.T_ORG_Position  where  FNumber='" + PosDuty + "'   )	) t1 where auth in ('99','9','1','5') 	";
                        List<Map<String, Object>> rsRoleAuthList02 = sqlService.getList(CloudSqlService.htEas, sqlRoleAuthList02);
                        try {
                            for (Map<String, Object> mm : rsRoleAuthList02) {
                                String fnumber = "P" + (String) mm.get("fnumber");
                                if (fnumber != null) {
                                    VedioRoleAuthList = VedioRoleAuthList + "," + fnumber;
                                }
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }


                        VedioRoleAuthList = VedioRoleAuthList + ' ';
                        VedioRoleAuthList = VedioRoleAuthList.substring(1, VedioRoleAuthList.length());
                        String PosDutyC = PosDuty + 'C';
                        obj.UpdateRole(Vediotoken, PosDutyC, "实时预览,录像回放", "录像查看,", "test");
                        obj.UpdateRole(Vediotoken, PosDutyC, "实时预览,录像回放", "录像查看,", "test ");
                        obj.UpdateRole(Vediotoken, PosDutyC, "实时预览,录像回放", "录像查看,", VedioRoleAuthList);
                        obj.UpdateRole(Vediotoken, PosDutyC, "实时预览,录像回放", "录像查看,", VedioRoleAuthList + " ");
                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            //给授权岗A  PosA、上级直至董事长授权
            PosName = "";
            String sqlA = "select fid,  fname_l2 ,FNUMBER from  [HG_LINK].[hg].dbo.T_ORG_Position where  fnumber='" + PosA + "'";
            List<Map<String, Object>> rsA = sqlService.getList(CloudSqlService.htEas, sqlA);
            try {
                for (Map<String, Object> m : rsA) {
                    PosFid = (String) m.get("fid");
                    PosName = (String) m.get("fname_l2");
                    PosDuty = (String) m.get("FNUMBER");
                    if (PosFid != null && !PosName.equalsIgnoreCase("董事长")) {
                        //找岗位上级直至董事长
                        sql = "select B.fname_l2 as fname_l2,B.FNUMBER as FNUMBER ,B.FID from  [HG_LINK].[hg].dbo.T_ORG_PositionHierarchy A inner join [HG_LINK].[hg].dbo.T_ORG_POSITION B ON A.FPARENTID=B.FID where  A.fchildid='" + PosFid + "'";
                        rsA = sqlService.getList(CloudSqlService.htEas, sql);
                        //查询该岗位下的所有监控权限
                        VedioRoleAuthList = "";
//							String sqlRoleAuthList01="select   FP_1 ,fnumber from  T_FIL_DeptFileBae   where  FNumber  like  'MT%' and  FOrgDeptID=(select  fid  from   T_ORG_Admin where  FNumber='02.12'   )";
                        String sqlRoleAuthList01 = "select * from (select  CASE  RIGHT('" + PosDuty + "',3)  WHEN  '001' THEN    FP_1    WHEN  '002'  THEN  FP_2  WHEN  '003'  THEN  FP_3 WHEN  '004'  THEN  FP_4 WHEN  '005'  THEN  FP_5 WHEN  '006'  THEN  FP_6 WHEN  '007'  THEN  FP_7 WHEN  '008'  THEN  FP_8 WHEN  '009'  THEN  FP_9 WHEN  '010'  THEN  FP_10 END as auth , FNumber from  [HG_LINK].[hg].dbo.T_FIL_DeptFileBae    where  FNumber  like  'MT%'  and  FOrgDeptID=(select  FAdminOrgUnitID  from   [HG_LINK].[hg].dbo.T_ORG_Position  where  FNumber='" + PosDuty + "'   )	) t1 where auth in ('99','8','9') 	";
                        List<Map<String, Object>> rsRoleAuthList01 = sqlService.getList(CloudSqlService.htEas, sqlRoleAuthList01);
                        try {
                            for (Map<String, Object> mm : rsRoleAuthList01) {
                                String fnumber = (String) mm.get("fnumber");
                                if (fnumber != null) {
                                    VedioRoleAuthList = VedioRoleAuthList + "," + fnumber;
                                }
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        VedioRoleAuthList = VedioRoleAuthList + ' ';
                        VedioRoleAuthList = VedioRoleAuthList.substring(1, VedioRoleAuthList.length());
                        obj.UpdateRole(Vediotoken, PosDuty, "实时预览", "", "test");
                        obj.UpdateRole(Vediotoken, PosDuty, "实时预览", "", VedioRoleAuthList);


                        //查询该岗位+C下的所有监控权限
                        VedioRoleAuthList = "";
                        String sqlRoleAuthList02 = "select * from (select  CASE  RIGHT('" + PosDuty + "',3)  WHEN  '001' THEN    FP_1    WHEN  '002'  THEN  FP_2  WHEN  '003'  THEN  FP_3 WHEN  '004'  THEN  FP_4 WHEN  '005'  THEN  FP_5 WHEN  '006'  THEN  FP_6 WHEN  '007'  THEN  FP_7 WHEN  '008'  THEN  FP_8 WHEN  '009'  THEN  FP_9 WHEN  '010'  THEN  FP_10 END as auth , FNumber from  [HG_LINK].[hg].dbo.T_FIL_DeptFileBae    where  FNumber  like  'MT%'  and  FOrgDeptID=(select  FAdminOrgUnitID  from   [HG_LINK].[hg].dbo.T_ORG_Position  where  FNumber='" + PosDuty + "'   )	) t1 where auth in ('99','9','1','5') 	";
                        List<Map<String, Object>> rsRoleAuthList02 = sqlService.getList(CloudSqlService.htEas, sqlRoleAuthList02);
                        try {
                            for (Map<String, Object> mm : rsRoleAuthList02) {
                                String fnumber = "P" + (String) mm.get("fnumber");
                                if (fnumber != null) {
                                    VedioRoleAuthList = VedioRoleAuthList + "," + fnumber;
                                }
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        VedioRoleAuthList = VedioRoleAuthList + ' ';
                        VedioRoleAuthList = VedioRoleAuthList.substring(1, VedioRoleAuthList.length());
                        String PosDutyC = PosDuty + 'C';
                        obj.UpdateRole(Vediotoken, PosDutyC, "实时预览,录像回放", "录像查看,", "test");
                        obj.UpdateRole(Vediotoken, PosDutyC, "实时预览,录像回放", "录像查看,", VedioRoleAuthList);

                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if (PosB != null) {
                PosName = "";
                String sqlB = "select fid,  fname_l2 ,FNUMBER from  [HG_LINK].[hg].dbo.T_ORG_Position where  fnumber='" + PosB + "'";
                List<Map<String, Object>> rsB = sqlService.getList(CloudSqlService.htEas, sqlB);
                try {
                    for (Map<String, Object> m : rsB) {
                        String fnumber = (String) m.get("FNUMBER");
                        PosFid = (String) m.get("fid");
                        PosName = (String) m.get("fname_l2");
                        PosDuty = (String) m.get("FNUMBER");
                        if (fnumber == null) {
                            //找岗位上级直至董事长
                            sql = "select B.fname_l2 as fname_l2,B.FNUMBER as FNUMBER ,B.FID from  [HG_LINK].[hg].dbo.T_ORG_PositionHierarchy A inner join [HG_LINK].[hg].dbo.T_ORG_POSITION B ON A.FPARENTID=B.FID where  A.fchildid='" + PosFid + "'";
                            rsB = sqlService.getList(CloudSqlService.htEas, sql);
                            //查询该岗位下的所有监控权限
                            VedioRoleAuthList = "";
//								String sqlRoleAuthList01="select   FP_1 ,fnumber from  T_FIL_DeptFileBae   where  FNumber  like  'MT%' and  FOrgDeptID=(select  fid  from   T_ORG_Admin where  FNumber='02.12'   )";
                            String sqlRoleAuthList01 = "select * from (select  CASE  RIGHT('" + PosDuty + "',3)  WHEN  '001' THEN    FP_1    WHEN  '002'  THEN  FP_2  WHEN  '003'  THEN  FP_3 WHEN  '004'  THEN  FP_4 WHEN  '005'  THEN  FP_5 WHEN  '006'  THEN  FP_6 WHEN  '007'  THEN  FP_7 WHEN  '008'  THEN  FP_8 WHEN  '009'  THEN  FP_9 WHEN  '010'  THEN  FP_10 END as auth , FNumber from  [HG_LINK].[hg].dbo.T_FIL_DeptFileBae    where  FNumber  like  'MT%'  and  FOrgDeptID=(select  FAdminOrgUnitID  from   [HG_LINK].[hg].dbo.T_ORG_Position  where  FNumber='" + PosDuty + "'   )	) t1 where auth in ('99','8','9') 	";
                            List<Map<String, Object>> rsRoleAuthList01 = sqlService.getList(CloudSqlService.htEas, sqlRoleAuthList01);
                            try {
                                for (Map<String, Object> mm : rsRoleAuthList01) {
                                    fnumber = (String) mm.get("fnumber");
                                    if (fnumber != null) {
                                        VedioRoleAuthList = VedioRoleAuthList + "," + fnumber;
                                    }
                                }
                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            VedioRoleAuthList = VedioRoleAuthList + ' ';
                            VedioRoleAuthList = VedioRoleAuthList.substring(1, VedioRoleAuthList.length());
                            obj.UpdateRole(Vediotoken, PosDuty, "实时预览", "", "test");
                            obj.UpdateRole(Vediotoken, PosDuty, "实时预览", "", VedioRoleAuthList);

                            //查询该岗位+C下的所有监控权限
                            VedioRoleAuthList = "";
                            String sqlRoleAuthList02 = "select * from (select  CASE  RIGHT('" + PosDuty + "',3)  WHEN  '001' THEN    FP_1    WHEN  '002'  THEN  FP_2  WHEN  '003'  THEN  FP_3 WHEN  '004'  THEN  FP_4 WHEN  '005'  THEN  FP_5 WHEN  '006'  THEN  FP_6 WHEN  '007'  THEN  FP_7 WHEN  '008'  THEN  FP_8 WHEN  '009'  THEN  FP_9 WHEN  '010'  THEN  FP_10 END as auth , FNumber from  [HG_LINK].[hg].dbo.T_FIL_DeptFileBae    where  FNumber  like  'MT%'  and  FOrgDeptID=(select  FAdminOrgUnitID  from   [HG_LINK].[hg].dbo.T_ORG_Position  where  FNumber='" + PosDuty + "'   )	) t1 where auth in ('99','9','1','5') 	";
                            List<Map<String, Object>> rsRoleAuthList02 = sqlService.getList(CloudSqlService.htEas, sqlRoleAuthList02);
                            try {
                                for (Map<String, Object> mm : rsRoleAuthList02) {
                                    fnumber = (String) m.get("fnumber");
                                    if (fnumber == null) {
                                        VedioRoleAuthList = VedioRoleAuthList + "," + fnumber;
                                    }
                                }
                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            VedioRoleAuthList = VedioRoleAuthList + ' ';
                            VedioRoleAuthList = VedioRoleAuthList.substring(1, VedioRoleAuthList.length());
                            String PosDutyC = PosDuty + 'C';
                            obj.UpdateRole(Vediotoken, PosDutyC, "实时预览,录像回放", "录像查看,", "test");
                            obj.UpdateRole(Vediotoken, PosDutyC, "实时预览,录像回放", "录像查看,", VedioRoleAuthList);
                        }
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }


            fileCodeSortController.sort(bizObject.getId(), "test11", "Sheet1658798318323", "archTypeTypeNumber", "fileCode");
            int n = subSheet.size();
            for (int i = 0; i < n; i++) {
                objectMap = subSheet.get(i);
                desc = (String) objectMap.get("des");// 获取说明
                String InfoCode = (String) objectMap.get("fileCode");//MT-0108-05-010
                String InfoCode01 = InfoCode.substring(InfoCode.indexOf("-") + 1, InfoCode.length());//0108-05-010
                String InfoCode02 = InfoCode01.substring(0, InfoCode01.lastIndexOf("-"));//0108-05
                String InfoCodeRegion = InfoCode02.substring(0, InfoCode02.indexOf("-"));//0108
                String InfoCodeRegion01 = InfoCode02.substring(InfoCode02.indexOf("-") + 1, InfoCode02.length()).replace(" ", "");//05
                String ParentAttributeRegionNum = "";
                String SDParentAttribute = "";
                String FIsLeaf = "";
                int count = 0;
                String FParentAttribute = "";
                String FAttribute = "";

                String ParentAttributeFNum = "";
                int RegionLevelCount = 0;//"-"的个数
                String SDAttribute = "";
                String InfoCodeCount = InfoCode;//MT-0108-05-010
                elec = (String) subSheet.get(i).get("elec");
                while (InfoCodeCount.indexOf("-") != -1) {
                    InfoCodeCount = InfoCodeCount.substring(InfoCodeCount.indexOf("-") + 1, InfoCodeCount.length());
                    RegionLevelCount++;
                }


                int InfoCodeFLen = InfoCodeRegion.length();//0106的长度
                for (int s = 0; s < InfoCodeFLen; s++) {
                    String FCode = InfoCode02.substring(0, s + 1);
                    String sqlFCode = "select fname_l2 ,isnull(fisleaf,'0')  fisleaf from [HG_LINK].[hg].dbo.T_man_siteinfo WHERE FNUMBER ='" + FCode + "'";
                    List<Map<String, Object>> rsFCode = sqlService.getList(CloudSqlService.htEas, sqlFCode);
                    try {
                        String FName = (String) rsFCode.get(0).get("fname_l2");
                        FIsLeaf = (String) rsFCode.get(0).get("fisleaf");
                        if (FName != null) {
                            if (count == 0) {
                                FParentAttribute = manageDept + " " + "MT" + " " + manageDeptSimpleName;//
                            } else {
                                FParentAttribute = "MT-" + ParentAttributeFNum + " " + manageDept;//
                            }


                            FAttribute = "MT-" + FCode + " " + manageDept;//
                            JSONObject FFileIdObj = api.getFileID(FAttribute);
                            int num = Integer.parseInt(FFileIdObj.getString("num"));
//							if(num==1)
//							{
//								continue;
//							}
                            //添加对应文件夹
                            JSONObject FParentFileID = api.getFileID(FParentAttribute);
                            int FParentFileNum = Integer.parseInt(FParentFileID.getString("num"));
                            if (FParentFileNum == 0) {
                                continue;
                            }
                            JSONArray FParentFileIdDataArray = FParentFileID.getJSONArray("data");
                            JSONObject FParentFileIdDataObj = FParentFileIdDataArray.getJSONObject(0);
                            String FParentFileId = FParentFileIdDataObj.getString("id");
                            if (num == 0) {
                                JSONObject FolderAddObj = api.getAddFolder(FParentFileId, FCode + " " + FName, FAttribute);
                            }
                            if (num > 0) {
                                JSONArray FolderIdDataArray = FFileIdObj.getJSONArray("data");
                                JSONObject FolderIdDataObj = FolderIdDataArray.getJSONObject(0);
                                String FolderId = FolderIdDataObj.getString("id");
                                api.getRename(token, FolderId, FCode + " " + FName);
                            }


                            ParentAttributeFNum = FCode;
                            count++;
                        }
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                }
                String sqlRegionCode = "select fname_l2 from [HG_LINK].[hg].dbo.t_man_siteinfo  WHERE FNUMBER ='" + InfoCodeRegion + "'";
                List<Map<String, Object>> rsRegionCode = sqlService.getList(CloudSqlService.htEas, sqlRegionCode);
                try {
                    String RegionName = (String) rsRegionCode.get(0).get("fname_l2");
                    if (RegionName != null) {
                        SDAttribute = "MT-" + InfoCodeRegion + " " + manageDept;//MT-0108
                        System.out.println("SDAttribute");
                        net.sf.json.JSONObject SDFileObj = api.getFileID(SDAttribute);
                        //如已存在文件夹则跳过
                        int num = Integer.parseInt(SDFileObj.getString("num"));
                        SDParentAttribute = manageDept + " " + "MT" + " " + manageDeptSimpleName;//


                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                net.sf.json.JSONObject FolderObjID = api.getFileID(SDAttribute);
                JSONArray FolderIdDataArray = FolderObjID.getJSONArray("data");
                net.sf.json.JSONObject FolderIdDataObj = FolderIdDataArray.getJSONObject(0);
                String FolderID = FolderIdDataObj.getString("id");
                String att = (String) objectMap.get("fileCode");
                String aj = att.substring(0, att.lastIndexOf("-")) + " " + manageDept;
                api.getAddFolder(FolderID, InfoCodeRegion01, aj);


                net.sf.json.JSONObject FolderObjID02 = api.getFileID(aj);
                JSONArray FolderIdDataArray02 = FolderObjID02.getJSONArray("data");
                net.sf.json.JSONObject FolderIdDataObj02 = FolderIdDataArray02.getJSONObject(0);
                String FolderID02 = FolderIdDataObj02.getString("id");


                String aj02 = (String) objectMap.get("fileCode") + " " + manageDept;
                att = (String) objectMap.get("fileCode");
                api.getAddFolder(FolderID02, att.substring(att.lastIndexOf("-") + 1, att.length()), aj02);


                net.sf.json.JSONObject FolderObjID03 = api.getFileID(aj02);
                JSONArray FolderIdDataArray03 = FolderObjID03.getJSONArray("data");
                net.sf.json.JSONObject FolderIdDataObj03 = FolderIdDataArray03.getJSONObject(0);
                AuthFileId = FolderIdDataObj03.getString("id");

                //.................


                parentattribute = (String) objectMap.get("fileCode") + ' ' + manageDept;// 对应上级文件夹扩展属性
                JSONObject ParentFolderIdObj = api.getFileID(parentattribute);
                JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                JSONObject ParentFolderIdDataObj = ParentFolderIdDataArray.getJSONObject(0);
                String ParentFolderId = ParentFolderIdDataObj.getString("id");
                JSONObject FolderPathObj = api.getFullPath(ParentFolderId);
                String destfullpath = FolderPathObj.getString("fullPath");


                JSONObject FileID = api.getFileID((String) objectMap.get("fileCode"));
                int ArrFileNum = Integer.parseInt(FileID.getString("num"));
                if (ArrFileNum == 0) {
                    continue;
                }
                JSONArray FileIDDataArray = FileID.getJSONArray("data");
                JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                String NewFileName = FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));
                if (FileIDDataArray.size() == 1) {
                    api.getMove(fileid, destfullpath);
                }//有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的

                //有2个及以上相同扩展属性的文件时，A,B岗位未变更
                else {
                    api.getRename(token, fileid2, "待删除");//先将老文件名改为"待删除"

                    //将老文件放入userFolder
                    JSONObject FolderPathObj01 = api.getFullPath("1688009");
                    String CAdestfullpath01 = FolderPathObj01.getString("fullPath");
                    api.getMove(fileid2, CAdestfullpath01);//将老文件放入userFolder


                    api.getMove(fileid, destfullpath);//将新文件移动到对应文件夹，此时和老文件放在同一个文件夹

                    api.getRemove(token, fileid2);//删除老文件
                }


            }

            String sqlMainDeptAssDutyCode = "select fname_l2 from [HG_LINK].[hg].dbo.T_org_position WHERE FNUMBER ='" + MainDeptAssDuty + "'";
            List<Map<String, Object>> rsMainDeptAssDutyCode = sqlService.getList(CloudSqlService.htEas, sqlMainDeptAssDutyCode);
            try {
                MainDeptAssDutyName = (String) rsMainDeptAssDutyCode.get(0).get("fname_l2");
                if (MainDeptAssDutyName != null) {
                    MainDeptAssDutyName = (String) rsMainDeptAssDutyCode.get(0).get("fname_l2");
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
            RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
            RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);    //档案上传岗所有权限，上级直到董事长授下载权
            RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
            RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
            if (PosA != null) {

                RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
            }
            if (PosB != null) {
                PosBDuty = PosB;
                PosBName = PosBName;
                RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
            }


        }

        if (SubmitPubFolderNum > 0) {
            JSONArray SubmitPubFolderIDDataArray = SubmitPubFolderIDObj.getJSONArray("data");
            JSONObject SubmitPubFolderIDDataObj = SubmitPubFolderIDDataArray.getJSONObject(0);
            String SubmitPubFolderID = SubmitPubFolderIDDataObj.getString("id");
            JSONObject SubmitPubFolderListObj = api.getList(SubmitPubFolderID);
            JSONArray SubmitPubFolderListDataArray = SubmitPubFolderListObj.getJSONArray("data");//公共区域对应岗文件夹里的文件List
            int n = subSheet.size();
            for (int i = 0; i < subSheet.size(); i++) {
                SubItemORNot = 0;//分项数量
                SubmitPubSubItemFileNum = 0;
                objectMap = subSheet.get(i);
                attribute = (String) objectMap.get("fileCode");
                attribute = attribute.replace(" ", "");//档案代码
                desc = (String) objectMap.get("des");// 获取说明
                ChaNum = 0;//档案名称带"-"的个数
                elec = (String) subSheet.get(i).get("elec");


                // 获取档案类型号
                ArchType = (String) objectMap.get("archTypeNumber");


                if (elec.equalsIgnoreCase("×")) {

                    if (ArchType.equalsIgnoreCase("01") || ArchType.equalsIgnoreCase("0201") || ArchType.equalsIgnoreCase("0202") || ArchType.equalsIgnoreCase("0203") || ArchType.equalsIgnoreCase("0204") || ArchType.equalsIgnoreCase("03") || ArchType.equalsIgnoreCase("0503")) {
                        attribute = attribute + " " + MainDeptCode;
                    }
                    if (ArchType.equalsIgnoreCase("0502")) {
                        attribute = attribute + " " + PosMainDuty;
                    }


                    JSONObject ExistFolderObj = api.getFileID(attribute);//
                    int FileNum = Integer.parseInt(ExistFolderObj.getString("num"));
                    if (FileNum == 0) {
                        continue;
                    }
                    if (FileNum != 0) {

                        JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                        for (i = 0; i < ExistFolderDataArray.size(); i++) {
                            JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(i);
                            String ExitFolderId = ExistFolderObj0.getString("id");
                            api.getRemove(token, ExitFolderId);
                        }

                    }

                    continue;

                }

                // 档案类型为06、11、19上传是“管理部门-岗A”
                if (ArchType.equalsIgnoreCase("06") || ArchType.equalsIgnoreCase("11") || ArchType.equalsIgnoreCase("19")) {
                    PosADuty = PosA;
                }

                String sqlMainDeptAssDutyCode = "select fname_l2 from [HG_LINK].[hg].dbo.T_org_position WHERE FNUMBER ='" + MainDeptAssDuty + "'";
                List<Map<String, Object>> rsMainDeptAssDutyCode = sqlService.getList(CloudSqlService.htEas, sqlMainDeptAssDutyCode);
                try {
                    MainDeptAssDutyName = (String) rsMainDeptAssDutyCode.get(0).get("fname_l2");
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (elec.equalsIgnoreCase("×")) {
                    continue;
                }
                String ArchCode = (String) objectMap.get("fileCode");
                ArchCode = ArchCode.replace(" ", "");
                while (ArchCode.contains("-")) {
                    ChaNum++;
                    ArchCode = ArchCode.substring(ArchCode.indexOf("-") + 1, ArchCode.length());
                }
                String[] SubmitPubFileList = new String[200];//公共区域对应岗位文件夹里的文件List
                String[] SubmitPubSubItemFileList = new String[200];
                String[] SubItemFileList = new String[200];
                int IsNomalFile = 0;
                //遍历公共区域对应岗位文件夹里的文件，带有分项的放入SubmitPubSubItemFileList，和EAS报批的文件名一致的放入SubItemFileList
                for (int PubNum = 0; PubNum < SubmitPubFolderListDataArray.size(); PubNum++) {
                    JSONObject SubmitPubFolderListNumDataArray = SubmitPubFolderListDataArray.getJSONObject(PubNum);
                    SubmitPubFileList[PubNum] = SubmitPubFolderListNumDataArray.getString("remarks");
                    String trans = SubmitPubFileList[PubNum].replace(',', '，');
                    SubmitPubFileList[PubNum] = trans;
                    attribute = (String) objectMap.get("fileCode");
                    attribute = attribute.replace(" ", "");
                    if (SubmitPubFileList[PubNum].equals(attribute)) {
                        IsNomalFile = 1;
                    }

                    if (trans.contains("，")) {
                        SubmitPubSubItemFileList[SubmitPubSubItemFileNum] = trans;//带有分项的放入SubmitPubSubItemFileList
                        String trans01 = trans.substring(0, trans.indexOf("，"));
                        String asf = (String) objectMap.get("fileCode");

                        if (trans01.equalsIgnoreCase(asf.replace(" ", ""))) {
                            SubItemFileList[SubItemORNot] = trans;//分项文件List
                            SubItemORNot++;//分项数量
                        }
                        SubmitPubSubItemFileNum++;
                        System.out.println(PubNum);
                    }
                }


                if (ArchType.equalsIgnoreCase("11")) {
                    desc = (String) objectMap.get("des");// 获取说明
                    String InfoCode = (String) objectMap.get("fileCode");//SD-142341302-3016-01
                    String InfoCode01 = InfoCode.substring(InfoCode.indexOf("-") + 1, InfoCode.length());//142341302-3016-01
                    String InfoCode02 = InfoCode01.substring(0, InfoCode01.lastIndexOf("-"));//142341302-3016
                    String InfoCodeRegion = InfoCode02.substring(0, InfoCode02.indexOf("-"));//142341302
                    String InfoCodeRegion01 = InfoCode02.substring(InfoCode02.indexOf("-") + 1, InfoCode02.length()).replace(" ", "");//9005
                    String ParentAttributeRegionNum = "";
                    String SDParentAttribute = "";
                    int RegionLevelCount = 0;//"-"的个数
                    String SDAttribute = "";
                    String InfoCodeCount = InfoCode;//SD-142331024-9002-31-02
                    elec = (String) subSheet.get(i).get("elec");
                    while (InfoCodeCount.indexOf("-") != -1) {
                        InfoCodeCount = InfoCodeCount.substring(InfoCodeCount.indexOf("-") + 1, InfoCodeCount.length());
                        RegionLevelCount++;
                    }


                    //岗位代码下面添加对应的地区文件夹

                    int InfoCodeRegionLen = InfoCodeRegion.length();//142331024的长度
                    for (int s = 0; s < InfoCodeRegionLen; s++) {
                        String RegionCode = InfoCodeRegion.substring(0, s + 1);
                        String sqlRegionCode = "select fname_l2,fenglish from [HG_LINK].[hg].dbo.T_ARE_AREABASE WHERE FNUMBER ='" + RegionCode + "'";
                        List<Map<String, Object>> rsRegionCode = sqlService.getList(CloudSqlService.htEas, sqlRegionCode);
                        try {
                            String RegionName = (String) rsRegionCode.get(0).get("fname_l2");
                            String RegionEnglishName = (String) rsRegionCode.get(0).get("fenglish");
                            SDAttribute = "GSD-" + MainDeptCode.substring(0, MainDeptCode.indexOf(".")) + "-" + RegionCode + " " + PosADuty;//SD-02-1 02.06.07.003
                            System.out.println("SDAttribute");
                            JSONObject SDFileObj = api.getFileID(SDAttribute);
                            //如已存在文件夹则跳过
                            int num = Integer.parseInt(SDFileObj.getString("num"));

                            if (s == 0) {
                                SDParentAttribute = "GSD-" + MainDeptCode.substring(0, MainDeptCode.indexOf(".")) + " " + PosADuty;//GSD-02 02.06.07.003

                            } else {
                                SDParentAttribute = "GSD-" + MainDeptCode.substring(0, MainDeptCode.indexOf(".")) + "-" + ParentAttributeRegionNum + " " + PosADuty;//GSD-02-1 02.06.07.003
                            }
                            //添加对应文件夹

                            ParentAttributeRegionNum = RegionCode;
                            JSONObject SDParentFileID = api.getFileID(SDParentAttribute);
                            int SDParentFileNum = Integer.parseInt(SDParentFileID.getString("num"));
                            if (SDParentFileNum == 0) {
                                continue;
                            }
                            JSONArray SDParentFileIdDataArray = SDParentFileID.getJSONArray("data");
                            JSONObject SDParentFileIdDataObj = SDParentFileIdDataArray.getJSONObject(0);
                            String SDParentFileId = SDParentFileIdDataObj.getString("id");
                            api.getAddFolder(SDParentFileId, RegionCode + " " + RegionEnglishName + " " + RegionName, SDAttribute);
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }


                    String RegionFolder = "GSD-" + MainDeptCode.substring(0, MainDeptCode.indexOf(".")) + "-" + InfoCodeRegion + " " + PosADuty;//获取SD-02-142331024的文件夹ID，将9002这个放在该文件夹下
                    String sqlRegionCode01 = "select fSIMPLENAME from [HG_LINK].[hg].dbo.T_CON_SUPPLIERINFO WHERE FNUMBER ='" + InfoCode02 + "'";
                    List<Map<String, Object>> rsRegionCode01 = sqlService.getList(CloudSqlService.htEas, sqlRegionCode01);
                    try {
                        String RegionName = (String) rsRegionCode01.get(0).get("fSIMPLENAME");
                        SDParentAttribute = "GSD-" + MainDeptCode.substring(0, MainDeptCode.indexOf(".")) + "-" + InfoCodeRegion + " " + PosADuty;//GSD-02-1 02.06.07.003
                        SDAttribute = InfoCodeRegion + "-" + InfoCodeRegion01 + " " + PosADuty;//SD-02-1 02.06.07.003
                        JSONObject SDParentFileID = api.getFileID(SDParentAttribute);
                        int SDParentFileNum = Integer.parseInt(SDParentFileID.getString("num"));
                        if (SDParentFileNum == 0) {
                            continue;
                        }
                        JSONArray SDParentFileIdDataArray = SDParentFileID.getJSONArray("data");
                        JSONObject SDParentFileIdDataObj = SDParentFileIdDataArray.getJSONObject(0);
                        String SDParentFileId = SDParentFileIdDataObj.getString("id");
                        api.getAddFolder(SDParentFileId, InfoCodeRegion01 + " " + RegionName, SDAttribute);

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                    if (RegionLevelCount > 2) {

                        if (SubItemORNot > 0) {//带分项的文件
                            // 先加文件夹
                            prefix = attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length());//文件夹前缀
                            foldername = prefix + ' ' + rsRegionCode01.get(0).get("fileName");//需要添加的文件夹名称（01 报告 02公司行政职能资信）
                            parentattribute = attribute.substring(0, attribute.lastIndexOf("-")) + ' ' + PosADuty;// 对应上级文件夹扩展属性

                            JSONObject FolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                            int SubFolderNum = Integer.parseInt(FolderIdObj.getString("num"));
                            if (SubFolderNum == 0) {
                                continue;
                            }
                            JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                            JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                            String folederid = FolderIdObj0.getString("id");
                            String SubFolderAttribute = attribute + ' ' + PosADuty;
                            JSONObject SubFolderIdObj = api.getFileID(SubFolderAttribute);// 分项文件夹是否存在
                            int num = Integer.parseInt(SubFolderIdObj.getString("num"));
                            if (num > 0) {
                                JSONArray SubFolderIdDataArray = SubFolderIdObj.getJSONArray("data");
                                JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(0);
                                String SubFolderId = SubFolderIdObj0.getString("id");
                                api.getRename(token, SubFolderId, foldername);
                            } else {
                                api.getAddFolder(folederid, foldername, attribute + ' ' + PosADuty);
                            }
                            //查询分项文件对应文件夹的FolderId
                            String ExistFolderAttribute = attribute + ' ' + PosADuty;
                            JSONObject ExistFolderObj = api.getFileID(ExistFolderAttribute);//
                            JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                            JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(0);
                            String ExitFolderId = ExistFolderObj0.getString("id");
                            AuthFileId = ExitFolderId;
                            JSONObject ExitFolderFullPath = api.getFullPath(ExitFolderId);
                            String SubItemFolderDestFullpath = ExitFolderFullPath.getString("fullPath");
                            JSONObject ExitFolderListObj = api.getList(ExitFolderId);
                            JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                            int m = ExitFolderListDataArray.size();
                            for (int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++) {
                                JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                String IsFolder = ExitFolderListDataObj.getString("fileType");
                                if (!IsFolder.equals("1")) {
                                    delId = ExitFolderListDataObj.getString("id");
                                    api.getRemove(token, delId);
                                    System.out.println(ExitFolderListDataObj.get("id"));
                                }
                            }
                            //移动公共区域的分项文件到对应路径
                            for (int Sub = 0; Sub < SubItemORNot; Sub++) {
                                String SubItemattribute = SubItemFileList[Sub].replace("，", ",");//02-A-01-16-02-14-01-01-02,01
                                JSONObject SubItemFileIdObj = api.getFileID(SubItemattribute);
                                JSONArray SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                JSONObject SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                String SubItemFileId = SubItemFileIdDataObj.getString("id");
//                            AuthFileId=SubItemFileId;
                                api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                                if (IsNomalFile == 1) {//有分项也有正常文件时候，正常文件也传过去
                                    SubItemFileIdObj = api.getFileID((String) objectMap.get("fileCode"));
                                    SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                    SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                    SubItemFileId = SubItemFileIdDataObj.getString("id");
                                    api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                                }

                                // 获取主责岗位角色及其上级直至董事长，并授权
                            }
                            RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                            RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                            RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                            RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                            RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                            if (PosA != null) {

                                //RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                                RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);

                            }
                            if (PosB != null) {
                                PosBDuty = PosB;
                                PosBName = PosBName;
                                RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                            }
                            continue;
                        }


                        // 如果是框架则在文档系统中加对应的文件夹
// 如果是框架则在文档系统中加对应的文件夹
                        if (desc != null) {
                            if (desc.length() >= 2) {
                                if (desc.substring(0, 2).equalsIgnoreCase("框架")) {
                                    // 添加文件夹到指定路径
                                    // 根据扩展属性获取将添加的框架的parentId

                                    attribute = (String) objectMap.get("fileCode");
                                    attribute = attribute.replace(" ", "") + ' ' + PosADuty;
                                    if (RegionLevelCount == 3) {
                                        parentattribute = InfoCode02 + " " + PosADuty;
                                    } else {
                                        parentattribute = (String) objectMap.get("fileCode");
                                        parentattribute = parentattribute.substring(0, parentattribute.lastIndexOf("-")) + " " + PosADuty;
                                    }
                                    JSONObject ParentFolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id
                                    int ParentFolderNum = Integer.parseInt(ParentFolderIdObj.getString("num"));
                                    if (ParentFolderNum == 0) {
                                        continue;
                                    }
                                    JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                                    JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                                    String ParentFolderId = ParentFolderIdObj0.getString("id");
                                    prefix = (String) objectMap.get("fileCode");
                                    prefix = prefix.substring(prefix.lastIndexOf("-") + 1, prefix.length());//文件夹前缀


                                    foldername = prefix + ' ' + (String) objectMap.get("fileName");// 从表单获取文件夹名称
                                    String FrameAttribute = attribute;
                                    JSONObject FrameAttributeObj = api.getFileID(FrameAttribute);
                                    String FrameNum = FrameAttributeObj.getString("num");
                                    int FrameNum01 = Integer.parseInt(FrameNum);
                                    if (FrameNum01 > 0) {
                                        JSONArray FrameAttributeDataArray = FrameAttributeObj.getJSONArray("data");
                                        JSONObject FrameAttributeDataObj = FrameAttributeDataArray.getJSONObject(0);
                                        String fileId = FrameAttributeDataObj.getString("id");
                                        api.getRename(token, fileId, foldername);
                                    } else {
                                        api.getAddFolder(ParentFolderId, foldername, FrameAttribute);
                                    }
                                    continue;
                                }
                            }
                        }


                        if (elec.equalsIgnoreCase("√") && SubItemORNot == 0 && IsNomalFile == 1) {//不是分项的文件
                            SDAttribute = (String) objectMap.get("fileCode");
                            SDAttribute = SDAttribute.replace(" ", "");
                            String SDLevelParAttribute = SDAttribute.substring(0, SDAttribute.lastIndexOf("-")) + " " + PosADuty;//上级文件夹扩展属性SD-02-142331024-9002-31 02.06.07.003
                            JSONObject SDParentFolderID = api.getFileID(SDLevelParAttribute);
                            int SDParentFolderNum = Integer.parseInt(SDParentFolderID.getString("num"));
                            if (SDParentFolderNum == 0) {
                                SDLevelParAttribute = InfoCode02 + " " + PosADuty;//上级文件夹扩展属性142331024-9002-31 02.06.07.003
                                SDParentFolderID = api.getFileID(SDLevelParAttribute);
                            }
                            JSONArray SDParentFolderIdDataArray = SDParentFolderID.getJSONArray("data");
                            JSONObject SDParentFolderIdDataObj = SDParentFolderIdDataArray.getJSONObject(0);
                            String SDParentFolderId = SDParentFolderIdDataObj.getString("id");
                            //查找文件对应文件夹
                            JSONObject ExistFolderIdObj = api.getFileID(SDAttribute + " " + PosADuty);
                            int SDArrFolderNum = Integer.parseInt(ExistFolderIdObj.getString("num"));
                            //如果不存在文件对应文件夹则先添加对应文件夹
                            if (SDArrFolderNum == 0) {
                                String att = (String) objectMap.get("fileCode");
                                JSONObject SDFolderIdObj = api.getAddFolder(SDParentFolderId, att.replace(" ", "").substring(att.lastIndexOf("-") + 1, att.replace(" ", "").length()) + " " + (String) objectMap.get("fileName"), SDAttribute + " " + PosADuty);

                                //JSONObject SDFolderIdObj=api.getAddFolder( SDParentFolderId,objectMap.get("fileCode").replace(" ", "").substring(objectMap.get("fileCode").lastIndexOf("-")+1,objectMap.get("fileCode").replace(" ", "").length()) +" "+(String) objectMap.get("fileName"), SDAttribute+" "+PosADuty);


                                //获取对应文件夹的FolderId，以便之后将文件放入该文件夹
                                addfileid = SDFolderIdObj.getString("fileId");
                            }
                            //如果存在文件对应文件夹则先需要找到其FolderId
                            else {
                                JSONArray ExistFolderIdDataArray = ExistFolderIdObj.getJSONArray("data");
                                JSONObject ExistFolderIdDataObj = ExistFolderIdDataArray.getJSONObject(0);
                                addfileid = ExistFolderIdDataObj.getString("id");
                                String att = (String) objectMap.get("fileCode");
                                api.getRename(token, addfileid, att.replace(" ", "").substring(att.lastIndexOf("-") + 1, att.replace(" ", "").length()) + " " + (String) objectMap.get("fileName"));
                                JSONObject ExitFolderListObj = api.getList(addfileid);
                                JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                                for (int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++) {
                                    JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                    String IsFolder = ExitFolderListDataObj.getString("fileType");
                                    String filename = ExitFolderListDataObj.getString("fileName");
                                    if (!IsFolder.equals("1")) {
                                        if (filename.contains(",")) {
                                            delId = ExitFolderListDataObj.getString("id");
                                            api.getRemove(token, delId);
                                            System.out.println(ExitFolderListDataObj.get("id"));
                                        }
                                    }
                                }
                            }
                            JSONObject FolderPathObj = api.getFullPath(addfileid);
                            String destfullpath = FolderPathObj.getString("fullPath");
                            JSONObject FileID = api.getFileID(SDAttribute);
                            int SDArrFileNum = Integer.parseInt(FileID.getString("num"));
                            if (SDArrFileNum == 0) {
                                continue;
                            }
                            JSONArray FileIDDataArray = FileID.getJSONArray("data");
                            JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                            String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                            String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                            JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                            fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                            String NewFileName = FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));
                            ;
                            if (FileIDDataArray.size() == 1) {
                                api.getMove(fileid, destfullpath);
                            }//有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的

                            else if (!addfileid.equals(Parentid2)) {

                                api.getMove(fileid, destfullpath);
//                                  api.getMove( fileid,destfullpath);

                                JSONObject ListFileAuth = api.getListFileAuth(token, fileid2);
                                JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                                int a = ListFileAuthDataArray.size();
                                for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++) {

                                    JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                    String RemoveAuthPos = ListFileAuthDataObj.getString("name");
                                    String auth = ListFileAuthDataObj.getString("auth");
                                    JSONObject array1 = api.getRole(RemoveAuthPos);
                                    String num = array1.getString("num");
                                    if (!num.equals("0")) {
                                        JSONArray array = array1.getJSONArray("data");
                                        JSONObject array2 = array.getJSONObject(0);
                                        String userId = array2.getString("id");
                                        api.getRoleAuth(RemoveAuthPos, userId, auth, fileid);
                                    }
                                }
                                api.getRemove(token, fileid2);
//                                  api.getRename(token, fileid2, NewFileName);
//                                  String MOVEUpdate = api.getUpdate(token,fileid, fileid2);


                            }
                            //有2个及以上相同扩展属性的文件时，A,B岗位未变更
                            else if (FileIDDataArray.size() > 1) {

                                //将老文件放入userFolder
                                JSONObject FolderPathObj01 = api.getFullPath("1688009");
                                String CAdestfullpath01 = FolderPathObj01.getString("fullPath");
                                api.getMove(fileid2, CAdestfullpath01);//将老文件放入userFolder
//                                  api.getRename(token, fileid2, "待删除");//先将老文件名改为"待删除"


                                api.getMove(fileid, destfullpath);//将新文件移动到对应文件夹，此时和老文件放在同一个文件夹
                                JSONObject ListFileAuth = api.getListFileAuth(token, fileid2);//获取老文件的权限
                                JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                                int a = ListFileAuthDataArray.size();
                                for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++) {//将老文件的权限授予新文件

                                    JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                    String RemoveAuthPos = ListFileAuthDataObj.getString("name");
                                    String auth = ListFileAuthDataObj.getString("auth");
                                    JSONObject array1 = api.getRole(RemoveAuthPos);
                                    String num = array1.getString("num");
                                    if (!num.equals("0")) {
                                        JSONArray array = array1.getJSONArray("data");
                                        JSONObject array2 = array.getJSONObject(0);
                                        String userId = array2.getString("id");
                                        api.getRoleAuth(RemoveAuthPos, userId, auth, fileid);
                                    }
                                }
                                api.getRemove(token, fileid2);//删除老文件
                            }
                            AuthFileId = fileid;
                            // 获取主责岗位角色及其上级直至董事长，并授权
                            RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                            RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                            RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                            RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                            RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                            if (PosA != null) {

                                RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                            }
                            if (PosB != null) {
                                PosBDuty = PosB;
                                PosBName = PosBName;
                                RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                            }

                        }
                    }

                }

                // 档案类型为RD的执行下面的程序
                if (ArchType.equalsIgnoreCase("08")) {


                    if (ArchType.equalsIgnoreCase("08")) {
                        int countlevel = 0;
                        String Sreial = (String) objectMap.get("fileCode");
                        while (Sreial.contains("-")) {
                            Sreial = Sreial.substring(Sreial.indexOf("-") + 1, Sreial.length());
                            countlevel++;

                        }

                        for (int q = 1; q < 6; q++) {
                            if (q <= countlevel) {
                                int Position = getCharacterPosition(attribute, q, "-");
                                String SplitCode = attribute.substring(0, Position);
                                prefix = SplitCode.substring(SplitCode.lastIndexOf("-") + 1, SplitCode.length());
                                String sqlFileCode = "select fname_l2 from [HG_LINK].[hg].dbo.T_fil_fileinfo WHERE FNUMBER ='" + SplitCode + "'";
                                List<Map<String, Object>> rsFileCode = sqlService.getList(CloudSqlService.htEas, sqlFileCode);
                                String FolderName = prefix + " " + (String) rsFileCode.get(0).get("fname_l2");
                                if (FolderName != null) {

                                    JSONObject FolderIdObj = api.getFileID(SplitCode + ' ' + MainDeptCode);// 分项文件夹是否存在
                                    int num = Integer.parseInt(FolderIdObj.getString("num"));
                                    if (num > 0) {
                                        JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                        JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                                        String FolderId = FolderIdObj0.getString("id");
                                        api.getRename(token, FolderId, FolderName);
                                    } else {
                                        if (q == 2) {
                                            parentattribute = MainDeptCode + " RD " + manageDeptSimpleName;
                                        } else {
                                            parentattribute = SplitCode.substring(0, SplitCode.lastIndexOf("-")) + ' ' + MainDeptCode;// 对应上级文件夹扩展属性
                                        }

                                        JSONObject FolderIdObj01 = api.getFileID(parentattribute);
                                        JSONArray FolderIdDataArray01 = FolderIdObj01.getJSONArray("data");
                                        JSONObject FolderIdObj001 = FolderIdDataArray01.getJSONObject(0);
                                        String FolderId = FolderIdObj001.getString("id");
                                        api.getAddFolder(FolderId, FolderName, SplitCode + ' ' + MainDeptCode);
                                    }

                                }


                            }
                        }
                    }


                    if (SubItemORNot > 0) {//带分项的文件
                        // 先加文件夹
                        prefix = attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length());//文件夹前缀
                        foldername = prefix + ' ' + (String) objectMap.get("fileName");//需要添加的文件夹名称（01 报告 02公司行政职能资信）


                        parentattribute = attribute.substring(0, attribute.lastIndexOf("-")) + ' ' + MainDeptCode;// 对应上级文件夹扩展属性

                        JSONObject FolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                        int SubFolderNum = Integer.parseInt(FolderIdObj.getString("num"));
                        if (SubFolderNum == 0) {
                            continue;
                        }
                        JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                        JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                        String folederid = FolderIdObj0.getString("id");
                        String SubFolderAttribute = attribute + ' ' + MainDeptCode;
                        JSONObject SubFolderIdObj = api.getFileID(SubFolderAttribute);// 分项文件夹是否存在
                        int num = Integer.parseInt(SubFolderIdObj.getString("num"));
                        if (num > 0) {
                            JSONArray SubFolderIdDataArray = SubFolderIdObj.getJSONArray("data");
                            JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(0);
                            String SubFolderId = SubFolderIdObj0.getString("id");
                            api.getRename(token, SubFolderId, foldername);
                        } else {
                            api.getAddFolder(folederid, foldername, attribute + ' ' + MainDeptCode);
                        }
                        //查询分项文件对应文件夹的FolderId
                        String ExistFolderAttribute = attribute + ' ' + MainDeptCode;
                        JSONObject ExistFolderObj = api.getFileID(ExistFolderAttribute);//
                        JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                        JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(0);
                        String ExitFolderId = ExistFolderObj0.getString("id");
                        AuthFileId = ExitFolderId;
                        JSONObject ExitFolderFullPath = api.getFullPath(ExitFolderId);
                        String SubItemFolderDestFullpath = ExitFolderFullPath.getString("fullPath");
                        JSONObject ExitFolderListObj = api.getList(ExitFolderId);
                        JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                        int m = ExitFolderListDataArray.size();
                        for (int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++) {
                            JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                            String IsFolder = ExitFolderListDataObj.getString("fileType");
                            if (!IsFolder.equals("1")) {
                                delId = ExitFolderListDataObj.getString("id");
                                api.getRemove(token, delId);
                                System.out.println(ExitFolderListDataObj.get("id"));
                            }
                        }
                        //移动公共区域的分项文件到对应路径
                        for (int Sub = 0; Sub < SubItemORNot; Sub++) {
                            String SubItemattribute = SubItemFileList[Sub].replace("，", ",");//02-A-01-16-02-14-01-01-02,01
                            JSONObject SubItemFileIdObj = api.getFileID(SubItemattribute);
                            JSONArray SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                            JSONObject SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                            String SubItemFileId = SubItemFileIdDataObj.getString("id");
//                      AuthFileId=SubItemFileId;
                            api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                            if (IsNomalFile == 1) {//有分项也有正常文件时候，正常文件也传过去
                                SubItemFileIdObj = api.getFileID((String) objectMap.get("fileCode"));
                                SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                SubItemFileId = SubItemFileIdDataObj.getString("id");
                                api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                            }

                            // 获取主责岗位角色及其上级直至董事长，并授权
                        }
                        RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                        RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                        RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                        if (PosA != null) {

                            RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                        }
                        if (PosB != null) {
                            PosBDuty = PosB;
                            PosBName = PosBName;
                            RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                        }
                        continue;
                    }


                    // 没有分项的情况
                    prefix = attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length());
                    FolderAttribute = attribute + ' ' + MainDeptCode;// 从表单获取文件夹扩展属性（=档案代码+主责部门）
                    addfileid = "";
                    desc = (String) objectMap.get("des");
                    // 如果是框架则在文档系统中加对应的文件夹
                    if (desc != null) {
                        if (desc.length() >= 2) {
                            if (desc.substring(0, 2).equalsIgnoreCase("框架")) {
                                // 添加文件夹到指定路径
                                // 根据扩展属性获取将添加的框架的parentId
                                if (ChaNum == 1) {
                                    parentattribute = MainDeptCode + " RD " + manageDeptSimpleName;
                                } else {
                                    parentattribute = attribute.substring(0, attribute.lastIndexOf("-")) + ' ' + MainDeptCode;// 对应上级文件夹扩展属性
                                }
                                attribute = (String) objectMap.get("fileCode");
                                attribute = attribute.replace(" ", "") + ' ' + MainDeptCode;
                                JSONObject ParentFolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id
                                int ParentFolderNum = Integer.parseInt(ParentFolderIdObj.getString("num"));
                                if (ParentFolderNum == 0) {
                                    continue;
                                }
                                JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                                JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                                String ParentFolderId = ParentFolderIdObj0.getString("id");
                                foldername = prefix + ' ' + (String) objectMap.get("fileName");// 从表单获取文件夹名称
                                String FrameAttribute = attribute;
                                JSONObject FrameAttributeObj = api.getFileID(FrameAttribute);
                                String FrameNum = FrameAttributeObj.getString("num");
                                int FrameNum01 = Integer.parseInt(FrameNum);
                                if (FrameNum01 > 0) {
                                    JSONArray FrameAttributeDataArray = FrameAttributeObj.getJSONArray("data");
                                    JSONObject FrameAttributeDataObj = FrameAttributeDataArray.getJSONObject(0);
                                    String fileId = FrameAttributeDataObj.getString("id");
                                    api.getRename(token, fileId, foldername);
                                } else {
                                    api.getAddFolder(ParentFolderId, foldername, FolderAttribute);
                                }
                            }
                        }
                    }

                    if (elec.equalsIgnoreCase("√") && SubItemORNot == 0 && IsNomalFile == 1) {

                        parentattribute = attribute.substring(0, attribute.lastIndexOf("-")) + ' ' + MainDeptCode;// 对应上级文件夹扩展属性

//                   String LevelParAttribute=attribute.substring(0, attribute.lastIndexOf("-"))+" "+MainDeptCode;//上级文件夹扩展属性SD-02-142331024-9002-31 02.06.07.003
                        JSONObject ParentFolderIdObj = api.getFileID(parentattribute);
                        int ParentFolderNum = Integer.parseInt(ParentFolderIdObj.getString("num"));
                        if (ParentFolderNum == 0) {
                            continue;
                        }
                        JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                        JSONObject ParentFolderIdDataObj = ParentFolderIdDataArray.getJSONObject(0);
                        String ParentFolderId = ParentFolderIdDataObj.getString("id");

                        //查找文件对应文件夹
                        JSONObject ExistFolderIdObj = api.getFileID(attribute + " " + MainDeptCode);
                        int ArrFolderNum = Integer.parseInt(ExistFolderIdObj.getString("num"));
                        JSONObject FolderIdObj = null;
                        if (ArrFolderNum == 0) {
                            //如果不存在文件对应文件夹则先添加对应文件夹
                            FolderIdObj = api.getAddFolder(ParentFolderId, attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length()) + " " + (String) objectMap.get("fileName"), attribute + " " + MainDeptCode);
                            //获取对应文件夹的FolderId，以便之后将文件放入该文件夹
                            addfileid = FolderIdObj.getString("fileId");
                        }
                        //如果存在文件对应文件夹则先需要找到其FolderId
                        else {
                            JSONArray ExistFolderIdDataArray = ExistFolderIdObj.getJSONArray("data");
                            JSONObject ExistFolderIdDataObj = ExistFolderIdDataArray.getJSONObject(0);
                            addfileid = ExistFolderIdDataObj.getString("id");
                            api.getRename(token, addfileid, attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length()) + " " + (String) objectMap.get("fileName"));
                            JSONObject ExitFolderListObj = api.getList(addfileid);
                            JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                            for (int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++) {
                                JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                String IsFolder = ExitFolderListDataObj.getString("fileType");
                                String filename = ExitFolderListDataObj.getString("fileName");
                                if (!IsFolder.equals("1")) {
                                    if (filename.contains(",")) {
                                        delId = ExitFolderListDataObj.getString("id");
                                        api.getRemove(token, delId);
                                        System.out.println(ExitFolderListDataObj.get("id"));
                                    }
                                }
                            }
                        }
                        JSONObject FolderPathObj = api.getFullPath(addfileid);
                        String destfullpath = FolderPathObj.getString("fullPath");
                        JSONObject FileID = api.getFileID(attribute);
                        int ArrFileNum = Integer.parseInt(FileID.getString("num"));
                        if (ArrFileNum == 0) {
                            continue;
                        }

                        JSONArray FileIDDataArray = FileID.getJSONArray("data");
                        JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                        String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                        String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                        JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                        fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                        String NewFileName = FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));
                        if (FileIDDataArray.size() == 1) {
                            api.getMove(fileid, destfullpath);
                        }//有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的

                        else if (!addfileid.equals(Parentid2)) {
                            api.getMove(fileid, destfullpath);
                            JSONObject ListFileAuth = api.getListFileAuth(token, fileid2);
                            JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                            int a = ListFileAuthDataArray.size();
                            for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++) {

                                JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                String RemoveAuthPos = ListFileAuthDataObj.getString("name");
                                String auth = ListFileAuthDataObj.getString("auth");
                                JSONObject array1 = api.getRole(RemoveAuthPos);
                                String num = array1.getString("num");
                                if (!num.equals("0")) {
                                    JSONArray array = array1.getJSONArray("data");
                                    JSONObject array2 = array.getJSONObject(0);
                                    String userId = array2.getString("id");
                                    api.getRoleAuth(RemoveAuthPos, userId, auth, fileid);
                                }
                            }

                            api.getRemove(token, fileid2);

//                         api.getRename(token, fileid2, NewFileName);
//                         String MOVEUpdate = api.getUpdate(token,fileid, fileid2);
                        }
                        //有2个及以上相同扩展属性的文件时，A,B岗位未变更
                        else if (FileIDDataArray.size() > 1) {
                            api.getRename(token, fileid2, "待删除");//先将老文件名改为"待删除"

                            //将老文件放入userFolder
                            JSONObject FolderPathObj01 = api.getFullPath("1688009");
                            String CAdestfullpath01 = FolderPathObj01.getString("fullPath");
                            api.getMove(fileid2, CAdestfullpath01);//将老文件放入userFolder


                            api.getMove(fileid, destfullpath);//将新文件移动到对应文件夹，此时和老文件放在同一个文件夹
                            JSONObject ListFileAuth = api.getListFileAuth(token, fileid2);//获取老文件的权限
                            JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                            int a = ListFileAuthDataArray.size();
                            for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++) {//将老文件的权限授予新文件

                                JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                String RemoveAuthPos = ListFileAuthDataObj.getString("name");
                                String auth = ListFileAuthDataObj.getString("auth");
                                JSONObject array1 = api.getRole(RemoveAuthPos);
                                String num = array1.getString("num");
                                if (!num.equals("0")) {
                                    JSONArray array = array1.getJSONArray("data");
                                    JSONObject array2 = array.getJSONObject(0);
                                    String userId = array2.getString("id");
                                    api.getRoleAuth(RemoveAuthPos, userId, auth, fileid);
                                }
                            }
                            api.getRemove(token, fileid2);//删除老文件
                        }


                        AuthFileId = fileid;
                        RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                        RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                        RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                        if (PosA != null) {
                            RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                        }
                        if (PosB != null) {
                            PosBDuty = PosB;
                            PosBName = PosBName;
                            RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                        }

                    }
                }


                if (ArchType.equalsIgnoreCase("05")) {
                    //DS-O
                    int countlevel = 0;
                    String Sreial = (String) objectMap.get("fileCode");
                    while (Sreial.contains("-")) {
                        Sreial = Sreial.substring(Sreial.indexOf("-") + 1, Sreial.length());
                        countlevel++;
                    }
                    if (countlevel == 2) {
                        parentattribute = MainDeptCode + " DS-O " + manageDeptSimpleName;// 对应上级文件夹扩展属性
                    } else {
                        String att = (String) objectMap.get("fileCode");
                        parentattribute = att.substring(0, att.indexOf("-"));
                    }
                    if (SubItemORNot > 0) {//带分项的文件
                        // 先加文件夹
                        prefix = attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length());//文件夹前缀
                        foldername = prefix + ' ' + (String) objectMap.get("fileName");//需要添加的文件夹名称（01 报告 02公司行政职能资信）
                        JSONObject FolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                        int SubFolderNum = Integer.parseInt(FolderIdObj.getString("num"));
                        if (SubFolderNum == 0) {
                            continue;
                        }
                        JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                        JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                        String folederid = FolderIdObj0.getString("id");
                        String SubFolderAttribute = attribute + ' ' + MainDeptCode;
                        JSONObject SubFolderIdObj = api.getFileID(SubFolderAttribute);// 分项文件夹是否存在
                        int num = Integer.parseInt(SubFolderIdObj.getString("num"));
                        if (num > 0) {
                            JSONArray SubFolderIdDataArray = SubFolderIdObj.getJSONArray("data");
                            JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(0);
                            String SubFolderId = SubFolderIdObj0.getString("id");
                            api.getRename(token, SubFolderId, foldername);
                        } else {
                            api.getAddFolder(folederid, foldername, attribute + ' ' + MainDeptCode);
                        }
                        //查询分项文件对应文件夹的FolderId
                        String ExistFolderAttribute = attribute + ' ' + MainDeptCode;
                        JSONObject ExistFolderObj = api.getFileID(ExistFolderAttribute);//
                        JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                        JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(0);
                        String ExitFolderId = ExistFolderObj0.getString("id");
                        AuthFileId = ExitFolderId;
                        JSONObject ExitFolderFullPath = api.getFullPath(ExitFolderId);
                        String SubItemFolderDestFullpath = ExitFolderFullPath.getString("fullPath");
                        JSONObject ExitFolderListObj = api.getList(ExitFolderId);
                        JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                        for (int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++) {
                            JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);

                            String IsFolder = ExitFolderListDataObj.getString("fileType");
                            if (!IsFolder.equals("1")) {
                                delId = ExitFolderListDataObj.getString("id");
                                api.getRemove(token, delId);
                                System.out.println(ExitFolderListDataObj.get("id"));
                            }
                        }
                        //移动公共区域的分项文件到对应路径
                        for (int Sub = 0; Sub < SubItemORNot; Sub++) {
                            String SubItemattribute = SubItemFileList[Sub].replace("，", ",");//02-A-01-16-02-14-01-01-02,01
                            JSONObject SubItemFileIdObj = api.getFileID(SubItemattribute);
                            JSONArray SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                            JSONObject SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                            String SubItemFileId = SubItemFileIdDataObj.getString("id");
//                      AuthFileId=SubItemFileId;
                            api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                            if (IsNomalFile == 1) {//有分项也有正常文件时候，正常文件也传过去
                                SubItemFileIdObj = api.getFileID((String) objectMap.get("fileCode"));
                                SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                SubItemFileId = SubItemFileIdDataObj.getString("id");
                                api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                            }

                            // 获取主责岗位角色及其上级直至董事长，并授权
                        }
                        RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                        RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                        RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                        if (PosA != null) {
                            RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                        }
                        if (PosB != null) {
                            PosBDuty = PosB;
                            PosBName = PosBName;
                            RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                        }
                        continue;
                    }


                    // 没有分项的情况
                    prefix = attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length());
                    FolderAttribute = attribute + ' ' + MainDeptCode;// 从表单获取文件夹扩展属性（=档案代码+主责部门）
                    addfileid = "";
                    desc = (String) objectMap.get("des");
                    // 如果是框架则在文档系统中加对应的文件夹
                    if (desc != null) {
                        if (desc.length() >= 2) {
                            if (desc.substring(0, 2).equalsIgnoreCase("框架")) {
                                // 添加文件夹到指定路径
                                // 根据扩展属性获取将添加的框架的parentId
                                attribute = (String) objectMap.get("fileCode");
                                attribute = attribute.replace(" ", "") + ' ' + MainDeptCode;
                                JSONObject ParentFolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id
                                int ParentFolderNum = Integer.parseInt(ParentFolderIdObj.getString("num"));
                                if (ParentFolderNum == 0) {
                                    continue;
                                }
                                JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                                JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                                String ParentFolderId = ParentFolderIdObj0.getString("id");
                                foldername = prefix + ' ' + (String) objectMap.get("fileName");// 从表单获取文件夹名称
                                String FrameAttribute = attribute;
                                JSONObject FrameAttributeObj = api.getFileID(FrameAttribute);
                                String FrameNum = FrameAttributeObj.getString("num");
                                int FrameNum01 = Integer.parseInt(FrameNum);
                                if (FrameNum01 > 0) {
                                    JSONArray FrameAttributeDataArray = FrameAttributeObj.getJSONArray("data");
                                    JSONObject FrameAttributeDataObj = FrameAttributeDataArray.getJSONObject(0);
                                    String fileId = FrameAttributeDataObj.getString("id");
                                    api.getRename(token, fileId, foldername);
                                } else {
                                    api.getAddFolder(ParentFolderId, foldername, FolderAttribute);
                                }
                            }
                        }
                    }

                    if (elec.equalsIgnoreCase("√") && SubItemORNot == 0 && IsNomalFile == 1) {
                        JSONObject ParentFolderIdObj = api.getFileID(parentattribute);
                        int ParentFolderNum = Integer.parseInt(ParentFolderIdObj.getString("num"));
                        if (ParentFolderNum == 0) {
                            continue;
                        }
                        JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                        JSONObject ParentFolderIdDataObj = ParentFolderIdDataArray.getJSONObject(0);
                        String ParentFolderId = ParentFolderIdDataObj.getString("id");

                        //查找文件对应文件夹
                        JSONObject ExistFolderIdObj = api.getFileID(attribute + " " + MainDeptCode);
                        int ArrFolderNum = Integer.parseInt(ExistFolderIdObj.getString("num"));
                        JSONObject FolderIdObj = null;
                        if (ArrFolderNum == 0) {
                            //如果不存在文件对应文件夹则先添加对应文件夹
                            FolderIdObj = api.getAddFolder(ParentFolderId, attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length()) + " " + (String) objectMap.get("fileName"), attribute + " " + MainDeptCode);
                            //获取对应文件夹的FolderId，以便之后将文件放入该文件夹
                            addfileid = FolderIdObj.getString("fileId");
                        }
                        //如果存在文件对应文件夹则先需要找到其FolderId
                        else {
                            JSONArray ExistFolderIdDataArray = ExistFolderIdObj.getJSONArray("data");
                            JSONObject ExistFolderIdDataObj = ExistFolderIdDataArray.getJSONObject(0);
                            addfileid = ExistFolderIdDataObj.getString("id");
                            api.getRename(token, addfileid, attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length()) + " " + (String) objectMap.get("fileName"));
                            JSONObject ExitFolderListObj = api.getList(addfileid);
                            JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                            for (int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++) {
                                JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                String IsFolder = ExitFolderListDataObj.getString("fileType");
                                String filename = ExitFolderListDataObj.getString("fileName");
                                if (!IsFolder.equals("1")) {
                                    if (filename.contains(",")) {
                                        delId = ExitFolderListDataObj.getString("id");
                                        api.getRemove(token, delId);
                                        System.out.println(ExitFolderListDataObj.get("id"));
                                    }
                                }
                            }
                        }
                        JSONObject FolderPathObj = api.getFullPath(addfileid);
                        String destfullpath = FolderPathObj.getString("fullPath");
                        JSONObject FileID = api.getFileID(attribute);
                        int ArrFileNum = Integer.parseInt(FileID.getString("num"));
                        if (ArrFileNum == 0) {
                            continue;
                        }

                        JSONArray FileIDDataArray = FileID.getJSONArray("data");
                        JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                        String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                        JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                        fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                        String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                        String NewFileName = FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));
                        if (FileIDDataArray.size() == 1) {
                            api.getMove(fileid, destfullpath);
                        }//有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的

                        else if (!addfileid.equals(Parentid2)) {
                            api.getMove(fileid, destfullpath);
                            JSONObject ListFileAuth = api.getListFileAuth(token, fileid2);
                            JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                            int a = ListFileAuthDataArray.size();
                            for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++) {

                                JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                String RemoveAuthPos = ListFileAuthDataObj.getString("name");
                                String auth = ListFileAuthDataObj.getString("auth");
                                JSONObject array1 = api.getRole(RemoveAuthPos);
                                String num = array1.getString("num");
                                if (!num.equals("0")) {
                                    JSONArray array = array1.getJSONArray("data");
                                    JSONObject array2 = array.getJSONObject(0);
                                    String userId = array2.getString("id");
                                    api.getRoleAuth(RemoveAuthPos, userId, auth, fileid);
                                }
                            }
//                         api.getRename(token, fileid2, NewFileName);
//                         String MOVEUpdate = api.getUpdate(token,fileid, fileid2);
                            api.getRemove(token, fileid2);
                        }
                        //有2个及以上相同扩展属性的文件时，A,B岗位未变更
                        else if (FileIDDataArray.size() > 1) {
                            api.getRename(token, fileid2, "待删除");//先将老文件名改为"待删除"

                            //将老文件放入userFolder
                            JSONObject FolderPathObj01 = api.getFullPath("1688009");
                            String CAdestfullpath01 = FolderPathObj01.getString("fullPath");
                            api.getMove(fileid2, CAdestfullpath01);//将老文件放入userFolder
                            api.getMove(fileid, destfullpath);//将新文件移动到对应文件夹，此时和老文件放在同一个文件夹
                            JSONObject ListFileAuth = api.getListFileAuth(token, fileid2);//获取老文件的权限
                            JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                            int a = ListFileAuthDataArray.size();
                            for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++) {//将老文件的权限授予新文件

                                JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                String RemoveAuthPos = ListFileAuthDataObj.getString("name");
                                String auth = ListFileAuthDataObj.getString("auth");
                                JSONObject array1 = api.getRole(RemoveAuthPos);
                                String num = array1.getString("num");
                                if (!num.equals("0")) {
                                    JSONArray array = array1.getJSONArray("data");
                                    JSONObject array2 = array.getJSONObject(0);
                                    String userId = array2.getString("id");
                                    api.getRoleAuth(RemoveAuthPos, userId, auth, fileid);
                                }
                            }
                            api.getRemove(token, fileid2);//删除老文件
                        }


                        AuthFileId = fileid;
                        RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                        RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                        RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                        if (PosA != null) {

                            RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                        }
                        if (PosB != null) {
                            PosBDuty = PosB;
                            PosBName = PosBName;
                            RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                        }

                    }
                }


                if (ArchType.equalsIgnoreCase("01")) {

                    parentattribute = MainDeptCode + " Ⅰ " + manageDeptSimpleName;// 对应上级文件夹扩展属性

                    if (SubItemORNot > 0) {//带分项的文件
                        // 先加文件夹
                        prefix = attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length());//文件夹前缀
                        foldername = prefix + ' ' + (String) objectMap.get("fileName");//需要添加的文件夹名称（01 报告 02公司行政职能资信）
                        JSONObject FolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                        int SubFolderNum = Integer.parseInt(FolderIdObj.getString("num"));
                        if (SubFolderNum == 0) {
                            continue;
                        }
                        JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                        JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                        String folederid = FolderIdObj0.getString("id");
                        String SubFolderAttribute = attribute + ' ' + MainDeptCode;
                        JSONObject SubFolderIdObj = api.getFileID(SubFolderAttribute);// 分项文件夹是否存在
                        int num = Integer.parseInt(SubFolderIdObj.getString("num"));
                        if (num > 0) {
                            JSONArray SubFolderIdDataArray = SubFolderIdObj.getJSONArray("data");
                            JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(0);
                            String SubFolderId = SubFolderIdObj0.getString("id");
                            api.getRename(token, SubFolderId, foldername);
                        } else {
                            api.getAddFolder(folederid, foldername, attribute + ' ' + MainDeptCode);
                        }
                        //查询分项文件对应文件夹的FolderId
                        String ExistFolderAttribute = attribute + ' ' + MainDeptCode;
                        JSONObject ExistFolderObj = api.getFileID(ExistFolderAttribute);//
                        JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                        JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(0);
                        String ExitFolderId = ExistFolderObj0.getString("id");
                        AuthFileId = ExitFolderId;
                        JSONObject ExitFolderFullPath = api.getFullPath(ExitFolderId);
                        String SubItemFolderDestFullpath = ExitFolderFullPath.getString("fullPath");
                        JSONObject ExitFolderListObj = api.getList(ExitFolderId);
                        JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                        for (int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++) {
                            JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);

                            String IsFolder = ExitFolderListDataObj.getString("fileType");
                            if (!IsFolder.equals("1")) {
                                delId = ExitFolderListDataObj.getString("id");
                                api.getRemove(token, delId);
                                System.out.println(ExitFolderListDataObj.get("id"));
                            }
                        }
                        //移动公共区域的分项文件到对应路径
                        for (int Sub = 0; Sub < SubItemORNot; Sub++) {
                            String SubItemattribute = SubItemFileList[Sub].replace("，", ",");//02-A-01-16-02-14-01-01-02,01
                            JSONObject SubItemFileIdObj = api.getFileID(SubItemattribute);
                            JSONArray SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                            JSONObject SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                            String SubItemFileId = SubItemFileIdDataObj.getString("id");
//                      AuthFileId=SubItemFileId;
                            api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                            if (IsNomalFile == 1) {//有分项也有正常文件时候，正常文件也传过去
                                SubItemFileIdObj = api.getFileID((String) objectMap.get("fileCode"));
                                SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                SubItemFileId = SubItemFileIdDataObj.getString("id");
                                api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                            }

                            // 获取主责岗位角色及其上级直至董事长，并授权
                        }
                        RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                        RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                        RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                        if (PosA != null) {
                            RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                        }
                        if (PosB != null) {
                            PosBDuty = PosB;
                            PosBName = PosBName;
                            RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                        }
                        continue;
                    }


                    // 没有分项的情况
                    prefix = attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length());
                    FolderAttribute = attribute + ' ' + MainDeptCode;// 从表单获取文件夹扩展属性（=档案代码+主责部门）
                    addfileid = "";
                    desc = (String) objectMap.get("des");
                    // 如果是框架则在文档系统中加对应的文件夹
                    if (desc != null) {
                        if (desc.length() >= 2) {
                            if (desc.substring(0, 2).equalsIgnoreCase("框架")) {
                                // 添加文件夹到指定路径
                                // 根据扩展属性获取将添加的框架的parentId
                                attribute = (String) objectMap.get("fileCode");
                                attribute = attribute.replace(" ", "") + ' ' + MainDeptCode;
                                JSONObject ParentFolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id
                                int ParentFolderNum = Integer.parseInt(ParentFolderIdObj.getString("num"));
                                if (ParentFolderNum == 0) {
                                    continue;
                                }
                                JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                                JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                                String ParentFolderId = ParentFolderIdObj0.getString("id");
                                foldername = prefix + ' ' + (String) objectMap.get("fileName");// 从表单获取文件夹名称
                                String FrameAttribute = attribute;
                                JSONObject FrameAttributeObj = api.getFileID(FrameAttribute);
                                String FrameNum = FrameAttributeObj.getString("num");
                                int FrameNum01 = Integer.parseInt(FrameNum);
                                if (FrameNum01 > 0) {
                                    JSONArray FrameAttributeDataArray = FrameAttributeObj.getJSONArray("data");
                                    JSONObject FrameAttributeDataObj = FrameAttributeDataArray.getJSONObject(0);
                                    String fileId = FrameAttributeDataObj.getString("id");
                                    api.getRename(token, fileId, foldername);
                                } else {
                                    api.getAddFolder(ParentFolderId, foldername, FolderAttribute);
                                }
                            }
                        }
                    }

                    if (elec.equalsIgnoreCase("√") && SubItemORNot == 0 && IsNomalFile == 1) {
                        JSONObject ParentFolderIdObj = api.getFileID(parentattribute);
                        int ParentFolderNum = Integer.parseInt(ParentFolderIdObj.getString("num"));
                        if (ParentFolderNum == 0) {
                            continue;
                        }
                        JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                        JSONObject ParentFolderIdDataObj = ParentFolderIdDataArray.getJSONObject(0);
                        String ParentFolderId = ParentFolderIdDataObj.getString("id");

                        //查找文件对应文件夹
                        JSONObject ExistFolderIdObj = api.getFileID(attribute + " " + MainDeptCode);
                        int ArrFolderNum = Integer.parseInt(ExistFolderIdObj.getString("num"));
                        JSONObject FolderIdObj = null;
                        if (ArrFolderNum == 0) {
                            //如果不存在文件对应文件夹则先添加对应文件夹
                            FolderIdObj = api.getAddFolder(ParentFolderId, attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length()) + " " + (String) objectMap.get("fileName"), attribute + " " + MainDeptCode);
                            //获取对应文件夹的FolderId，以便之后将文件放入该文件夹
                            addfileid = FolderIdObj.getString("fileId");
                        }
                        //如果存在文件对应文件夹则先需要找到其FolderId
                        else {
                            JSONArray ExistFolderIdDataArray = ExistFolderIdObj.getJSONArray("data");
                            JSONObject ExistFolderIdDataObj = ExistFolderIdDataArray.getJSONObject(0);
                            addfileid = ExistFolderIdDataObj.getString("id");
                            api.getRename(token, addfileid, attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length()) + " " + (String) objectMap.get("fileName"));
                            JSONObject ExitFolderListObj = api.getList(addfileid);
                            JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                            for (int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++) {
                                JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                String IsFolder = ExitFolderListDataObj.getString("fileType");
                                String filename = ExitFolderListDataObj.getString("fileName");
                                if (!IsFolder.equals("1")) {
                                    if (filename.contains(",")) {
                                        delId = ExitFolderListDataObj.getString("id");
                                        api.getRemove(token, delId);
                                        System.out.println(ExitFolderListDataObj.get("id"));
                                    }
                                }
                            }
                        }
                        JSONObject FolderPathObj = api.getFullPath(addfileid);
                        String destfullpath = FolderPathObj.getString("fullPath");
                        JSONObject FileID = api.getFileID(attribute);
                        int ArrFileNum = Integer.parseInt(FileID.getString("num"));
                        if (ArrFileNum == 0) {
                            continue;
                        }

                        JSONArray FileIDDataArray = FileID.getJSONArray("data");
                        JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                        String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                        JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                        fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                        String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                        String NewFileName = FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));
                        if (FileIDDataArray.size() == 1) {
                            api.getMove(fileid, destfullpath);
                        }//有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的

                        else if (!addfileid.equals(Parentid2)) {
                            api.getMove(fileid, destfullpath);


                            JSONObject ListFileAuth = api.getListFileAuth(token, fileid2);
                            JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                            int a = ListFileAuthDataArray.size();
                            for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++) {

                                JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                String RemoveAuthPos = ListFileAuthDataObj.getString("name");
                                String auth = ListFileAuthDataObj.getString("auth");
                                JSONObject array1 = api.getRole(RemoveAuthPos);
                                String num = array1.getString("num");
                                if (!num.equals("0")) {
                                    JSONArray array = array1.getJSONArray("data");
                                    JSONObject array2 = array.getJSONObject(0);
                                    String userId = array2.getString("id");
                                    api.getRoleAuth(RemoveAuthPos, userId, auth, fileid);
                                }
                            }
//                         api.getRename(token, fileid2, NewFileName);
//                         String MOVEUpdate = api.getUpdate(token,fileid, fileid2);
                            api.getRemove(token, fileid2);
                        }
                        //有2个及以上相同扩展属性的文件时，A,B岗位未变更
                        else if (FileIDDataArray.size() > 1) {
                            api.getRename(token, fileid2, "待删除");//先将老文件名改为"待删除"

                            //将老文件放入userFolder
                            JSONObject FolderPathObj01 = api.getFullPath("1688009");
                            String CAdestfullpath01 = FolderPathObj01.getString("fullPath");
                            api.getMove(fileid2, CAdestfullpath01);//将老文件放入userFolder


                            api.getMove(fileid, destfullpath);//将新文件移动到对应文件夹，此时和老文件放在同一个文件夹
                            JSONObject ListFileAuth = api.getListFileAuth(token, fileid2);//获取老文件的权限
                            JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                            int a = ListFileAuthDataArray.size();
                            for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++) {//将老文件的权限授予新文件

                                JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                String RemoveAuthPos = ListFileAuthDataObj.getString("name");
                                String auth = ListFileAuthDataObj.getString("auth");
                                JSONObject array1 = api.getRole(RemoveAuthPos);
                                String num = array1.getString("num");
                                if (!num.equals("0")) {
                                    JSONArray array = array1.getJSONArray("data");
                                    JSONObject array2 = array.getJSONObject(0);
                                    String userId = array2.getString("id");
                                    api.getRoleAuth(RemoveAuthPos, userId, auth, fileid);
                                }
                            }
                            api.getRemove(token, fileid2);//删除老文件
                        }


                        AuthFileId = fileid;
                        RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                        RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                        RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                        if (PosA != null) {
                            RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                        }
                        if (PosB != null) {
                            PosBDuty = PosB;
                            PosBName = PosBName;
                            RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                        }

                    }


                }


                if (ArchType.equalsIgnoreCase("03") || ArchType.equalsIgnoreCase("02") || ArchType.equalsIgnoreCase("04")) {
                    int countlevel = 0;
                    String Sreial = (String) objectMap.get("fileCode");
                    shouwei = Sreial.substring(0, 1);
                    FolderAttr = "";
                    attribute = (String) objectMap.get("fileCode");
                    attribute = attribute.replace(" ", "");
                    while (Sreial.contains("-")) {
                        Sreial = Sreial.substring(Sreial.indexOf("-") + 1, Sreial.length());
                        countlevel++;
                    }

                    if (elec.equalsIgnoreCase("×") || !shouwei.equals("0")) {
                        continue;
                    }
                    if (countlevel >= 2) {
                        OTTFileName = attribute;

                        //加第一级框架   02-Ⅲ11 02.07
                        int PositionOri1 = getCharacterPosition(OTTFileName, 2, "-");
                        String SplitCode1 = OTTFileName.substring(OTTFileName.indexOf("-") + 1, PositionOri1);//Ⅲ11
                        String sqlFileCode011 = "select fname_l2 ,fjobdesc from [HG_LINK].[hg].dbo.T_fil_fileinfo WHERE ffilestatus ='启用' and FNUMBER ='" + SplitCode1 + "'";
                        List<Map<String, Object>> rsFileCode011 = sqlService.getList(CloudSqlService.htEas, sqlFileCode011);
                        try {
                            String fjobdesc = (String) rsFileCode011.get(0).get("fjobdesc");
                            if (fjobdesc.contains("框架")) {
                                prefix = SplitCode1.substring(SplitCode1.length() - 2, SplitCode1.length());
                                String FolderName = prefix + " " + (String) rsFileCode011.get(0).get("fname_l2");
                                FolderAttr = attribute.substring(0, attribute.indexOf("-")) + "-" + SplitCode1 + ' ' + MainDeptCode;// 02-Ⅲ11 02.07
                                JSONObject FolderIdObj = api.getFileID(FolderAttr);//
                                int num = Integer.parseInt(FolderIdObj.getString("num"));
                                if (num > 0) {
                                    JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                    JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                                    String FolderId = FolderIdObj0.getString("id");
                                    api.getRename(token, FolderId, FolderName);
                                } else {
                                    int countlevel01 = 0;
                                    String Sreial01 = FolderAttr;//02-Ⅲ11 02.07
                                    attribute = (String) rsFileCode011.get(0).get("fileCode");
                                    attribute = attribute.replace(" ", "");
                                    while (Sreial01.contains("-")) {
                                        Sreial01 = Sreial01.substring(Sreial01.indexOf("-") + 1, Sreial01.length());
                                        countlevel01++;

                                    }
                                    if (countlevel01 == 1) {
                                        parentattribute = MainDeptCode + " " + FolderAttr.substring(FolderAttr.indexOf("-") + 1, FolderAttr.indexOf("-") + 2) + ' ' + manageDeptSimpleName;// 对应上级文件夹扩展属性
                                    } else {
                                        parentattribute = FolderAttr.substring(0, FolderAttr.lastIndexOf("-")) + ' ' + MainDeptCode;// 对应上级文件夹扩展属性
                                    }
                                    JSONObject FolderIdObj01 = api.getFileID(parentattribute);
                                    JSONArray FolderIdDataArray01 = FolderIdObj01.getJSONArray("data");
                                    JSONObject FolderIdObj001 = FolderIdDataArray01.getJSONObject(0);
                                    String FolderId = FolderIdObj001.getString("id");
                                    api.getAddFolder(FolderId, FolderName, FolderAttr);
                                }
                            }


                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }


                        //加第二、三、四。。。。级框架   02-Ⅲ11-01 02.07
                        for (int Senum = 3; Senum < countlevel + 1; Senum++) {
                            int PositionOri = getCharacterPosition(OTTFileName, Senum, "-");
                            String SplitCode = OTTFileName.substring(OTTFileName.indexOf("-") + 1, PositionOri);
                            String SplitCodeCom = OTTFileName.substring(0, PositionOri);
                            String sqlFileCode01 = "select fname_l2 ,isnull(fjobdesc,'') as fjobdesc from [HG_LINK].[hg].dbo.T_fil_fileinfo WHERE ffilestatus ='启用' and FNUMBER ='" + SplitCode + "'";
                            List<Map<String, Object>> rsFileCode01 = sqlService.getList(CloudSqlService.htEas, sqlFileCode01);
                            try {
                                String fjobdesc = (String) rsFileCode01.get(0).get("fjobdesc");
                                if (fjobdesc.contains("框架")) {
                                    prefix = SplitCode.substring(SplitCode.lastIndexOf("-") + 1, SplitCode.length());
                                    String FolderName = prefix + " " + (String) rsFileCode01.get(0).get("fname_l2");
                                    FolderAttr = attribute.substring(0, attribute.indexOf("-")) + "-" + SplitCode + ' ' + MainDeptCode;//
                                    JSONObject FolderIdObj = api.getFileID(FolderAttr);//
                                    int num = Integer.parseInt(FolderIdObj.getString("num"));
                                    if (num > 0) {
                                        JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                        JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                                        String FolderId = FolderIdObj0.getString("id");
                                        api.getRename(token, FolderId, FolderName);
                                    } else {
                                        parentattribute = FolderAttr.substring(0, FolderAttr.lastIndexOf("-")) + ' ' + MainDeptCode;// 对应上级文件夹扩展属性
                                        JSONObject FolderIdObj01 = api.getFileID(parentattribute);
                                        JSONArray FolderIdDataArray01 = FolderIdObj01.getJSONArray("data");
                                        JSONObject FolderIdObj001 = FolderIdDataArray01.getJSONObject(0);
                                        String FolderId = FolderIdObj001.getString("id");
                                        api.getAddFolder(FolderId, FolderName, FolderAttr);
                                    }
                                }

                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }


                            String sqlFileCodeCom01 = "select fname_l2 ,isnull(fjobdesc,'') as fjobdesc from [HG_LINK].[hg].dbo.T_fil_fileinfo WHERE ffilestatus ='启用' and FNUMBER ='" + SplitCodeCom + "'";
                            List<Map<String, Object>> rsFileCodeCom01 = sqlService.getList(CloudSqlService.htEas, sqlFileCodeCom01);
                            try {
                                prefix = SplitCodeCom.substring(SplitCodeCom.lastIndexOf("-") + 1, SplitCodeCom.length());
                                String FolderName = prefix + " " + (String) rsFileCodeCom01.get(0).get("fname_l2");
                                FolderAttr = SplitCodeCom + ' ' + MainDeptCode;//
                                JSONObject FolderIdObj = api.getFileID(FolderAttr);//
                                int num = Integer.parseInt(FolderIdObj.getString("num"));
                                if (num > 0) {
                                    JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                    JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                                    String FolderId = FolderIdObj0.getString("id");
                                    api.getRename(token, FolderId, FolderName);
                                }


                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }

                        }

                        parentattribute = attribute.substring(attribute.indexOf("-") + 1, attribute.lastIndexOf("-"));// 对应上级文件夹扩展属性
                    }

                    parentattribute = attribute.substring(0, attribute.lastIndexOf("-")) + ' ' + MainDeptCode;// 对应上级文件夹扩展属性

                    if (SubItemORNot > 0) {//带分项的文件
                        // 先加文件夹
                        prefix = attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length());//文件夹前缀
                        foldername = prefix + ' ' + (String) objectMap.get("fileName");//需要添加的文件夹名称
                        JSONObject FolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                        int SubFolderNum = Integer.parseInt(FolderIdObj.getString("num"));
                        if (SubFolderNum == 0) {
                            continue;
                        }
                        JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                        JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                        String folederid = FolderIdObj0.getString("id");
                        String SubFolderAttribute = attribute + ' ' + MainDeptCode;
                        JSONObject SubFolderIdObj = api.getFileID(SubFolderAttribute);// 分项文件夹是否存在
                        int num = Integer.parseInt(SubFolderIdObj.getString("num"));
                        if (num > 0) {
                            JSONArray SubFolderIdDataArray = SubFolderIdObj.getJSONArray("data");
                            JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(0);
                            String SubFolderId = SubFolderIdObj0.getString("id");
                            api.getRename(token, SubFolderId, foldername);
                        } else {
                            api.getAddFolder(folederid, foldername, attribute + ' ' + MainDeptCode);
                        }
                        //查询分项文件对应文件夹的FolderId
                        String ExistFolderAttribute = attribute + ' ' + MainDeptCode;
                        JSONObject ExistFolderObj = api.getFileID(ExistFolderAttribute);//
                        JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                        JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(0);
                        String ExitFolderId = ExistFolderObj0.getString("id");
                        AuthFileId = ExitFolderId;
                        JSONObject ExitFolderFullPath = api.getFullPath(ExitFolderId);
                        String SubItemFolderDestFullpath = ExitFolderFullPath.getString("fullPath");
                        JSONObject ExitFolderListObj = api.getList(ExitFolderId);
                        JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                        for (int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++) {
                            JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                            String IsFolder = ExitFolderListDataObj.getString("fileType");
                            if (!IsFolder.equals("1")) {
                                delId = ExitFolderListDataObj.getString("id");
                                api.getRemove(token, delId);
                                System.out.println(ExitFolderListDataObj.get("id"));
                            }
                        }
                        //移动公共区域的分项文件到对应路径
                        for (int Sub = 0; Sub < SubItemORNot; Sub++) {
                            String asggsd = attribute + ' ' + MainDeptCode;
                            String SubItemattribute = SubItemFileList[Sub].replace("，", ",");//02-A-01-16-02-14-01-01-02,01
                            JSONObject SubItemFileIdObj = api.getFileID(SubItemattribute);
                            JSONArray SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                            int sgg = SubItemFileIdDataArray.size();
                            JSONObject SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(SubItemFileIdDataArray.size() - 1);
                            String SubItemFileId = SubItemFileIdDataObj.getString("id");
//                      AuthFileId=SubItemFileId;
                            api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                            if (IsNomalFile == 1) {//有分项也有正常文件时候，正常文件也传过去
                                SubItemFileIdObj = api.getFileID((String) objectMap.get("fileCode"));
                                SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                SubItemFileId = SubItemFileIdDataObj.getString("id");
                                api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                            }

                            // 获取主责岗位角色及其上级直至董事长，并授权
                        }
                        RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                        RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                        RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                        if (PosA != null) {
                            RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                        }
                        if (PosB != null) {
                            PosBDuty = PosB;
                            PosBName = PosBName;
                            RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                        }
                        continue;
                    }


                    // 没有分项的情况
                    prefix = attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length());
                    FolderAttribute = attribute + ' ' + MainDeptCode;// 从表单获取文件夹扩展属性（=档案代码+主责部门）
                    addfileid = "";
                    desc = (String) objectMap.get("des");
                    // 如果是框架则在文档系统中加对应的文件夹
                    if (desc != null) {
                        if (desc.length() >= 2) {
                            if (desc.contains("框架")) {
                                shouwei = attribute.substring(0, 1);
                                if (shouwei.equals("0")) {
                                    FolderAttribute = attribute + ' ' + MainDeptCode;
                                    String att = (String) objectMap.get("fileCode");
                                    attribute = att.replace(" ", "") + ' ' + MainDeptCode;
                                    parentattribute = attribute.substring(0, attribute.lastIndexOf("-")) + ' ' + MainDeptCode;// 对应上级文件夹扩展属性
                                } else {
                                    FolderAttribute = MainDeptCode + "-" + attribute + ' ' + MainDeptCode;
                                    String att = (String) objectMap.get("fileCode");
                                    attribute = MainDeptCode + "-" + att.replace(" ", "") + ' ' + MainDeptCode;
                                    parentattribute = attribute.substring(0, attribute.lastIndexOf("-")) + ' ' + MainDeptCode;// 对应上级文件夹扩展属性
                                }


                                JSONObject ParentFolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id
                                int ParentFolderNum = Integer.parseInt(ParentFolderIdObj.getString("num"));
                                if (ParentFolderNum == 0) {
                                    continue;
                                }
                                JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                                JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                                String ParentFolderId = ParentFolderIdObj0.getString("id");
                                foldername = prefix + ' ' + (String) objectMap.get("fileName");// 从表单获取文件夹名称
                                String FrameAttribute = attribute;
                                JSONObject FrameAttributeObj = api.getFileID(FrameAttribute);
                                String FrameNum = FrameAttributeObj.getString("num");
                                int FrameNum01 = Integer.parseInt(FrameNum);
                                if (FrameNum01 > 0) {
                                    JSONArray FrameAttributeDataArray = FrameAttributeObj.getJSONArray("data");
                                    JSONObject FrameAttributeDataObj = FrameAttributeDataArray.getJSONObject(0);
                                    String fileId = FrameAttributeDataObj.getString("id");
                                    api.getRename(token, fileId, foldername);
                                } else {
                                    api.getAddFolder(ParentFolderId, foldername, FolderAttribute);
                                }
                            }
                        }
                    }

                    if (elec.equalsIgnoreCase("√") && SubItemORNot == 0 && IsNomalFile == 1) {
                        parentattribute = attribute.substring(0, attribute.lastIndexOf("-")) + ' ' + MainDeptCode;// 对应上级文件夹扩展属性
                        JSONObject ParentFolderIdObj = api.getFileID(parentattribute);
                        int ParentFolderNum = Integer.parseInt(ParentFolderIdObj.getString("num"));
                        if (ParentFolderNum == 0) {
                            continue;
                        }
                        JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                        JSONObject ParentFolderIdDataObj = ParentFolderIdDataArray.getJSONObject(0);
                        String ParentFolderId = ParentFolderIdDataObj.getString("id");

                        //查找文件对应文件夹
                        JSONObject ExistFolderIdObj = api.getFileID(attribute + " " + MainDeptCode);
                        int ArrFolderNum = Integer.parseInt(ExistFolderIdObj.getString("num"));
                        JSONObject FolderIdObj = null;
                        if (ArrFolderNum == 0) {
                            //如果不存在文件对应文件夹则先添加对应文件夹
                            FolderIdObj = api.getAddFolder(ParentFolderId, attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length()) + " " + (String) objectMap.get("fileName"), attribute + " " + MainDeptCode);
                            //获取对应文件夹的FolderId，以便之后将文件放入该文件夹
                            addfileid = FolderIdObj.getString("fileId");
                        }
                        //如果存在文件对应文件夹则先需要找到其FolderId,并删除其中的分项文件
                        else {
                            JSONArray ExistFolderIdDataArray = ExistFolderIdObj.getJSONArray("data");
                            JSONObject ExistFolderIdDataObj = ExistFolderIdDataArray.getJSONObject(0);
                            addfileid = ExistFolderIdDataObj.getString("id");
                            api.getRename(token, addfileid, attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length()) + " " + (String) objectMap.get("fileName"));
                            JSONObject ExitFolderListObj = api.getList(addfileid);
                            JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                            for (int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++) {
                                JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                String IsFolder = ExitFolderListDataObj.getString("fileType");
                                String filename = ExitFolderListDataObj.getString("fileName");
                                if (!IsFolder.equals("1")) {
                                    if (filename.contains(",")) {
                                        delId = ExitFolderListDataObj.getString("id");
                                        api.getRemove(token, delId);
                                        System.out.println(ExitFolderListDataObj.get("id"));
                                    }
                                }
                            }


                        }
                        JSONObject FolderPathObj = api.getFullPath(addfileid);
                        String destfullpath = FolderPathObj.getString("fullPath");
                        JSONObject FileID = api.getFileID(attribute);
                        int ArrFileNum = Integer.parseInt(FileID.getString("num"));
                        if (ArrFileNum == 0) {
                            continue;
                        }

                        JSONArray FileIDDataArray = FileID.getJSONArray("data");
                        JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                        String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                        String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                        JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                        fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                        String NewFileName = FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));
                        if (FileIDDataArray.size() == 1) {
                            api.getMove(fileid, destfullpath);
                        } else if (!addfileid.equals(Parentid2)) {
                            api.getRename(token, fileid2, "待删除");//先将老文件名改为"待删除"

                            api.getMove(fileid, destfullpath);

                            JSONObject ListFileAuth = api.getListFileAuth(token, fileid2);
                            JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                            int a = ListFileAuthDataArray.size();
                            for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++) {

                                JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                String RemoveAuthPos = ListFileAuthDataObj.getString("name");
                                String auth = ListFileAuthDataObj.getString("auth");
                                JSONObject array1 = api.getRole(RemoveAuthPos);
                                String num = array1.getString("num");
                                if (!num.equals("0")) {
                                    JSONArray array = array1.getJSONArray("data");
                                    JSONObject array2 = array.getJSONObject(0);
                                    String userId = array2.getString("id");
                                    api.getRoleAuth(RemoveAuthPos, userId, auth, fileid);
                                }
                            }
                            api.getRemove(token, fileid2);
                        }
                        //有2个及以上相同扩展属性的文件时，A,B岗位未变更
                        else if (FileIDDataArray.size() > 1) {
                            api.getRename(token, fileid2, "待删除");//先将老文件名改为"待删除"
                            //将老文件放入userFolder
                            JSONObject FolderPathObj01 = api.getFullPath("1688009");
                            String CAdestfullpath01 = FolderPathObj01.getString("fullPath");
                            api.getMove(fileid2, CAdestfullpath01);//将老文件放入userFolder


                            api.getMove(fileid, destfullpath);//将新文件移动到对应文件夹，此时和老文件放在同一个文件夹
                            JSONObject ListFileAuth = api.getListFileAuth(token, fileid2);//获取老文件的权限
                            JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                            int a = ListFileAuthDataArray.size();
                            for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++) {//将老文件的权限授予新文件

                                JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                String RemoveAuthPos = ListFileAuthDataObj.getString("name");
                                String auth = ListFileAuthDataObj.getString("auth");
                                JSONObject array1 = api.getRole(RemoveAuthPos);
                                String num = array1.getString("num");
                                if (!num.equals("0")) {
                                    JSONArray array = array1.getJSONArray("data");
                                    JSONObject array2 = array.getJSONObject(0);
                                    String userId = array2.getString("id");
                                    api.getRoleAuth(RemoveAuthPos, userId, auth, fileid);
                                }
                            }
                            api.getRemove(token, fileid2);//删除老文件
                        }
                        AuthFileId = fileid;

                        RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                        RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                        RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                        if (PosA != null) {
                            RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                        }
                        if (PosB != null) {
                            PosBDuty = PosB;
                            PosBName = PosBName;
                            RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                        }
                    }
                }
                if (ArchType.equalsIgnoreCase("09") || ArchType.equalsIgnoreCase("10")) {
                    int Position02 = getCharacterPosition(attribute, 2, "-");
                    int countlevel = 1;
                    String Sreial01 = attribute.substring(attribute.indexOf("-") + 1, Position02);
                    ;//201607
                    String Sreial02 = attribute.substring(10, 13);//002
                    String Sreial03 = "";//01
                    ParentFolderAttr = "";
                    String YearFolder = "";
                    String YearMonthFolder = "";
                    String SerialFolder = "";
                    if (elec.equalsIgnoreCase("×")) {
                        continue;
                    }
                    String FileAttr = (String) objectMap.get("fileCode");
                    String Sreial = (String) objectMap.get("fileCode");
                    while (Sreial.contains("-")) {
                        Sreial = Sreial.substring(Sreial.indexOf("-") + 1, Sreial.length());
                        countlevel++;

                    }
                    Sreial = attribute.substring(attribute.indexOf("-") + 1, attribute.lastIndexOf("-"));

                    String DeptFolderAttr = MainDeptCode + " PD " + manageDeptSimpleName;//02.01 PD 综管部
                    JSONObject DeptFolderObj = api.getFileID(DeptFolderAttr);
                    JSONArray DeptFolderDataArray = DeptFolderObj.getJSONArray("data");
                    JSONObject DeptFolderDataObj = DeptFolderDataArray.getJSONObject(0);
                    String DeptFolderId = DeptFolderDataObj.getString("id");
                    if (ArchType.equalsIgnoreCase("09")) {
                        YearFolder = "PD" + "-" + Sreial01.substring(0, 4) + " " + MainDeptCode;//PD-2006 02.01
                    }
                    if (ArchType.equalsIgnoreCase("10")) {
                        YearFolder = "BK" + "-" + Sreial01.substring(0, 4) + " " + MainDeptCode;//PD-2006 02.01
                    }
                    api.getAddFolder(DeptFolderId, Sreial01.substring(0, 4) + "年", YearFolder);
                    JSONObject YearFolderObj = api.getFileID(YearFolder);
                    JSONArray YearFolderDataArray = YearFolderObj.getJSONArray("data");
                    JSONObject YearFolderDataObj = YearFolderDataArray.getJSONObject(0);
                    String YearFolderId = YearFolderDataObj.getString("id");

                    if (ArchType.equalsIgnoreCase("09")) {
                        YearMonthFolder = "PD" + "-" + Sreial01 + " " + MainDeptCode;//PD-200603 02.01
                    }
                    if (ArchType.equalsIgnoreCase("10")) {
                        YearMonthFolder = "BK" + "-" + Sreial01 + " " + MainDeptCode;//BK-200603 02.01
                    }
                    JSONObject YearMonthFolderObj = api.getFileID(YearMonthFolder);
                    int YearMonthNum = Integer.parseInt(YearMonthFolderObj.getString("num"));
                    if (YearMonthNum == 0) {
                        api.getAddFolder(YearFolderId, Sreial01.substring(4, 6) + "月", YearMonthFolder);
                    } else {
                        JSONArray YearMonthFolderDataArray = YearMonthFolderObj.getJSONArray("data");
                        JSONObject YearMonthFolderDataObj = YearMonthFolderDataArray.getJSONObject(0);
                        String YearMonthFolderId = YearMonthFolderDataObj.getString("id");
                        api.getRename(token, YearMonthFolderId, Sreial01.substring(4, 6) + "月");

                    }

                    JSONObject FinalYearMonthFolderObj = api.getFileID(YearMonthFolder);
                    JSONArray FinalYearMonthFolderArray = FinalYearMonthFolderObj.getJSONArray("data");
                    JSONObject FinalYearMonthFolderDataObj = FinalYearMonthFolderArray.getJSONObject(0);
                    String FinalYearMonthFolderId = FinalYearMonthFolderDataObj.getString("id");
                    if (ArchType.equalsIgnoreCase("09")) {
                        SerialFolder = "PD" + "-" + Sreial01 + "-" + Sreial02 + " " + MainDeptCode;//PD-200603-002 02.01
                    }
                    if (ArchType.equalsIgnoreCase("10")) {
                        SerialFolder = "BK" + "-" + Sreial01 + "-" + Sreial02 + " " + MainDeptCode;//PD-200603-002 02.01
                    }
                    JSONObject SerialFolderObj = api.getFileID(SerialFolder);
                    int SerialFolderNum = Integer.parseInt(SerialFolderObj.getString("num"));
                    if (SerialFolderNum == 0) {
                        api.getAddFolder(FinalYearMonthFolderId, Sreial02, SerialFolder);
                        JSONObject FinalSerialFolderObj = api.getFileID(SerialFolder);
                        JSONArray FinalSerialFolderDataArray = FinalSerialFolderObj.getJSONArray("data");
                        JSONObject FinalSerialFolderDataObj = FinalSerialFolderDataArray.getJSONObject(0);
                        String FinalSerialFolderId = FinalSerialFolderDataObj.getString("id");


                        if (countlevel == 4) {
                            int Position03 = getCharacterPosition(attribute, 3, "-");
                            Sreial03 = attribute.substring(Position03 + 1, attribute.length());//02
                            FolderAttr = attribute + " " + MainDeptCode;//PD-200603-002-01 02.01
                            JSONObject FolderAttrObj = api.getFileID(FolderAttr);
                            int FolderAttrNum = Integer.parseInt(FolderAttrObj.getString("num"));
                            if (FolderAttrNum == 0) {
                                api.getAddFolder(FinalSerialFolderId, Sreial03 + " " + (String) objectMap.get("fileName"), attribute + " " + MainDeptCode);
                            } else {
                                JSONArray FolderAttrDataArray = FolderAttrObj.getJSONArray("data");
                                JSONObject FolderAttrDataObj = FolderAttrDataArray.getJSONObject(0);
                                String FolderAttrId = FolderAttrDataObj.getString("id");
                                api.getRename(token, FolderAttrId, attribute + " " + MainDeptCode);

                            }

                        }
                    }


                    if (SubItemORNot > 0) {//带分项的文件
                        // 先加文件夹
                        prefix = attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length());//文件夹前缀
                        foldername = prefix + ' ' + (String) objectMap.get("fileName");//需要添加的文件夹名称（01 报告 02公司行政职能资信）


                        parentattribute = attribute.substring(0, attribute.lastIndexOf("-")) + ' ' + MainDeptCode;// 对应上级文件夹扩展属性

                        JSONObject FolderIdObjN03 = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                        int SubFolderNum = Integer.parseInt(FolderIdObjN03.getString("num"));
                        if (SubFolderNum == 0) {
                            continue;
                        }
                        JSONArray FolderIdDataArrayN03 = FolderIdObjN03.getJSONArray("data");
                        JSONObject FolderIdObjN003 = FolderIdDataArrayN03.getJSONObject(0);
                        String folederid = FolderIdObjN003.getString("id");
                        String SubFolderAttribute = attribute + ' ' + MainDeptCode;
                        JSONObject SubFolderIdObj = api.getFileID(SubFolderAttribute);// 分项文件夹是否存在
                        int num = Integer.parseInt(SubFolderIdObj.getString("num"));
                        if (num > 0) {
                            JSONArray SubFolderIdDataArray = SubFolderIdObj.getJSONArray("data");
                            JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(0);
                            String SubFolderId = SubFolderIdObj0.getString("id");
                            api.getRename(token, SubFolderId, foldername);
                        } else {
                            api.getAddFolder(folederid, foldername, attribute + ' ' + MainDeptCode);
                        }
                        //查询分项文件对应文件夹的FolderId
                        String ExistFolderAttribute = attribute + ' ' + MainDeptCode;
                        JSONObject ExistFolderObj = api.getFileID(ExistFolderAttribute);//
                        JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                        JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(0);
                        String ExitFolderId = ExistFolderObj0.getString("id");
                        AuthFileId = ExitFolderId;
                        JSONObject ExitFolderFullPath = api.getFullPath(ExitFolderId);
                        String SubItemFolderDestFullpath = ExitFolderFullPath.getString("fullPath");
                        JSONObject ExitFolderListObj = api.getList(ExitFolderId);
                        JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                        for (int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++) {
                            JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);

                            String IsFolder = ExitFolderListDataObj.getString("fileType");
                            if (!IsFolder.equals("1")) {
                                delId = ExitFolderListDataObj.getString("id");
                                api.getRemove(token, delId);
                                System.out.println(ExitFolderListDataObj.get("id"));
                            }
                        }
                        //移动公共区域的分项文件到对应路径
                        for (int Sub = 0; Sub < SubItemORNot; Sub++) {
                            String asggsd = attribute + ' ' + MainDeptCode;
                            String SubItemattribute = SubItemFileList[Sub].replace("，", ",");//02-A-01-16-02-14-01-01-02,01
                            JSONObject SubItemFileIdObj = api.getFileID(SubItemattribute);
                            JSONArray SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                            JSONObject SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                            String SubItemFileId = SubItemFileIdDataObj.getString("id");
//                   AuthFileId=SubItemFileId;
                            api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                            if (IsNomalFile == 1) {//有分项也有正常文件时候，正常文件也传过去
                                SubItemFileIdObj = api.getFileID((String) objectMap.get("fileCode"));
                                SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                SubItemFileId = SubItemFileIdDataObj.getString("id");
                                api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                            }

                            // 获取主责岗位角色及其上级直至董事长，并授权
                        }
                        RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                        RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                        RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                        if (PosA != null) {
                            RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                        }
                        if (PosB != null) {
                            PosBDuty = PosB;
                            PosBName = PosBName;
                            RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                        }
                        continue;
                    }

                    if (elec.equalsIgnoreCase("√") && IsNomalFile == 0) {
                        continue;
                    }

                    if (elec.equalsIgnoreCase("√") && IsNomalFile == 1) {
                        ParentFolderAttr = (String) objectMap.get("fileCode");
                        ParentFolderAttr = ParentFolderAttr.substring(0, ParentFolderAttr.lastIndexOf("-")) + " " + MainDeptCode;
                        JSONObject FolderIdObjNEW01 = api.getFileID(ParentFolderAttr);
                        JSONArray FolderIdDataArrayNEW01 = FolderIdObjNEW01.getJSONArray("data");
                        JSONObject FolderIdDataObjNEW01 = FolderIdDataArrayNEW01.getJSONObject(0);
                        String FParentFileId01 = FolderIdDataObjNEW01.getString("id");

                        String FolderAttr01 = objectMap.get("fileCode") + " " + MainDeptCode;
                        JSONObject FolderIdObjNEW = api.getFileID(FolderAttr01);
                        int FileNum = Integer.parseInt(FolderIdObjNEW.getString("num"));
                        String FolName = attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length()) + " " + (String) objectMap.get("fileName");

                        if (FileNum > 0) {
                            JSONArray FolderIdDataArrayNEW = FolderIdObjNEW.getJSONArray("data");

                            JSONObject FolderIdDataObjNEW = FolderIdDataArrayNEW.getJSONObject(0);
                            String FParentFileId = FolderIdDataObjNEW.getString("id");
                            api.getRename(token, FParentFileId, FolName);
                        } else {
                            api.getAddFolder(FParentFileId01, FolName, attribute + " " + MainDeptCode);

                        }
                        ParentFolderAttr = objectMap.get("fileCode") + " " + MainDeptCode;

                    }
//                else{//
//                   ParentFolderAttr=  objectMap.get("fileCode").substring(0, objectMap.get("fileCode").lastIndexOf("-"))+" "+MainDeptCode;
//                }
                    JSONObject ParentFolderIdObj = api.getFileID(ParentFolderAttr);
                    JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                    JSONObject ParentFolderIdDataObj = ParentFolderIdDataArray.getJSONObject(0);
                    String FParentFileId = ParentFolderIdDataObj.getString("id");
                    JSONObject FolderPathObj = api.getFullPath(FParentFileId);
                    String CAdestfullpath = FolderPathObj.getString("fullPath");
                    JSONObject FileID = api.getFileID(FileAttr);
                    int CAArrFileNum = Integer.parseInt(FileID.getString("num"));
                    if (CAArrFileNum == 0) {
                        continue;
                    }

                    JSONArray FileIDDataArray = FileID.getJSONArray("data");
                    JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                    String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                    String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                    JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                    fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                    String NewFileName = FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));
                    if (FileIDDataArray.size() == 1) {
                        api.getMove(fileid, CAdestfullpath);
                    }//有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的
                    else if (!addfileid.equals(Parentid2)) {
                        api.getMove(fileid, CAdestfullpath);


                        JSONObject ListFileAuth = api.getListFileAuth(token, fileid2);
                        JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                        int a = ListFileAuthDataArray.size();
                        for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++) {

                            JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                            String RemoveAuthPos = ListFileAuthDataObj.getString("name");
                            String auth = ListFileAuthDataObj.getString("auth");
                            JSONObject array1 = api.getRole(RemoveAuthPos);
                            String num = array1.getString("num");
                            if (!num.equals("0")) {
                                JSONArray array = array1.getJSONArray("data");
                                JSONObject array2 = array.getJSONObject(0);
                                String userId = array2.getString("id");
                                api.getRoleAuth(RemoveAuthPos, userId, auth, fileid);
                            }
                        }

//                   api.getRename(token, fileid2, NewFileName);
//                   String MOVEUpdate = api.getUpdate(token,fileid, fileid2);
                        api.getRemove(token, fileid2);
                    }
                    //有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的
                    else if (FileIDDataArray.size() > 1) {
//                   api.getRemove(token, fileid2);
//                   api.getMove( fileid,destfullpath);

                        api.getRename(token, fileid2, NewFileName);
                        String MOVEUpdate = api.getUpdate(token, fileid, fileid2);
                        System.out.print(MOVEUpdate);
                    }
                    //有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的


                    JSONObject FileID01 = api.getFileID(FileAttr);
                    JSONArray FileIDDataArray01 = FileID01.getJSONArray("data");
                    JSONObject FileIDDataObj01 = FileIDDataArray01.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                    String fileid3 = FileIDDataObj01.getString("id");
                    AuthFileId = fileid3;
                    RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                    RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                    RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                    RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                    RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                    if (PosA != null) {
                        RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                    }
                    if (PosB != null) {
                        PosBDuty = PosB;
                        PosBName = PosBName;
                        RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                    }
                }


                if (ArchType.equalsIgnoreCase("07")) {

                    desc = (String) objectMap.get("des");// 获取说明
                    String InfoCode = (String) objectMap.get("fileCode");
                    InfoCode = InfoCode.replace(" ", "");//DS-F-0106-043
                    int Position02 = getCharacterPosition(attribute, 2, "-");
                    int Position03 = getCharacterPosition(attribute, 3, "-");
//                String InfoCode01=InfoCode.substring(InfoCode.indexOf("-")+1, InfoCode.length());//01060301-01
                    String InfoCode02 = InfoCode.substring(Position02 + 1, Position03);//01060301
                    String ParentAttributeFNum = "";
                    String FParentAttribute = "";
                    String FAttribute = "";
                    String trans = (String) objectMap.get("fileCode");
                    elec = (String) subSheet.get(0).get("elec");
                    int count = 0;
                    int level = 0;//"-"的个数,level=2时才需要判断文件夹属性是否带leaf
                    String FIsLeaf = "1";
                    String FLevelParAttribute = "";
                    System.out.println(FLevelParAttribute);
                    while (trans.contains("-")) {
                        trans = trans.substring(trans.indexOf("-") + 1, trans.length());
                        level++;
                    }

                    int InfoCodeFLen = InfoCode02.length();//0106的长度
                    for (int s = 0; s < InfoCodeFLen; s++) {
                        String FCode = InfoCode02.substring(0, s + 1);
                        String sqlFCode = "select fname_l2 ,isnull(fisleaf,'0')  fisleaf from [HG_LINK].[hg].dbo.T_man_siteinfo WHERE FNUMBER ='" + FCode + "'";
                        List<Map<String, Object>> rsFCode = sqlService.getList(CloudSqlService.htEas, sqlFCode);
                        try {
                            String FName = (String) rsFCode.get(0).get("fname_l2");
                            FIsLeaf = (String) rsFCode.get(0).get("fisleaf");
                            if (count == 0) {
                                FParentAttribute = MainDeptCode + " DS-F " + manageDeptSimpleName;//
                            } else {
                                FParentAttribute = "DS-F-" + ParentAttributeFNum + " " + MainDeptCode;//
                            }
                            FAttribute = "DS-F" + "-" + FCode + " " + MainDeptCode;//F-01 02.09.004
                            JSONObject FFileIdObj = api.getFileID(FAttribute);
                            int num = Integer.parseInt(FFileIdObj.getString("num"));
//                      if(num==1)
//                      {
//                         continue;
//                      }
                            //添加对应文件夹
                            JSONObject FParentFileID = api.getFileID(FParentAttribute);
                            int FParentFileNum = Integer.parseInt(FParentFileID.getString("num"));
                            if (FParentFileNum == 0) {
                                continue;
                            }
                            JSONArray FParentFileIdDataArray = FParentFileID.getJSONArray("data");
                            JSONObject FParentFileIdDataObj = FParentFileIdDataArray.getJSONObject(0);
                            String FParentFileId = FParentFileIdDataObj.getString("id");
                            if (num == 0) {
                                JSONObject FolderAddObj = api.getAddFolder(FParentFileId, FCode + " " + FName, FAttribute);
                            }
                            if (num > 0) {
                                JSONArray FolderIdDataArray = FFileIdObj.getJSONArray("data");
                                JSONObject FolderIdDataObj = FolderIdDataArray.getJSONObject(0);
                                String FolderId = FolderIdDataObj.getString("id");
                                api.getRename(token, FolderId, FCode + " " + FName);
                            }
                            JSONObject FolderAddObj = api.getFileID(FAttribute);

                            String FolderId = "";
                            if (FolderAddObj.containsValue("fileId")) {
                                FolderId = FolderAddObj.getString("fileId");
                            } else {
                                JSONObject FolderIdObj = api.getFileID(FAttribute);
                                JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                                JSONObject FolderIdDataObj = FolderIdDataArray.getJSONObject(0);
                                FolderId = FolderIdDataObj.getString("id");
                            }
                            if (FIsLeaf.equals("0")) {

                                FFileIdObj = api.getFileID(FAttribute + " leaf");
                                num = Integer.parseInt(FFileIdObj.getString("num"));
                                if (num == 0) {
                                    api.getAddFolder(FolderId, FCode + " " + FName, FAttribute + " leaf");
                                }
                                if (num > 0) {
                                    FFileIdObj = api.getFileID(FAttribute + " leaf");
                                    JSONArray FolderIdDataArray = FFileIdObj.getJSONArray("data");
                                    JSONObject FolderIdDataObj = FolderIdDataArray.getJSONObject(0);
                                    FolderId = FolderIdDataObj.getString("id");
                                    api.getRename(token, FolderId, FCode + " " + FName);
                                }


                            }
                            ParentAttributeFNum = FCode;
                            count++;


                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }


                    }

                    if (level == 2) {
                        String sqlFCode01 = "select fname_l2 ,isnull(fisleaf,'0')  fisleaf from [HG_LINK].[hg].dbo.T_man_siteinfo WHERE FNUMBER ='" + InfoCode02 + "'";
                        List<Map<String, Object>> rsFCode01 = sqlService.getList(CloudSqlService.htEas, sqlFCode01);
                        try {
                            FIsLeaf = (String) rsFCode01.get(0).get("fisleaf");

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                    }
                    //带分项的文件
                    if (SubItemORNot > 0) {
                        //带分项的文件
                        // 先加文件夹
                        String att = (String) objectMap.get("fileCode");
                        prefix = att.replace(" ", "").substring(att.lastIndexOf("-") + 1, att.replace(" ", "").length());//文件夹前缀
                        foldername = prefix + ' ' + (String) objectMap.get("fileName");//需要添加的文件夹名称（01 报告 02公司行政职能资信）
                        if (FIsLeaf.equals("0") && level == 2) {
                            att = (String) objectMap.get("fileCode");
                            parentattribute = att.substring(0, att.replace(" ", "").lastIndexOf("-")) + ' ' + MainDeptCode + " leaf";
                        } else {
                            att = (String) objectMap.get("fileCode");
                            parentattribute = att.replace(" ", "").substring(0, att.replace(" ", "").lastIndexOf("-")) + ' ' + MainDeptCode;
                        }
                        JSONObject FolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                        int FolderNum = Integer.parseInt(FolderIdObj.getString("num"));
                        if (FolderNum == 0) {
                            continue;
                        }
                        JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                        JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                        String folederid = FolderIdObj0.getString("id");
                        att = (String) objectMap.get("fileCode");
                        String SubFolderAttribute = att.replace(" ", "") + ' ' + MainDeptCode;
                        JSONObject SubFolderIdObj = api.getFileID(SubFolderAttribute);// 分项文件夹是否存在
                        int num = Integer.parseInt(SubFolderIdObj.getString("num"));
                        if (num > 0) {
                            JSONArray SubFolderIdDataArray = SubFolderIdObj.getJSONArray("data");
                            JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(0);
                            String SubFolderId = SubFolderIdObj0.getString("id");
                            api.getRename(token, SubFolderId, foldername);
                        } else {
                            att = (String) objectMap.get("fileCode");
                            api.getAddFolder(folederid, foldername, att.replace(" ", "") + ' ' + MainDeptCode);
                        }
                        //查询分项文件对应文件夹的FolderId
                        String ExistFolderAttribute = (String) objectMap.get("fileCode");
                        ExistFolderAttribute = ExistFolderAttribute.replace(" ", "") + ' ' + MainDeptCode;
                        JSONObject ExistFolderObj = api.getFileID(ExistFolderAttribute);//
                        JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                        JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(0);
                        String ExitFolderId = ExistFolderObj0.getString("id");
                        AuthFileId = ExitFolderId;
                        JSONObject ExitFolderFullPath = api.getFullPath(ExitFolderId);
                        String SubItemFolderDestFullpath = ExitFolderFullPath.getString("fullPath");
                        JSONObject ExitFolderListObj = api.getList(ExitFolderId);
                        JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                        for (int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++) {
                            JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);


                            String IsFolder = ExitFolderListDataObj.getString("fileType");
                            if (!IsFolder.equals("1")) {
                                delId = ExitFolderListDataObj.getString("id");
                                api.getRemove(token, delId);
                                System.out.println(ExitFolderListDataObj.get("id"));
                            }
                        }
                        //移动公共区域的分项文件到对应路径
                        for (int Sub = 0; Sub < SubItemORNot; Sub++) {
                            String SubItemattribute = SubItemFileList[Sub].replace("，", ",");//02-A-01-16-02-14-01-01-02,01
                            JSONObject SubItemFileIdObj = api.getFileID(SubItemattribute);
                            JSONArray SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                            JSONObject SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                            String SubItemFileId = SubItemFileIdDataObj.getString("id");
//                   AuthFileId=SubItemFileId;
                            api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                            if (IsNomalFile == 1) {//有分项也有正常文件时候，正常文件也传过去
                                SubItemFileIdObj = api.getFileID((String) objectMap.get("fileCode"));
                                SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                SubItemFileId = SubItemFileIdDataObj.getString("id");
                                api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                            }

                            // 获取主责岗位角色及其上级直至董事长，并授权

                        }

                        RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                        RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                        RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                        if (PosA != null) {
                            RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                        }
                        if (PosB != null) {
                            PosBDuty = PosB;
                            PosBName = PosBName;
                            RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                        }

                        continue;
                    }
//                InfoCodeCount=InfoCode.substring(InfoCode.indexOf("-")+1, InfoCode.length());//02-142331024-9002-31-02
//                InfoCodeCount=InfoCodeCount.substring(InfoCodeCount.indexOf("-")+1, InfoCodeCount.length());//142331024-9002-31-02
//                InfoCodeCount=InfoCodeCount.substring(InfoCodeCount.indexOf("-")+1, InfoCodeCount.length());//9002-31-02

                    // 如果是框架则在文档系统中加对应的文件夹
                    if (desc != null) {
                        if (desc.length() >= 2) {
                            if (desc.contains("框架")) {
                                FAttribute = (String) objectMap.get("fileCode");
                                FAttribute = FAttribute.replace(" ", "") + " " + MainDeptCode;
                                if (FIsLeaf.equals("0") && level == 2) {
                                    FLevelParAttribute = FAttribute.substring(0, FAttribute.lastIndexOf("-")) + " " + MainDeptCode + " leaf";
                                } else {
                                    FLevelParAttribute = FAttribute.substring(0, FAttribute.lastIndexOf("-")) + " " + MainDeptCode;
                                }
                                JSONObject FParentFileID = api.getFileID(FLevelParAttribute);
//                if(!SDParentFileID.containsValue("data")){continue;}
                                JSONArray FParentFileIdDataArray = FParentFileID.getJSONArray("data");
                                JSONObject FParentFileIdDataObj = FParentFileIdDataArray.getJSONObject(0);
                                String FParentFileId = FParentFileIdDataObj.getString("id");
                                String att = (String) objectMap.get("fileCode");
                                api.getAddFolder(FParentFileId, att.replace(" ", "").substring(att.lastIndexOf("-") + 1, att.replace(" ", "").length()) + " " + (String) objectMap.get("fileName"), FAttribute);
                            }
                        }
                    }
                    if (elec.equalsIgnoreCase("√") && IsNomalFile == 1) {//不是分项的文件
                        FAttribute = (String) objectMap.get("fileCode");
                        if (FIsLeaf.equals("0") && level == 3) {
                            FLevelParAttribute = FAttribute.substring(0, FAttribute.lastIndexOf("-")) + " " + MainDeptCode + " leaf";

                        } else {
                            FLevelParAttribute = FAttribute.substring(0, FAttribute.lastIndexOf("-")) + " " + MainDeptCode;
                        }
//                      FLevelParAttribute=FAttribute.substring(0, FAttribute.lastIndexOf("-"))+" "+PosADuty;//上级文件夹扩展属性SD-02-142331024-9002-31 02.06.07.003
                        JSONObject FParentFolderID = api.getFileID(FLevelParAttribute);
                        int FParentFolderNum = Integer.parseInt(FParentFolderID.getString("num"));
                        if (FParentFolderNum == 0) {
                            continue;
                        }
                        JSONArray FParentFolderIdDataArray = FParentFolderID.getJSONArray("data");
                        JSONObject FParentFolderIdDataObj = FParentFolderIdDataArray.getJSONObject(0);
                        String FParentFolderId = FParentFolderIdDataObj.getString("id");
                        //查找文件对应文件夹
                        JSONObject ExistFolderIdObj = api.getFileID(FAttribute + " " + MainDeptCode);
                        int FArrFolderNum = Integer.parseInt(ExistFolderIdObj.getString("num"));
                        //如果不存在文件对应文件夹则先添加对应文件夹
                        if (FArrFolderNum == 0) {
                            String asd = (String) objectMap.get("fileCode");
                            JSONObject FFolderIdObj = api.getAddFolder(FParentFolderId, asd.replace(" ", "").substring(asd.lastIndexOf("-") + 1, asd.replace(" ", "").length()) + " " + (String) objectMap.get("fileName"), FAttribute + " " + MainDeptCode);
                            //获取对应文件夹的FolderId，以便之后将文件放入该文件夹
                            addfileid = FFolderIdObj.getString("fileId");
                        }
                        //如果存在文件对应文件夹则先需要找到其FolderId
                        else {
                            JSONArray ExistFolderIdDataArray = ExistFolderIdObj.getJSONArray("data");
                            JSONObject ExistFolderIdDataObj = ExistFolderIdDataArray.getJSONObject(0);
                            addfileid = ExistFolderIdDataObj.getString("id");
                            String att = (String) objectMap.get("fileCode");
                            api.getRename(token, addfileid, att.replace(" ", "").substring(att.lastIndexOf("-") + 1, att.replace(" ", "").length()) + " " + (String) objectMap.get("fileName"));
                            JSONObject ExitFolderListObj = api.getList(addfileid);
                            JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                            for (int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++) {
                                JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                String IsFolder = ExitFolderListDataObj.getString("fileType");
                                String filename = ExitFolderListDataObj.getString("fileName");
                                if (!IsFolder.equals("1")) {
                                    if (filename.contains(",")) {
                                        delId = ExitFolderListDataObj.getString("id");
                                        api.getRemove(token, delId);
                                        System.out.println(ExitFolderListDataObj.get("id"));
                                    }
                                }
                            }
                        }
                        JSONObject FolderPathObj = api.getFullPath(addfileid);
                        String destfullpath = FolderPathObj.getString("fullPath");
                        JSONObject FileID = api.getFileID(FAttribute);
                        int FArrFileNum = Integer.parseInt(FileID.getString("num"));
                        if (FArrFileNum == 0) {
                            continue;
                        }
                        JSONArray FileIDDataArray = FileID.getJSONArray("data");
                        JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                        String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                        JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                        fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                        String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                        String NewFileName = FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));
                        ;
                        if (FileIDDataArray.size() == 1) {
                            api.getMove(fileid, destfullpath);
                        }//有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的
                        else if (!addfileid.equals(Parentid2)) {
                            api.getMove(fileid, destfullpath);


                            JSONObject ListFileAuth = api.getListFileAuth(token, fileid2);
                            JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                            int a = ListFileAuthDataArray.size();
                            for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++) {

                                JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                String RemoveAuthPos = ListFileAuthDataObj.getString("name");
                                String auth = ListFileAuthDataObj.getString("auth");
                                JSONObject array1 = api.getRole(RemoveAuthPos);
                                String num = array1.getString("num");
                                if (!num.equals("0")) {
                                    JSONArray array = array1.getJSONArray("data");
                                    JSONObject array2 = array.getJSONObject(0);
                                    String userId = array2.getString("id");
                                    api.getRoleAuth(RemoveAuthPos, userId, auth, fileid);
                                }
                            }
//                          api.getRename(token, fileid2, NewFileName);
//                          String MOVEUpdate = api.getUpdate(token,fileid, fileid2);
                            api.getRemove(token, fileid2);
                        }
                        //有2个及以上相同扩展属性的文件时，A,B岗位未变更
                        else if (FileIDDataArray.size() > 1) {
//                          api.getRename(token, fileid2, "待删除");//先将老文件名改为"待删除"


                            //将老文件放入userFolder
                            JSONObject FolderPathObj01 = api.getFullPath("1688009");
                            String CAdestfullpath01 = FolderPathObj01.getString("fullPath");
                            api.getMove(fileid2, CAdestfullpath01);//将老文件放入userFolder

                            api.getMove(fileid, destfullpath);//将新文件移动到对应文件夹，此时和老文件放在同一个文件夹
                            JSONObject ListFileAuth = api.getListFileAuth(token, fileid2);//获取老文件的权限
                            JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                            int a = ListFileAuthDataArray.size();
                            for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++) {//将老文件的权限授予新文件

                                JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                String RemoveAuthPos = ListFileAuthDataObj.getString("name");
                                String auth = ListFileAuthDataObj.getString("auth");
                                JSONObject array1 = api.getRole(RemoveAuthPos);
                                String num = array1.getString("num");
                                if (!num.equals("0")) {
                                    JSONArray array = array1.getJSONArray("data");
                                    JSONObject array2 = array.getJSONObject(0);
                                    String userId = array2.getString("id");
                                    api.getRoleAuth(RemoveAuthPos, userId, auth, fileid);
                                }
                            }
                            api.getRemove(token, fileid2);//删除老文件
                        }
                        AuthFileId = fileid;
                        // 获取主责岗位角色及其上级直至董事长，并授权
                        if (PosMainDuty != null) {
                            PosMainDuty = MainDeptCode;
                            PosMainName = MainDeptName;
                            RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                            RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);
                            while (FAttribute.contains("-")) {
                                FAttribute = FAttribute.substring(0, FAttribute.lastIndexOf("-"));
                                JSONObject AuthFileID = api.getFileID(FAttribute);
                                int FArrAuthFileNum = Integer.parseInt(AuthFileID.getString("num"));
                                if (FArrAuthFileNum != 0) {


                                    JSONArray AuthFileIDDataArray = AuthFileID.getJSONArray("data");
                                    JSONObject AuthFileIDDataObj = AuthFileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                                    String Authfileid = AuthFileIDDataObj.getString("id");//最早上传的老文件的fileid2
                                    RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                                    RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                                    RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                                    RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                                    RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                                    if (PosA != null) {
                                        RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                                    }
                                    if (PosB != null) {
                                        PosBDuty = PosB;
                                        PosBName = PosBName;
                                        RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                                    }
                                }
                            }

                        }


                    }

                }
                if (ArchType.equalsIgnoreCase("06") || ArchType.equalsIgnoreCase("19") || ArchType.substring(0, 2).equalsIgnoreCase("18")) {
                    desc = (String) objectMap.get("des");// 获取说明
                    String InfoCode = (String) objectMap.get("baseCode");//21071456545411
//                   objectMap.get("fileCode").replace(" ", "");//DS-A-21071456545411-07
                    String MatCode = (String) objectMap.get("fileCode");

                    String OrgCode = (String) objectMap.get("mainComYingshe");

                    String MatType01 = InfoCode.substring(0, 2);//02
                    String MatType02 = InfoCode.substring(2, 4);//05
                    String MatType03 = InfoCode.substring(4, 6);//12
                    String MatType04 = InfoCode.substring(6, InfoCode.length());//12

                    String strSql01 = "SELECT FName_L2 as name FROM dbo.t_bd_materialgroup where fnumber='" + MatType01 + "'";
                    List<Map<String, Object>> rs01 = sqlService.getList(CloudSqlService.htEas, strSql01);
                    try {

                        String MaterialName01 = (String) rs01.get(0).get("name");
                        if (ArchType.equalsIgnoreCase("06")) {
                            ParentFolderAttr = manageDept + " DS" + "-A " + manageDeptSimpleName;
                            FolderAttr = "DS" + "-A" + "-" + MatType01 + " " + manageDept;//
                        }
                        if (ArchType.equalsIgnoreCase("19")) {
                            ParentFolderAttr = manageDept + " GAS " + manageDeptSimpleName;
                            FolderAttr = "AS" + "-" + MatType01 + " " + manageDept;//
                        }
                        if (ArchType.substring(0, 2).equalsIgnoreCase("18")) {
                            ParentFolderAttr = PosADuty.substring(0, PosADuty.lastIndexOf(".")) + " GWD " + MatCode.substring(2, 4) + " " + deptsimplename;
                            FolderAttr = MatCode.substring(0, 5) + MatType01 + " " + PosADuty.substring(0, PosADuty.lastIndexOf("."));//
                        }

                        JSONObject ParentFolderIdObj = api.getFileID(ParentFolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                        JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                        JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                        String ParentFolderId = ParentFolderIdObj0.getString("id");
                        String FolderName = MatType01 + " " + MaterialName01;


                        JSONObject FolderIdObj = api.getFileID(FolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                        int SubFolderNum = Integer.parseInt(FolderIdObj.getString("num"));
                        if (SubFolderNum == 0) {

                            api.getAddFolder(ParentFolderId, MatType01 + " " + MaterialName01, FolderAttr);
                        } else {
                            JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                            JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                            ParentFolderId = FolderIdObj0.getString("id");
                            api.getRename(token, ParentFolderId, FolderName);
                        }


                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                    String strSql02 = "SELECT FName_L2 as name FROM dbo.t_bd_materialgroup where fnumber='" + MatType01 + MatType02 + "'";
                    List<Map<String, Object>> rs02 = sqlService.getList(CloudSqlService.htEas, strSql02);
                    try {
                        String MaterialName02 = (String) rs02.get(0).get("name");
                        if (ArchType.equalsIgnoreCase("06")) {
                            ParentFolderAttr = "DS" + "-A" + "-" + MatType01 + " " + manageDept;
                            FolderAttr = "DS" + "-A" + "-" + MatType01 + MatType02 + " " + manageDept;//
                        }
                        if (ArchType.equalsIgnoreCase("19")) {
                            ParentFolderAttr = "AS" + "-" + MatType01 + " " + manageDept;
                            FolderAttr = "AS" + "-" + MatType01 + MatType02 + " " + manageDept;//
                        }
                        if (ArchType.substring(0, 2).equalsIgnoreCase("18")) {
                            ParentFolderAttr = MatCode.substring(0, 5) + MatType01 + " " + PosADuty.substring(0, PosADuty.lastIndexOf("."));//
                            FolderAttr = MatCode.substring(0, 5) + MatType01 + MatType02 + " " + PosADuty.substring(0, PosADuty.lastIndexOf("."));//
                        }
                        JSONObject ParentFolderIdObj = api.getFileID(ParentFolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                        JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                        JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                        String ParentFolderId = ParentFolderIdObj0.getString("id");
                        String FolderName = MatType02 + " " + MaterialName02;


                        JSONObject FolderIdObj = api.getFileID(FolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                        int SubFolderNum = Integer.parseInt(FolderIdObj.getString("num"));
                        if (SubFolderNum == 0) {

                            api.getAddFolder(ParentFolderId, FolderName, FolderAttr);
                        } else {
                            JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                            JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                            ParentFolderId = FolderIdObj0.getString("id");
                            api.getRename(token, ParentFolderId, FolderName);
                        }


                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                    String strSql03 = "SELECT FName_L2 as name FROM dbo.t_bd_materialgroup where fnumber='" + MatType01 + MatType02 + MatType03 + "'";
                    List<Map<String, Object>> rs03 = sqlService.getList(CloudSqlService.htEas, strSql03);
                    try {
                        String MaterialName03 = (String) rs03.get(0).get("name");
                        if (ArchType.equalsIgnoreCase("06")) {
                            ParentFolderAttr = "DS" + "-A" + "-" + MatType01 + MatType02 + " " + manageDept;
                            FolderAttr = "DS" + "-A" + "-" + MatType01 + MatType02 + MatType03 + " " + manageDept;//
                        }
                        if (ArchType.equalsIgnoreCase("19")) {
                            ParentFolderAttr = "AS" + "-" + MatType01 + MatType02 + " " + manageDept;
                            FolderAttr = "AS" + "-" + MatType01 + MatType02 + MatType03 + " " + manageDept;//
                        }
                        if (ArchType.substring(0, 2).equalsIgnoreCase("18")) {
                            ParentFolderAttr = MatCode.substring(0, 5) + MatType01 + MatType02 + " " + PosADuty.substring(0, PosADuty.lastIndexOf("."));//
                            FolderAttr = MatCode.substring(0, 5) + MatType01 + MatType02 + MatType03 + " " + PosADuty.substring(0, PosADuty.lastIndexOf("."));//
                        }
                        JSONObject ParentFolderIdObj = api.getFileID(ParentFolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                        JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                        JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                        String ParentFolderId = ParentFolderIdObj0.getString("id");
                        String FolderName = MatType03 + " " + MaterialName03;


                        JSONObject FolderIdObj = api.getFileID(FolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                        int SubFolderNum = Integer.parseInt(FolderIdObj.getString("num"));
                        if (SubFolderNum == 0) {

                            api.getAddFolder(ParentFolderId, FolderName, FolderAttr);
                        } else {
                            JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                            JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                            ParentFolderId = FolderIdObj0.getString("id");
                            api.getRename(token, ParentFolderId, FolderName);
                        }


                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                    String strSql04 = "SELECT FName_L2 as name FROM dbo.t_con_materialinfo where fnumber='" + MatType01 + MatType02 + MatType03 + MatType04 + "'";
                    List<Map<String, Object>> rs04 = sqlService.getList(CloudSqlService.htEas, strSql04);
                    try {
                        String MaterialName04 = (String) rs04.get(0).get("name");
                        if (ArchType.equalsIgnoreCase("06")) {
                            ParentFolderAttr = "DS" + "-A" + "-" + MatType01 + MatType02 + MatType03 + " " + manageDept;
                            FolderAttr = "DS" + "-A" + "-" + MatType01 + MatType02 + MatType03 + MatType04 + " " + manageDept;//
                        }
                        if (ArchType.equalsIgnoreCase("19")) {
                            ParentFolderAttr = "AS" + "-" + MatType01 + MatType02 + MatType03 + " " + manageDept;
                            FolderAttr = "AS" + "-" + MatType01 + MatType02 + MatType03 + MatType04 + " " + manageDept;//
                        }
                        if (ArchType.substring(0, 2).equalsIgnoreCase("18")) {
                            ParentFolderAttr = MatCode.substring(0, 5) + MatType01 + MatType02 + MatType03 + " " + PosADuty.substring(0, PosADuty.lastIndexOf("."));//
                            FolderAttr = MatCode.substring(0, 5) + MatType01 + MatType02 + MatType03 + MatType04 + " " + PosADuty.substring(0, PosADuty.lastIndexOf("."));//
                        }
                        JSONObject ParentFolderIdObj = api.getFileID(ParentFolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                        JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                        JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                        String ParentFolderId = ParentFolderIdObj0.getString("id");
                        String FolderName = MatType01 + MatType02 + MatType03 + MatType04 + " " + MaterialName04;


                        JSONObject FolderIdObj = api.getFileID(FolderAttr);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                        int SubFolderNum = Integer.parseInt(FolderIdObj.getString("num"));
                        FolderName = FolderName.replace("%", "");
                        FolderName = FolderName + "\\";
//                            FolderName=FolderName.replace("/", "");
                        FolderName = FolderName.substring(0, FolderName.indexOf("\\"));
                        if (SubFolderNum == 0) {
                            api.getAddFolder(ParentFolderId, FolderName, FolderAttr);
//                               System.out.println("SubFolderNum");
                        } else {
                            JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                            JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                            ParentFolderId = FolderIdObj0.getString("id");
                            api.getRename(token, ParentFolderId, FolderName);
                        }


                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                    String suffix = MainDeptCode;
                    if (ArchType.substring(0, 2).equalsIgnoreCase("18")) {
                        suffix = PosADuty.substring(0, PosADuty.lastIndexOf("."));

                    }


                    // 没有分项的情况
                    elec = (String) subSheet.get(0).get("elec");
                    attribute = (String) objectMap.get("fileCode");// 从表单获取文件扩展属性==档案代码
                    prefix = attribute.substring(attribute.lastIndexOf("-") + 1, attribute.length());
                    if (ArchType.equalsIgnoreCase("06") || ArchType.equalsIgnoreCase("19")) {
                        String att = (String) objectMap.get("fileCode");
                        FolderAttribute = att + ' ' + manageDept;// 从表单获取文件夹扩展属性（=档案代码+主责部门）
                    }
                    if (ArchType.substring(0, 2).equalsIgnoreCase("18")) {
                        String att = (String) objectMap.get("fileCode");
                        FolderAttribute = att + ' ' + PosADuty.substring(0, PosADuty.lastIndexOf("."));// 从表单获取文件夹扩展属性（=档案代码+主责部门）
                    }
                    addfileid = "";
                    desc = (String) objectMap.get("des");
                    String CAAttribute = "";


                    //带分项的文件
                    if (SubItemORNot > 0) {
                        //带分项的文件
                        // 先加文件夹
                        String SubFolderAttribute = "";
                        String att = (String) objectMap.get("fileCode");
                        prefix = att.replace(" ", "").substring(att.lastIndexOf("-") + 1, att.replace(" ", "").length());//文件夹前缀
                        foldername = prefix + ' ' + (String) objectMap.get("fileName");//需要添加的文件夹名称（01 报告 02公司行政职能资信）
                        if (ArchType.equalsIgnoreCase("06") || ArchType.equalsIgnoreCase("19")) {
                            att = (String) objectMap.get("fileCode");
                            parentattribute = att.replace(" ", "").substring(0, att.replace(" ", "").lastIndexOf("-")) + ' ' + MainDeptCode;
                        }
                        if (ArchType.substring(0, 2).equalsIgnoreCase("18")) {
                            att = (String) objectMap.get("fileCode");
                            parentattribute = att.replace(" ", "").substring(0, att.replace(" ", "").lastIndexOf("-")) + ' ' + PosADuty.substring(0, PosADuty.lastIndexOf("."));
                        }
                        JSONObject FolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id，以便在其下添加文件夹
                        int FolderNum = Integer.parseInt(FolderIdObj.getString("num"));
                        if (FolderNum == 0) {
                            continue;
                        }
                        JSONArray FolderIdDataArray = FolderIdObj.getJSONArray("data");
                        JSONObject FolderIdObj0 = FolderIdDataArray.getJSONObject(0);
                        String folederid = FolderIdObj0.getString("id");
                        if (ArchType.equalsIgnoreCase("06") || ArchType.equalsIgnoreCase("19")) {
                            att = (String) objectMap.get("fileCode");
                            SubFolderAttribute = att.replace(" ", "") + ' ' + MainDeptCode;
                        }
                        if (ArchType.substring(0, 2).equalsIgnoreCase("18")) {
                            att = (String) objectMap.get("fileCode");
                            SubFolderAttribute = att.replace(" ", "") + ' ' + PosADuty.substring(0, PosADuty.lastIndexOf("."));
                        }

                        JSONObject SubFolderIdObj = api.getFileID(SubFolderAttribute);// 分项文件夹是否存在
                        int num = Integer.parseInt(SubFolderIdObj.getString("num"));
                        if (num > 0) {
                            JSONArray SubFolderIdDataArray = SubFolderIdObj.getJSONArray("data");
                            JSONObject SubFolderIdObj0 = SubFolderIdDataArray.getJSONObject(0);
                            String SubFolderId = SubFolderIdObj0.getString("id");
                            api.getRename(token, SubFolderId, foldername);
                        } else {
                            if (ArchType.equalsIgnoreCase("06") || ArchType.equalsIgnoreCase("19")) {
                                att = (String) objectMap.get("fileCode");
                                api.getAddFolder(folederid, foldername, att.replace(" ", "") + ' ' + MainDeptCode);
                            }
                            if (ArchType.substring(0, 2).equalsIgnoreCase("18")) {
                                att = (String) objectMap.get("fileCode");
                                api.getAddFolder(folederid, foldername, att.replace(" ", "") + ' ' + PosADuty.substring(0, PosADuty.lastIndexOf(".")));
                            }
                        }
                        //查询分项文件对应文件夹的FolderId
                        String ExistFolderAttribute = "";
                        if (ArchType.equalsIgnoreCase("06") || ArchType.equalsIgnoreCase("19")) {
                            att = (String) objectMap.get("fileCode");
                            ExistFolderAttribute = att.replace(" ", "") + ' ' + MainDeptCode;
                        }
                        if (ArchType.substring(0, 2).equalsIgnoreCase("18")) {
                            att = (String) objectMap.get("fileCode");
                            ExistFolderAttribute = att.replace(" ", "") + ' ' + PosADuty.substring(0, PosADuty.lastIndexOf("."));
                        }
                        JSONObject ExistFolderObj = api.getFileID(ExistFolderAttribute);//
                        JSONArray ExistFolderDataArray = ExistFolderObj.getJSONArray("data");
                        JSONObject ExistFolderObj0 = ExistFolderDataArray.getJSONObject(0);
                        String ExitFolderId = ExistFolderObj0.getString("id");
                        AuthFileId = ExitFolderId;
                        JSONObject ExitFolderFullPath = api.getFullPath(ExitFolderId);
                        String SubItemFolderDestFullpath = ExitFolderFullPath.getString("fullPath");
                        JSONObject ExitFolderListObj = api.getList(ExitFolderId);
                        JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                        for (int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++) {
                            JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);


                            String IsFolder = ExitFolderListDataObj.getString("fileType");
                            if (!IsFolder.equals("1")) {
                                delId = ExitFolderListDataObj.getString("id");
                                api.getRemove(token, delId);
                                System.out.println(ExitFolderListDataObj.get("id"));
                            }
                        }
                        //移动公共区域的分项文件到对应路径
                        for (int Sub = 0; Sub < SubItemORNot; Sub++) {
                            String SubItemattribute = SubItemFileList[Sub].replace("，", ",");//02-A-01-16-02-14-01-01-02,01
                            JSONObject SubItemFileIdObj = api.getFileID(SubItemattribute);
                            JSONArray SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                            JSONObject SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                            String SubItemFileId = SubItemFileIdDataObj.getString("id");
//                   AuthFileId=SubItemFileId;
                            api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                            if (IsNomalFile == 1) {//有分项也有正常文件时候，正常文件也传过去
                                SubItemFileIdObj = api.getFileID((String) objectMap.get("fileCode"));
                                SubItemFileIdDataArray = SubItemFileIdObj.getJSONArray("data");
                                SubItemFileIdDataObj = SubItemFileIdDataArray.getJSONObject(0);
                                SubItemFileId = SubItemFileIdDataObj.getString("id");
                                api.getMove(SubItemFileId, SubItemFolderDestFullpath);//
                            }

                            // 获取主责岗位角色及其上级直至董事长，并授权

                        }

                        RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                        RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                        RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                        if (PosA != null) {
                            RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                        }
                        if (PosB != null) {
                            PosBDuty = PosB;
                            PosBName = PosBName;
                            RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                        }

                        continue;
                    }


                    // 如果是框架则在文档系统中加对应的文件夹
                    if (desc != null) {
                        if (desc.length() == 2) {
                            if (desc.substring(0, 2).equalsIgnoreCase("框架")) {
                                // 添加文件夹到指定路径
                                // 根据扩展属性获取将添加的框架的parentId
                                parentattribute = attribute.substring(0, attribute.lastIndexOf("-")) + ' ' + suffix;// 对应上级文件夹扩展属性
                                attribute = (String) objectMap.get("fileCode") + ' ' + suffix;
                                JSONObject ParentFolderIdObj = api.getFileID(parentattribute);// 根据上级文件夹的扩展属性查询上级文件夹id
                                int ParentFolderNum = Integer.parseInt(ParentFolderIdObj.getString("num"));
                                if (ParentFolderNum == 0) {
                                    continue;
                                }
                                JSONArray ParentFolderIdDataArray = ParentFolderIdObj.getJSONArray("data");
                                JSONObject ParentFolderIdObj0 = ParentFolderIdDataArray.getJSONObject(0);
                                String ParentFolderId = ParentFolderIdObj0.getString("id");
                                String FolderName = prefix + ' ' + (String) objectMap.get("fileName");// 从表单获取文件夹名称
                                String FrameAttribute = attribute;
                                JSONObject FrameAttributeObj = api.getFileID(FrameAttribute);
                                String FrameNum = FrameAttributeObj.getString("num");
                                int FrameNum01 = Integer.parseInt(FrameNum);
                                if (FrameNum01 > 0) {
                                    JSONArray FrameAttributeDataArray = FrameAttributeObj.getJSONArray("data");
                                    JSONObject FrameAttributeDataObj = FrameAttributeDataArray.getJSONObject(0);
                                    String fileId = FrameAttributeDataObj.getString("id");
                                    api.getRename(token, fileId, FolderName);
                                } else {
                                    api.getAddFolder(ParentFolderId, FolderName, FolderAttribute);
                                }
                            }
                        }
                    }

                    if (elec.equalsIgnoreCase("√") && IsNomalFile == 1) {
                        CAAttribute = (String) objectMap.get("fileCode");
                        String CALevelParAttribute = CAAttribute.substring(0, CAAttribute.lastIndexOf("-")) + " " + suffix;//上级文件夹扩展属性SD-02-142331024-9002-31 02.06.07.003
                        JSONObject CAParentFolderIdObj = api.getFileID(CALevelParAttribute);
                        int CAParentFolderNum = Integer.parseInt(CAParentFolderIdObj.getString("num"));
                        if (CAParentFolderNum == 0) {
                            continue;
                        }
                        JSONArray CAParentFolderIdDataArray = CAParentFolderIdObj.getJSONArray("data");
                        JSONObject CAParentFolderIdDataObj = CAParentFolderIdDataArray.getJSONObject(0);
                        String CAParentFolderId = CAParentFolderIdDataObj.getString("id");

                        //查找文件对应文件夹
                        JSONObject ExistFolderIdObj = api.getFileID(CAAttribute + " " + suffix);
                        int CAArrFolderNum = Integer.parseInt(ExistFolderIdObj.getString("num"));
                        JSONObject FolderIdObj = null;
                        if (CAArrFolderNum == 0) {
                            String att = (String) objectMap.get("fileCode");
                            //如果不存在文件对应文件夹则先添加对应文件夹
                            FolderIdObj = api.getAddFolder(CAParentFolderId, att.substring(att.lastIndexOf("-") + 1, att.length()) + " " + (String) objectMap.get("fileName"), CAAttribute + " " + suffix);
                            //获取对应文件夹的FolderId，以便之后将文件放入该文件夹
                            addfileid = FolderIdObj.getString("fileId");
                        }
                        //如果存在文件对应文件夹则先需要找到其FolderId
                        else {
                            JSONArray ExistFolderIdDataArray = ExistFolderIdObj.getJSONArray("data");
                            JSONObject ExistFolderIdDataObj = ExistFolderIdDataArray.getJSONObject(0);
                            addfileid = ExistFolderIdDataObj.getString("id");
                            String att = (String) objectMap.get("fileCode");
                            api.getRename(token, addfileid, att.substring(att.lastIndexOf("-") + 1, att.length()) + " " + (String) objectMap.get("fileName"));
                            JSONObject ExitFolderListObj = api.getList(addfileid);
                            JSONArray ExitFolderListDataArray = ExitFolderListObj.getJSONArray("data");
                            for (int ExitFileNum = 0; ExitFileNum < ExitFolderListDataArray.size(); ExitFileNum++) {
                                JSONObject ExitFolderListDataObj = ExitFolderListDataArray.getJSONObject(ExitFileNum);
                                String IsFolder = ExitFolderListDataObj.getString("fileType");
                                String filename = ExitFolderListDataObj.getString("fileName");
                                if (!IsFolder.equals("1")) {
                                    if (filename.contains(",")) {
                                        delId = ExitFolderListDataObj.getString("id");
                                        api.getRemove(token, delId);
                                        System.out.println(ExitFolderListDataObj.get("id"));
                                    }
                                }
                            }
                        }
                        JSONObject FolderPathObj = api.getFullPath(addfileid);
                        String CAdestfullpath = FolderPathObj.getString("fullPath");
                        JSONObject FileID = api.getFileID(CAAttribute);
                        int CAArrFileNum = Integer.parseInt(FileID.getString("num"));
                        if (CAArrFileNum == 0) {
                            continue;
                        }

                        JSONArray FileIDDataArray = FileID.getJSONArray("data");
                        JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
                        String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
                        String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
                        JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
                        fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
                        String NewFileName = FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));
                        if (FileIDDataArray.size() == 1) {
                            api.getMove(fileid, CAdestfullpath);
                        }//有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的

                        else if (!addfileid.equals(Parentid2)) {


                            api.getMove(fileid, CAdestfullpath);


                            JSONObject ListFileAuth = api.getListFileAuth(token, fileid2);
                            JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
                            int a = ListFileAuthDataArray.size();
                            for (int ListFileAuthNum = 0; ListFileAuthNum < ListFileAuthDataArray.size(); ListFileAuthNum++) {

                                JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(ListFileAuthNum);
                                String RemoveAuthPos = ListFileAuthDataObj.getString("name");
                                String auth = ListFileAuthDataObj.getString("auth");
                                JSONObject array1 = api.getRole(RemoveAuthPos);
                                String num = array1.getString("num");
                                if (!num.equals("0")) {
                                    JSONArray array = array1.getJSONArray("data");
                                    JSONObject array2 = array.getJSONObject(0);
                                    String userId = array2.getString("id");
                                    api.getRoleAuth(RemoveAuthPos, userId, auth, fileid);
                                }
                            }
//                             api.getRename(token, fileid2, NewFileName);
//                      String MOVEUpdate = api.getUpdate(token,fileid, fileid2);
                            api.getRemove(token, fileid2);
                        }
                        //有2个及以上相同扩展属性的文件时，需要用最新的覆盖最老的
                        else if (FileIDDataArray.size() > 1) {
//                      api.getRemove(token, fileid2);
//                      api.getMove( fileid,destfullpath);

                            api.getRename(token, fileid2, NewFileName);
                            String MOVEUpdate = api.getUpdate(token, fileid, fileid2);
                            System.out.print(MOVEUpdate);
                        }
                        AuthFileId = fileid2;
                        RoleAuth.RoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(PosMainDuty, PosMainName, AuthFileId);//主责岗助理授下载权
                        RoleAuth.RoleAAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId); //档案上传岗所有权限，上级直到董事长授下载权
                        RoleAuth.AssRoleAuthDis(MainDeptAssDuty, MainDeptAssDutyName, AuthFileId);//档案上传岗助理下载权
                        RoleAuth.RoleAuthDis(FunMainDeptAssDuty, FunMainDeptAssDutyName, AuthFileId);    //职能总责部门助理，上级直到董事长授下载权
                        if (PosA != null) {
                            RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
                        }
                        if (PosB != null) {
                            PosBDuty = PosB;
                            PosBName = PosBName;
                            RoleAuth.RoleAuthDis(PosBDuty, PosBName, AuthFileId);
                        }

                    }
                }


            }

        }


        return getOkResponseResult("成功");


    }


//        for (int i = 0; i < subSheet.size(); i++) {
//        Map<String, Object> objectMap = subSheet.get(i);
//        //获取表单上主责公司
//        String fileCode = (String) objectMap.get("fileCode");
//        String PosADuty = (String) objectMap.get("PositionACode");
//        String dept = PosADuty.substring(0,PosADuty.lastIndexOf("."));
//        String fileName = (String) objectMap.get("fileName");
//        String addfileid="";
//        String destfullpath="";
//        String fileid="";
//
//
////            http://47.104.86.126:188/app/file/upload-file/2599/c7176c48be36edad213c13032364dcbb20760fcd14bd372d726602495279f6fd18b033/mobile_sha256/-1/
//
//
//
//        String SDLevelParAttribute=fileCode.substring(0, fileCode.lastIndexOf("-"))+" "+dept;
//        JSONObject SDParentFolderID = DocAPI.getFileID(SDLevelParAttribute);
//        JSONArray SDParentFolderIdDataArray = SDParentFolderID.getJSONArray("data");
//        JSONObject SDParentFolderIdDataObj = SDParentFolderIdDataArray.getJSONObject(0);
//        String SDParentFolderId = SDParentFolderIdDataObj.getString("id");
//        JSONObject ExistFolderIdObj=DocAPI.getFileID(fileCode+" "+dept);
//        JSONObject SDFolderIdObj=DocAPI.getAddFolder(SDParentFolderId,fileCode.replace(" ", "").substring(fileCode.lastIndexOf("-")+1,fileCode.replace(" ", "").length()) +" "+fileName, fileCode+" "+dept);
//        //获取对应文件夹的FolderId，以便之后将文件放入该文件夹
//        addfileid = SDFolderIdObj.getString("fileId");
//        JSONObject FolderPathObj = DocAPI.getFullPath(addfileid);
//        destfullpath = FolderPathObj.getString("fullPath");
//        JSONObject FileID = DocAPI.getFileID(fileCode);
//        JSONArray FileIDDataArray = FileID.getJSONArray("data");
//        JSONObject FileIDDataObj = FileIDDataArray.getJSONObject(0);// 如果文件夹中存在需要替换的文件则为要替换的文件id ，否则还是公共区新上传的文件id
//        String fileid2 = FileIDDataObj.getString("id");//最早上传的老文件的fileid2
//        String Parentid2 = FileIDDataObj.getString("parentId");//最早上传的老文件的parentId
//        JSONObject FileIDPublicObj = FileIDDataArray.getJSONObject(FileIDDataArray.size() - 1);// 公共区新上传的文件id
//        fileid = FileIDPublicObj.getString("id");//最新上传的文件的fileid
//        String NewFileName=FileIDPublicObj.getString("fileName").substring(0, FileIDPublicObj.getString("fileName").lastIndexOf("."));;
//        DocAPI.getMove(fileid,destfullpath);
//
//
//    }


//        return getOkResponseResult("成功");
//}
//     @Async
    @RequestMapping("fileIsExist")
    //校验总公司级文件是否存在

    public ResponseResult fileIsExist(@RequestBody Map<String, Object> params) {

        List<Map<String, Object>> list = (List<Map<String, Object>>) params.get("list");
        String appEmp = (String) params.get("employeeNo");
        DocAPI api = new DocAPI();
        StringBuilder result = new StringBuilder();
        String fnumber = "";

        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> map = list.get(i);
            String elec = (String) map.get("elec");
            String CFRemarks = (String) map.get("remarks");//备注
            if (!elec.equals("√") || CFRemarks.contains("缺失")) {
                continue;
            }

            String fNumber = (String) map.get("MainDeptId");//主责部门
            String mainPosNumber = (String) map.get("mainPositionCode");//获取表单上主责岗位
            String posANumber = (String) map.get("PositionACode");
            String submitAttribute = fNumber + ".006 PUB " + appEmp;

//            JSONObject pubFolderObject = DocAPI.getFileID(submitAttribute);

            String CFCode01 = "";


//            int fileNum = Integer.parseInt(pubFolderObject.getString("num"));
//            if (fileNum == 0) {
//                result.append(String.format("第%d行档案请上传文件！", i + 1));
//                //文件未上传,退出循环
//                break;
//            }

            //判断是否有文件名称 = 档案名称+档案代码+备注
            String CFName = (String) map.get("fileName");
            CFName = CFName.replace(" ", "");//

            String CFCode = (String) map.get("fileCode");
            String CorrDesc = (String) map.get("refDes");//对应说明
            String Desc = (String) map.get("des");//说明
            String fileNames = CFName.concat(CFCode).concat(CFRemarks);
            int FileSubmitted = 0;
            int SubmitPubSubItemFileNum = 0;
            int SubItemORNot = 0;
            int IsNomalFile = 0;
            int YomiDateEquEasDate = 1;
            String[] Date = new String[200];
            String[] SubmitPubSubItemFileList = new String[200];
            String[] SubItemFileList = new String[200];




            String ArchType = (String) map.get("archTypeNumber");
            ArchType = ArchType.substring(0, 2);

            if (ArchType.equals("22") || ArchType.equals("23") || ArchType.equals("24")) {
                continue;
            }
            String YomiName = "";
            int MaxDate = 0;

            String EASName = (CFName + CFCode + CFRemarks).replace(" ", "");//

            EASName=EASName.replace(" ", "");
            String EASName01 = (CFName + CFCode).replace(" ", "");
            String EASName02 = (CFName  + CFCode + CFRemarks).replace(" ", "");
//            EASName02=EASName02.replace(" ", "");
            //获取表单上主责部门
            String mainDeptNumber = (String) map.get("MainDeptId");


            //获取公共区域对应文件夹里的文件List（主责部门助理）
            String SubmitAttribute = mainDeptNumber + ".006" + " PUB " + appEmp;
            //获取公共区域对应岗位A文件夹里的文件List
            if (ArchType.equals("06") || ArchType.equals("11") || ArchType.equals("19")) {
                SubmitAttribute = posANumber + " PUB " + appEmp;
            }
            if (ArchType.substring(0, 2).equals("18")) {//上传岗：工作档案框架主责岗
                CFCode01 = CFCode.substring(0, CFCode.lastIndexOf("-")) + "-0";//工作档案框架
                if (CFCode01.equals(CFCode)) {
                    fnumber = mainPosNumber;
                } else {
                    String sqlstr = "select b.fname_l2 FNAME,b.fnumber FNUMBER from [HG_LINK].[hg].dbo.t_fil_fileinfo a inner join [HG_LINK].[hg].dbo.T_ORG_position b on a.fpiccodeid=b.fid  where a.fnumber = '" + CFCode01 + "'";
                    List<Map<String, Object>> rs = sqlService.getList(CloudSqlService.htEas, sqlstr);
                    try {
//                        String value = (String) rs.get(0).get("FNUMBER");
                        if (rs.size() != 0) {
                            fnumber = (String) rs.get(0).get("FNUMBER");
                        } else {
                            fnumber = mainPosNumber;
                        }
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                SubmitAttribute = fnumber.substring(0, fnumber.lastIndexOf(".")) + ".006 PUB " + appEmp;
            }


            JSONObject SubmitPubFolderIDObj = api.getFileID(SubmitAttribute);
            int FileNum = Integer.parseInt(SubmitPubFolderIDObj.getString("num"));
            if (FileNum > 0) {
                JSONArray SubmitPubFolderIDDataArray = SubmitPubFolderIDObj.getJSONArray("data");
                JSONObject SubmitPubFolderIDDataObj = SubmitPubFolderIDDataArray.getJSONObject(0);
                String SubmitPubFolderID = SubmitPubFolderIDDataObj.getString("id");
                JSONObject SubmitPubFolderListObj = api.getList(SubmitPubFolderID);
                JSONArray SubmitPubFolderListDataArray = SubmitPubFolderListObj.getJSONArray("data");

                String[] SubmitPubFileList = new String[120];//公共文件夹里的文件列表

//                int fsff = SubmitPubFolderListDataArray.size();
                //遍历公共区域对应A岗位文件夹里的文件，带有分项的放入SubmitPubFileList，和EAS报批的文件名一致的放入SubItemFileList
                for (int PubNum = 0; PubNum < SubmitPubFolderListDataArray.size(); PubNum++) {
                    JSONObject SubmitPubFolderListNumDataArray = SubmitPubFolderListDataArray.getJSONObject(PubNum);
                    YomiName = (SubmitPubFolderListNumDataArray.getString("fileName").substring(0, SubmitPubFolderListNumDataArray.getString("fileName").lastIndexOf(".")));
//                    YomiName = YomiName.replace(YomiName," ","");
//
						  YomiName=YomiName.replace(" ", "");

                    SubmitPubFileList[PubNum] = SubmitPubFolderListNumDataArray.getString("remarks");
                    if (SubmitPubFileList[PubNum].equals(CFCode.replace(" ", ""))) {
                        IsNomalFile = 1;
                    }
                    String trans = SubmitPubFileList[PubNum].replace(',', '，');
                    SubmitPubFileList[PubNum] = trans;
                    if (trans.contains("，")) {
                        SubmitPubSubItemFileList[SubmitPubSubItemFileNum] = trans;
                        String trans01 = trans.substring(0, trans.indexOf("，"));
                        if (trans01.equalsIgnoreCase(CFCode)) {
                            Date[SubItemORNot] = YomiName.trim().substring(YomiName.trim().length() - 10, YomiName.trim().length());

//                            YomiDateEquEasDate = 1;
                            if (!Date[SubItemORNot].equals(CFRemarks.trim())) {

                                YomiDateEquEasDate = 0;
                            }
                            SubItemORNot++;//分项数量
                        }
                        SubmitPubSubItemFileNum++;
//							 System.out.println(PubNum);
                    }
//						 if(ArchType.equals("CA")){

//						if( ArchType.equals("01")||ArchType.equals("02")||ArchType.equals("03")||ArchType.equals("04")||ArchType.equals("05")||ArchType.equals("06")||ArchType.equals("07")||ArchType.equals("08")||ArchType.equals("09")||ArchType.equals("10"))
                    if (!ArchType.equals("11") && !ArchType.substring(0, 2).equals("18")) {
                        {
                        }
                        if (YomiName.equals(EASName)) {
                            FileSubmitted++;
                        }
                    }
                    if (ArchType.substring(0, 2).equals("18")) {
                        if (YomiName.contains(CFName) && YomiName.contains(CFCode) && YomiName.contains(CFRemarks)) {
                            FileSubmitted++;

                        }
                    }
                    if (ArchType.equals("11")) {
                        if (YomiName.equals(EASName02)) {
                            FileSubmitted++;
                        }
                    }

                }
            }

            if (FileSubmitted == 0 && SubItemORNot == 0)//如果没有对应的文件，需要判断备注日期和说明，系统里面是否存在一条一样的记录，如果是则不需要传文件
            {
                result.append(String.format("第%d行档案,浩通云盘未发现档案: %s ,", i + 1, CFCode));
                break;

//                //只修改说明，可以不用上传文件
//                String sqlstr = "select fnumber  from T_FIL_FILEINFO where FRemarks = '"+CFRemarks+ "' and Fnumber = '"+CFCode+ "' ";
//                List<Map<String, Object>> rs = sqlService.getList(CloudSqlService.htEas, sqlstr);
//                try {
//
//                    String value =  (String)rs.get(0).get("FNUMBER");
//
//                    if(value!=null){
//                        if(IsNomalFile==0){
//                            String abd="adms";
//                            continue;
//                        }
//                        if(IsNomalFile==1){
//                            result.append(String.format("第%d行档案,请检查: %s ,", i+1,CFCode));
//                            break;
//
//                        }
//
//                    }
//                } catch (Exception e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
            }
            if (Desc != null) {
                if (Desc.contains("待编制"))//
                {
                    if (IsNomalFile == 0 && SubItemORNot == 0) {

                        result.append(String.format("第%d行档案,请检查: %s ,", i + 1, CFCode));
                        break;

                    }
                    continue;
                }
            }
            if (IsNomalFile == 0 && SubItemORNot == 0) {
                result.append(String.format("第%d行档案,浩通云盘未发现档案: %s ,", i + 1, CFCode));
                break;
            }
            if (IsNomalFile > 0 ) {
                if ( FileSubmitted == 0){
                    result.append(String.format("第%d行档案,请在浩通云盘中上传对应文件!: %s ,", i + 1, CFCode));
                    break;
                }
            }
            if (SubItemORNot > 0) {
                MaxDate = Integer.parseInt(Date[0].substring(0, 4) + Date[0].substring(5, 7) + Date[0].substring(8, 10));
                for (int m = 0; m < SubItemORNot - 1; m++) {
                    int Date01 = Integer.parseInt(Date[m + 1].substring(0, 4) + Date[m + 1].substring(5, 7) + Date[m + 1].substring(8, 10));
//						 int Date00 =Integer.parseInt(Date[m].substring(0, 4)+Date[m].substring(5, 7)+Date[m].substring(8, 10));
//						 MaxDate=Date00;
                    if (Date01 != MaxDate) {
                        result.append(String.format("第%d行档案,请检查分项文件日期!: %s ,", i + 1, CFCode));
                        MaxDate = Date01;
                    }

                }

            }
            if (SubItemORNot > 0) {
                int EasDate = Integer.parseInt(CFRemarks.trim().substring(0, 4) + CFRemarks.trim().substring(5, 7) + CFRemarks.trim().substring(8, 10));
                if (MaxDate != EasDate) {
                    result.append(String.format("第%d行档案,请检查分项文件日期!: %s ,", i + 1, CFCode));
                    break;
                }
            }
            if (FileSubmitted == 0 && SubItemORNot == 0) {
                result.append(String.format("第%d行档案,请先在浩通云盘中上传对应文件!: %s ,", i + 1, CFCode));
                break;
            }
            if (YomiDateEquEasDate != 1 && SubItemORNot > 0) {
                result.append(String.format("第%d行档案,请检查文件日期!: %s ,", i + 1, CFCode));
                break;
            }


//            //授权
//            String mainDeptAssDuty = (String) map.get("mainDeptAssDuty");
//            if(!fileAuthorization(mainDeptAssDuty)){
//                result.append(String.format("第%d行档案,浩通云盘文档授权失败: %s ,", i+1,fileNames));
//                break;
//            }

        }


        if (result.length() > 0) {
            return getErrResponseResult(-1L, result.toString());
        }

        return getOkResponseResult("成功");
    }

//    @Async
    @RequestMapping("comfileIsExist")
    //校验公司级文件是否存在

    public ResponseResult comfileIsExist(@RequestBody Map<String, Object> params) {

        List<Map<String, Object>> list = (List<Map<String, Object>>) params.get("list");
        String appEmp = (String) params.get("employeeNo");
        DocAPI api = new DocAPI();
        StringBuilder result = new StringBuilder();
        int SubCheck=0;

        for (int i = 0; i < list.size(); i++) {
            String fnumber = "";
            Map<String, Object> map = list.get(i);

            String CFRemarks = (String) map.get("remarks");//备注
            if (CFRemarks.contains("/") || CFRemarks.contains("缺失")) {
                continue;
            }  //备注为/或缺失，不执行后续
            String CFElectric = (String) map.get("elec");
            if (CFRemarks.contains("/") || CFRemarks.contains("缺失")) {
                continue;
            }  //备注为/或缺失，不执行后续

            String fNumber = (String) map.get("MainDeptId");//主责部门
            String mainPosNumber = (String) map.get("mainPositionCode");//获取表单上主责岗位
            String POSA = (String) map.get("PostionACode");
            String POSB = (String) map.get("PostionBCode");

            String submitAttribute = fNumber + ".006 PUB " + appEmp;

            JSONObject pubFolderObject = DocAPI.getFileID(submitAttribute);

            //判断是否有文件名称 = 档案名称+档案代码+备注
            String companyCode = (String) map.get("companyCode");//公司
            String CFName = (String) map.get("fileName");

            String CFCode = (String) map.get("fileCode");//档案代码
            String CorrDesc = (String) map.get("refDes");//对应说明
            String Desc = (String) map.get("des");//说明
            String InfoCode = (String) map.get("relevBaseCode");//基础代码，HR人员编号02.0293
            String InfoName = (String) map.get("name01");//人员名称HR用
            String InfoDesc = (String) map.get("refDes");//对应说明，往来单位简称
            String fileNames = CFName.concat(CFCode).concat(CFRemarks);
            int FileSubmitted = 0;
            int SubmitPubSubItemFileNum = 0;
            int SubItemORNot = 0;
            int IsNomalFile = 0;
            int YomiDateEquEasDate = 1;
            String[] Date = new String[200];
            String[] SubmitPubSubItemFileList = new String[200];
            String[] SubItemFileList = new String[200];



            String attribute = CFCode;
            String[] SubmitPubFileList = new String[120];//公共文件夹里的文件列表
            String CFCode01 = CFCode.substring(0, CFCode.lastIndexOf("-")) + "-0";//WD01-02-0101020080000005-0
            String YomiName = "";
            int MaxDate = 0;
            SubCheck = 0;
            String ArchType = (String) map.get("fileTypeNumber");
            String ArchType2 = ArchType.substring(0, 2);
            if (CFElectric.equalsIgnoreCase("√"))//电子版时校验
            {
                String EASName = (CFName + CFCode + CFRemarks).replace(" ", "");//流程、SOPWD06-02-3037060084450549-022021.02.01
                String EASName01 = (CFName + InfoDesc + CFCode + CFRemarks).replace(" ", "");//InfoDesc是简称
                String EASName02 = (CFName  + CFCode + CFRemarks).replace(" ", "");//InfoName是人员名称
                String EASName03 = (CFName + InfoCode + CFCode + CFRemarks).replace(" ", "");
                String RegionCode = CFCode.substring(CFCode.indexOf("-"), CFCode.length());
                RegionCode = RegionCode.substring(RegionCode.indexOf("-"), RegionCode.length());
                RegionCode = RegionCode.substring(0, RegionCode.lastIndexOf("-"));


                //获取公共区域对应岗位A文件夹里的文件List
                String SubmitAttribute = POSA + " PUB " + appEmp;
                if (ArchType2.substring(0, 2).equals("20")) {
                    String sqlstr = "select b.fname_l2 FNAME,b.fnumber FNUMBER from [HG_LINK].[hg].dbo.ct_gen_comgeneralarchbase a inner join [HG_LINK].[hg].dbo.T_ORG_position b on a.cfmaindeptpiccodei=b.fid  where a.fnumber = '" + CFCode01 + "'";
                    List<Map<String, Object>> rs = sqlService.getList(CloudSqlService.htEas, sqlstr);
                    try {

//                        String value = (String) rs.get(0).get("FNUMBER");

                        if (rs.size() != 0)  {
                            fnumber = (String) rs.get(0).get("FNUMBER");
                        } else {
                            fnumber = mainPosNumber;
                        }


                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    SubmitAttribute = fnumber.substring(0, fnumber.lastIndexOf(".")) + ".006 PUB " + appEmp;
                }


                JSONObject SubmitPubFolderIDObj = api.getFileID(SubmitAttribute);
                int num = Integer.parseInt(SubmitPubFolderIDObj.getString("num"));

                if (num > 0) {
                    JSONArray SubmitPubFolderIDDataArray = SubmitPubFolderIDObj.getJSONArray("data");


                    JSONObject SubmitPubFolderIDDataObj = SubmitPubFolderIDDataArray.getJSONObject(0);
                    String SubmitPubFolderID = SubmitPubFolderIDDataObj.getString("id");
                    JSONObject SubmitPubFolderListObj = api.getList(SubmitPubFolderID);
                    JSONArray SubmitPubFolderListDataArray = SubmitPubFolderListObj.getJSONArray("data");
                    int fsff = SubmitPubFolderListDataArray.size();

                    //遍历公共区域对应A岗位文件夹里的文件，带有分项的放入SubmitPubFileList，和EAS报批的文件名一致的放入SubItemFileList
                    for (int PubNum = 0; PubNum < SubmitPubFolderListDataArray.size(); PubNum++) {

                        JSONObject SubmitPubFolderListNumDataArray = SubmitPubFolderListDataArray.getJSONObject(PubNum);
                        YomiName = (SubmitPubFolderListNumDataArray.getString("fileName").substring(0, SubmitPubFolderListNumDataArray.getString("fileName").lastIndexOf("."))).replace(" ", "");
//						 String YomiName01=YomiName.replace(" ", "");

                        SubmitPubFileList[PubNum] = SubmitPubFolderListNumDataArray.getString("remarks");
                        if (SubmitPubFileList[PubNum].equals(CFCode.replace(" ", ""))) {
                            IsNomalFile = 1;
                            MaxDate = Integer.parseInt(CFRemarks.substring(0, 4) + CFRemarks.substring(5, 7) + CFRemarks.substring(8, 10));
//
                        }
                        String trans = SubmitPubFileList[PubNum].replace(',', '，');
                        SubmitPubFileList[PubNum] = trans;
                        if (trans.contains("，")) {
                            SubmitPubSubItemFileList[SubmitPubSubItemFileNum] = trans;
                            String trans01 = trans.substring(0, trans.indexOf("，"));
                            if (trans01.equalsIgnoreCase(CFCode)) {
//                                YomiDateEquEasDate = 1;
                                Date[SubItemORNot] = YomiName.trim().substring(YomiName.trim().length() - 10, YomiName.trim().length());
                                if (!Date[SubItemORNot].equals(CFRemarks.trim())) {
                                    YomiDateEquEasDate = 0;
                                }
                                if (YomiName.contains(InfoDesc)) {
                                    SubCheck++;
                                }

                                SubItemORNot++;//分项数量
                            }
                            SubmitPubSubItemFileNum++;
//							 System.out.println(PubNum);

                        }


                        if (ArchType2.equals("15")) {//CA
                            if (YomiName.equals(EASName)) {
                                FileSubmitted++;
                            }
                        }
                        if (ArchType2.equals("13")) {//CSD
                            if (YomiName.equals(EASName))
                            //重要交往记录上海飒米网络SD-02-1423115-3095-05-012019.10.16
                            //重要交往记录上海飒米网络SD-02-1423115-3095-05-012019.10.16
//									if(YomiName.contains(EASName)||YomiName.contains(EASName01)||YomiName.contains(EASName02)||YomiName.contains(EASName03))
                            {
                                FileSubmitted++;

                            }
                        }


                        if (ArchType2.equals("14")) {//HR
                            if (YomiName.equals(EASName02))
//									if(YomiName.contains(EASName)||YomiName.contains(EASName01)||YomiName.contains(EASName02)||YomiName.contains(EASName03))
                            {
                                FileSubmitted++;
                            }
                        }
                        if (ArchType2.equals("12")) {//F
                            if (YomiName.equals(EASName))
//									if(YomiName.contains(EASName)||YomiName.contains(EASName01)||YomiName.contains(EASName02)||YomiName.contains(EASName03))
                            {
                                FileSubmitted++;

                            }
                        }
                        if (ArchType2.substring(0, 2).equals("20")) {//F
                            if (YomiName.contains(CFName) && YomiName.contains(CFCode) && YomiName.contains(CFRemarks)) {
                                FileSubmitted++;
                                //CFName+InfoDesc+CFCode+
                                //WD06-02-3037060054450549-02流程、SOP2021.02.01
                            }
                        }
                        if (ArchType2.equals("16")) {//OD
                            if (YomiName.contains(EASName)) {
                                FileSubmitted++;

                            }
                        }


                    }

                }
//                if (SubItemORNot > 0)//
//                {
//                    continue;
//                }
//                if (FileSubmitted == 0)//如果没有对应的文件，需要判断备注日期和说明，系统里面是否存在一条一样的记录，如果是则不需要传文件
//                {
//                    result.append(String.format("第%d行档案,请在浩通云盘中上传对应文件!: %s ,", i + 1, CFCode));
//                    break;
//                }

                if (Desc != null) {
                    if (Desc.contains("待编制"))//
                    {
                        if (IsNomalFile == 0 && SubItemORNot == 0) {
                            result.append(String.format("第%d行档案,请在浩通云盘中上传对应文件!: %s ,", i + 1, CFCode));
                            break;
                        }
                        continue;
                    }
                }

                if (num == 0) {
                    result.append (String.format("第%d行档案,请在浩通云盘中上传对应文件!: %s ,", i + 1, CFCode));
                    break;
                }
                if (IsNomalFile == 0 && SubItemORNot == 0 && FileSubmitted == 0) {
                    result.append(String.format("第%d行档案,请在浩通云盘中上传对应文件!: %s ,", i + 1, CFCode));
                    break;
                }
                if (IsNomalFile > 0 ) {
                    if ( FileSubmitted == 0){
                        result.append(String.format("第%d行档案,请在浩通云盘中上传对应文件!: %s ,", i + 1, CFCode));
                        break;
                    }
                }
                if (SubItemORNot > 0) {
                    MaxDate = Integer.parseInt(Date[0].substring(0, 4) + Date[0].substring(5, 7) + Date[0].substring(8, 10));
                    for (int m = 0; m < SubItemORNot - 1; m++) {
                        int Date01 = Integer.parseInt(Date[m + 1].substring(0, 4) + Date[m + 1].substring(5, 7) + Date[m + 1].substring(8, 10));
//						 int Date00 =Integer.parseInt(Date[m].substring(0, 4)+Date[m].substring(5, 7)+Date[m].substring(8, 10));
//						 MaxDate=Date00;
                        if (Date01 != MaxDate) {
                            result.append(String.format("第%d行档案,请检查分项文件日期!: %s ,", i + 1, CFCode));
                            MaxDate = Date01;
                        }
                    }
                }
                if (SubItemORNot > 0 && IsNomalFile == 0) {
                    int EasDate = Integer.parseInt(CFRemarks.trim().substring(0, 4) + CFRemarks.trim().substring(5, 7) + CFRemarks.trim().substring(8, 10));
                    if (MaxDate != EasDate) {
                        result.append(String.format("第%d行档案,请检查文件日期!: %s ,", i + 1, CFCode));
                        break;
                    }
                }
                if (SubItemORNot > 0 && IsNomalFile > 0) {
                    int EasDate = Integer.parseInt(CFRemarks.substring(0, 4) + CFRemarks.substring(5, 7) + CFRemarks.substring(8, 10));
                    if (MaxDate != EasDate) {
                        result.append(String.format("第%d行档案,请检查文件日期!: %s ,", i + 1, CFCode));
                        break;
                    }
                }
                if (FileSubmitted == 0 && SubItemORNot == 0) {
                    result.append(String.format("第%d行档案,请先在浩通云盘中上传对应文件!: %s ,", i + 1, CFCode));
                    break;
                }
                if (ArchType.equals("CSD") && SubItemORNot != 0 && SubItemORNot != SubCheck) {
                    result.append(String.format("第%d行档案,请检查分项文件名称，应包含往来单位简称!: %s ,", i + 1, CFCode));
                    break;
                }
                if (YomiDateEquEasDate != 1 ) {
                    result.append(String.format("第%d行档案,请检查文件日期!: %s ,", i + 1, CFCode));
                    break;
                }

            }
        }


        if (result.length() > 0) {
            return getErrResponseResult(-1L, result.toString());
        }

        return getOkResponseResult("成功");
    }


    @RequestMapping("personInfoIsExist")
    //校验人员信息是否存在
    /**
     exec     CheckExistsPerson			----检查人员是否存在记录，人员作废时校验
     @number			nvarchar(100),			----人员代码
     @name				nvarchar(200),			----姓名
     @flag				int  output,				----  1   可以提交    0   校验失败
     @Desc				nvarchar(500)  output   ----  @flag 为0时，错误信息
      */



    public ResponseResult personInfoIsExist(@RequestBody Map<String, Object> params) {

        List<Map<String, Object>> list = (List<Map<String, Object>>) params.get("list");
        String appEmp = (String) params.get("employeeNo");
        DocAPI api = new DocAPI();
        StringBuilder result = new StringBuilder();
        int SubCheck = 0;

        String returnValue = "";
        for (int i = 0; i < list.size(); i++) {
            String fnumber = "";
            Map<String, Object> map = list.get(i);

            // 调用存储过程
            String personCode = (String) map.get("personCode");// 人员代码
            String personName = (String) map.get("personName");// 档案类型号
            String status = (String) map.get("status");// 档案类型号
            if (status.equalsIgnoreCase("√")) {
                continue;
            }
            String flag = "";//
            String desc = "";//

//            String execSql = String.format("exec [HG_LINK].[hg].[dbo].CheckExistsPerson '%s','%s','%s','%s','%s'",
//                    companyCode, archtypeCode, typeCode, edit, learningReq, courseware, questions, missItem,empower,train,practicable,jobDescription,auditer);
//            log.info("\n==========准备调用存储过程:{}", execSql);
//            sqlService.execute(CloudSqlService.htEas, execSql);
            JdbcTemplate jdbcTemplate = sqlService.getJdbcTemplateByDbCode(CloudSqlService.htEas);

            returnValue = (String) jdbcTemplate.execute(
                    new CallableStatementCreator() {
                        public CallableStatement createCallableStatement(Connection con) throws SQLException {
                            CallableStatement cs = con.prepareCall("{call [HG_LINK].[hg].[dbo].CheckExistsPerson (?,?,?,?)}");
                            // 设置输入参数的值
                            cs.setString(1, personCode);
                            cs.setString(2, personName);
                            cs.setString(3, flag);
                            cs.setString(4, desc);


                            cs.registerOutParameter(3, Types.VARCHAR);// 注册输出参数的类型
                            cs.registerOutParameter(4, Types.VARCHAR);// 注册输出参数的类型
                            return cs;
                        }
                    }, new CallableStatementCallback() {
                        public Object doInCallableStatement(CallableStatement cs) throws SQLException, DataAccessException {
                            cs.execute();
                            String flag = cs.getString(3);
                            String desc = cs.getString(4);
                            if (flag.equalsIgnoreCase("0")) {
                                return desc;
                            }
                            else
                            {
                                return flag;
                            }

                        }
                    });

            log.info("\n=============[待完成工作清单]存储过程执行完成，返回的fid:{}", returnValue);

//            if (returnValue.equalsIgnoreCase("0")) {
//                return getErrResponseResult(-1L, result.toString());
//            }
//
//            if (returnValue.equalsIgnoreCase("1")) {
//                return getOkResponseResult("成功");
//            }
        }


//        if (result.length() > 0) {
//            return getErrResponseResult(-1L, result.toString());
//        }
//
//        return getOkResponseResult("成功");


        return getOkResponseResult(returnValue);

    }

    @RequestMapping("WorkNoIsExist")
    //校验工号是否存在
    /**
     exec     alter    proc     [dbo].[CheckExistsWorkNo]			----检查工号是否存在
     @number				nvarchar(100),			----员工代码
     @workno				nvarchar(100),			----工号
     @flag				int  output,				----  1   可以提交    0   校验失败
     @Desc				nvarchar(500)  output   ----  @flag 为0时，错误信息

     */

    public ResponseResult WorkNoIsExist(@RequestBody Map<String, Object> params) {

        List<Map<String, Object>> list = (List<Map<String, Object>>) params.get("list");
        String appEmp = (String) params.get("employeeNo");
//        DocAPI api = new DocAPI();
        StringBuilder result = new StringBuilder();
        int SubCheck = 0;

        String returnValue = "";
        for (int i = 0; i < list.size(); i++) {
            String fnumber = "";
            Map<String, Object> map = list.get(i);

            // 调用存储过程
            String WorkNo = (String) map.get("personNumber");// 工号
//            String personName = (String) map.get("personName");// 档案类型号
//            String status = (String) map.get("status");// 档案类型号
//            if (status.equalsIgnoreCase("√")) {
//                continue;
//            }
            String flag = "";//
            String desc = "";//

//            String execSql = String.format("exec [HG_LINK].[hg].[dbo].CheckExistsPerson '%s','%s','%s','%s','%s'",
//                    companyCode, archtypeCode, typeCode, edit, learningReq, courseware, questions, missItem,empower,train,practicable,jobDescription,auditer);
//            log.info("\n==========准备调用存储过程:{}", execSql);
//            sqlService.execute(CloudSqlService.htEas, execSql);
            JdbcTemplate jdbcTemplate = sqlService.getJdbcTemplateByDbCode(CloudSqlService.htEas);

            returnValue = (String) jdbcTemplate.execute(
                    new CallableStatementCreator() {
                        public CallableStatement createCallableStatement(Connection con) throws SQLException {
                            CallableStatement cs = con.prepareCall("{call [HG_LINK].[hg].[dbo].CheckExistsWorkNo (?,?,?,?)}");
                            // 设置输入参数的值
                            cs.setString(1, appEmp);
                            cs.setString(2, WorkNo);
                            cs.setString(3, flag);
                            cs.setString(4, desc);


                            cs.registerOutParameter(3, Types.VARCHAR);// 注册输出参数的类型
                            cs.registerOutParameter(4, Types.VARCHAR);// 注册输出参数的类型
                            return cs;
                        }
                    }, new CallableStatementCallback() {
                        public Object doInCallableStatement(CallableStatement cs) throws SQLException, DataAccessException {
                            cs.execute();
                            String flag = cs.getString(3);
                            String desc = cs.getString(4);
                            if (flag.equalsIgnoreCase("0")) {
                                return desc;
                            }
                            else
                            {
                                return flag;
                            }

                        }
                    });

            log.info("\n=============[检查工号是否存在]存储过程执行完成，返回的fid:{}", returnValue);

//            if (returnValue.equalsIgnoreCase("0")) {
//                return getErrResponseResult(-1L, result.toString());
//            }
//
//            if (returnValue.equalsIgnoreCase("1")) {
//                return getOkResponseResult("成功");
//            }
        }


//        if (result.length() > 0) {
//            return getErrResponseResult(-1L, result.toString());
//        }
//
//        return getOkResponseResult("成功");


        return getOkResponseResult(returnValue);

    }

    //校验附件文件是否存在
    private ResponseResult AttachfileIsExist(List<Map<String, Object>> list, String appEmp) {

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> map = list.get(0);
            String fNumber = (String) map.get("MainDeptId");
            String att = (String) map.get("att");

            String submitAttribute = fNumber + ".006 PUB " + appEmp;
            JSONObject pubFolderObject = DocAPI.getFileID(submitAttribute);

            int fileNum = Integer.parseInt(pubFolderObject.getString("num"));
            if (fileNum == 0) {
                result.append(String.format("第%d行档案请上传文件！", i + 1));
                //文件未上传,退出循环
                break;
            }

            //判断是否有文件名称 = 档案名称+档案代码+备注
            String fileName = (String) map.get("fileName");
            String remarks = (String) map.get("remarks");
            String fileCode = (String) map.get("fileCode");
            String fileNames = fileName.concat(fileCode).concat(remarks);
            if (!existFileName(pubFolderObject, fileNames)) {
                result.append(String.format("第%d行档案,浩通云盘未发现档案: %s ,", i + 1, fileNames));
                break;
            }
            //授权
            String mainDeptAssDuty = (String) map.get("mainDeptAssDuty");
            if (!fileAuthorization(mainDeptAssDuty)) {
                result.append(String.format("第%d行档案,浩通云盘文档授权失败: %s ,", i + 1, fileNames));
                break;
            }

        }

        if (result.length() > 0) {
            return getErrResponseResult(-1L, result.toString());
        }

        return getOkResponseResult("成功");
    }

    //判断是否有文件名称= 档案名称+档案代码+备注
    private boolean existFileName(JSONObject pubFolderObject, String fileNames) {

        JSONArray dataArray = pubFolderObject.getJSONArray("data");
        JSONObject data = dataArray.getJSONObject(0);
        String id = data.getString("id");
        JSONObject pubFolderList = DocAPI.getList(id);
        JSONArray pubFolderData = pubFolderList.getJSONArray("data");

        for (int i = 0; i < pubFolderData.size(); i++) {
            JSONObject folderDataJSONObject = pubFolderData.getJSONObject(i);
            String name = folderDataJSONObject.getString("fileName");

            name = name.substring(0, name.lastIndexOf("."));
            if (fileNames.equals(name)) {
                return true;
            }
        }

        return false;
    }


    //文件授权
    private boolean fileAuthorization(String mainDeptAssDuty) {

        String sqlMainDeptAssDutyCode = "select fname_l2 from [HG_LINK].[hg].dbo.T_org_position WHERE FNUMBER ='" + mainDeptAssDuty + "'";
        Map<String, Object> result = sqlService.getMap(CloudSqlService.htEas, sqlMainDeptAssDutyCode);

        if (result.size() == 0) {
            return false;
        }
        String mainDeptAssDutyName = (String) result.get("fname_l2");

        if (StringUtils.isEmpty(mainDeptAssDutyName)) {
            return false;
        }

        String posMainDuty, posMainName, authFileId, MainDeptAssDuty, MainDeptAssDutyName, FunMainDeptAssDuty, FunMainDeptAssDutyName;
        posMainDuty = posMainName = authFileId = "";
        MainDeptAssDuty = MainDeptAssDutyName = FunMainDeptAssDuty = FunMainDeptAssDutyName = "";

        roleAuthService.RoleAuthDis(mainDeptAssDuty, posMainName, authFileId);//主责岗，上级直到董事长授下载权
//        roleAuthService.AssRoleAuthDis( posMainDuty, posMainName, authFileId);//主责岗助理授下载权
//        roleAuthService.RoleAAuthDis( MainDeptAssDuty, MainDeptAssDutyName, authFileId); //档案上传岗所有权限，上级直到董事长授下载权
//        roleAuthService.AssRoleAuthDis( MainDeptAssDuty, MainDeptAssDutyName, authFileId);//档案上传岗助理下载权
//        roleAuthService.RoleAuthDis( FunMainDeptAssDuty, FunMainDeptAssDutyName, authFileId);    //职能总责部门助理，上级直到董事长授下载权
//        if (PosA != null)
//        {
//
//            RoleAuth.RoleAuthDis(PosA, PosAName, AuthFileId);
//        }
//        if (PosB != null)
//        {
//            PosBDuty = PosB;
//            PosBName=PosBName;
//            RoleAuth.RoleAuthDis(PosBDuty, PosBName, authFileId);
//        }


        return true;
    }


    @GetMapping("/getDyjl")
    public Map<String, Object> getDyjl(String code, Integer page, Integer pageNum) {

        String sqlBase = "select \n" +
                "'公司物料信息' as 类型,\n" +
                "A.FNumber as 代码,\n" +
                "A.FName_l2 as 名称,\n" +
                "A.FNote as 对应说明,\n" +
                "DEPT.FNumber AS 管理部门代码,\n" +
                "DEPT.FSimpleName  AS  管理部门,\n" +
                "CODEA.FNumber AS 岗位A代码,\n" +
                "CODEA.FName_L2  as 岗位A ,\n" +
                "CODEB.FNumber AS 岗位B代码,\n" +
                "CODEB.FName_L2 as 岗位B\n" +
                "FROM   T_MAN_SiteInfo   A \n" +
                "LEFT  JOIN   T_ORG_Admin  DEPT  ON   A.FManageDeptID=DEPT.FID\n" +
                "left  join  T_ORG_Position  CODEA  on  a.FPositionAID=CODEA.FID\n" +
                "left  join  T_ORG_Position  CODEB  ON  A.FPositionBID=CODEB.FID\n" +
                "left  join  T_ORG_Admin  org  on  a.FOrgID=org.FID\n" +
                "left  join  T_DUT_DutyRecord_Base  duty  on  a.FComFunCodeID=duty.FID\n" +
                "left  join  T_BD_Person  pa  on  a.FManagerAID=pa.FID\n" +
                "left  join  T_BD_Person  pb  on  a.FManagerBID=pb.FID\n" +
                "WHERE  A.FStatus ='启用' \n" +
                "union  all\t\t\n" +
                "select \n" +
                "'公司场地信息' as 类型,\n" +
                "CMAT.FNumber  AS  代码,\n" +
                "MAT.FName_l2 AS 名称,\n" +
                "MAT.FsimpleNAme 规格型号,\n" +
                "COMMAINDEPT.FNumber  公司主责部门代码,\n" +
                "commaindept.FName_L2 公司主责部门,\n" +
                "POSA.FNumber  公司主责岗位A代码,\n" +
                "posa.FName_L2 公司主责岗位A,\n" +
                "POSB.FNumber  公司主责岗位B代码,\n" +
                "posB.FName_L2 公司主责岗位B\n" +
                "from   T_CON_CompanyMaterialInfo  CMAT    \n" +
                "JOIN   T_CON_MaterialInfo  MAT  \n" +
                "ON  CMAT.FMatCodeID=MAT.FID  \n" +
                "LEFT  JOIN  T_BD_MeasureUnit  BASEUNIT  \n" +
                "ON  MAT.FUnitID=BASEUNIT.FID\n" +
                "LEFT JOIN  T_BD_MeasureUnit   ASSTUNIT\n" +
                "ON  MAT.FAuxUnitID=ASSTUNIT.FID \n" +
                "LEFT  JOIN  T_ORG_Admin  MAINDEPT  \n" +
                "ON  MAT.FMainDeptID=MAINDEPT.FID \n" +
                "LEFT JOIN  T_ORG_Position  MAINPOS  \n" +
                "ON  MAT.FPositionID=MAINPOS.FID \n" +
                "LEFT  JOIN   T_ORG_Admin  COMMAINDEPT  \n" +
                "ON  CMAT.FCompanyDeptID=COMMAINDEPT.FID \n" +
                "left join  T_ORG_Position  posa  \n" +
                "on  CMAT.FPositionAID=POSA.FID\n" +
                "left join  T_ORG_Position  posB  \n" +
                "on  CMAT.FPositionBID=POSB.FID \n" +
                "left  join  T_BD_Person  persona\n" +
                "on  cmat.FComMainPICAID=persona.fid\n" +
                "left  join  T_BD_Person  personb\n" +
                "on  cmat.FComMainPICBID=personb.fid  \n" +
                "left join  T_BD_Person  pur \n" +
                "on  CMAT.FPurchasingID=PUR.FID \n";


        Integer offset = page * pageNum;
        Integer limit = (page + 1) * pageNum;

        StringBuilder sql = new StringBuilder();

        code = code == null ? "" : code;

        sql.append("select count(*) as count from ")
                .append("( ").append(sqlBase).append(" ) as M")
                .append("where M.代码 like '%").append(code).append("%'");

        //Map<String, Object> map = CloudSqlService.getMap(CloudSqlService.htEas, sql.toString());
        Map<String, Object> map = sqlService.getMap(CloudSqlService.htEas, sql.toString());

        Integer count = (Integer) map.get("count");

        sql.setLength(0);
        sql.append("SELECT * FROM (  SELECT ROW_NUMBER () OVER(ORDER BY 代码) AS Row, zz.*  FROM ")
                .append("(").append(sqlBase).append(") as zz")
                .append(" where  1=1  and zz.代码 like '%").append(code).append("%'   ) AS M ")
                .append("where M.Row> ").append(offset).append(" AND  M.Row<=").append(limit);

        //List<Map<String, Object>> list = CloudSqlService.getList(CloudSqlService.htEas, sql.toString());
        List<Map<String, Object>> list = sqlService.getList(CloudSqlService.htEas, sql.toString());


        Map<String, Object> result = new HashMap<>();
        result.put("data", list);
        result.put("count", count);

        return result;
    }

    /**
     * 根据EAM岗位编码查询对应的员工号，再通过对应的员工号查询BPM对应的id
     *
     * @param posnumber
     * @return
     */
    @RequestMapping(value = "/getDataByPost", method = {RequestMethod.POST, RequestMethod.GET})
    public List<Map<String, Object>> getDataByPost(@RequestParam(value = "posnumber", required = false) String posnumber) {

        String sqlMainDeptAssDutyCode = "select a.fid,b.FNumber personnumber,b.FName_L2 personname,c.FNumber posnumber,c.FName_L2 posname" +
                " from [HG_LINK].[hg].[dbo].T_ORG_PositionMember a inner join [HG_LINK].[hg].[dbo].t_bd_person b on a.fpersonid=b.fid " +
                " inner join [HG_LINK].[hg].[dbo].t_org_position c on a.fpositionid=c.fid " +
                " where c.FNumber = '" + posnumber + "'";
        log.info("getDataByPost，查询SQL:{}", sqlMainDeptAssDutyCode);
        List<Map<String, Object>> result = sqlService.getList(CloudSqlService.htEas, sqlMainDeptAssDutyCode);
        log.info("getDataByPost，查询结果:{}", result);

        if (result.size() == 0) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        for (Map<String, Object> map : result) {
            if (map != null && map.containsKey("personnumber")) {
                String personnumber = (String) map.get("personnumber");
                if (StringUtils.isEmpty(sb.toString())) {
                    sb.append("'" + personnumber + "'");
                } else {
                    sb.append("," + "'" + personnumber + "'");
                }
            }
        }
        //通过员工编号查询员工信息
        List<String> ids = new ArrayList<>();
        List<Map<String, Object>> userMaps = new ArrayList<>();
        List<SelectionValue> selectionValues = new ArrayList<>();
        if (!StringUtils.isEmpty(sb.toString())) {
            String sqlInPerson = "select id,name from h_org_user where employeeNo in (" + sb.toString() + ")";
            List<Map<String, Object>> resultPerson = sqlService.getList(CloudSqlService.htAz, sqlInPerson);
            if (CollectionUtils.isNotEmpty(resultPerson)) {
                for (Map<String, Object> resultMap : resultPerson) {
                    String id = (String) resultMap.get("id");
                    String name = (String) resultMap.get("name");
                    Map<String, Object> userMap = new HashMap<>();
                    userMap.put("id", id);
                    userMap.put("type", 3);
                    userMap.put("name", name);
                    userMaps.add(userMap);

                    SelectionValue selectionValue = new SelectionValue();
                    selectionValue.setId(id);
                    selectionValue.setName(name);
                    selectionValue.setType(UnitType.USER);
                    selectionValues.add(selectionValue);
                }
            }
        }
        return userMaps;
    }


    /**
     * 通过对应的员工号查询BPM对应的id
     *
     * @param jobNo
     * @return
     */
    @RequestMapping(value = "/getDataByJobNo", method = {RequestMethod.POST, RequestMethod.GET})
    public List<Map<String, Object>> getDataByJobNo(@RequestParam(value = "jobNo", required = false) String jobNo) {


        //通过员工编号查询员工信息
        List<Map<String, Object>> userMaps = new ArrayList<>();
        if (!StringUtils.isEmpty(jobNo)) {
            String sqlInPerson = "select id,name from h_org_user where employeeNo = '" + jobNo + "' and deleted = '0'";
//            List<Map<String, Object>> resultPerson = sqlService.getList(CloudSqlService.htAz, sqlInPerson);
            Map<String, Object> resultPerson = sqlService.getMap(sqlInPerson);
            if (resultPerson != null && Objects.nonNull(resultPerson.get("id"))) {
                String id = (String) resultPerson.get("id");
                String name = (String) resultPerson.get("name");
                Map<String, Object> userMap = new HashMap<>();
                userMap.put("id", id);
                userMap.put("type", 3);
                userMap.put("name", name);
                userMaps.add(userMap);
            }
        }
        return userMaps;
    }


    /**
     * 根据EAM岗位编码查询对应的员工号，通过对应的员工号查询BPM对应的id
     *
     * @param posnumber
     * @return
     */
    @RequestMapping(value = "/updateyPost", method = {RequestMethod.POST, RequestMethod.GET})
    private int insertPost(@RequestParam(value = "posnumber", required = false) String posnumber) {

        String sql = "insert into user (username,password) values (:username,:password)";
        Map<String, Object> map = new HashMap<>();
        map.put("username", "111");
        map.put("password", "222");
        int num = sqlService.update(CloudSqlService.htEas, sql, map);
        return num;
    }


//    /**
//     * 通过对应的员工号查询BPM对应的id
//     *
//     * @return
//     */
//    @RequestMapping(value="/postTest",method = {RequestMethod.POST ,RequestMethod.GET })
//    private void postTest(){
//        Map<String,Object> param=new HashMap<>();
//        param.put("content","审批");
//        param.put("url","");
//        param.put("username","02.0101");
//        param.put("classify","云枢");
//        param.put("publishTime","");
//        param.put("fassignId","dba15e2608ce446c9e9a34b856287660");
//        param.put("originaTor","");
//        param.put("processTakes","");
//        log.info("---------OaWorkItemServiceImpl.post--------------param:{}", com.alibaba.fastjson.JSONObject.toJSONString(param));
//        HttpHeaders headers = new HttpHeaders();
////        headers.setContentType(MediaType.parseMediaType("application/json;charset=UTF-8"));
////        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
//        headers.set("Authorization", "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYXBwX2FwaV9yZXNvdXJjZSIsImVudGVycHJpc2VfbW9iaWxlX3Jlc291cmNlIiwiYmZmX2FwaV9yZXNvdXJjZSJdLCJzY29wZSI6WyJyZWFkIl0sImV4cCI6MTY2ODg2OTY5NywiYXV0aG9yaXRpZXMiOlsiUk9MRV9BUFBMSUNBVElPTl9BUEkiLCJST0xFX0VORF9VU0VSIl0sImp0aSI6ImJiNDhlYmZjLTk1NmQtNDRlZS1iZDJiLWQxZTAyMDFlZjhmZCIsImNsaWVudF9pZCI6IjBiOTA4ZDM1ZmY4ZWYyYzEzODU3ZDYxZjlkMTNlOWQ4bzE0czNCZUw1WjQifQ.BQbcOmXbVmY5iHNxu0pzAmcoBlBdw8PvoO91X7VfEcA");
//
//        HttpEntity stringHttpEntity = new HttpEntity( param, headers);
////        log.info("---------OaWorkItemServiceImpl.post--------------url:{}",url);
////        log.info("---------OaWorkItemServiceImpl.post--------------stringHttpEntity:{}", com.alibaba.fastjson.JSONObject.toJSONString(stringHttpEntity));
//        Map result = restTemplate.postForObject("http://120.27.239.190/api/bff/v1.2/enduser/plugin_msg_centers/create", stringHttpEntity, Map.class);
//
//        log.info("\n==============浩通门户待办操作 响应: rest={}", com.alibaba.fastjson.JSONObject.toJSONString(result));
//
//        boolean success = MapUtils.getBoolean(result, "success",false);
//
//    }
//
//    /**
//     * 通过对应的员工号查询BPM对应的id
//     *
//     * @return
//     */
//    @RequestMapping(value="/postDeleteTest",method = {RequestMethod.POST ,RequestMethod.GET })
//    private void postDeleteTest(){
//        Map<String,Object> param=new HashMap<>();
//        param.put("content","审批");
//        param.put("url","http://116.62.18.107/websso/index.html#/form/detail?workitemId=dba15e2608ce446c9e9a34b856287660&workflowInstanceId=e217fbe49e5742fca0bca49303239333");
//        param.put("username","02.0101");
//        param.put("classify","云枢");
//        param.put("publishTime","");
//        param.put("fassignId","dba15e2608ce446c9e9a34b856287660002");
//        param.put("originaTor","");
//        param.put("processTakes","");
//        log.info("---------OaWorkItemServiceImpl.post--------------param:{}", com.alibaba.fastjson.JSONObject.toJSONString(param));
//        HttpHeaders headers = new HttpHeaders();
////        headers.setContentType(MediaType.parseMediaType("application/json;charset=UTF-8"));
////        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
//        headers.set("Authorization", "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYXBwX2FwaV9yZXNvdXJjZSIsImVudGVycHJpc2VfbW9iaWxlX3Jlc291cmNlIiwiYmZmX2FwaV9yZXNvdXJjZSJdLCJzY29wZSI6WyJyZWFkIl0sImV4cCI6MTY2ODg2OTY5NywiYXV0aG9yaXRpZXMiOlsiUk9MRV9BUFBMSUNBVElPTl9BUEkiLCJST0xFX0VORF9VU0VSIl0sImp0aSI6ImJiNDhlYmZjLTk1NmQtNDRlZS1iZDJiLWQxZTAyMDFlZjhmZCIsImNsaWVudF9pZCI6IjBiOTA4ZDM1ZmY4ZWYyYzEzODU3ZDYxZjlkMTNlOWQ4bzE0czNCZUw1WjQifQ.BQbcOmXbVmY5iHNxu0pzAmcoBlBdw8PvoO91X7VfEcA");
//
//        HttpEntity stringHttpEntity = new HttpEntity( param, headers);
////        log.info("---------OaWorkItemServiceImpl.post--------------url:{}",url);
////        log.info("---------OaWorkItemServiceImpl.post--------------stringHttpEntity:{}", com.alibaba.fastjson.JSONObject.toJSONString(stringHttpEntity));
//        ResponseEntity<Map> exchange = restTemplate.exchange("http://120.27.239.190/api/bff/v1.2/enduser/plugin_msg_centers/delkingdee", HttpMethod.DELETE, stringHttpEntity, Map.class);
//        Map result = exchange.getBody();
//
//        log.info("\n==============浩通门户待办操作 响应: rest={}", com.alibaba.fastjson.JSONObject.toJSONString(result));
//
//        boolean success = MapUtils.getBoolean(result, "success",false);
//
//    }

}

package com.authine.cloudpivot.ext.controller;

import com.alibaba.fastjson.JSONObject;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.ext.modules.vo.FileCodeSortVO;
import com.authine.cloudpivot.ext.service.CloudSqlService;
import com.authine.cloudpivot.web.api.controller.base.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * 档案代码排序
 **/
@RestController
@RequestMapping("/public/FileCodeSortController")
@Slf4j
public class FileCodeSortController extends BaseController {


    @Autowired
    CloudSqlService sqlService;

    //    public static void main(String[] args) {
//        List<FileCodeSortVO> list = new ArrayList<>();
//        list.add(new FileCodeSortVO("D-Ⅰ", "HG-002"));
//        list.add(new FileCodeSortVO("D-Ⅲ", "02-Ⅲ03-05-07-01"));
//        list.add(new FileCodeSortVO("D-Ⅱ", "02-Ⅱ03-05-05-05"));
//        list.add(new FileCodeSortVO("D-Ⅰ", "HG-001"));
//        list.add(new FileCodeSortVO("D-Ⅳ", "0202-Ⅳ03-05-07-01"));
//        list.add(new FileCodeSortVO("D-Ⅳ", "0201-Ⅳ03-06-07-01"));
//        list.add(new FileCodeSortVO("D-Ⅳ", "0201-Ⅳ03-01-07-01"));
//        list.add(new FileCodeSortVO("D-Ⅲ", "02-Ⅲ03-04-07-01"));
//        list.add(new FileCodeSortVO("RH", "RH-31-03-06-03"));
//        list.add(new FileCodeSortVO("AQ", "AQ-31-03-06-03"));
//        list.add(new FileCodeSortVO("D-Ⅳ", "02-Ⅳ03-05-03-03"));
//        list.add(new FileCodeSortVO("D-Ⅱ", "02-Ⅱ03-05-07-05"));
//        list.add(new FileCodeSortVO("RD", "RD-31-03-06-02"));
//        Collections.sort(list);
//        System.out.println(list);
//    }

    /**
     * @param bizObjectId          表单id
     * @param schemaCode           模型编码
     * @param childTableSchemaCode 子表编码
     * @param typeKey              子表中“档案类型”数据项编码（考虑到不同的模型子表中“档案类型”的数据项编码可能不一致的情况）
     * @param codeKey              子表中“档案代码”数据项编码（考虑到不同的模型子表中“档案代码”的数据项编码可能不一致的情况）
     * @return
     */
    @RequestMapping("/sort")
    public List<FileCodeSortVO> sort(String bizObjectId, String schemaCode, String childTableSchemaCode, String typeKey, String codeKey) {
        //通过 schemaCode 和 bizObjectId 获取主表数据
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(schemaCode, bizObjectId);
        //通过 childTableSchemaCode 获取子表数据
        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get(childTableSchemaCode);
        List<FileCodeSortVO> fileCodeSortVOList = new ArrayList<>();
        list.stream().forEach(map -> {
            String fileType = MapUtils.getString(map, typeKey);//通过“档案类型”数据项编码获取 档案类型值
            String fileCode = MapUtils.getString(map, codeKey);//通过“档案代码”数据项编码获取 档案代码值
            fileCodeSortVOList.add(new FileCodeSortVO(fileType, fileCode));//创建FileCodeSortVO对象并添加进排序集合
        });
        if (CollectionUtils.isNotEmpty(list)) {
            //此时fileCodeSortVOList不为空，调用FileCodeSortVO重写的排序方法
            Collections.sort(fileCodeSortVOList);
        }
        //此时已完成排序，返回结果
        return fileCodeSortVOList;
    }

    /**
     * 将List<map>转换成List<java对象>
     * * @param list
     * * @param cls
     * * @param <T>
     * * @return
     */
    public static <T> List<T> getListObject(List<Map<String, Object>> list, Class<T> cls) {
        List<T> paramList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(list)) {
            for (Map<String, Object> map : list) {
                paramList.add(parseMapObject(map, cls));
            }
        }
        return paramList;
    }

    /**
     * 将map转换成java对象
     *
     * @param paramMap * @param cls
     *                 * @param <T>
     *                 * @return
     */
    public static <T> T parseMapObject(Map<String, Object> paramMap, Class<T> cls) {
        return JSONObject.parseObject(JSONObject.toJSONString(paramMap), cls);
    }


}

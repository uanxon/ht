package com.authine.cloudpivot.ext.Utils;

/**
 * @Author hxd
 * @Date 2022/12/14 13:19
 * @Description
 **/
public class CustomSchemaCode {

    public static final String adminUserId = "2c9280a26706a73a016706a93ccf002b";
    /**
     * 总公司级档案基础信息 新增
     */
    public static final String fileBaseAdd = "test11";

    /**
     * 总公司级档案基础信息 作废
     */
    public static final String VoidMainComArch = "VoidMainComArch";

    /**
     * 基础表-总公司档案
     */
    public static final String fileInfoBase = "fileInfoBase";

    /**
     * 公司级档案基础信息 新增
     */
    public static final String companyFileBaseAdd = "companyFileBaseInfo";

    /**
     * 公司级档案基础信息 作废
     */
    public static final String companyFileBaseInfoVoid = "companyFileBaseInfoVoid";
    /**
     * 基础表-公司档案
     */
    public static final String comGeneralArchBase = "comGeneralArchBase";


    /**
     * 总公司级档案下级公司授权信息
     */
    public static final String SubordinateAuthInfo = "SubordinateAuthInfo";

    /**
     * 基础表-总公司级档案下级公司授权
     */
    public static final String lowerLevelComAuth = "lowerLevelComAuth";


    /**
     * 总公司级档案部门授权信息
     */
    public static final String MainCompanyDepartmentAuthInfo = "DepartmentAuthInfo";

    /**
     * 基础表-总公司级档案中间组织授权信息
     */
    public static final String MainCompanyDeptAuthInfo = "mainCompanyDeptAuthInfo";


    /**
     * 总公司级档案基层组织授权信息
     */
    public static final String MainPrimaryLevelAuthInfo = "PrimaryLevelAuthInfo";

    /**
     * 基础表-总公司级档案基层组织授权信息
     */
    public static final String MainCompanyFileBasicAuth = "mainCompanyFileBasicAuth";


    /**
     * 公司级档案部门授权信息
     */
    public static final String ComDepartmentAuthInfo = "comDepartmentAuthInfo";

    /**
     * 基础表-公司级档案部门授权信息
     */
    public static final String CompanyDeptAuthInfoBase = "companyDeptAuthInfo";


    /**
     * 公司级档案基层组织授权信息
     */
    public static final String ComPrimaryLevelAuthInfo = "comPrimaryLevelAuthInfo";

    /**
     * 基础表-公司级档案基层组织授权信息
     */
    public static final String CompanyFileBasicAuthBase = "companyFileBasicAuth";


    /**
     * 基础表-岗位配置信息基础 新增
     */
    public static final String posConfigBaseAdd = "PositionConfigBaseBill";

    /**
     * 基础表-岗位配置信息基础 作废
     */
    public static final String PosConfigBaseBillVoid = "PosConfigBaseBillVoid";


    /**
     * 基础表-岗位配置信息基础
     */
    public static final String posConfigBase = "PosConfigBaseBillBase";


    /**
     * 基础表-岗位配置信息配置 新增
     */
    public static final String PositionConfigBillAdd = "PositionConfigBill";

    /**
     * 基础表-岗位配置信息配置 作废
     */
    public static final String PositionConfigBillVoid = "PositionConfigBillVoid";


    /**
     * 基础表-岗位配置信息配置
     */
    public static final String PositionConfigBillBase = "PositionConfigBillBase";


    /**
     * 基础表-公司职能信息 新增
     */
    public static final String FunAddNew = "FunAddNew";


    /**
     * 基础表-公司职能信息
     */
    public static final String companyFunBase = "companyFunBase";
    /**
     * 基础表-公司职能信息 作废
     */
    public static final String ComFunVoid = "ComFunVoid";

    /**
     * 基础表-总公司职能信息 新增
     */
    public static final String headFunAddNew = "headFunAddNew";
    /**
     * 基础表-总公司职能信息 作废
     */
    public static final String headFunVoid = "headFunVoid";


    /**
     * 基础表-总公司职能信息
     */
    public static final String corpFunBase = "corpFunBase";

    /**
     * 职能职责表 一级
     */
    public static final String funOneLevelAdd = "funOneLevel";
    /**
     * 职能职责表 一级 - 作废
     */
    public static final String toVoidFunOneLevel = "voidFunOneLevel";

    /**
     * 职能职责表 一级 基础
     */
    public static final String funOneLevelBase = "funOneLevelBase";


    /**
     * 职能职责表 二级
     */
    public static final String funTwoLevelAdd = "funTwoLevel";

    /**
     * 职能职责表 二级 - 作废
     */
    public static final String toVoidFunTwoLevel = "voidFunTwoLevel";

    /**
     * 职能职责表 二级 基础
     */
    public static final String funTwoLevelBase = "funTwoLevelBase";


    /**
     * 配置周知卡布置
     */
    public static final String TrainingCardArrange = "trainingCardArrange";

    /**
     * 配置周知卡 实施
     */
    public static final String TrainingConduct = "trainingConduct";


    /**
     * “基层组织” 学习记录（二）
     */
    public static final String LearningRecord = "LearningRecord";


    /**
     * 基础表 : “基层组织” 学习记录（二）
     */
    public static final String LearningRecordBase = "LearningRecordBase";


    /**
     * 人员信息记录 基础信息
     */
    public static final String personInfoRecord = "personInfoRecord";
    /**
     * 人员信息记录 基础信息 作废
     */
    public static final String toVoidPersonInfoRecord = "voidPersonInfoRecord";
    /**
     * 人员信息 基础表
     */
    public static final String personInfoBase = "personInfoBase";
    /**
     * 人员信息记录 人事信息
     */
    public static final String personnelInfo = "personnelInfo";
    /**
     * 人员信息记录 人事信息 作废
     */
    public static final String toVoidPersonnelInfo = "voidPersonnelInfo";

    /**
     * 待完成工作清单
     */
    public static final String incompleteWorkList = "incompleteWorkList";
    /**
     * 待完成工作清单 子表
     */
    public static final String incompleteWorkListDetail = "incompleteWorkListDetail";
    /**
     * 待完成工作清单 修改
     */
    public static final String incompleteWorkListUpdate = "incompleteWorkListUpdate";
    /**
     * 待完成工作清单 修改 子表
     */
    public static final String incompleteWorkListDetailUpda = "incompleteWorkListDetailUpda";
    /**
     * 待完成工作清单 基础表
     */
    public static final String incompleteWorkList_Basic = "incompleteWorkList_Basic";
    /**
     * 待完成工作清单 作废
     */
    public static final String voidIncompleteWorkList = "voidIncompleteWorkList";
    /**
     * 待完成工作清单 作废 子表
     */
    public static final String voidIncompleteWorkListDetail = "voidIncompleteWorkListDetail";

    /**
     * 待优化工作清单
     */
    public static final String optimizedWorkList = "optimizedWorkList";
    /**
     * 待优化工作清单 子表
     */
    public static final String optimizedWorkListDetail = "optimizedWorkListDetail";
    /**
     * 待优化工作清单 基础表
     */
    public static final String optimizedWorkList_Basic = "optimizedWorkList_Basic";
    /**
     * 待优化工作清单 修改
     */
    public static final String optimizedWorkListUpdate = "optimizedWorkListUpdate";
    /**
     * 待优化工作清单 修改 子表
     */
    public static final String optimizedWorkListDetailUpda = "optimizedWorkListDetailUpda";
    /**
     * 待优化工作清单 作废
     */
    public static final String voidOptimizedWorkList = "voidOptimizedWorkList";
    /**
     * 待优化工作清单 作废 子表
     */
    public static final String voidOptimizedWorkListDetail = "voidOptimizedWorkListDetail";

    //常态工作清单编码
    public static final String NORMAL_WORK_LIST = "normalWorkList";
    //常态工作清单子表编码
    public static final String NORMAL_WORK_LIST_DETAIL = "normalWorkListDetail";

    //常态工作清单 修改
    public static final String normalWorkListUpdate = "normalWorkListUpdate";
    //常态工作清单 修改 子表编码
    public static final String normalWorkListDetailUpdate = "normalWorkListDetailUpdate";

    //常态工作清单基础表编码
    public static final String NORMAL_WORK_LIST_BASE = "normalWorkListBase";

    //作废常态工作清单编码
    public static final String VOID_NORMAL_WORK_LIST = "voidNormalWorkList";
    //作废常态工作清单子表编码
    public static final String VOID_NORMAL_WORK_LIST_DETAIL = "voidNormalWorkListDetail";

    /**
     * 总公司往来单位基础信息
     */
    public static final String headOfficeDealingUnit = "headOfficeDealingUnit";
    /**
     * 总公司往来单位基础信息 子表
     */
    public static final String headOfficeDealingUnitDetail = "headOfficeDealingUnitDetail";
    /**
     * 总公司往来单位基础信息 基础表
     */
    public static final String headOfficeDealingUnitBas = "headOfficeDealingUnitBas";
    /**
     * 总公司往来单位基础信息 作废
     */
    public static final String voidHeadOfficeDealing = "voidHeadOfficeDealing";
    /**
     * 总公司往来单位基础信息 作废 子表
     */
    public static final String voidHeadOfficeDealingDetail = "voidHeadOfficeDealingDetail";
    /**
     * 总公司往来单位财务信息
     */
    public static final String ZGS_DealingUnitFinance = "ZGS_DealingUnitFinance";
    /**
     * 总公司往来单位财务信息 子表
     */
    public static final String ZGS_DealingUnitFinanceDetail = "ZGS_DealingUnitFinanceDetail";
    /**
     * 总公司往来单位财务信息 作废
     */
    public static final String voidZGSDealUnitFinance = "voidZGSDealUnitFinance";
    /**
     * 总公司往来单位财务信息 作废 子表
     */
    public static final String voidZGSDealUnitFinanceDetail = "voidZGSDealUnitFinanceDetail";

    /**
     * 油卡信息记录
     */
    public static final String oilCarInformationRecord = "oilCarInformationRecord";
    /**
     * 油卡信息记录 子表
     */
    public static final String oilCarInformationRecordDetai = "oilCarInformationRecordDetai";
    /**
     * 油卡信息记录 基础表
     */
    public static final String oilCarInformationBasic = "oilCarInformationBasic";
    /**
     * 油卡信息记录 作废
     */
    public static final String toVoidOilCarInformation = "toVoidOilCarInformation";
    /**
     * 油卡信息记录 作废 子表
     */
    public static final String toVoidOilCarInformationDetai = "toVoidOilCarInformationDetai";
    /**
     * 加油记录
     */
    public static final String refuelRecord = "refuelRecord";
    /**
     * 加油记录 子表
     */
    public static final String refuelRecordDetai = "refuelRecordDetai";
    /**
     * 加油记录 基础表
     */
    public static final String refuelRecordBase = "refuelRecordBase";



    /**
     * 场地信息表
     */
    public static final String placeInformation = "placeInformation";
    /**
     * 场地信息表 子表
     */
    public static final String placeInformationDetail = "placeInformationDetail";
    /**
     * 场地信息基础表
     */
    public static final String placeInformationBase = "placeInformationBase";
    /**
     * 场地信息表 作废
     */
    public static final String toVoidPlaceInformation = "toVoidPlaceInformation";
    /**
     * 场地信息表 作废 子表
     */
    public static final String toVoidPlaceInformationDetail = "toVoidPlaceInformationDetail";


    /**
     * 融资规模
     */
    public static final String FinancingScaleAdd = "FinancingScale";


    /**
     * 融资规模 基础
     */
    public static final String FinancingScaleBase = "FinancingScaleBase";

    /**
     * 融资规模 作废
     */
    public static final String FinancingScaleVoid = "FinancingScaleVoid";

    /**
     * 融资记录
     */
    public static final String FinancingRecords = "FinancingRecords";


    /**
     * 融资记录 基础
     */
    public static final String FinancalRecordBase = "FinancalRecordBase";

    /**
     * 融资记录 作废
     */
    public static final String FinancingRecordsVoid = "FinancingRecordsVoid";



    /**
     * 融资规模使用
     */
    public static final String FinancingScaleUse = "FinancingScaleUse";


    /**
     * 融资规模使用 基础
     */
    public static final String FinancalScaleUseBase = "FinancalScaleUseBase";

    /**
     * 融资规模使用 作废
     */
    public static final String FinancalScaleUseVoid = "FinancalScaleUseVoid";


    /**
     * 信用额度使用
     */
    public static final String suppcredit = "suppcredit01";

    /**
     * 信用额度 结余
     */
    public static final String creditbalancebill = "creditbalancebill";

    /**
     * 信用额度使用 基础
     */
    public static final String CreditLineBase = "CreditLineBase";

    /**
     * 信用额度使用 作废
     */
    public static final String creditlinevoid = "creditlinevoid";

    /**
     * 错失标准
     */
    public static final String RPStandardAdd = "RPStandard";


    /**
     * 错失标准 基础
     */
    public static final String RPStandardBase = "RPStandardBase";

    /**
     * 错失标准 作废
     */
    public static final String RPStandardVoid = "RPStandardVoid";


    /**
     * 問題記錄
     */
    public static final String RpRecordAdd = "RpRecord";


    /**
     * 問題記錄 基础
     */
    public static final String RpRecordBase = "RpRecordBase";

    /**
     * 問題記錄 作废
     */
    public static final String RpRecordVoid = "RpRecordVoid";

    /**
     * 意见記錄
     */
    public static final String suggestrecord = "suggestrecord";


    /**
     * 意见記錄 基础
     */
    public static final String OpinionRecordBase = "OpinionRecordBase";

    /**
     * 意见記錄 作废
     */
    public static final String OpinionRecordVoid = "OpinionRecordVoid";

    /**
     * 人事档案点检
     */
    public static final String PersonnFileCheck = "PersonnFileCheck";


    /**
     * 問題記錄 基础
     */
    public static final String PersonFileCheckBase = "PersonFileCheckBase";

    /**
     * 采购入库
     */
    public static final String purchaseIntostorage = "purchaseIntostorage";

    /**
     * 生产入库
     */
    public static final String ProductInto = "ProductInto";

    /**
     * 其他入库
     */
    public static final String OtherInto = "OtherInto";

    /**
     * 调拨
     */
    public static final String TransferInto = "TransferInto";

    /**
     * 销售出库
     */
    public static final String SaleOut = "SaleOut";

    /**
     * 领料出库
     */
    public static final String pickingOut = "pickingOut";

    /**
     * 其他出库
     */
    public static final String otherOut = "otherOutFromStorage";


    /**
     * 库存
     */
    public static final String warehouse = "warehouse";


    /**
     * 预库存
     */
    public static final String warehousePre = "warehousePre";


    /**
     * 命名记录
     */
    public static final String NamedRecord = "namedRecord";

    /**
     * 命名记录基础表
     */
    public static final String NamedRecordBasic = "namedRecordBasic";

    /**
     * 命名记录子表
     */
    public static final String NamedRecordDetail = "namedRecordDetail";


    /**
     * 命名记录-修改
     */
    public static final String NamedRecordModify = "namedRecordUpdate";

    /**
     * 命名记录修改子表
     */
    public static final String NamedRecordDetailModify = "namedRecordDetailUpdate";


    /**
     * 命名记录-作废
     */
    public static final String NamedRecordCancel = "namedRecordCancel";

    /**
     * 命名记录作废子表
     */
    public static final String NamedRecordDetailCancel = "namedRecordDetailCancel";

    /**
     * 地区代码
     */
    public static final String REGION_CODE = "region_code";
    /**
     * 地区代码-作废
     */

    public static final String VOID_REGION_CODE = "void_region_code";

    /**
     * 地区代码基础表
     */
    public static final String REGION_CODE_BASE = "region_code_base";



    /**
     * 车辆信息
     */
    public static  final  String VehicleInfo = "vehicleInfo";


    /**
     * 车辆信息-子表
     */
    public static  final  String VehicleInfoDetail = "vehicleInfoDetail";


    /**
     * 车辆信息--基础信息表
     */
    public static  final  String VehicleInfoBasic = "vehicleInfoBasic";


    /**
     * 车辆信息--作废
     */
    public static  final  String VehicleInfoCancel = "vehicleInfoCancel";

    /**
     * 车辆信息--作废-子表
     */
    public static  final  String VehicleInfoDetailCancel = "vehicleInfoDetailCancel";

    /**
     * 车辆代码计数器
     */
    public static  final  String VehicleCodeCounter = "vehicleCodeCounter";

    /**
     * 出差计划
     */
    public static final String BusinessTripPlan = "businessTripPlan";

    /**
     * 出差计划 -子表
     */
    public static final String BusinessTripPlanDetail = "businessTripPlanDetail";

    /**
     * 出差计划基础表
     */
    public static final String BusinessTripPlanBasic = "businessTripPlanBase";



    /**
     * 出差计划 作废
     */
    public static final String BusinessTripPlanCancel = "businessTripPlanCancel";

    /**
     * 出差计划作废-子表
     */
    public static final String BusinessTripPlanDetailCancel = "businessTripPlanDetailCancel";

    /**
     * 相关档案
     */
    public static final String WORK_RELATED_FILES = "work_related_files";
    /**
     * 相关档案-子表
     */
    public static final String WORK_RELATED_FILES_DETAIL = "workRelatedFilesDetail";


    /**
     * 工作先关档案记录
     */
    public static final String WORK_RELATED_FILES_RECORD = "work_related_files_recor";

    /**
     * 相关档案-作废
     */
    public static final String VOID_WORK_RELATED_FILES = "void_work_related_files";
    /**
     * 相关档案-作废-子表
     */
    public static final String VOID_WORK_RELATED_FILES_DETAIL = "voidWorkRelatedFilesDetail";
    /**
     * 相关档案-作废-子表
     */
    public static final String QEO = "QEO";
    /**
     * 相关档案-作废-子表
     */
    public static final String QEO_DETAIL = "QEODetail";




    /**
     * 公司往来信息
     */
    public static final String CompanyBasicInfo = "companyBasicInfo";

    /**
     * 公司往来信息 -子表
     */
    public static final String CompanyBasicInfoDetail = "companyBasicInfoDetail";

    /**
     * 公司往来信息基础表
     */
    public static final String CompanyBasicInfoBasic = "companyBasicInfoBasic";

    /**
     * 公司往来信息 作废
     */
    public static final String CompanyBasicInfoCancel = "companyBasicInfoCancel";

    /**
     * 公司往来信息 作废 子表
     */
    public static final String CompanyBasicInfoDetailCancel = "companyBasicInfoDetailCancel";

    /**
     * 公司往来单位基础信息基础表 财务子表
     */
    public static final String CompanyFinancialDetailTotal = "companyFinancialDetailTotal";

    /**
     * 公司财务往来信息
     */
    public static final String CompanyFinancial = "companyFinancial";

    /**
     * 公司财务往来信息 -子表
     */
    public static final String CompanyFinancialDetail = "companyFinancialDetail";

    /**
     * 公司财务往来信息基础表
     */
    public static final String CompanyFinancialBasic = "companyFinancialBasic";

    /**
     * 公司财务往来信息 作废
     */
    public static final String CompanyFinancialCancel = "companyFinancialCancel";

    /**
     * 公司财务往来信息 作废 子表
     */
    public static final String CompanyFinancialDetailCancel = "companyFinancialDetailCancel";

    /**
     * 公司采购往来信息
     */
    public static final String CompanyProcurement = "companyProcurement";

    /**
     * 公司采购往来信息 -子表
     */
    public static final String CompanyProcurementDetail = "companyProcurementDetail";

    /**
     * 公司销售往来信息
     */
    public static final String CompanySales = "companySales";

    /**
     * 公司销售来信息 -子表
     */
    public static final String CompanySalesDetail = "companySalesDetail";

    /**
     * 总公司物料信息记录 基本信息
     */
    public static final String HEAD_OFFICE_MATERIALS_BA = "head_office_materials_ba";

    /**
     * 总公司物料信息记录 基本信息-明细表
     */
    public static final String HEAD_OFFICE_MATERIALS_DETAIL_BA = "headOfficeMaterialsDetailBa";

    /**
     * 总公司物料信息记录 基础表
     */
    public static final String HEAD_OFFICE_MATERIALS = "head_office_materials";

    /**
     * 总公司物料信息记录 规范信息
     */
    public static final String HEAD_OFFICE_MATERIALS_ST = "head_office_materials_st";

    /**
     * 总公司物料信息记录 规范信息-明细表
     */
    public static final String HEAD_OFFICE_MATERIALS_DETAIL_ST = "headOfficeMaterialsDetailSt";

    /**
     * 总公司物料信息记录 采购信息
     */
    public static final String HEAD_OFFICE_MATERIALS_PU = "head_office_materials_pu";

    /**
     * 总公司物料信息记录 采购信息-明细表
     */
    public static final String HEAD_OFFICE_MATERIALS_DETAIL_PU = "headOfficeMaterialsDetailPu";

    /**
     * 总公司物料信息记录 销售信息
     */
    public static final String HEAD_OFFICE_MATERIALS_SA = "head_office_materials_sa";

    /**
     * 总公司物料信息记录 销售信息
     */
    public static final String HEAD_OFFICE_MATERIALS_DETAIL_SA = "headOfficeMaterialsDetailSa";

    /**
     * 总公司物料信息记录 作废
     */
    public static final String HEAD_OFFICE_MATERIALS_VO = "head_office_materials_vo";

    /**
     * 总公司物料信息记录 作废-明细表
     */
    public static final String HEAD_OFFICE_MATERIALS_DETAIL_VO = "headOfficeMaterialsDetailVo";

    /**
     * 公司物料信息记录 基本信息
     */
    public static final String COMPANY_MATERIALS_BA = "company_materials_ba";

    /**
     * 公司物料信息记录 基本信息-明细表
     */
    public static final String COMPANY_MATERIALS_DETAIL_BA = "companyMaterialsDetailBa";

    /**
     * 公司物料信息记录 基础表
     */
    public static final String COMPANY_MATERIALS = "company_materials";

    /**
     * 公司物料信息记录 财务信息
     */
    public static final String COMPANY_MATERIALS_FA = "company_materials_fa";

    /**
     * 公司物料信息记录 财务信息-明细表
     */
    public static final String COMPANY_MATERIALS_DETAIL_FA = "companyMaterialsDetailFa";

    /**
     * 公司物料信息记录 采购信息
     */
    public static final String COMPANY_MATERIALS_PU = "company_materials_pu";

    /**
     * 公司物料信息记录 采购信息-明细表
     */
    public static final String COMPANY_MATERIALS_DETAIL_PU = "companyMaterialsDetailPu";

    /**
     * 公司物料信息记录 销售信息
     */
    public static final String COMPANY_MATERIALS_SA = "company_materials_sa";

    /**
     * 公司物料信息记录 销售信息
     */
    public static final String COMPANY_MATERIALS_DETAIL_SA = "companyMaterialsDetailSa";

    /**
     * 公司物料信息记录 作废
     */
    public static final String COMPANY_MATERIALS_VOID = "company_materials_void";

    /**
     * 公司物料信息记录 作废-明细表
     */
    public static final String COMPANY_MATERIALS_DETAIL_VOID = "companyMaterialsDetailVoid";


    /**
     * 场地档案落实汇总表
     */
    public static final String ArchivesInfo = "archivesInfo";

    /**
     * 场地档案落实汇总表-子表
     */
    public static final String ArchivesInfoDetail = "archivesInfoDetail";

    /**
     * 场地档案落实汇总表基础表
     */
    public static final String ArchivesInfoBasic = "archivesInfoBasic";

    /**
     * 场地档案落实汇总表作废
     */
    public static final String ArchivesInfoCancel = "archivesInfoCancel";

    /**
     * 场地档案落实汇总表作废 -子表
     */
    public static final String ArchivesInfoDetailCancel = "archivesInfoDetailCancel";



    /**
     * 工作事项
     */
    public static final String WorkItem = "workItem";

    /**
     * 工作事项-子表
     */
    public static final String WorkItemDetail = "workItemDetail";

    /**
     * 工作事项基础表
     */
    public static final String WorkItemBasic = "workItemBasic";

    /**
     * 工作事项作废
     */
    public static final String WorkItemCancel = "workItemCancel";

    /**
     * 工作事项作废 -子表
     */
    public static final String WorkItemDetailCancel = "workItemDetailCancel";



    /**
     * 工作记录
     */
    public static final String WorkRecord = "workRecord";

    /**
     * 工作记录-子表
     */
    public static final String WorkRecordDetail = "workRecordDetail";

    /**
     * 工作记录基础表
     */
    public static final String WorkRecordBasic = "workRecordBasic";

    /**
     * 工作记录作废
     */
    public static final String WorkRecordCancel = "workRecordCancel";

    /**
     * 工作记录作废 -子表
     */
    public static final String WorkRecordDetailCancel = "workRecordDetailCancel";

    /**
     *场地问题项提出
     */
    public static  final String SiteForward  ="siteForward";
    /**
     *场地问题项提出-子表
     */
    public static  final String SiteForwardDetail  ="siteForwardDetail";
    /**
     *场地问题项提出-基础
     */
    public static  final String SiteForwardBasic  ="siteForwardBasic";


    /**
     *场地问题项提出-作废
     */
    public static  final String SiteForwardCancel  ="siteForwardCancel";
    /**
     *场地问题项提出-子表作废
     */
    public static  final String SiteForwardDetailCancel  ="siteForwardDetailCancel";



    /**
     *场地问题项提出
     */
    public static  final String SiteImplement  ="siteImplement";
    /**
     *场地问题项提出-子表
     */
    public static  final String SiteImplementDetail  ="siteImplementDetail";
    /**
     *场地问题项提出-基础
     */
    public static  final String SiteImplementBasic  ="siteImplementBasic";


    /**
     *场地问题项提出-作废
     */
    public static  final String SiteImplementCancel  ="siteImplementCancel";
    /**
     *场地问题项提出-子表作废
     */
    public static  final String SiteImplementDetailCancel  ="siteImplementDetailCancel";




    /**
     *场地问题项提出
     */
    public static  final String SiteItem  ="siteItem";
    /**
     *场地问题项提出-子表
     */
    public static  final String SiteItemDetail  ="siteItemDetail";
    /**
     *场地问题项提出-基础
     */
    public static  final String SiteItemBasic  ="siteItemBasic";


    /**
     *场地问题项提出-作废
     */
    public static  final String SiteItemCancel  ="siteItemCancel";
    /**
     *场地问题项提出-子表作废
     */
    public static  final String SiteItemDetailCancel  ="siteItemDetailCancel";


    /**
     *物料管理表
     */
    public static  final String MatManage  ="MatManage";
    /**
     *物料管理表-子表
     */
    public static  final String SheetMatManage  ="SheetMatManage";
    /**
     *总公司物料管理信息
     */
    public static  final String CorpMatManageBase  ="CorpMatManageBase";

    /**
     *凭证核查表
     */
    public static  final String VoucherCheckBill  ="VoucherCheckBill";
    /**
     *凭证核查表-子表
     */
    public static  final String SheetVoucher  ="SheetVoucher";
    /**
     *凭证核查 基础资料
     */
    public static  final String VoucherCheckBase  ="VoucherCheck";

    /**
     * 总公司级档案总公司落实点检汇总记录
     */
    public static final String HEAD_OFFICE_ARCHIVES_1 = "head_office_archives_1";

    /**
     * 总公司级档案总公司落实点检汇总记录 明细表
     */
    public static final String HEAD_OFFICE_ARCHIVES_DETAIL = "headOfficeArchivesDetail";

    /**
     * 总公司级档案总公司落实点检汇总记录 基础表
     */
    public static final String OFFICE_ARCHIVES_BASE_1 = "office_archives_base_1";

    /**
     * 总公司级档案公司落实点检汇总记录
     */
    public static final String HEAD_OFFICE_ARCHIVES_2 = "head_office_archives_2";

    /**
     * 总公司级档案公司落实点检汇总记录 明细表
     */
    public static final String HEAD_OFFICE_ARCHIVES_DETAIL2 = "headOfficeArchivesDetail2";

    /**
     * 总公司级档案公司落实点检汇总记录 基础表
     */
    public static final String OFFICE_ARCHIVES_BASE_2 = "office_archives_base_2";

    /**
     * 公司级档案落实点检汇总记录
     */
    public static final String COMPANY_ARCHIVES = "company_archives";

    /**
     * 公司级档案落实点检汇总记录 明细表
     */
    public static final String COMPANY_ARCHIVES_DETAIL = "companyArchivesDetail";

    /**
     * 公司级档案落实点检汇总记录 基础表
     */
    public static final String COMPANY_ARCHIVES_BASE = "company_archives_base";

    /**
     * 总公司级往来单位档案总公司点检汇总记录
     */
    public static final String HEAD_OFFICE_UNIT_1 = "head_office_unit_1";

    /**
     * 总公司级往来单位档案总公司点检汇总记录 明细表
     */
    public static final String HEAD_OFFICE_UNIT_DETAIL1 = "headOfficeUnitDetail1";

    /**
     * 总公司级往来单位档案总公司点检汇总记录 基础表
     */
    public static final String OFFICE_UNIT_BASE_1 = "office_unit_base_1";

    /**
     * 总公司级往来单位档案公司点检汇总记录
     */
    public static final String HEAD_OFFICE_UNIT_2 = "head_office_unit_2";

    /**
     * 总公司级往来单位档案公司点检汇总记录 明细表
     */
    public static final String HEAD_OFFICE_UNIT_DETAIL2 = "headOfficeUnitDetail2";

    /**
     * 总公司级往来单位档案公司点检汇总记录 基础表
     */
    public static final String OFFICE_UNIT_BASE_2 = "office_unit_base_2";

    /**
     * 公司往来单位档案点检汇总记录
     */
    public static final String COMPANY_UNIT = "company_unit";

    /**
     * 公司往来单位档案点检汇总记录 明细表
     */
    public static final String COMPANY_UNIT_DETAIL = "companyUnitDetail";

    /**
     * 公司往来单位档案点检汇总记录 基础表
     */
    public static final String COMPANY_UNIT_BASE = "company_unit_base";

    /**
     * 设备维修记录
     */
    public static final String EQUIPMENT_MAINTENANCE = "equipment_maintenance";

    /**
     * 设备维修记录 明细表
     */
    public static final String MAINTENANCE_DETAIL = "maintenanceDetail";

    /**
     * 设备维修记录 基础表
     */
    public static final String EQUIPMENT_MAINTENANCE_BA = "equipment_maintenance_ba";

    /**
     * 入库业务计数器
     */
    public static  final  String WAREHOUSING_COUNTER = "WarehousingCounter";
}

package com.authine.cloudpivot.ext.Utils;


import com.authine.cloudpivot.ext.service.CloudSqlService;
import com.authine.cloudpivot.ext.service.impl.CloudSqlServiceImpl;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Component
public class RoleAuth {
    private static CloudSqlService sqlService;

    @Autowired
    public void setRedisService(CloudSqlService sqlService) {
        RoleAuth.sqlService = sqlService;

    }


    DocAPI api = new DocAPI();
//	String token = api.getToken();
//	RoleAuthDispatch RoleAuthDispatch = new RoleAuthDispatch();

    //	public  void  RoleAuthDis(Context ctx,String token,String PosDuty,String PosName,String AuthFileId) throws BOSException
//	{
//        String PosFid="";
//        String RolePosName="";
//        String RolePosId="";
//		String sql = "select fid,  fname_l2 ,FNUMBER from  T_ORG_Position where  fnumber='"+ PosDuty + "'";
//		IRowSet rs = SQLExecutorFactory.getLocalInstance(ctx, sql).executeSQL();
//		try {
//			while (rs.next()&&!PosName.equalsIgnoreCase("董事长")) {
//				PosFid = rs.getString("fid");
//				PosName = rs.getString("fname_l2");
//				PosDuty=rs.getString("FNUMBER");
//				RolePosName = PosDuty + " "+ PosName;
//				JSONObject RoleMainPosObj = api.getRole(RolePosName);
//				int RoleMainPosNum=Integer.parseInt(RoleMainPosObj.getString("num"));
//				if(RoleMainPosNum==0){
//									  continue;
//									  }
//				JSONArray RoleMainPosDataArray = RoleMainPosObj.getJSONArray("data");
//				JSONObject RoleMainPosDataObj = RoleMainPosDataArray.getJSONObject(RoleMainPosDataArray.size()-1);
//				RolePosId = RoleMainPosDataObj.getString("id");
//				RoleAuthDispatch.getRoleAuthDispatch(token, RolePosId, AuthFileId, RolePosName, "VPD");
//				sql = "select B.fname_l2 as fname_l2,B.FNUMBER as FNUMBER ,B.FID from  T_ORG_PositionHierarchy A inner join T_ORG_POSITION B ON A.FPARENTID=B.FID where  A.fchildid='"+ PosFid + "'";
//				rs = SQLExecutorFactory.getLocalInstance(ctx, sql).executeSQL();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		} catch (BOSException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//
//	}


    public  void  RoleDeptAss(String token,String PosDuty,String AuthFileId)
    {//如果是中间组织，需要给上级部门的助理授予下载权
        String deptassduty="";
        String deptassname="";
        String RolePosId="";
        String midorgfid="";
        String midorgnumber="";
        String RolePosName="";
        String deptcode=PosDuty.substring(0, PosDuty.lastIndexOf("."));
        String sqlorg = "select FNUMBER,fparentid from  [HG_LINK].[hg].dbo.T_ORG_admin where fcontrolunitid!=fparentid  and fnumber='"+ deptcode + "'";
        List<Map<String, Object>> rsorg = sqlService.getList(CloudSqlService.htEas, sqlorg);
        try {
            String value =  (String)rsorg.get(0).get("FNUMBER");
            midorgfid=(String)rsorg.get(0).get("fparentid");

                String sqlorg01 = "select FNUMBER from  [HG_LINK].[hg].dbo.T_ORG_admin where  fid='"+ midorgfid + "'";
                List<Map<String, Object>> rsorg01 = sqlService.getList(CloudSqlService.htEas, sqlorg01);
            try {

                    midorgnumber= (String)rsorg01.get(0).get("fnumber");
                    deptassduty=midorgnumber+".10.006";//中间组织助理
                    String sql=  "select fname_l2 name ,fparentid ,FNUMBER from  [HG_LINK].[hg].dbo.T_ORG_Position where  fnumber='"+ deptassduty + "'";
                    List<Map<String, Object>> rs = sqlService.getList(CloudSqlService.htEas, sql);

                    deptassname=(String)rs.get(0).get("name");
                    RolePosName = deptassduty + " "+ deptassname;
                    JSONObject RoleMainPosObj = api.getRole(RolePosName);
                    int RoleMainPosNum=Integer.parseInt(RoleMainPosObj.getString("num"));
                    if(RoleMainPosNum>0){

                        JSONArray RoleMainPosDataArray = RoleMainPosObj.getJSONArray("data");
                        JSONObject RoleMainPosDataObj = RoleMainPosDataArray.getJSONObject(RoleMainPosDataArray.size()-1);
                        RolePosId = RoleMainPosDataObj.getString("id");
                        api.getRoleAuth(RolePosName, RolePosId,"VPD", AuthFileId);

                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    //清理权限
    public  void  RoleAuthClear(String token,String role,String FileId)
    {

        JSONObject  ListFileAuth= api.getListFileAuth(token,FileId);
        JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
        int ListFileAuthNum=ListFileAuthDataArray.size();
        for (int i = 0; i < ListFileAuthNum; i++) {

            JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(i);
            String RemoveAuthFileId = ListFileAuthDataObj.getString("id");
            String RemoveAuthPos= ListFileAuthDataObj.getString("name");
            if(RemoveAuthPos.equals(role)){
                JSONObject RemoveAuthFileListObj=api.getRemoveFileAuth(token, RemoveAuthFileId);
                System.out.print("ListFileAuthNum");
            }
        }


    }

    //清理部门权限
    public  void  RoleDeptAuthClear(String token,String deptcode,String FileId)
    {

        JSONObject  ListFileAuth= api.getListFileAuth(token,FileId);
        JSONArray ListFileAuthDataArray = ListFileAuth.getJSONArray("data");
        int ListFileAuthNum=ListFileAuthDataArray.size();
        for (int i = 0; i < ListFileAuthNum; i++) {

            JSONObject ListFileAuthDataObj = ListFileAuthDataArray.getJSONObject(i);
            String RemoveAuthFileId = ListFileAuthDataObj.getString("id");
            String RemoveAuthPos= ListFileAuthDataObj.getString("name");
            int len=deptcode.length();
            if(RemoveAuthPos.length()>=len){
                if(RemoveAuthPos.substring(0, len).equals(deptcode)){
                    JSONObject RemoveAuthFileListObj=api.getRemoveFileAuth(token, RemoveAuthFileId);
                    System.out.print("ListFileAuthNum");
                }
            }
        }


    }

    public  void  RoleAuthDis(String PosDuty,String PosName,String AuthFileId)
    {
        String PosFid="";
        String RolePosName="";
        String RolePosId="";
        String AssRolePosId="";
        String sql = "select fid,  fname_l2 ,FNUMBER from  [HG_LINK].[hg].dbo.T_ORG_Position where  fnumber='"+ PosDuty + "'";
        List<Map<String, Object>> rs = sqlService.getList(CloudSqlService.htEas, sql);
        try {
            String value =  (String)rs.get(0).get("fid");


            while (value!=null&&!PosName.equalsIgnoreCase("董事长")) {
                PosFid = (String)rs.get(0).get("fid");
                PosName = (String)rs.get(0).get("fname_l2");
                PosDuty=(String)rs.get(0).get("FNUMBER");
                RolePosName = PosDuty + " "+ PosName;
                JSONObject RoleMainPosObj = api.getRole(RolePosName);
                int RoleMainPosNum=Integer.parseInt(RoleMainPosObj.getString("num"));
                if(RoleMainPosNum==0){
                    continue;
                }
                JSONArray RoleMainPosDataArray = RoleMainPosObj.getJSONArray("data");
                JSONObject RoleMainPosDataObj = RoleMainPosDataArray.getJSONObject(RoleMainPosDataArray.size()-1);
                RolePosId = RoleMainPosDataObj.getString("id");
                api.getRoleAuth(RolePosName, RolePosId,"VPD", AuthFileId);
                sql = "select B.fname_l2 as fname_l2,B.FNUMBER as FNUMBER ,B.FID from  [HG_LINK].[hg].dbo.T_ORG_PositionHierarchy A inner join [HG_LINK].[hg].dbo.T_ORG_POSITION B ON A.FPARENTID=B.FID where  A.fchildid='"+ PosFid + "'";
                rs = sqlService.getList(CloudSqlService.htEas, sql);
            }


        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



    }
    //不需要清权限的使用01接口
    public  void  RoleAAuthDis(String PosADuty,String PosAName,String AuthFileId)
    {
        String PosAFid="";
        String AssRolePosId="";
        String RolePosAName="";
        String RolePosAId="";
        String sql = "select fid,  fname_l2 ,FNUMBER from  [HG_LINK].[hg].dbo.T_ORG_Position where  fnumber='"+ PosADuty + "'";


        List<Map<String, Object>> rs = sqlService.getList(CloudSqlService.htEas, sql);
        try {
            for(Map<String,Object> m : rs){
                RolePosAName = PosADuty + " "+ PosAName;
                JSONObject RolePosAObj01 = api.getRole(RolePosAName);
                JSONArray RolePosADataArray01 = RolePosAObj01.getJSONArray("data");
                JSONObject RolePosADataObj01 = RolePosADataArray01.getJSONObject(0);
                RolePosAId = RolePosADataObj01.getString("id");
                String RolePosAId01=RolePosAId;
                String RolePosAName01=RolePosAName;
                PosAFid =  (String)m.get("fid");
                PosAName = (String)m.get("fname_l2");
                PosADuty= (String)m.get("FNUMBER");
                if(PosAFid!=null&&!PosAName.equalsIgnoreCase("董事长")){

                    RolePosAName = PosADuty + " "+ PosAName;
                    JSONObject RoleMainPosObj = api.getRole(RolePosAName);
                    int RoleMainPosNum=Integer.parseInt(RoleMainPosObj.getString("num"));
                    if(RoleMainPosNum==0){
                        continue;
                    }
                    JSONArray RoleMainPosDataArray = RoleMainPosObj.getJSONArray("data");
                    JSONObject RoleMainPosDataObj = RoleMainPosDataArray.getJSONObject(RoleMainPosDataArray.size()-1);
                    RolePosAId = RoleMainPosDataObj.getString("id");
                    api.getRoleAuth( RolePosAName, RolePosAId,"VPD", AuthFileId);
                    sql = "select B.fname_l2 as fname_l2,B.FNUMBER as FNUMBER ,B.FID from  [HG_LINK].[hg].dbo.T_ORG_PositionHierarchy A inner join [HG_LINK].[hg].dbo.T_ORG_POSITION B ON A.FPARENTID=B.FID where  A.fchildid='"+ PosAFid + "'";
                    rs = sqlService.getList(CloudSqlService.htEas, sql);

                }
                api.getRoleAuth( RolePosAName01, RolePosAId01,"A", AuthFileId);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }





    }




    public  void  AssRoleAuthDis(String PosDuty,String PosName,String AuthFileId)
    {
        String AssRolePosId="";
        String Dept= PosDuty.substring(0, PosDuty.lastIndexOf(".")) ;
        if(!Dept.equals("02.02")){
            String AssRolePosDuty= PosDuty.substring(0, PosDuty.lastIndexOf(".")) + ".006";
            String sqlAssRolePos = "select fname_l2  from   [HG_LINK].[hg].dbo.t_org_position  where  fnumber='"
                    + AssRolePosDuty + "'";

            List<Map<String, Object>> rsAssRolePos = sqlService.getList(CloudSqlService.htEas, sqlAssRolePos);
            try {
                    String AssRolePosName = AssRolePosDuty+" "+ (String)rsAssRolePos.get(0).get("fname_l2");
                    JSONObject AssRolePosObj = api.getRole(AssRolePosName);
                    int AssRolePosNum=Integer.parseInt(AssRolePosObj.getString("num"));
                    if(AssRolePosNum!=0){

                        JSONArray AssRolePosDataArray = AssRolePosObj.getJSONArray("data");
                        JSONObject AssRolePosDataObj = AssRolePosDataArray.getJSONObject(AssRolePosDataArray.size()-1);
                        AssRolePosId = AssRolePosDataObj.getString("id");
                        api.getRoleAuth(AssRolePosName, AssRolePosId,"VPD", AuthFileId);
                    }


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }



        }

    }






}

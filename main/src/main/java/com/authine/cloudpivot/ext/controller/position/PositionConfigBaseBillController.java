package com.authine.cloudpivot.ext.controller.position;

import cn.hutool.core.lang.Assert;
import com.authine.cloudpivot.engine.api.model.organization.UserModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkflowInstanceModel;
import com.authine.cloudpivot.engine.enums.status.SequenceStatus;
import com.authine.cloudpivot.ext.Utils.CustomSchemaCode;
import com.authine.cloudpivot.ext.Utils.DocAPI;
import com.authine.cloudpivot.web.api.config.EducationEnum;
import com.authine.cloudpivot.web.api.config.FirstEducationEnum;
import com.authine.cloudpivot.web.api.controller.base.BaseController;
import com.authine.cloudpivot.web.api.view.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.authine.cloudpivot.ext.service.CloudSqlService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import com.authine.cloudpivot.ext.Utils.RoleAuth;

@Slf4j
@RestController
@RequestMapping("/public/PositionConfigBaseBillController")
public class PositionConfigBaseBillController extends BaseController {
    @Autowired
    CloudSqlService sqlService;

//岗配基础

    @GetMapping("delPos")
    public ResponseResult delPos(String bizId) {


            return getOkResponseResult("成功");
    }


    @GetMapping("addPos")
    public ResponseResult addPos(String bizId) {


        // TODO Auto-generated method stub

        DocAPI api = new DocAPI();
        String token = api.getToken();
        RoleAuth RoleAuth = new RoleAuth();
        String position = "";
        String personnumber = "";
        String RolePosId = "";
        String deptname = "";
        String SeniAttr = "";
        String SeniAttr01 = "";
        String SeniAttr02 = "";
        String SeniAttr03 = "";
        String SeniAttr04 = "";
        String SeniAttr05 = "";
        String SeniAttr06 = "";
        String SeniAttr07 = "";
        String positionname = "";
        String deptnumber = "";
        String charge = "";

        Map<String, Object> objectMap = null;

        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject("PositionConfigBaseBill", bizId);

        List<Map<String, Object>> subSheet = (List<Map<String, Object>>) bizObject.getObject("subSheetPositionConfigBaseBi");

        int n = subSheet.size();
        for (int i = 0; i < n; i++) {
            objectMap = subSheet.get(i);
            position = (String) objectMap.get("posNumber");// 获取岗位代码
            positionname = (String) objectMap.get("posName");// 获取岗位名称
            deptnumber = (String) objectMap.get("baseOrgNumber");// 获取基层组织代码
            deptname = (String) objectMap.get("baseOrgName");// 获取基层组织名称
            //Boolean charge = (Boolean) subSheet.get(0).get("charge");// 获取负责人岗
            charge = (String) objectMap.get("charge");// 获取负责人岗
//			   添加个人文件夹中对应岗位
            SeniAttr = deptnumber + " PERSONAL " + deptname;
            net.sf.json.JSONObject FolderIdObj00 = api.getFileID(SeniAttr);
            net.sf.json.JSONArray FolderIdArray00 = FolderIdObj00.getJSONArray("data");
            net.sf.json.JSONObject FolderArray100 = FolderIdArray00.getJSONObject(0);
            String FolederId00 = FolderArray100.getString("id");
            String comcode = position.substring(0, position.indexOf("."));
            String Attr00 = "PERSONAL-" + comcode + " " + position;
            net.sf.json.JSONObject NewFolderIdObjPERSON = api.getFileID(Attr00);
            String num00 = NewFolderIdObjPERSON.getString("num");
            if (num00.equals("0")) {
                api.getAddFolder(FolederId00, position + " " + positionname, Attr00);
            } else {
                net.sf.json.JSONArray NewFolderIdArray = NewFolderIdObjPERSON.getJSONArray("data");
                net.sf.json.JSONObject NewFolderArray1 = NewFolderIdArray.getJSONObject(0);
                String NewFolederId = NewFolderArray1.getString("id");
                api.getRename(token, NewFolederId, position + " " + positionname);
            }

//					   添加公共文件夹中对应岗位
            SeniAttr = deptnumber + " PUB " + deptname;
            net.sf.json.JSONObject FolderIdObj = api.getFileID(SeniAttr);
            net.sf.json.JSONArray FolderIdArray = FolderIdObj.getJSONArray("data");
            net.sf.json.JSONObject FolderArray1 = FolderIdArray.getJSONObject(0);
            String FolederId = FolderArray1.getString("id");
            String Attr = position + " PUB " + deptname;
            net.sf.json.JSONObject NewFolderIdObj = api.getFileID(Attr);
            String num = NewFolderIdObj.getString("num");
            if (num.equals("0")) {
                api.getAddFolder(FolederId, position + " " + positionname, Attr);
            } else {
                net.sf.json.JSONArray NewFolderIdArray = NewFolderIdObj.getJSONArray("data");
                net.sf.json.JSONObject NewFolderArray1 = NewFolderIdArray.getJSONObject(0);
                String NewFolederId = NewFolderArray1.getString("id");
                api.getRename(token, NewFolederId, position + " " + positionname);
            }
            Attr = position.substring(0, position.lastIndexOf(".")) + " PUB " + deptname;
            net.sf.json.JSONObject ExitFolderIdObj = api.getFileID(Attr);
            net.sf.json.JSONArray ExitFolderIdArray = ExitFolderIdObj.getJSONArray("data");
            net.sf.json.JSONObject ExitFolderArray1 = ExitFolderIdArray.getJSONObject(0);
            String ExitFolederId = ExitFolderArray1.getString("id");
            RoleAuth.RoleAuthDis(position, positionname, ExitFolederId);

//				   添加SD文件夹中对应岗位
            SeniAttr01 = deptnumber + " SD " + deptname;
            net.sf.json.JSONObject FolderIdObj01 = api.getFileID(SeniAttr01);
            net.sf.json.JSONArray FolderIdArray01 = FolderIdObj01.getJSONArray("data");
            net.sf.json.JSONObject FolderArray101 = FolderIdArray01.getJSONObject(0);
            String FolederId01 = FolderArray101.getString("id");
            String Attr01 = "SD-" + deptnumber.substring(0, deptnumber.indexOf(".")) + " " + position;
            net.sf.json.JSONObject NewFolderIdObj01 = api.getFileID(Attr01);
            String num01 = NewFolderIdObj01.getString("num");
            if (num01.equals("0")) {
                api.getAddFolder(FolederId01, position + " " + positionname, Attr01);
            } else {
                net.sf.json.JSONArray NewFolderIdArray01 = NewFolderIdObj01.getJSONArray("data");
                net.sf.json.JSONObject NewFolderArray101 = NewFolderIdArray01.getJSONObject(0);
                String NewFolederId01 = NewFolderArray101.getString("id");
                api.getRename(token, NewFolederId01, position + " " + positionname);
            }

//				   添加F文件夹中对应岗位
            SeniAttr02 = deptnumber + " F " + deptname;
            net.sf.json.JSONObject FolderIdObj02 = api.getFileID(SeniAttr02);
            net.sf.json.JSONArray FolderIdArray02 = FolderIdObj02.getJSONArray("data");
            net.sf.json.JSONObject FolderArray102 = FolderIdArray02.getJSONObject(0);
            String FolederId02 = FolderArray102.getString("id");
            String Attr02 = "F-" + deptnumber.substring(0, deptnumber.indexOf(".")) + " " + position;
            net.sf.json.JSONObject NewFolderIdObj02 = api.getFileID(Attr02);
            String num02 = NewFolderIdObj02.getString("num");
            if (num02.equals("0")) {
                api.getAddFolder(FolederId02, position + " " + positionname, Attr02);
            } else {
                net.sf.json.JSONArray NewFolderIdArray02 = NewFolderIdObj02.getJSONArray("data");
                net.sf.json.JSONObject NewFolderArray102 = NewFolderIdArray02.getJSONObject(0);
                String NewFolederId02 = NewFolderArray102.getString("id");
                api.getRename(token, NewFolederId02, position + " " + positionname);
            }


//				   添加GSD文件夹中对应岗位
            SeniAttr03 = deptnumber + " GSD " + deptname;
            net.sf.json.JSONObject FolderIdObj03 = api.getFileID(SeniAttr03);
            net.sf.json.JSONArray FolderIdArray03 = FolderIdObj03.getJSONArray("data");
            net.sf.json.JSONObject FolderArray103 = FolderIdArray03.getJSONObject(0);
            String FolederId03 = FolderArray103.getString("id");
            String Attr03 = "GSD-" + deptnumber.substring(0, deptnumber.indexOf(".")) + " " + position;
            net.sf.json.JSONObject NewFolderIdObj03 = api.getFileID(Attr03);
            String num03 = NewFolderIdObj03.getString("num");
            if (num03.equals("0")) {
                api.getAddFolder(FolederId03, position + " " + positionname, Attr03);
            } else {
                net.sf.json.JSONArray NewFolderIdArray03 = NewFolderIdObj03.getJSONArray("data");
                net.sf.json.JSONObject NewFolderArray103 = NewFolderIdArray03.getJSONObject(0);
                String NewFolederId03 = NewFolderArray103.getString("id");
                api.getRename(token, NewFolederId03, position + " " + positionname);
            }

//				   添加HR文件夹中对应岗位
            SeniAttr04 = deptnumber + " HR " + deptname;
            net.sf.json.JSONObject FolderIdObj04 = api.getFileID(SeniAttr04);
            net.sf.json.JSONArray FolderIdArray04 = FolderIdObj04.getJSONArray("data");
            net.sf.json.JSONObject FolderArray104 = FolderIdArray04.getJSONObject(0);
            String FolederId04 = FolderArray104.getString("id");
            String Attr04 = "HR-" + deptnumber.substring(0, deptnumber.indexOf(".")) + " " + position;
            net.sf.json.JSONObject NewFolderIdObj04 = api.getFileID(Attr04);
            String num04 = NewFolderIdObj04.getString("num");
            if (num04.equals("0")) {
                api.getAddFolder(FolederId04, position + " " + positionname, Attr04);
            } else {
                net.sf.json.JSONArray NewFolderIdArray04 = NewFolderIdObj04.getJSONArray("data");
                net.sf.json.JSONObject NewFolderArray104 = NewFolderIdArray04.getJSONObject(0);
                String NewFolederId04 = NewFolderArray104.getString("id");
                api.getRename(token, NewFolederId04, position + " " + positionname);
            }


//				   添加DS-A文件夹中对应岗位
            SeniAttr05 = deptnumber + " DS-A " + deptname;
            net.sf.json.JSONObject FolderIdObj05 = api.getFileID(SeniAttr05);
            net.sf.json.JSONArray FolderIdArray05 = FolderIdObj05.getJSONArray("data");
            net.sf.json.JSONObject FolderArray105 = FolderIdArray05.getJSONObject(0);
            String FolederId05 = FolderArray105.getString("id");
            String Attr05 = position + " " + "DS-A " + deptnumber;
            net.sf.json.JSONObject NewFolderIdObj05 = api.getFileID(Attr05);
            String num05 = NewFolderIdObj05.getString("num");
            if (num05.equals("0")) {
                api.getAddFolder(FolederId05, position + " " + positionname, Attr05);
            } else {
                net.sf.json.JSONArray NewFolderIdArray05 = NewFolderIdObj05.getJSONArray("data");
                net.sf.json.JSONObject NewFolderArray105 = NewFolderIdArray05.getJSONObject(0);
                String NewFolederId05 = NewFolderArray105.getString("id");
                api.getRename(token, NewFolederId05, position + " " + positionname);
            }


//				   添加文档工具文件夹中对应岗位
            SeniAttr06 = deptnumber + " TOOL " + deptname;
            net.sf.json.JSONObject FolderIdObj06 = api.getFileID(SeniAttr06);
            net.sf.json.JSONArray FolderIdArray06 = FolderIdObj06.getJSONArray("data");
            net.sf.json.JSONObject FolderArray106 = FolderIdArray06.getJSONObject(0);
            String FolederId06 = FolderArray106.getString("id");
            String Attr06 = position + " " + "TOOL " + deptnumber;
            net.sf.json.JSONObject NewFolderIdObj06 = api.getFileID(Attr06);
            String num06 = NewFolderIdObj06.getString("num");

            String personnameTool = "";
            String name = "";
            String str = "select b.fname_l2 as positionname,b.fnumber as positionnumber ,c.fnumber as personnumber,c.fname_l2 as personname,d.fsimplename as deptname from [HG_LINK].[hg].dbo.T_pos_positionconfigbase a INNER JOIN [HG_LINK].[hg].dbo.T_Org_POSITION b ON a.fpositionid = b.fid INNER JOIN [HG_LINK].[hg].dbo.t_BD_PERSON c ON c.fid = a.fempid INNER JOIN [HG_LINK].[hg].dbo.t_org_admin d ON b.fadminorgunitid = d.fid WHERE a.fisfirst = '1' AND A.FSTATUS='启用'  AND B.fnumber='" + position + "'";


            List<Map<String, Object>> rsTOOL = sqlService.getList(CloudSqlService.htEas, str);
            try {

                for (Map<String, Object> m : rsTOOL) {

                    personnameTool = (String) m.get("personname");

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            name = position.substring(position.lastIndexOf(".") + 1, position.length()) + " " + positionname + " " + personnameTool;
            if (num06.equals("0")) {
                api.getAddFolder(FolederId06, name, Attr06);
            } else {
                net.sf.json.JSONArray NewFolderIdArray06 = NewFolderIdObj06.getJSONArray("data");
                net.sf.json.JSONObject NewFolderArray106 = NewFolderIdArray06.getJSONObject(0);
                String NewFolederId06 = NewFolderArray106.getString("id");
                api.getRename(token, NewFolederId06, name);
            }

        }


        return getOkResponseResult("成功");
    }

//    @GetMapping("addPos")
//    public ResponseResult addPos(String bizId) {
    @GetMapping("/finish")
    public void finish(String bizId) {
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.posConfigBaseAdd, bizId);

        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get("subSheetPositionConfigBaseBi");

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));


        //获取审批人
        String approval ="" ;
//        String approval = getFileBaseInfoWorkFlowApproval(bizObject);

        for (Map<String, Object> map : list) {

            try {
                //获取数据
                Map<String, Object> data = dataInfoBaseMap(map, now, approval);
                //判断是否已存在
                String id = existsBizObject(map);
                if (StringUtils.isNotEmpty(id)) {
                    data.put("id", id);
                }

                BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.posConfigBase, data, false);
                model.setSequenceStatus(SequenceStatus.COMPLETED.name());
                id = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                log.info("基础表-岗位配置信息基础作成功 ");
                //调用存储过程
                callProcess(id);

            } catch (Exception e) {
                String companyNumber = (String) map.get("companyCode");
                String posNumber = (String) map.get("posNumber");

                log.info("基础表-岗位配置信息基础操作失败 companyNumber={},posNumber={}", companyNumber, posNumber);
                log.info(e.getMessage(), e);
            }

        }
    }

    @RequestMapping("toVoid")
    public void toVoid(String bizId) {
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.PosConfigBaseBillVoid, bizId);
        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get("SheetPosConfigBaseBillVoid");

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        //获取审批人

        String approval = getFileBaseInfoWorkFlowApproval(bizObject);

        for (Map<String, Object> map : list) {


            try {
                //获取数据
                Map<String, Object> data = dataInfoBaseMap(map, now, approval);
                //判断是否已存在
                String id = existsBizObject(data);
                if (StringUtils.isNotEmpty(id)) {
                    data.put("id", id);
                }

                BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.posConfigBase, data, false);
                model.setSequenceStatus(SequenceStatus.CANCELED.name());
                id = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                log.info("基础表-人员信息基础作废操作成功 ");
                //调用存储过程
                callProcessToVoid(id);

            } catch (Exception e) {
                String companyNumber = (String) map.get("companyNumber");
                String personNumber = (String) map.get("personNumber");
                log.info("基础表-人员信息基础作废操作失败 personNumber={},companyNumber={}", personNumber, companyNumber);
                log.info(e.getMessage(), e);
            }

        }
    }

    /**
     * 调用存储过程
     * <p>
     * --------岗位配置信息记录   基础信息
     * exec    SyncPositionBase
     *
     * @CompanyFID nvarchar(100),            ----公司FID
     * @BaseFID nvarchar(100),            ----基层组织FID
     * @PosNumber nvarchar(100),            ----岗位代码
     * @PosName nvarchar(100),            ----岗位名称
     * @BegDate nvarchar(50),            ----生效日期 YYYY-MM-DD
     * @PosStatus nvarchar(50),            ----岗位禁用状态 普通 1  禁用 2
     * @EndDate nvarchar(50),            ----失效日期 YYYY-MM-DD
     * @Authorized nvarchar(50),            ----核定编制
     * @IsInCharge int,                ----负责人岗 0  空   1  √
     * @UpPositionFID nvarchar(100),            ----上级岗代码
     * @Auditor nvarchar(50)            ----审批人 02.0100 李*
     */
    private void callProcess(String bizId) {

        if (StringUtils.isEmpty(bizId)) {
            return;
        }
        //编写sql
        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.posConfigBase);
        StringBuilder sql = new StringBuilder("SELECT companyId,bseOrgId,posNumber,posName,effectiveDate,Status,expiryDate,approvedNumber,charge,supPosNumber,auditer,auditDate from ")
                .append(tableName).append(" where id ='")
                .append(bizId).append("';");
        //查询到入参
        Map<String, Object> map = sqlService.getMap(sql.toString());

        //调用存储过程
        String companyId = MapUtils.getString(map, "companyId", "");
        String baseOrgId = MapUtils.getString(map, "bseOrgId", "");
        String posNumber = MapUtils.getString(map, "posNumber", "");
        String posName = MapUtils.getString(map, "posName", "");
//        Date effectiveDateTemp = (Date) map.get("effectiveDate");
//        String effectiveDate = effectiveDateTemp == null ? "" : DateFormatUtils.format(effectiveDateTemp, "yyyy-MM-dd");
        //Date effectiveDate = (Date) map.get("effectiveDate");
        LocalDateTime effectiveDateTemp = (LocalDateTime) map.get("effectiveDate");
        String effectiveDate =  ObjectUtils.isEmpty(effectiveDateTemp) ? "" : effectiveDateTemp.toLocalDate().toString();


        String posDisableStatusTemp = MapUtils.getString(map, "Status", "");
        Integer posDisableStatus = "普通".equals(posDisableStatusTemp) ? 1 : 2;
//        Date expiryDateTemp = (Date) map.get("expiryDate");
//        String expiryDate = expiryDateTemp == null ? "" : DateFormatUtils.format(expiryDateTemp, "yyyy-MM-dd");
        //Date expiryDate = (Date) map.get("expiryDate");
        LocalDateTime expiryDateTemp = (LocalDateTime) map.get("expiryDate");
        String expiryDate =  ObjectUtils.isEmpty(expiryDateTemp) ? "" : expiryDateTemp.toLocalDate().toString();

        String approvedNumber = MapUtils.getString(map, "approvedNumber", "");
        String chargeTemp = MapUtils.getString(map, "charge", "");
        Integer charge = "√".equals(chargeTemp)? 1:0;


        String superPosNumber = MapUtils.getString(map, "supPosNumber", "");

        String auditer = MapUtils.getString(map, "auditer", "");

//        Assert.isFalse(StringUtils.isEmpty(companyId), "{}不能为空", "baseOrgId");
////        Assert.isFalse(firstDept==null ,"{}不能为空","firstDept");
//        Assert.isFalse(StringUtils.isEmpty(baseOrgId), "{}不能为空", "baseOrgId");
//        Assert.isFalse(StringUtils.isEmpty(posNumber), "{}不能为空", "posNumber");
//        Assert.isFalse(StringUtils.isEmpty(posName), "{}不能为空", "posName");
//        Assert.isFalse(StringUtils.isEmpty(effectiveDate), "{}不能为空", "effectiveDate");
//        // Assert.isFalse(StringUtils.isEmpty(posDisableStatus),"{}不能为空","posDisableStatus");
//        Assert.isFalse(StringUtils.isEmpty(expiryDate), "{}不能为空", "expiryDate");
//        Assert.isFalse(StringUtils.isEmpty(approvedNumber), "{}不能为空", "approvedNumber");
//        //Assert.isFalse(StringUtils.isEmpty(charge), "{}不能为空", "charge");
//        Assert.isFalse(StringUtils.isEmpty(superPosNumber), "{}不能为空", "superPosNumber");
//        Assert.isFalse(StringUtils.isEmpty(auditer), "{}不能为空", "auditer");
//

        String execSql = String.format("exec [HG_LINK].[hg].[dbo].SyncPositionBase     '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s' ",
                companyId, baseOrgId, posNumber, posName, effectiveDate, posDisableStatus, expiryDate, approvedNumber, charge, superPosNumber, auditer);

        log.info("\n==========准备调用存储过程:{}", execSql);

        sqlService.execute(CloudSqlService.htEas, execSql);

        log.info("\n=============存储过程执行完成");


    }

    /**
     * 调用作废存储过程
     * --------岗位配置信息记录  基础信息 作废
     * exec   PositionBaseVoid
     *
     * @param bizId
     * @PosNumber nvarchar(50)    ----岗位代码
     */
    private void callProcessToVoid(String bizId) {

        if (StringUtils.isEmpty(bizId)) {
            return;
        }
        //编写sql
        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.posConfigBase);
        StringBuilder sql = new StringBuilder("SELECT posNumber from ")
                .append(tableName).append(" where id ='")
                .append(bizId).append("';");
        //查询到入参
        Map<String, Object> map = sqlService.getMap(sql.toString());

        log.info("入参map={}", map);

        //调用存储过程


        String posNumber = MapUtils.getString(map, "posNumber", "");


        Assert.isFalse(StringUtils.isEmpty(posNumber), "{}不能为空", "posNumber");


        String execSql = String.format("exec [HG_LINK].[hg].[dbo].PositionBaseVoid '%s' ",
                posNumber);

        log.info("\n==========准备调用作废存储过程:{}", execSql);

        sqlService.execute(CloudSqlService.htEas, execSql);

        log.info("\n=============作废存储过程执行完成");

    }

    /**
     * 判断是否已存在
     *
     * @return
     */
    private String existsBizObject(Map<String, Object> data) {

        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.posConfigBase);

        StringBuilder sql = new StringBuilder("select id  from ").append(tableName)
                .append(" where posNumber ='").append(data.get("posNumber"))
                .append("'; ");

        Map<String, Object> map = sqlService.getMap(sql.toString());

        return (String) map.get("id");
    }

    /**
     * 查询审批人,返回  员工号+姓名
     *
     * @param bizObject
     * @return
     */
    private String getFileBaseInfoWorkFlowApproval(BizObjectCreatedModel bizObject) {


        WorkflowInstanceModel instanceModel = getWorkflowInstanceFacade().getByObjectId(bizObject.getId());
        List<WorkItemModel> workItems = getWorkflowInstanceFacade().getWorkItems(instanceModel.getId(), true);
        final String finalActivityCode = "Activity18";
        Optional<WorkItemModel> first = workItems.stream().filter(a -> a.getActivityCode().equals(finalActivityCode)).findFirst();
        String participant = null;
        if (first.isPresent()) {
            WorkItemModel workItemModel = first.get();
            participant = workItemModel.getParticipant();
        }

        if (participant == null) {
            participant = bizObject.getCreater().getId();
        }

        UserModel user = getOrganizationFacade().getUser(participant);


        return new StringBuilder(user.getEmployeeNo()).append("　").append(user.getName()).toString();
    }

    /**
     * 转换成  基础表-总公司档案 的数据
     *
     * @param map
     * @param auditDate
     * @return
     */

    private Map<String, Object> dataInfoBaseMap(Map<String, Object> map, String auditDate, String auditer) {
        Map<String, Object> data = new HashMap<>();
        //关联公司
        data.put("companyId", map.get("companyId"));
        //公司代码
        data.put("companyCode", map.get("companyCode"));
        //公司名称
        data.put("companyName", map.get("companyName"));

        //关联基层组织代码
        data.put("bseOrgId", map.get("baseOrgId"));
        //基层组织代码
        data.put("bseOrgNumber", map.get("baseOrgNumber"));
        //基层组织名称
        data.put("bseOrgName", map.get("baseOrgName"));


        //岗代码
        data.put("posNumber", map.get("posNumber"));
        //岗名称
        data.put("posName", map.get("posName"));

        //生效日期
        data.put("effectiveDate", map.get("effectiveDate"));
        //岗禁用状态
        String posDisableStatus = (String) map.get("posDisableStatus");
        data.put("status", posDisableStatus);


        if ("普通".equals(posDisableStatus)) {
            data.put("expiryDate", map.get("expiryDate"));
        } else if ("作废".equals(posDisableStatus)) {
            data.put("expiryDate", auditDate);
        }

        //失效日期
//        data.put("expiryDate",map.get("expiryDate"));

        //核定编制
        data.put("approvedNumber", map.get("approvedNumber"));
        //负责人岗
        data.put("charge", map.get("charge"));
        //上级岗代码
        data.put("supPosNumber", map.get("superPosNumber"));
        //上级岗名称
        data.put("supPosName", map.get("superPosName"));

        //审批时间
        data.put("auditDate", auditDate);
        //审批人
        data.put("auditer", auditer);

        return data;
    }

}

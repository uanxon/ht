package com.authine.cloudpivot.ext.controller;

import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
import com.authine.cloudpivot.web.api.controller.runtime.WorkflowInstanceController;
import com.authine.cloudpivot.web.api.handler.CustomizedOrigin;
import com.authine.cloudpivot.web.api.view.PageVO;
import com.authine.cloudpivot.web.api.view.ResponseResult;
import com.authine.cloudpivot.web.api.view.runtime.WorkItemSearchVO;
import com.authine.cloudpivot.web.api.view.runtime.WorkItemVO;
import com.authine.cloudpivot.web.api.view.runtime.WorkflowInstanceSearchVO;
import com.authine.cloudpivot.web.api.view.runtime.WorkflowInstanceVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 定制列表查询功能Controller.
 */
@Api(
        tags = {"运行时::流程::定制实例"}
)
@RestController
@RequestMapping({"/api/runtime/workflow"})
@Slf4j
@CustomizedOrigin(level = 1)
public class CustomizedWorkflowInstanceController extends WorkflowInstanceController {


    /**
     * 查询我的流程列表
     *
     * @param workflowInstanceSearchVO 查询对象
     * @return page
     */
    @GetMapping({"/list_my_instances"})
    @ApiOperation(
            value = "查询我的流程列表",
            notes = "查询我的流程列表数据"
    )
    @Override
    public ResponseResult<PageVO<WorkflowInstanceVO>> listMyInstance(@ModelAttribute WorkflowInstanceSearchVO workflowInstanceSearchVO) {
        ResponseResult<PageVO<WorkflowInstanceVO>> pageVOResponseResult = super.listMyInstance(workflowInstanceSearchVO);
        List<WorkflowInstanceVO> content = pageVOResponseResult.getData().getContent();
        for (WorkflowInstanceVO workflowInstanceVO : content) {
            String workflowInstanceId = workflowInstanceVO.getId();
            List<WorkItemModel> workItems = getWorkflowInstanceFacade().getWorkItems(workflowInstanceId, false);
            String allActivityName = "";
            for (WorkItemModel workItem : workItems) {
                String activityName = new StringBuilder("（").append(workItem.getActivityName()).append("）").toString();
                if (!allActivityName.contains(activityName)) {
                    allActivityName = allActivityName + new StringBuilder("（").append(workItem.getActivityName()).append("）").toString();
                }
            }
            workflowInstanceVO.getModel().setInstanceName(workflowInstanceVO.getInstanceName() + allActivityName);
        }

        return pageVOResponseResult;
    }

    @GetMapping({"/search_workitems"})
    @ApiOperation(
            value = "查询待办列表",
            notes = "根据流程名称或发起人查询代办列表数据"
    )
    @Override
    public ResponseResult<PageVO<WorkItemVO>> searchWorkItems(@ModelAttribute WorkItemSearchVO workItemSearchVO) {
        workItemSearchVO.setUserId(this.getUserId());
        ResponseResult<PageVO<WorkItemVO>> pageVOResponseResult = this.handleSearchWorkItems(workItemSearchVO);
        List<WorkItemVO> content = pageVOResponseResult.getData().getContent();
        for (WorkItemVO workItemVO : content) {
            String activityName = workItemVO.getActivityName();
            String instanceName = workItemVO.getInstanceName();
            if (!instanceName.contains(activityName)) {
                instanceName = new StringBuffer(instanceName).append("（").append(workItemVO.getActivityName()).append("）").toString();
                workItemVO.getModel().setInstanceName(instanceName);
            }
        }
        return pageVOResponseResult;
    }
}

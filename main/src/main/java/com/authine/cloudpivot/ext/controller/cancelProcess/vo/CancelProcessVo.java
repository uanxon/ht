package com.authine.cloudpivot.ext.controller.cancelProcess.vo;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class CancelProcessVo {
    private String workflowInstanceId;
    private List<Map<String,Object>> user;
    private String activeCode;
}

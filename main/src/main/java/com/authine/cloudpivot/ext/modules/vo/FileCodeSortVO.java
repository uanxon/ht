package com.authine.cloudpivot.ext.modules.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "排序模型")
@Slf4j
public class FileCodeSortVO implements Comparable<FileCodeSortVO> {

    @ApiModelProperty("档案类型")
    private String fileType;

    @ApiModelProperty("档案代码")
    private String fileCode;

    /**
     * 重写 compareTo
     * 定义:比较此对象与指定对象的顺序
     * 返回:负整数、零或正整数。如果该对象小于、等于或大于指定对象，则分别返回负整数、零或正整数
     * <p>
     * 当fileCode属于 D-Ⅰ  D-Ⅱ  D-Ⅲ  D-Ⅳ  四类时，执行特定排序方法。
     * 当fileCode不属于 D-Ⅰ  D-Ⅱ  D-Ⅲ  D-Ⅳ  四类时，调用字符串自带排序方法即可
     *
     * @param other
     * @return
     */
    @Override
    public int compareTo(FileCodeSortVO other) {
        String fileType = this.fileType;
        int sort = getSort(fileType);
        String fileCode = this.fileCode;
        log.info("fileType:{},sort:{},fileCode:{}", fileType, sort, fileCode);
        String otherFileType = other.fileType;
        int otherSort = getSort(otherFileType);
        String otherFileCode = other.fileCode;
        log.info("otherFileType:{},otherSort:{},otherFileCode:{}", otherFileType, otherSort, otherFileCode);

        if (sort == 0 && otherSort == 0) {
            //当sort和otherSort都为0，说明两个对比对象都不属于四级档案范围，直接按照字符串排序
            return fileCode.compareTo(otherFileCode);
        }

        if (sort == 0 && otherSort > 0) {
            //当sort=0，otherSort>0，说明this不属于四级档案范围，other属于四级档案范围，返回1，将this排在other后面
            return 1;
        }

        if (sort > 0 && otherSort == 0) {
            //当sort>0，otherSort=0，说明this属于四级档案范围，other不属于四级档案范围，返回1，将this排在other前面
            return -1;
        }

        if (sort > 0 && otherSort > 0) {
            //当sort>0，otherSort>0，说明this和other都属于四级档案范围

            if (sort == otherSort) {
                //当两个比较对象的档案类型一样时：先按照公司代码排序，再按照后面的序号排序
                //例如：02-Ⅱ03-05-05-05 和 0202-Ⅱ03-05-07-05 比较
                log.info("档案类型一样，按公司代码排序。fileCode：{}，otherFileCode：{}", fileCode, otherFileCode);
                String companyNumber = fileCode.split("-")[0];
                String otherCompanyNumber = otherFileCode.split("-")[0];
                log.info("companyNumber:{},otherCompanyNumber:{}", companyNumber, otherCompanyNumber);

                if (companyNumber.equals(otherCompanyNumber)) {
                    //如果公司代码一样，比较后面的序号
                    String thisSerialNumber = fileCode.substring(fileCode.indexOf("-") + 2);
                    String otherSerialNumber = otherFileCode.substring(otherFileCode.indexOf("-") + 2);
                    return thisSerialNumber.compareTo(otherSerialNumber);
                } else {
                    //如果公司代码不一样，将公司代码按字符串排序
                    return companyNumber.compareTo(otherCompanyNumber);
                }
            } else {
                //当两个比较对象的档案类型不一样时：直接按照档案类型排序
                //例如：
                //D-Ⅰ和D-Ⅱ对比，1-2 返回 -1
                //D-Ⅲ和D-Ⅱ对比，3-2 返回 1
                return sort - otherSort;
            }

        }

        //如以上情况都不符合，默认相同，不需要排序
        return 0;
    }

    private int getSort(String fileType) {
        //局部内部类
        switch (fileType) {
            case "D-Ⅰ":
                return 1;
            case "D-Ⅱ":
                return 2;
            case "D-Ⅲ":
                return 3;
            case "D-Ⅳ":
                return 4;
        }
        return 0;
    }


}

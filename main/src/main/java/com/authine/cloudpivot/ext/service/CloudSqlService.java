package com.authine.cloudpivot.ext.service;

import com.authine.cloudpivot.engine.service.datasource.DataSourceService;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Map;

/**
 * @Author hxd
 * @Date 2022/9/21 9:47
 * @Description
 **/
public interface CloudSqlService {
    public final static String htEas = "HTEAS";

    public final static String htAz = "cloudpivot";

    int update(String dbCode, String sql, Map<String, ?> paramMap);

    DataSourceService getDataSourceService(String dbConnPoolCode);

    List<Map<String, Object>> getList(String dbCode, String sql);

    Map<String, Object> getMap(String dbCode, String sql);

    public List<Map<String, Object>> getList(String sql);

    public Map<String, Object> getMap(String sql);

    public int getCount(String sql);

    public int update(String sql);

    public void execute(String dbCode, String sql);

    public JdbcTemplate getJdbcTemplateByDbCode(String dbCode);

}

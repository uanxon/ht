package com.authine.cloudpivot.ext.controller.RP;

import cn.hutool.core.lang.Assert;
import com.authine.cloudpivot.engine.api.model.organization.UserModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkflowInstanceModel;
import com.authine.cloudpivot.engine.enums.status.SequenceStatus;
import com.authine.cloudpivot.ext.Utils.CustomSchemaCode;
import com.authine.cloudpivot.ext.service.CloudSqlService;
import com.authine.cloudpivot.web.api.config.EducationEnum;
import com.authine.cloudpivot.web.api.config.FirstEducationEnum;
import com.authine.cloudpivot.web.api.controller.base.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 错失标准
 **/
@RestController
@RequestMapping("/public/RpRecord")
@Slf4j
public class RpRecord extends BaseController {


    @Autowired
    CloudSqlService sqlService;

    /**
     * 错失标准  审批流完成后 数据写到-错失标准 基础表 基础
     */
    @RequestMapping("finish")
    public void finish(String bizId) {
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.RpRecordAdd, bizId);

        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get("SheetRpRecord");

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        //获取审批人

        String approval = getFileBaseInfoWorkFlowApproval(bizObject);

        for (Map<String, Object> map : list) {

            try {
                //获取数据
                Map<String, Object> data = fileInfoBaseMap(map, now, approval);
                //判断是否已存在
                String id = existsBizObject(map);
                if (StringUtils.isNotEmpty(id)) {
                    data.put("id", id);
                }

                BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.RpRecordBase, data, false);
                model.setSequenceStatus(SequenceStatus.COMPLETED.name());
                id = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                log.info("基础表-人员信息基础作成功 ");
                //调用存储过程
                callProcess(id);

            } catch (Exception e) {
                String funNumber = (String) map.get("funNumber");
                String rpstandardcode = (String) map.get("rpstandardcode");
                log.info("基础表-错失标准信息基础操作失败 funNumber={},rpstandardcode={}", funNumber, rpstandardcode);
                log.info(e.getMessage(), e);
            }

        }
    }

    @RequestMapping("toVoid")
    public void toVoid(String bizId) {
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.RpRecordVoid, bizId);
        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get("SheetRpRecordVoid");

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        //获取审批人

        String approval = getFileBaseInfoWorkFlowApproval(bizObject);

        for (Map<String, Object> map : list) {


            try {
                //获取数据
                Map<String, Object> data = fileInfoBaseMap(map, now, approval);
                //判断是否已存在
                String id = existsBizObject(data);
                if (StringUtils.isNotEmpty(id)) {
                    data.put("id", id);
                }

                BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.RpRecordBase, data, false);
                model.setSequenceStatus(SequenceStatus.CANCELED.name());
                id = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                log.info("基础表-人员信息基础作废操作成功 ");
                //调用存储过程
                callProcessToVoid(id);

            } catch (Exception e) {
                String funNumber = (String) map.get("funNumber");
                String rpstandardcode = (String) map.get("rpstandardcode");

                log.info("基础表-人员信息基础操作失败 funNumber={},rpstandardcode={}", funNumber, rpstandardcode);
                log.info(e.getMessage(), e);
            }

        }
    }


    /**
     * 调用存储过程
     * ------错失记录
     * CREATE  PROC   SyncQuestionRecord
     *
     * @param bizId
     * @CompanyFID nvarchar(100),    ----公司FID
     * @OrgFID nvarchar(100),    ----基层组织FID
     * @PosCode nvarchar(100),    ----岗代码
     * @PersonCode nvarchar(100),    ----人员代码
     * @funcode nvarchar(20),    ----职能代码
     * @QuestionCode nvarchar(100),    ----问题代码
     * @CheckDeptFID nvarchar(100),    ----检查部门FID
     * @CheckManCode nvarchar(100),    ----检查人代码
     * @happenday datetime,     ----发生日期
     * @ItemCode nvarchar(100),    ----事项代码
     * @Desc nvarchar(300),    ----问题说明
     * @Auditor nvarchar(50)    ----审批人   02.0100 张三
     */
    private void callProcess(String bizId) {

        if (StringUtils.isEmpty(bizId)) {
            return;
        }
        //编写sql
        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.RpRecordBase);
        StringBuilder sql = new StringBuilder("SELECT RelevanceCompany,RelevanceBaseOrg,posNumber,personNumber,funNumber,rpstandardcode,checkPersonNumber,Date,itemNumber,problemDesc,auditer from ")
                .append(tableName).append(" where id ='")
                //.append("' and oneLevelCode='").append(oneLevelCode)
                .append(bizId).append("';");
        //查询到入参
        Map<String, Object> map = sqlService.getMap(sql.toString());

        log.info("入参map={}", map);

        //调用存储过程
        String RelevanceCompany = MapUtils.getString(map, "RelevanceCompany", "");
        String RelevanceBaseOrg = MapUtils.getString(map, "RelevanceBaseOrg", "");
        String posNumber = MapUtils.getString(map, "posNumber", "");
        String personNumber = MapUtils.getString(map, "personNumber", "");
        String funNumber = MapUtils.getString(map, "funNumber", "");

        String rpstandardcode = MapUtils.getString(map, "rpstandardcode", "");
        String checkDeptNumber = "";
        String checkPersonNumber = MapUtils.getString(map, "checkPersonNumber", "");
//        Date DateTemp = (Date) map.get("Date");
//        String Date = DateTemp == null ? null : DateFormatUtils.format(DateTemp, "yyyy-MM-dd");
        LocalDateTime DateTemp = (LocalDateTime) map.get("Date");
        String Date =  ObjectUtils.isEmpty(DateTemp) ? "" : DateTemp.toLocalDate().toString();

        String itemNumber = MapUtils.getString(map, "itemNumber", "");
        String problemDesc = MapUtils.getString(map, "problemDesc", "");
        String auditer = MapUtils.getString(map, "auditer", "");


//        Assert.isFalse(StringUtils.isEmpty(RelevanceCompany),"{}不能为空","RelevanceCompany");
////        Assert.isFalse(firstDept==null ,"{}不能为空","firstDept");
//        Assert.isFalse(StringUtils.isEmpty(RelevanceBaseOrg),"{}不能为空","txtfuncode");
//        Assert.isFalse(StringUtils.isEmpty(posNumber),"{}不能为空","serial");
//        Assert.isFalse(StringUtils.isEmpty(rpstandardcode),"{}不能为空","rpstandardcode");
//        Assert.isFalse(StringUtils.isEmpty(personNumber),"{}不能为空","rpstandard");
//        Assert.isFalse(StringUtils.isEmpty(funNumber),"{}不能为空","desc");
//        Assert.isFalse(StringUtils.isEmpty(checkDeptNumber),"{}不能为空","rplevel");
//        Assert.isFalse(StringUtils.isEmpty(checkPersonNumber),"{}不能为空","relevantfilecode");
//        Assert.isFalse(StringUtils.isEmpty(Date),"{}不能为空","relevantfile");
//        Assert.isFalse(StringUtils.isEmpty(itemNumber),"{}不能为空","relevantfilecode");
//        Assert.isFalse(StringUtils.isEmpty(problemDesc),"{}不能为空","relevantfile");
//        Assert.isFalse(StringUtils.isEmpty(auditer),"{}不能为空","auditer");


        String execSql = String.format("exec [HG_LINK].[hg].[dbo].SyncQuestionRecord   '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'",
                RelevanceCompany, RelevanceBaseOrg, posNumber, personNumber, funNumber, rpstandardcode, checkDeptNumber, checkPersonNumber, Date, itemNumber, problemDesc, auditer);

        log.info("\n==========准备调用存储过程:{}", execSql);

        sqlService.execute(CloudSqlService.htEas, execSql);

        log.info("\n=============存储过程执行完成");

    }

    /**
     * 调用作废存储过程
     * --------人员信息记录   基础信息 作废
     * exec  QuestionRecordVoid
     *
     * @param bizId
     * @PersonCode nvarchar(100),    ----人员代码
     * @ItemCode nvarchar(100)     ----事项代码
     */
    private void callProcessToVoid(String bizId) {

        if (StringUtils.isEmpty(bizId)) {
            return;
        }
        //编写sql
        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.RpRecordBase);
        StringBuilder sql = new StringBuilder("SELECT itemNumber,personNumber from ")
                .append(tableName).append(" where id ='")
                .append(bizId).append("';");
        //查询到入参
        Map<String, Object> map = sqlService.getMap(sql.toString());

        log.info("入参map={}", map);

        //调用存储过程
        String personNumber = MapUtils.getString(map, "personNumber", "");
        String itemNumber = MapUtils.getString(map, "itemNumber", "");


        Assert.isFalse(StringUtils.isEmpty(itemNumber), "{}不能为空", "itemNumber");
        Assert.isFalse(StringUtils.isEmpty(personNumber), "{}不能为空", "personNumber");


        String execSql = String.format("exec [HG_LINK].[hg].[dbo].QuestionRecordVoid '%s','%s'",
                personNumber, itemNumber);

        log.info("\n==========准备调用作废存储过程:{}", execSql);

        sqlService.execute(CloudSqlService.htEas, execSql);

        log.info("\n=============作废存储过程执行完成");

    }

    /**
     * 查询审批人,返回  员工号+姓名
     *
     * @param bizObject
     * @return
     */
    private String getFileBaseInfoWorkFlowApproval(BizObjectCreatedModel bizObject) {


        WorkflowInstanceModel instanceModel = getWorkflowInstanceFacade().getByObjectId(bizObject.getId());
        List<WorkItemModel> workItems = getWorkflowInstanceFacade().getWorkItems(instanceModel.getId(), true);
        final String finalActivityCode = "Activity11";
        Optional<WorkItemModel> first = workItems.stream().filter(a -> a.getActivityCode().equals(finalActivityCode)).findFirst();
        String participant = null;
        if (first.isPresent()) {
            WorkItemModel workItemModel = first.get();
            participant = workItemModel.getParticipant();
        }

        if (participant == null) {
            participant = bizObject.getCreater().getId();
        }

        UserModel user = getOrganizationFacade().getUser(participant);


        return new StringBuilder(user.getEmployeeNo()).append("　").append(user.getName()).toString();
    }


    /**
     * 判断是否已存在
     *
     * @return
     */
    private String existsBizObject(Map data) {

        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.RpRecordBase);
        String personNumber = (String) data.get("personNumber");
        String itemNumber = (String) data.get("itemNumber");

        StringBuilder sql = new StringBuilder("select id  from ").append(tableName)
                .append(" where personNumber='").append(personNumber)
                .append("' and itemNumber='").append(itemNumber)
                .append("';");

        Map<String, Object> map = sqlService.getMap(sql.toString());

        return (String) map.get("id");
    }

    /**
     * 转换成  基础表-错失标准 的数据
     *
     * @param map
     * @param auditDate
     * @return
     */
    private Map<String, Object> fileInfoBaseMap(Map<String, Object> map, String auditDate, String auditer) {
        Map<String, Object> data = new HashMap<>();


        //关联单选公司
        data.put("RelevanceCompany", map.get("RelevanceCompany"));
        //公司代碼
        data.put("companyNumber", map.get("companyNumber"));

        //关联单选基层组织代码
        data.put("RelevanceBaseOrg", map.get("RelevanceBaseOrg"));
        //基层组织代码
        data.put("baseOrgNumber", map.get("baseOrgNumber"));
        //基层组织
        data.put("baseOrgName", map.get("baseOrgName"));

        //关联单选岗
        data.put("RelevancePos", map.get("RelevancePos"));
        //岗代码
        data.put("posNumber", map.get("posNumber"));
        //岗
        data.put("posName", map.get("posName"));

        //关联单选人员代码
        data.put("RelevancePerson", map.get("RelevancePerson"));
        //人员代码
        data.put("personNumber", map.get("personNumber"));
        //人员
        data.put("personName", map.get("personName"));

        //关联单选职能
        data.put("RelevanceFun", map.get("RelevanceFun"));
        //职能
        data.put("funNumber", map.get("funNumber"));


        //問題項代码
        data.put("rpstandardcode", map.get("rpstandardcode"));
        //问题项
        data.put("rpstandard", map.get("rpstandard"));

        //关联檢查部門
        data.put("checkDeptNumber", map.get("checkDeptNumber"));
        //檢查部門
        data.put("checkDeptName", map.get("checkDeptName"));

        //关联檢查人
        data.put("RelevanceCheckPerson", map.get("RelevanceCheckPerson"));
        //檢查人代码
        data.put("checkPersonNumber", map.get("checkPersonNumber"));
        //檢查人
        data.put("checkPersonName", map.get("checkPersonName"));

        //发生日期
        data.put("Date", map.get("Date"));
        //事项代码
        data.put("itemNumber", map.get("itemNumber"));
        //问题说明
        data.put("problemDesc", map.get("problemDesc"));


        //审批时间
        data.put("auditDate", auditDate);
        //审批人
        data.put("auditer", auditer);

        return data;
    }
}


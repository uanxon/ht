package com.authine.cloudpivot.ext.service;

import com.authine.cloudpivot.engine.api.model.dto.MessageDTO;
import com.authine.cloudpivot.engine.api.model.dto.SimpleMsgDTO;
import com.authine.cloudpivot.engine.enums.type.CorpRelatedType;
import com.authine.cloudpivot.engine.service.message.IMessageService;
import com.authine.cloudpivot.engine.service.organization.exception.DataSourceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author hxd
 * @Date 2022/8/11 15:27
 * @Description 11
 **/
@Service
@Slf4j
public class TestMessageService implements IMessageService {
    @Override
    public List<String> sendMsg(final MessageDTO message) throws DataSourceException {
        log.info("\n=============> sendMsg");

        return null;
    }

    @Override
    public List<String> sendSimpleTextMsg(final SimpleMsgDTO message) throws DataSourceException {
        log.info("\n=============> sendSimpleTextMsg");
        return null;
    }

    @Override
    public String initChannel() {


        return CorpRelatedType.OTHER.toString();
    }
}

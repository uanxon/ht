package com.authine.cloudpivot.ext.controller.fun;

import cn.hutool.core.lang.Assert;
import com.authine.cloudpivot.engine.api.model.organization.UserModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkflowInstanceModel;
import com.authine.cloudpivot.engine.enums.status.SequenceStatus;
import com.authine.cloudpivot.ext.Utils.CustomSchemaCode;
import com.authine.cloudpivot.ext.service.CloudSqlService;
import com.authine.cloudpivot.web.api.controller.base.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *  职能职责表 一级
 **/
@RestController
@RequestMapping("/public/funOneLevel")
@Slf4j
public class FunOneLevelController extends BaseController {


    @Autowired
    CloudSqlService sqlService;

    /**
     *  职能职责表 一级  审批流完成后 数据写到-职能职责表 一级 基础
     */
    @RequestMapping("finish")
    public void finish(String bizId){
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.funOneLevelAdd, bizId);

        List<Map<String,Object>> list = (List<Map<String, Object>>) bizObject.get("funOneLevelDetail");

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        //获取审批人

        String approval = getFileBaseInfoWorkFlowApproval(bizObject);

        for (Map<String, Object> map : list) {
            String oneLevelCode = (String) map.get("oneLevelCode");
            String funCode = (String) map.get("funCode");

            try {
                //获取数据
                Map<String, Object> data = fileInfoBaseMap(map, now, approval);
                //判断是否已存在
                String id = existsBizObject(map);
                if (StringUtils.isNotEmpty(id)) {
                    data.put("id", id);
                }

                BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.funOneLevelBase, data, false);
                model.setSequenceStatus(SequenceStatus.COMPLETED.name());
                id = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                log.info("基础表-公司级档案操作成功 ");
                //调用存储过程
                callProcess(id);

            } catch (Exception e) {
                log.info("基础表-总公司档案操作失败 oneLevelCode={},funCode={}", oneLevelCode,funCode);
                log.info(e.getMessage(), e);
            }

        }
    }
    @RequestMapping("toVoid")
    public void toVoid(String bizId){
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.toVoidFunOneLevel, bizId);
        List<Map<String,Object>> list = (List<Map<String, Object>>) bizObject.get("voidFunOneLevelDetail");

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        //获取审批人

        String approval = getFileBaseInfoWorkFlowApproval(bizObject);

        for (Map<String, Object> map : list) {
            String oneLevelCode = (String) map.get("oneLevelCode");
            String funCode = (String) map.get("funCode");

            try {
                //获取数据
                Map<String, Object> data = fileInfoBaseMap(map, now, approval);
                //判断是否已存在
                String id = existsBizObject(data);
                if (StringUtils.isNotEmpty(id)) {
                    data.put("id", id);
                }

                BizObjectCreatedModel model = new BizObjectCreatedModel(CustomSchemaCode.funOneLevelBase, data, false);
                model.setSequenceStatus(SequenceStatus.CANCELED.name());
                id = getBizObjectFacade().saveBizObject(CustomSchemaCode.adminUserId, model, false);
                log.info("基础表-公司级档案作废操作成功 ");
                //调用存储过程
                callProcessToVoid(id);

            } catch (Exception e) {
                log.info("基础表-总公司档案作废操作失败 oneLevelCode={},funCode={}", oneLevelCode,funCode);
                log.info(e.getMessage(), e);
            }

        }
    }



    /**
     *  调用存储过程
     *  exec   SyncRespBaseOne
     * funCode   nvarchar(50),			----总公司职能代码  01  02  03
     * Number  nvarchar(100),				----一级代码
     * Name   nvarchar(200),				----一级职责
     * Auditor  nvarchar(50)				----审批人员工代码+空格+姓名
     * @param bizId
     */
    private void callProcess(String bizId){

        if (StringUtils.isEmpty(bizId)) {
            return;
        }
        //编写sql
        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.funOneLevelBase);
        StringBuilder sql = new StringBuilder("SELECT funCode,oneLevelCode,oneLevelDuty,auditer from ")
                .append(tableName).append(" where id ='")
                .append(bizId).append("';");
        //查询到入参
        Map<String, Object> map = sqlService.getMap(sql.toString());

        log.info("入参map={}",map);

        //调用存储过程

        String funCode = MapUtils.getString(map,"funCode","");
        String oneLevelCode = MapUtils.getString(map,"oneLevelCode","");
        String oneLevelDuty = MapUtils.getString(map,"oneLevelDuty","");
        String auditer = MapUtils.getString(map,"auditer","");

        Assert.isFalse(StringUtils.isEmpty(funCode),"{}不能为空","funCode");
        Assert.isFalse(StringUtils.isEmpty(oneLevelCode),"{}不能为空","oneLevelCode");
        Assert.isFalse(StringUtils.isEmpty(oneLevelDuty),"{}不能为空","oneLevelDuty");
        Assert.isFalse(StringUtils.isEmpty(auditer),"{}不能为空","auditer");

        String execSql = String.format("exec [HG_LINK].[hg].[dbo].SyncRespBaseOne '%s','%s','%s','%s'",
                funCode,oneLevelCode,oneLevelDuty,auditer );

        log.info("\n==========准备调用存储过程:{}",execSql);

        sqlService.execute(CloudSqlService.htEas,execSql);

        log.info("\n=============存储过程执行完成");

    }

    /**
     *  调用作废存储过程
     *  exec   RespBaseOneVoid
 *      FunCode   nvarchar(100),			----职能代码
     *  Number  nvarchar(100),				----一级代码
     *  Auditor  nvarchar(50)				----审批人员工代码+空格+姓名
     * @param bizId
     */
    private void callProcessToVoid(String bizId){

        if (StringUtils.isEmpty(bizId)) {
            return;
        }
        //编写sql
        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.funOneLevelBase);
        StringBuilder sql = new StringBuilder("SELECT funCode,oneLevelCode,oneLevelDuty,auditer from ")
                .append(tableName).append(" where id ='")
                .append(bizId).append("';");
        //查询到入参
        Map<String, Object> map = sqlService.getMap(sql.toString());

        log.info("入参map={}",map);

        //调用存储过程

        String funCode = MapUtils.getString(map,"funCode","");
        String oneLevelCode = MapUtils.getString(map,"oneLevelCode","");
        String auditer = MapUtils.getString(map,"auditer","");

        Assert.isFalse(StringUtils.isEmpty(funCode),"%s不能为空","funCode");
        Assert.isFalse(StringUtils.isEmpty(oneLevelCode),"{}不能为空","oneLevelCode");

        Assert.isFalse(StringUtils.isEmpty(auditer),"{}不能为空","auditer");

        String execSql = String.format("exec [HG_LINK].[hg].[dbo].RespBaseOneVoid '%s','%s','%s'",
                funCode,oneLevelCode,auditer );

        log.info("\n==========准备调用作废存储过程:{}",execSql);

        sqlService.execute(CloudSqlService.htEas,execSql);

        log.info("\n=============作废存储过程执行完成");

    }

    /**
     *  查询审批人,返回  员工号+姓名
     * @param bizObject
     * @return
     */
    private String getFileBaseInfoWorkFlowApproval(BizObjectCreatedModel bizObject) {


        WorkflowInstanceModel instanceModel = getWorkflowInstanceFacade().getByObjectId(bizObject.getId());
        List<WorkItemModel> workItems = getWorkflowInstanceFacade().getWorkItems(instanceModel.getId(), true);
        final String finalActivityCode = "Activity7";
        Optional<WorkItemModel> first = workItems.stream().filter(a -> a.getActivityCode().equals(finalActivityCode)).findFirst();
        String participant = null;
        if (first.isPresent()) {
            WorkItemModel workItemModel = first.get();
            participant = workItemModel.getParticipant();
        }

        if (participant == null) {
            participant = bizObject.getCreater().getId();
        }

        UserModel user = getOrganizationFacade().getUser(participant);


        return new StringBuilder(user.getEmployeeNo()).append(" ").append(user.getName()).toString();
    }



    /**
     * 判断是否已存在
     * @return
     */
    private String existsBizObject(Map data){

        String tableName = getBizObjectFacade().getTableName(CustomSchemaCode.funOneLevelBase);
        String oneLevelCode = (String) data.get("oneLevelCode");
        String funCode = (String) data.get("funCode");

        StringBuilder sql = new StringBuilder("select id  from ").append(tableName)
                .append(" where funCode='").append(funCode).append("' and oneLevelCode='")
                .append(oneLevelCode).append("';");

        Map<String, Object> map = sqlService.getMap(sql.toString());

        return (String) map.get("id");
    }

    /**
     *  转换成  基础表-总公司档案 的数据
     * @param map
     * @param auditDate
     * @return
     */
    private Map<String,Object> fileInfoBaseMap(Map<String,Object> map,String auditDate,String auditer){
        Map<String, Object> data = new HashMap<>();

        //审批时间
        data.put("auditDate",map.get("auditDate"));
        //审批人
        data.put("auditer",map.get("auditer"));
        //职能代码隐藏
        data.put("funCode",map.get("funCode"));
        //职能总责部门代码隐藏
        data.put("funDutyDeptCode",map.get("funDutyDeptCode"));
        //职能总责部门
        data.put("funDutyDeptName",map.get("funDutyDeptName"));
        //职能
        data.put("funName",map.get("funName"));
        //一级代码
        data.put("oneLevelCode",map.get("oneLevelCode"));
        //一级职责
        data.put("oneLevelDuty",map.get("oneLevelDuty"));
        //职能代码
        data.put("relevFun",map.get("relevFun"));
        //职能总责部门代码
        data.put("relevFunDutyDept",map.get("relevFunDutyDept"));

        //审批时间
        data.put("auditDate",auditDate);
        //审批人
        data.put("auditer",auditer);

        return data;
    }
}

package com.authine.cloudpivot.ext.controller;

import com.authine.cloudpivot.engine.api.facade.FileStoreFacade;
import com.authine.cloudpivot.engine.api.model.runtime.AttachmentModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.engine.domain.runtime.BizAttachment;
import com.authine.cloudpivot.ext.Utils.FormDataFileUpload;
import com.authine.cloudpivot.web.api.controller.base.BaseController;
import com.authine.cloudpivot.web.api.view.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.util.EncodingUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.*;
import java.util.List;
import java.util.Map;

/**
 * @Author hxd
 * @Date 2022/10/9 23:23
 * @Description
 **/
@Slf4j
@Controller
@RequestMapping("/public/testUpload")
public class FileUploadTestContoller  extends BaseController {


    @Autowired
    private FileStoreFacade fileStoreFacade;

    /**
     *  测试上传优米云盘
     * @param bizObjectId
     * @param schemaCode
     * @return
     * @throws Exception
     */
    @RequestMapping("testUpload")
    public ResponseResult uploadTo(String bizObjectId,String schemaCode) throws Exception {

        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(schemaCode, bizObjectId);

        List<Map<String,Object>> attachmentList = (List<Map<String,Object>>)bizObject.get("att");

        Map<String,Object> bizAttachment = attachmentList.get(0);


        AttachmentModel attachment = getBizObjectFacade().getAttachmentByRefId((String) bizAttachment.get("refId"));



        InputStream inputStream = fileStoreFacade.downloadFile(attachment.getRefId());
        int size = inputStream.available();

        int lastIndexOf = attachment.getName().lastIndexOf(".");


        File tempFile = File.createTempFile(attachment.getName().substring(0, lastIndexOf), attachment.getName().substring(lastIndexOf) + 1);

        FileOutputStream outputStream = new FileOutputStream(tempFile);

        int ch;
        while ((ch = inputStream.read()) != -1) {
            outputStream.write(ch);
        }

        String s = FormDataFileUpload.fileUpload(tempFile, size, attachment.getName(),"","测试");
        return null ;
    }


}

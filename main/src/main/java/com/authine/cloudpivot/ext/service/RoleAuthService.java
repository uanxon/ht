package com.authine.cloudpivot.ext.service;

/**
 * @Author hxd
 * @Date 2022/9/21 10:14
 * @Description  浩通云盘 文件授权
 **/

public interface RoleAuthService {

    public void RoleAuthDis(String PosDuty,String PosName,String AuthFileId);

}

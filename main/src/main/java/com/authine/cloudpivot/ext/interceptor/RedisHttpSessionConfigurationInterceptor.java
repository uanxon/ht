package com.authine.cloudpivot.ext.interceptor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.config.ConfigureRedisAction;

/**
 * 禁用redis的config命令时注入ConfigureRedisAction.NO_OP
 */
@Configuration
public class RedisHttpSessionConfigurationInterceptor {

    @Bean
    public static ConfigureRedisAction configureRedisAction(){
        return ConfigureRedisAction.NO_OP;
    }

}

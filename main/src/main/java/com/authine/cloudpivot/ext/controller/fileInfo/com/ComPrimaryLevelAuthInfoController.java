package com.authine.cloudpivot.ext.controller.fileInfo.com;

import com.authine.cloudpivot.engine.api.model.organization.UserModel;
import com.authine.cloudpivot.engine.api.model.runtime.BizObjectCreatedModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
import com.authine.cloudpivot.engine.api.model.runtime.WorkflowInstanceModel;
import com.authine.cloudpivot.engine.enums.status.SequenceStatus;
import com.authine.cloudpivot.ext.Utils.CustomSchemaCode;
import com.authine.cloudpivot.ext.service.CloudSqlService;
import com.authine.cloudpivot.web.api.controller.base.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 公司级档案基层组织授权信息
 **/
@RestController
@RequestMapping("/public/comPrimaryLevelAuth")
@Slf4j
public class ComPrimaryLevelAuthInfoController extends BaseController {


    @Autowired
    CloudSqlService sqlService;

    /**
     * 总公司级档案部门授权信息  审批流完成后 数据写到基础表-总公司级档案中间组织授权信息(总公司级档案部门授权信息)
     */
    @Async
    @RequestMapping("empower")
    public void empower(String bizId) {
        BizObjectCreatedModel bizObject = getBizObjectFacade().getBizObject(CustomSchemaCode.ComPrimaryLevelAuthInfo, bizId);

//        Object pushAllResult = bizObject.get("pushAllResult");


        List<Map<String, Object>> list = (List<Map<String, Object>>) bizObject.get("comPrimaryLevelAuthInfoSheet");

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));


        //获取审批人
        String approvalType = (String) list.get(0).get("approvalType");
        String approval = getFileBaseInfoWorkFlowApproval(bizObject, approvalType);


        for (Map<String, Object> map : list) {
            String fileCode = (String) map.get("fileCode");

            try {

                //调用存储过程
                callProcess(map);

            } catch (Exception e) {
                log.info("基础表-总公司档案  添加失败 fileCode={}", fileCode);
                log.info(e.getMessage(), e);
            }

        }
    }


    /**
     * 调用存储过程
     -----基层组织公司级档案			----2023.03.22
     exec   SyncComBaseArchive
     @BaseOrgNumber  nvarchar(50),		----基层组织代码
     @Number		 nvarchar(100),		----档案代码
     @TotalQty		 int,				----纸质
     @DeptQty		 int,				----部门纸质
     @PosQty		 int,				----岗位纸质
     @WorkDesc		 nvarchar(200),		----工作说明
     @AuditTime		 Datetime,			----审批时间
     @Auditor		 nvarchar(50),		----审批人(员工号+姓名)
     @BasePosCode       	 nvarchar(100),		----基层组织责任岗代码
     @BasePerson	 	 nvarchar(50),		----基层组织责任人
     @P001			 nvarchar(10),		----001权限(★ ▲ √ '')     '1'  五角    '5'  三角    '6'  对勾   '7' 空（取消授权）
     @Q001			 int,				----001数量
     @R001			 nvarchar(10),		----001学习要求(Ⅰ Ⅱ Ⅲ  '')     WHEN  '1' THEN  'Ⅰ'  when '2' then 'Ⅱ' when  '3' then 'Ⅲ'  else  '' end
     @P002			 nvarchar(10),		----002
     @Q002			 int,
     @R002			 nvarchar(10),
     @P003			 nvarchar(10),		----003
     @Q003			 int,
     @R003			 nvarchar(10),
     @P004			 nvarchar(10),		----004
     @Q004			 int,
     @R004			 nvarchar(10),
     @P005			 nvarchar(10),		----005
     @Q005			 int,
     @R005			 nvarchar(10),
     @P006			 nvarchar(10),		----006
     @Q006			 int,
     @R006			 nvarchar(10),
     @P007			 nvarchar(10),		----007
     @Q007			 int,
     @R007			 nvarchar(10),
     @P008			 nvarchar(10),		----008
     @Q008			 int,
     @R008			 nvarchar(10),
     @P009			 nvarchar(10),		----009
     @Q009			 int,
     @R009			 nvarchar(10),
     @P010			 nvarchar(10),		----010
     @Q010			 int,
     @R010			 nvarchar(10)
     */
    private void callProcess(Map<String, Object> map) {
        log.info("入参map={}", map);

//        String baseOrgid = MapUtils.getString(map, "baseOrgid", "");
        String baseOrgNumber = MapUtils.getString(map, "baseOrgNumber", "");//基层组织代码
//        String fileNumber = MapUtils.getString(map, "fileNumber", "");//
        String fileCode = MapUtils.getString(map, "fileCode", "");//档案代码

        String totalPaperStr = map.get("totalPaper") == null ? "0" : map.get("totalPaper").toString();//纸质
        BigDecimal totalPaper = "0E-8".equals(totalPaperStr) ? new BigDecimal("0") : new BigDecimal(totalPaperStr).setScale(0, BigDecimal.ROUND_HALF_UP);
        String totalPaperValue = totalPaper.setScale(0).toString();//
        BigDecimal totalPaper01 = (BigDecimal) map.get("totalPaper") ;


        String deptPaperStr = map.get("deptPaper") == null ? "0" : map.get("deptPaper").toString();//部门纸质
        BigDecimal deptPaper = "0E-8".equals(deptPaperStr) ? new BigDecimal("0") : new BigDecimal(deptPaperStr).setScale(0, BigDecimal.ROUND_HALF_UP);
        String deptPaperValue = deptPaper.setScale(0).toString();//

        String posPaperStr = map.get("posPaper") == null ? "0" : map.get("posPaper").toString();//岗位纸质
        BigDecimal posPaper = "0E-8".equals(posPaperStr) ? new BigDecimal("0") : new BigDecimal(posPaperStr).setScale(0, BigDecimal.ROUND_HALF_UP);
        String posPaperValue = posPaper.setScale(0).toString();//

        String jobDesc = MapUtils.getString(map, "jobDesc", "");//工作说明

        Date auditDateTemp = (Date) map.get("auditDate");//审批时间
        String auditDate = auditDateTemp == null ? "" : DateFormatUtils.format(auditDateTemp, "yyyy-MM-dd");

        String approval = MapUtils.getString(map, "approval", "");//审批人 02.0100 张三//审批人(员工号+姓名)
        String baseOrgResPosNumber = MapUtils.getString(map, "baseOrgResPosNumber", "");//基层组织责任岗代码
        String baseOrgResPerson = MapUtils.getString(map, "baseOrgResPerson", "");//基层组织责任人

        String auth001 = getQuanXianValue(MapUtils.getString(map, "auth001", ""));//001权限(★ ▲ √ '')     '1'  五角    '5'  三角    '6'  对勾   '7' 空（取消授权）
        String qua001Str = map.get("qua001") == null ? "0" : map.get("qua001").toString();//
        BigDecimal qua001 = "0E-8".equals(qua001Str) ? new BigDecimal("0") : new BigDecimal(qua001Str).setScale(0, BigDecimal.ROUND_HALF_UP);
        String qua001Value = qua001.setScale(0).toString();//001数量
        String res001 = MapUtils.getString(map, "res001", "");//001学习要求(Ⅰ Ⅱ Ⅲ  '')     WHEN  '1' THEN  'Ⅰ'  when '2' then 'Ⅱ' when  '3' then 'Ⅲ'  else  '' end
        res001 = res001 == null ? "" : res001;
        switch (res001) {
            case "Ⅰ":
                res001 = "1";
                break;
            case "Ⅱ":
                res001 = "2";
                break;
            case "Ⅲ":
                res001 = "3";
                break;
        }



        String auth002 = getQuanXianValue(MapUtils.getString(map, "auth002", ""));//
        String qua002Str = map.get("qua002") == null ? "0" : map.get("qua002").toString();//
        BigDecimal qua002 = "0E-8".equals(qua002Str) ? new BigDecimal("0") : new BigDecimal(qua002Str).setScale(0, BigDecimal.ROUND_HALF_UP);
        String qua002Value = qua002.setScale(0).toString();//
        String res002 = MapUtils.getString(map, "res002", "");//
        res002 = res002 == null ? "" : res002;
        switch (res002) {
            case "Ⅰ":
                res002 = "1";
                break;
            case "Ⅱ":
                res002 = "2";
                break;
            case "Ⅲ":
                res002 = "3";
                break;
        }

        String auth003 = getQuanXianValue(MapUtils.getString(map, "auth003", ""));//
        String qua003Str = map.get("qua003") == null ? "0" : map.get("qua003").toString();//
        BigDecimal qua003 = "0E-8".equals(qua003Str) ? new BigDecimal("0") : new BigDecimal(qua003Str).setScale(0, BigDecimal.ROUND_HALF_UP);
        String qua003Value = qua003.setScale(0).toString();//
        String res003 = MapUtils.getString(map, "res003", "");//
        res003 = res003 == null ? "" : res003;
        switch (res003) {
            case "Ⅰ":
                res003 = "1";
                break;
            case "Ⅱ":
                res003 = "2";
                break;
            case "Ⅲ":
                res003 = "3";
                break;
        }

        String auth004 = getQuanXianValue(MapUtils.getString(map, "auth004", ""));//
        String qua004Str = map.get("qua004") == null ? "0" : map.get("qua004").toString();//
        BigDecimal qua004 = "0E-8".equals(qua004Str) ? new BigDecimal("0") : new BigDecimal(qua004Str).setScale(0, BigDecimal.ROUND_HALF_UP);
        String qua004Value = qua004.setScale(0).toString();//
        String res004 = MapUtils.getString(map, "res004", "");//
        res004 = res004 == null ? "" : res004;
        switch (res004) {
            case "Ⅰ":
                res004 = "1";
                break;
            case "Ⅱ":
                res004 = "2";
                break;
            case "Ⅲ":
                res004 = "3";
                break;
        }
//
        String auth005 = getQuanXianValue(MapUtils.getString(map, "auth005", ""));//
        String qua005Str = map.get("qua005") == null ? "0" : map.get("qua005").toString();//
        BigDecimal qua005 = "0E-8".equals(qua005Str) ? new BigDecimal("0") : new BigDecimal(qua005Str).setScale(0, BigDecimal.ROUND_HALF_UP);
        String qua005Value = qua005.setScale(0).toString();
        String res005 = MapUtils.getString(map, "res005", "");
        res005 = res005 == null ? "" : res005;
        switch (res005) {
            case "Ⅰ":
                res005 = "1";
                break;
            case "Ⅱ":
                res005 = "2";
                break;
            case "Ⅲ":
                res005 = "3";
                break;
        }

        String auth006 = getQuanXianValue(MapUtils.getString(map, "auth006", ""));
        String qua006Str = map.get("qua006") == null ? "0" : map.get("qua006").toString();
        BigDecimal qua006 = "0E-8".equals(qua006Str) ? new BigDecimal("0") : new BigDecimal(qua006Str).setScale(0, BigDecimal.ROUND_HALF_UP);
        String qua006Value = qua006.setScale(0).toString();
        String res006 = MapUtils.getString(map, "res006", "");
        res006 = res006 == null ? "" : res006;
        switch (res006) {
            case "Ⅰ":
                res006 = "1";
                break;
            case "Ⅱ":
                res006 = "2";
                break;
            case "Ⅲ":
                res006 = "3";
                break;
        }

        String auth007 = getQuanXianValue(MapUtils.getString(map, "auth007", ""));
        String qua007Str = map.get("qua007") == null ? "0" : map.get("qua007").toString();
        BigDecimal qua007 = "0E-8".equals(qua007Str) ? new BigDecimal("0") : new BigDecimal(qua007Str).setScale(0, BigDecimal.ROUND_HALF_UP);
        String qua007Value = qua007.setScale(0).toString();
        String res007 = MapUtils.getString(map, "res007", "");
        res007 = res007 == null ? "" : res007;
        switch (res007) {
            case "Ⅰ":
                res007 = "1";
                break;
            case "Ⅱ":
                res007 = "2";
                break;
            case "Ⅲ":
                res007 = "3";
                break;
        }

        String auth008 = getQuanXianValue(MapUtils.getString(map, "auth008", ""));
        String qua008Str = map.get("qua008") == null ? "0" : map.get("qua008").toString();
        BigDecimal qua008 = "0E-8".equals(qua008Str) ? new BigDecimal("0") : new BigDecimal(qua008Str).setScale(0, BigDecimal.ROUND_HALF_UP);
        String qua008Value = qua008.setScale(0).toString();
        String res008 = MapUtils.getString(map, "res008", "");
        res008 = res008 == null ? "" : res008;
        switch (res008) {
            case "Ⅰ":
                res008 = "1";
                break;
            case "Ⅱ":
                res008 = "2";
                break;
            case "Ⅲ":
                res008 = "3";
                break;
        }

        String auth009 = getQuanXianValue(MapUtils.getString(map, "auth009", ""));
        String qua009Str = map.get("qua009") == null ? "0" : map.get("qua009").toString();
        BigDecimal qua009 = "0E-9".equals(qua009Str) ? new BigDecimal("0") : new BigDecimal(qua009Str).setScale(0, BigDecimal.ROUND_HALF_UP);
        String qua009Value = qua009.setScale(0).toString();
        String res009 = MapUtils.getString(map, "res009", "");
        res009 = res009 == null ? "" : res009;
        switch (res009) {
            case "Ⅰ":
                res009 = "1";
                break;
            case "Ⅱ":
                res009 = "2";
                break;
            case "Ⅲ":
                res009 = "3";
                break;
        }

        String auth010 = getQuanXianValue(MapUtils.getString(map, "auth010", ""));
        String qua010Str = map.get("qua010") == null ? "0" : map.get("qua010").toString();
        BigDecimal qua010 = "0E-10".equals(qua010Str) ? new BigDecimal("0") : new BigDecimal(qua010Str).setScale(0, BigDecimal.ROUND_HALF_UP);
        String qua010Value = qua010.setScale(0).toString();
        String res010 = MapUtils.getString(map, "res010", "");
        res010 = res010 == null ? "" : res010;
        switch (res010) {
            case "Ⅰ":
                res010 = "1";
                break;
            case "Ⅱ":
                res010 = "2";
                break;
            case "Ⅲ":
                res010 = "3";
                break;
        }

        //调用存储过程
        StringBuffer sql = new StringBuffer("exec [HG_LINK].[hg].[dbo].SyncComBaseArchive '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',")
                .append("'%s','%s','%s','%s','%s','%s','%s','%s',")
                .append("'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',")
                .append("'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',")
                .append("'%s','%s'");
        String execSql = String.format(sql.toString(),
                 baseOrgNumber,  fileCode, totalPaperValue,
                deptPaperValue, posPaperValue, jobDesc, auditDate, approval,
                baseOrgResPosNumber, baseOrgResPerson,
                auth001, qua001Value, res001,
                auth002, qua002Value, res002,
                auth003, qua003Value, res003,
                auth004, qua004Value, res004,
                auth005, qua005Value, res005,
                auth006, qua006Value, res006,
                auth007, qua007Value, res007,
                auth008, qua008Value, res008,
                auth009, qua009Value, res009,
                auth010, qua010Value, res010);
        log.info("\n==========准备调用[基层组织公司级档案]存储过程:{}", execSql);
        sqlService.execute(CloudSqlService.htEas, execSql);
        log.info("\n=============[基层组织公司级档案]存储过程执行完成");
    }

    private String getQuanXianValue(String qx) {
        String qxValue = "";
        qx = qx == null ? "" : qx;
        switch (qx) {
            case "★":
                qxValue = "1";
                break;
            case "▲":
                qxValue = "5";
                break;
            case "√":
                qxValue = "6";
                break;
            case "":
                qxValue = "7";
                break;
        }
        return qxValue;
    }

    /**
     * 查询审批人,返回  员工号+姓名
     *
     * @param bizObject
     * @param approvalType
     * @return
     */
    private String getFileBaseInfoWorkFlowApproval(BizObjectCreatedModel bizObject, String approvalType) {
        String mainDeptCode = (String) bizObject.get("mainDeptCode");
        String activityCode = "";
        if ("01".equals(approvalType)) {
            String manageDeptCode = (String) bizObject.get("manageDeptCode");
            if (mainDeptCode.equals(manageDeptCode)) {
                activityCode = "Activity11";
            } else {
                activityCode = "Activity13";

            }
        } else if ("03".equals(approvalType)) {
            String frameDeptCode = (String) bizObject.get("frameDeptCode");
            if (mainDeptCode.equals(frameDeptCode)) {
                activityCode = "Activity16";
            } else {
                activityCode = "Activity20";
            }
        }


        WorkflowInstanceModel instanceModel = getWorkflowInstanceFacade().getByObjectId(bizObject.getId());
        List<WorkItemModel> workItems = getWorkflowInstanceFacade().getWorkItems(instanceModel.getId(), true);
        final String finalActivityCode = activityCode;
        Optional<WorkItemModel> first = workItems.stream().filter(a -> a.getActivityCode().equals(finalActivityCode)).findFirst();
        String participant = null;
        if (first.isPresent()) {
            WorkItemModel workItemModel = first.get();
            participant = workItemModel.getParticipant();
        }

        if (participant == null) {
            participant = bizObject.getCreater().getId();
        }

        UserModel user = getOrganizationFacade().getUser(participant);


        return new StringBuilder(user.getEmployeeNo()).append(user.getName()).toString();
    }


    /**
     * 判断是否已存在
     *
     * @param field
     * @param value
     * @param schemaCode
     * @return
     */
    private String existsBizObject(String field, String value, String schemaCode) {

        String tableName = getBizObjectFacade().getTableName(schemaCode);

        StringBuilder sql = new StringBuilder("select id  from ").append(tableName)
                .append(" where ").append(field).append("='").append(value).append("'");

        Map<String, Object> map = sqlService.getMap(sql.toString());

        return (String) map.get("id");
    }

    /**
     * 转换成  基础表-总公司档案 的数据
     *
     * @param map
     * @param auditDate
     * @return
     */
    private Map<String, Object> fileInfoBaseMap(Map<String, Object> map, String auditDate, String auditer) {
        Map<String, Object> data = new HashMap<>();

        //审批类型
        data.put("approvalType", map.get("approvalType"));
        //档案类型缩略语
        data.put("archTypeAcronym", map.get("archTypeAcronym"));
        //档案类型号
        data.put("archTypeNumber", map.get("archTypeNumber"));

        //基础代码隐藏
        data.put("baseCode", map.get("baseCode"));
        //基层组织助理代码
        data.put("baseOrgAss", map.get("baseOrgAss"));
        //基层组织分管代码
        data.put("baseOrgDir", map.get("baseOrgDir"));
        //基层组织代码
        data.put("baseOrgid", map.get("baseOrgid"));
        //基层组织领导代码
        data.put("baseOrgLeader", map.get("baseOrgLeader"));
        //基层组织
        data.put("baseOrgName", map.get("baseOrgName"));
        //基础组织代码隐藏
        data.put("baseOrgNumber", map.get("baseOrgNumber"));
        //基层组织责任人
        data.put("baseOrgResPerson", map.get("baseOrgResPerson"));
        //基层组织责任岗代码
        data.put("baseOrgResPosId", map.get("baseOrgResPosId"));
        //基层组织责任岗
        data.put("baseOrgResPosName", map.get("baseOrgResPosName"));
        //基层组织责任岗代码隐藏
        data.put("baseOrgResPosNumber", map.get("baseOrgResPosNumber"));
        //基层组织上级代码
        data.put("baseOrgSupeeLeader", map.get("baseOrgSupeeLeader"));
        //基础组织责任人
        data.put("baseOrgUser", map.get("baseOrgUser"));
        //基础代码
        data.put("basicCode", map.get("basicCode"));
        //公司代码隐藏
        data.put("companyCode", map.get("companyCode"));
        //部门权限
        data.put("deptAuth", map.get("deptAuth"));
        //部门纸质
        data.put("deptPaper", map.get("deptPaper"));
        //说明
        data.put("des", map.get("des"));
        //电子版
        data.put("elec", map.get("elec"));
        //档案代码隐藏
        data.put("fileCode", map.get("fileCode"));
        //档案名称
        data.put("fileName", map.get("fileName"));
        //档案代码选择
        data.put("fileNumber", map.get("fileNumber"));
        //工作说明
        data.put("jobDesc", map.get("jobDesc"));
        //主责部门
        data.put("mainDepartmentName", map.get("mainDepartmentName"));
        //主责部门代码
        data.put("mainDepartmentNumber", map.get("mainDepartmentNumber"));
        //主责岗代码隐藏
        data.put("mainPositionCode", map.get("mainPositionCode"));
        //主责岗代码
        data.put("mainPositionId", map.get("mainPositionId"));
        //主责岗
        data.put("mainPostionName", map.get("mainPostionName"));
        //管理部门代码
        data.put("manaDeptId", map.get("manaDeptId"));
        //管理部门
        data.put("manaDeptName", map.get("manaDeptName"));
        //管理部门分管代码
        data.put("manageChangeCode", map.get("manageChangeCode"));
        //管理部门助理代码
        data.put("manageDuputyCode", map.get("manageDuputyCode"));
        //管理职能
        data.put("manageFun", map.get("manageFun"));
        //管理部门领导代码
        data.put("manageLeaderCode", map.get("manageLeaderCode"));
        //管理部门上级代码
        data.put("manageSperiorCode", map.get("manageSperiorCode"));
        //名称
        data.put("name01", map.get("name01"));
        //岗A
        data.put("positionAName", map.get("positionAName"));
        //岗B代码
        data.put("positionB", map.get("positionB"));
        //岗B代码隐藏
        data.put("positionBCode", map.get("positionBCode"));
        //岗位纸质
        data.put("posPaper", map.get("posPaper"));
        //岗A代码隐藏
        data.put("postionACode", map.get("postionACode"));
        //岗B
        data.put("postionBname", map.get("postionBname"));

        //对应记录
        data.put("record", map.get("record"));
        //对应说明
        data.put("refDes", map.get("refDes"));
        //公司
        data.put("relevCompany", map.get("relevCompany"));
        //关联管理部门
        data.put("relevManageDept", map.get("relevManageDept"));
        //岗A代码
        data.put("relevPositionA", map.get("relevPositionA"));
        //关联类型职能
        data.put("relevTypeFun", map.get("relevTypeFun"));
        //备注
        data.put("remarks", map.get("remarks"));

        //上级组织
        data.put("superOrg", map.get("superOrg"));
        //纸质
        data.put("totalPaper", map.get("totalPaper"));
        //档案类型
        data.put("typeName", map.get("typeName"));
        //序号
        data.put("xh", map.get("xh"));
        //职能
        data.put("zn", map.get("zn"));
        //职能
        data.put("znCode", map.get("znCode"));
        //职能名称
        data.put("znName", map.get("znName"));
        //001要求
        data.put("res001", map.get("res001"));
        //002要求
        data.put("res002", map.get("res002"));
        //003要求
        data.put("res003", map.get("res003"));
        //004要求
        data.put("res004", map.get("res004"));
        //005要求
        data.put("res005", map.get("res005"));
        //006要求
        data.put("res006", map.get("res006"));
        //007要求
        data.put("res007", map.get("res007"));
        //008要求
        data.put("res008", map.get("res008"));
        //009要求
        data.put("res009", map.get("res009"));
        //010要求
        data.put("res010", map.get("res010"));
        //001数量
        data.put("qua001", map.get("qua001"));
        //002数量
        data.put("qua002", map.get("qua002"));
        //003数量
        data.put("qua003", map.get("qua003"));
        //004数量
        data.put("qua004", map.get("qua004"));
        //005数量
        data.put("qua005", map.get("qua005"));
        //006数量
        data.put("qua006", map.get("qua006"));
        //007数量
        data.put("qua007", map.get("qua007"));
        //008数量
        data.put("qua008", map.get("qua008"));
        //009数量
        data.put("qua009", map.get("qua009"));
        //010数量
        data.put("qua010", map.get("qua010"));
        //001
        data.put("auth001", map.get("auth001"));
        //002
        data.put("auth002", map.get("auth002"));
        //003
        data.put("auth003", map.get("auth003"));
        //004
        data.put("auth004", map.get("auth004"));
        //005
        data.put("auth005", map.get("auth005"));
        //006
        data.put("auth006", map.get("auth006"));
        //007
        data.put("auth007", map.get("auth007"));
        //008
        data.put("auth008", map.get("auth008"));
        //009
        data.put("auth009", map.get("auth009"));
        //010
        data.put("auth010", map.get("auth010"));


        //审批时间
        data.put("auditDate", auditDate);
        //审批人
        data.put("auditer", auditer);

        return data;
    }
}

package com.authine.cloudpivot.ext.controller;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


/**
 * @Description idaas单点登录
 * @Author fengjie
 * @Date 2022/05/17
 */
@Slf4j
@Controller
@RequestMapping("/public/idaas/sso")
public class IdaasOauthController {

    @GetMapping("/login")
    @ApiOperation(value = "登录中间跳转", notes = "登录中间跳转")
    public ModelAndView login(Model model, @RequestParam("id_token") String token) {
        //return "redirect:/idaas/sso_login?id_token=token;
        log.info("id_token：{}", token);
        model.addAttribute("id_token", token);
        return new ModelAndView("sso_login");
    }

    @GetMapping("/loginByEmployeeNO")
    @ApiOperation(value = "登录中间跳转", notes = "登录中间跳转")
    public ModelAndView loginByEmployeeNO(Model model, @RequestParam("employeeNO") String employeeNO, @RequestParam("target_url") String targetUrl) {
        log.info("employeeNO：{}", employeeNO);
        model.addAttribute("employeeNO", employeeNO);
        model.addAttribute("targetUrl", targetUrl);
        return new ModelAndView("sso_login_EmployeeNO");
    }
}

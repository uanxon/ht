//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.authine.cloudpivot.web.api.view.runtime;

import com.authine.cloudpivot.engine.api.model.runtime.WorkflowInstanceModel;
import com.authine.cloudpivot.engine.enums.status.WorkflowInstanceStatus;
import com.authine.cloudpivot.engine.enums.type.ProcessDataType;
import com.authine.cloudpivot.engine.enums.type.ProcessRunMode;
import com.authine.cloudpivot.engine.enums.type.WorkflowTrustType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Maps;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

@ApiModel("流程实例列表信息")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class WorkflowInstanceVO {
    @JsonIgnore
    private WorkflowInstanceModel model;
    @ApiModelProperty("流程模板名称")
    private String workflowName;
    @ApiModelProperty("双语")
    private String name_i18n;
    @ApiModelProperty("节点名称")
    private String activityName;
    @ApiModelProperty("节点编码")
    private String activityCode;
    @ApiModelProperty("发起人")
    private String userName;
    @ApiModelProperty("发起人头像")
    private String imgUrl;
    @ApiModelProperty("任务id")
    private String workItemId;
    @ApiModelProperty("处理人头像")
    private String participantImgUrl;
    @ApiModelProperty("应用名称")
    private String appName;
    @ApiModelProperty("容器编码")
    private String containerCode;
    @ApiModelProperty("容器名称")
    private String containerName;
    @ApiModelProperty("当前用户是否是委托人")
    private Boolean isTrustor;
    @ApiModelProperty("单据号")
    private String sequenceNo;

    public WorkflowInstanceVO(WorkflowInstanceModel model) {
        this.isTrustor = Boolean.FALSE;
        this.model = model;
    }

    @ApiModelProperty("当前停留时间,返回耗秒数")
    public Long getStayTime() {
        return this.model.getStartTime() == null ? 0L : System.currentTimeMillis() - this.model.getStartTime().getTime();
    }

    @ApiModelProperty("流程完成花费时间，毫秒数")
    public Long getCostTime() {
        if (this.model.getStartTime() != null && this.model.getFinishTime() != null) {
            return this.model.getState() == WorkflowInstanceStatus.PROCESSING ? 0L : this.model.getFinishTime().getTime() - this.model.getStartTime().getTime();
        } else {
            return 0L;
        }
    }

    public String getId() {
        return this.model.getId();
    }

    public String getWorkflowName() {
        return this.workflowName;
    }

    public String getActivityName() {
        return this.activityName;
    }

    public String getUserName() {
        return this.userName;
    }

    public String getImgUrl() {
        return this.imgUrl;
    }

    @ApiModelProperty("当前处理人")
    public Map<String, String> getParticipants() {
        Map<String, String> map = Maps.newHashMap();
        if (StringUtils.isEmpty(this.model.getParticipant())) {
            return map;
        } else {
            map.put("id", this.model.getParticipant());
            map.put("name", this.model.getParticipantName());
            map.put("imgUrl", this.getParticipantImgUrl());
            return map;
        }
    }

    public String getParticipantImgUrl() {
        return this.participantImgUrl;
    }

    public String getWorkItemId() {
        return this.workItemId;
    }

    public String getInstanceName() {
        return this.model.getInstanceName();
    }

    public WorkflowInstanceStatus getState() {
        return this.model.getState();
    }

    public String getBizObjectId() {
        return this.model.getBizObjectId();
    }

    public String getWorkflowCode() {
        return this.model.getWorkflowCode();
    }

    public int getWorkflowVersion() {
        return this.model.getWorkflowVersion();
    }

    public String getOriginator() {
        return this.model.getOriginator();
    }

    public String getOriginatorName() {
        return this.model.getOriginatorName();
    }

    public String getDepartmentId() {
        return this.model.getDepartmentId();
    }

    public String getDepartmentName() {
        return this.model.getDepartmentName();
    }

    public long getUsedTime() {
        return this.model.getUsedTime();
    }

    public long getWaitTime() {
        return this.model.getWaitTime();
    }

    public Date getStartTime() {
        return this.model.getStartTime();
    }

    public Date getReceiveTime() {
        return this.model.getReceiveTime();
    }

    public Date getFinishTime() {
        return this.model.getFinishTime();
    }

    public String getWorkflowTokenId() {
        return this.model.getWorkflowTokenId();
    }

    public ProcessDataType getDataType() {
        return this.model.getDataType();
    }

    public ProcessRunMode getRunMode() {
        return this.model.getRunMode();
    }

    public String getName_i18n() {
        return this.name_i18n;
    }

    @ApiModelProperty("是否被委托发起")
    public Boolean getIsTrustStart() {
        return this.model.getTrustType() == WorkflowTrustType.START ? Boolean.TRUE : Boolean.FALSE;
    }

    @ApiModelProperty("被委托人id")
    public String getTrustee() {
        return this.model.getTrustee();
    }

    @ApiModelProperty("被委托人名称")
    public String getTrusteeName() {
        return this.model.getTrusteeName();
    }

    public Boolean getIsTrustor() {
        return this.isTrustor;
    }

    public String getSequenceNo() {
        return this.sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getActivityCode() {
        return this.activityCode;
    }

    public String getAppName() {
        return this.appName;
    }

    public String getSchemaCode() {
        return this.model.getSchemaCode();
    }

    public String getAppCode() {
        return this.model.getAppCode();
    }

    public String getContainerCode() {
        return this.containerCode;
    }

    public void setContainerCode(String containerCode) {
        this.containerCode = containerCode;
    }

    public String getContainerName() {
        return this.containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    @JsonIgnore
    public void setModel(final WorkflowInstanceModel model) {
        this.model = model;
    }

    public void setWorkflowName(final String workflowName) {
        this.workflowName = workflowName;
    }

    public void setName_i18n(final String name_i18n) {
        this.name_i18n = name_i18n;
    }

    public void setActivityName(final String activityName) {
        this.activityName = activityName;
    }

    public void setActivityCode(final String activityCode) {
        this.activityCode = activityCode;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public void setImgUrl(final String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public void setWorkItemId(final String workItemId) {
        this.workItemId = workItemId;
    }

    public void setParticipantImgUrl(final String participantImgUrl) {
        this.participantImgUrl = participantImgUrl;
    }

    public void setAppName(final String appName) {
        this.appName = appName;
    }

    public void setIsTrustor(final Boolean isTrustor) {
        this.isTrustor = isTrustor;
    }
}

package com.authine.cloudpivot.web.api.config;

import java.util.Arrays;
import java.util.Optional;

/**
 *  第一学历
 */
public enum FirstEducationEnum {
    Senior("大专之下"),
    JuniorCollege("大专以上"),
    Bachelor1("本1"),
    Bachelor2("本2")   ;
    private String text;

    FirstEducationEnum( String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public static Optional<FirstEducationEnum> valueOfText(String text) {
        FirstEducationEnum[] values = FirstEducationEnum.values();
        return Arrays.stream(values).filter(a -> a.getText().equals(text)).findFirst();
    }


}

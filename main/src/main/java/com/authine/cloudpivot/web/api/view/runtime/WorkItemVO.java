//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.authine.cloudpivot.web.api.view.runtime;

import com.authine.cloudpivot.engine.api.model.runtime.WorkItemModel;
import com.authine.cloudpivot.engine.enums.status.WorkItemApprovalStatus;
import com.authine.cloudpivot.engine.enums.status.WorkItemStatus;
import com.authine.cloudpivot.engine.enums.status.WorkItemTimeoutStatus;
import com.authine.cloudpivot.engine.enums.status.WorkflowInstanceStatus;
import com.authine.cloudpivot.engine.enums.type.WorkItemType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Maps;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel("流程任务列表信息")
public class WorkItemVO {
    @JsonIgnore
    private WorkItemModel model;
    @ApiModelProperty("流程状态")
    private WorkflowInstanceStatus workflowInstanceState;
    @ApiModelProperty("发起人头像")
    private String imgUrl;
    @ApiModelProperty("流程模板名称")
    private String workflowName;
    @ApiModelProperty("双语")
    private String name_i18n;
    @ApiModelProperty("催办次数")
    private int urgeCount = 0;
    @ApiModelProperty("是否是传阅")
    private boolean isCirculate;
    @ApiModelProperty("单据号")
    private String sequenceNo;
    @ApiModelProperty("处理人头像")
    private String participantImgUrl;
    @ApiModelProperty("容器编码")
    private String containerCode;
    @ApiModelProperty("容器名称")
    private String containerName;
    @ApiModelProperty("待办上的按钮")
    private ActionPermissionVO actionPermissionVO;
    @ApiModelProperty("已办上操作名称")
    private String approvalName;
    @ApiModelProperty("流程审批任务委托")
    private WorkItemTrustVO workItemTrust;

    public WorkItemVO(WorkItemModel model) {
        if (model == null) {
            model = new WorkItemModel();
        }

        this.model = model;
    }

    public ActionPermissionVO getActionPermissionVO() {
        return this.actionPermissionVO;
    }

    public void setActionPermissionVO(ActionPermissionVO actionPermissionVO) {
        this.actionPermissionVO = actionPermissionVO;
    }

    public boolean isCirculate() {
        return this.isCirculate;
    }

    public void setCirculate(boolean circulate) {
        this.isCirculate = circulate;
    }

    public String getSequenceNo() {
        return this.sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    @ApiModelProperty("当前停留时间,返回耗秒数")
    public Long getStayTime() {
        if (this.model.getStartTime() == null) {
            return 0L;
        } else {
            long stayTime = System.currentTimeMillis() - this.model.getStartTime().getTime();
            return stayTime < 0L ? 0L : stayTime;
        }
    }

    @ApiModelProperty("距离超时时间,返回耗秒数")
    public Long getRemainingTime() {
        if (null == this.model.getAllowedTime()) {
            return 0L;
        } else if (WorkItemTimeoutStatus.RED == this.model.getWorkItemTimeoutStatus()) {
            return this.model.getAllowedTime().getTime() - System.currentTimeMillis();
        } else {
            return WorkItemTimeoutStatus.ORANGE == this.model.getWorkItemTimeoutStatus() ? this.model.getAllowedTime().getTime() - System.currentTimeMillis() : 0L;
        }
    }

    public String getId() {
        return this.model.getId();
    }

    public void setId(String id) {
        this.model.setId(id);
    }

    public String getActivityCode() {
        return this.model.getActivityCode();
    }

    public String getActivityName() {
        return this.model.getActivityName();
    }

    public String getInstanceId() {
        return this.model.getInstanceId();
    }

    public String getInstanceName() {
        return this.model.getInstanceName();
    }

    public long getUsedtime() {
        return this.model.getUsedtime();
    }

    public String getWorkflowCode() {
        return this.model.getWorkflowCode();
    }

    public Integer getWorkflowVersion() {
        return this.model.getWorkflowVersion();
    }

    public String getOriginator() {
        return this.model.getOriginator();
    }

    public String getOriginatorName() {
        return this.model.getOriginatorName();
    }

    public String getDepartmentId() {
        return this.model.getDepartmentId();
    }

    public String getDepartmentName() {
        return this.model.getDepartmentName();
    }

    public String getFormCode() {
        return this.model.getFormCode();
    }

    public WorkItemStatus getState() {
        return this.model.getState();
    }

    public WorkItemApprovalStatus getApproval() {
        return this.model.getApproval();
    }

    public String getParticipant() {
        return this.model.getParticipant();
    }

    public String getParticipantName() {
        return this.model.getParticipantName();
    }

    public Date getStartTime() {
        return this.model.getStartTime();
    }

    public Date getReceiveTime() {
        return this.model.getReceiveTime();
    }

    public Date getFinishTime() {
        return this.model.getFinishTime();
    }

    public String getSourceId() {
        return this.model.getSourceId();
    }

    public String getSourceName() {
        return this.model.getSourceName();
    }

    public WorkItemType getWorkItemType() {
        return this.model.getWorkItemType();
    }

    public String getWorkflowTokenId() {
        return this.model.getWorkflowTokenId();
    }

    public Date getAllowedTime() {
        return this.model.getAllowedTime();
    }

    public Date getTimeoutWarning1() {
        return this.model.getTimeoutWarning1();
    }

    public Date getTimeoutWarning2() {
        return this.model.getTimeoutWarning2();
    }

    public String getAppCode() {
        return this.model.getAppCode();
    }

    public WorkflowInstanceStatus getWorkflowInstanceState() {
        return this.workflowInstanceState;
    }

    public void setWorkflowInstanceState(WorkflowInstanceStatus workflowInstanceState) {
        this.workflowInstanceState = workflowInstanceState;
    }

    public String getImgUrl() {
        return this.imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getWorkflowName() {
        return this.workflowName;
    }

    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }

    public String getName_i18n() {
        return this.model.getName_i18n();
    }

    public void setName_i18n(String name_i18n) {
        this.name_i18n = name_i18n;
    }

    public WorkItemTimeoutStatus getWorkItemTimeoutStatus() {
        return this.model.getWorkItemTimeoutStatus();
    }

    @ApiModelProperty("当前处理人")
    public Map<String, String> getParticipants() {
        Map<String, String> map = Maps.newHashMap();
        if (StringUtils.isEmpty(this.getParticipant())) {
            return map;
        } else {
            map.put("id", this.getParticipant());
            map.put("name", this.getParticipantName());
            map.put("imgUrl", this.getParticipantImgUrl());
            return map;
        }
    }

    public String getParticipantImgUrl() {
        return this.participantImgUrl;
    }

    public int getUrgeCount() {
        return this.urgeCount;
    }

    public WorkItemTrustVO getWorkItemTrust() {
        if (this.workItemTrust == null) {
            this.workItemTrust = new WorkItemTrustVO();
            this.workItemTrust.setTrust(this.model.getIsTrust() == null ? Boolean.FALSE : this.model.getIsTrust());
            this.workItemTrust.setTrustor(this.model.getTrustor());
            this.workItemTrust.setTrustorName(this.model.getTrustorName());
        }

        return this.workItemTrust;
    }

    public void setWorkItemTrust(WorkItemTrustVO workItemTrust) {
        this.workItemTrust = workItemTrust;
    }

    public String getContainerName() {
        return this.containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public String getContainerCode() {
        return this.containerCode;
    }

    public void setContainerCode(String containerCode) {
        this.containerCode = containerCode;
    }

    @JsonIgnore
    public void setModel(final WorkItemModel model) {
        this.model = model;
    }

    public void setUrgeCount(final int urgeCount) {
        this.urgeCount = urgeCount;
    }

    public void setParticipantImgUrl(final String participantImgUrl) {
        this.participantImgUrl = participantImgUrl;
    }
    public String getApprovalName() {
        return this.approvalName;
    }
    public void setApprovalName(final String approvalName) {
        this.approvalName = approvalName;
    }
}

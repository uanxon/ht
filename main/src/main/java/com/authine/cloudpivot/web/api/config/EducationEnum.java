package com.authine.cloudpivot.web.api.config;

import java.util.Arrays;
import java.util.Optional;

/**
 * 学历
 **/
public enum EducationEnum {

    Senior("高中以下"),
    JuniorCollege("大专"),
    Bachelor1("本1"),
    Bachelor2("本2"),
    Bachelor3("本3"),
    Master("硕士研究生"),
    Master1("硕1"),
    Master2("硕2"),
    Doctor0("博士研究生"),
    Doctor("博士");
    private String text;

    EducationEnum( String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public static Optional<EducationEnum> valueOfText(String text) {
        EducationEnum[] values = EducationEnum.values();
        return Arrays.stream(values).filter(a -> a.getText().equals(text)).findFirst();
    }

}

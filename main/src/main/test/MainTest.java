import org.junit.jupiter.api.Test;

/**
 * @author fengjie
 * @version 1.0.0
 * @ClassName MainTest
 * @Description TODO
 * @createTime 2023/2/10 10:40
 */
public class MainTest {
    @Test
    public void testSubString() {
        String fileName = "测试 RD-ALI-03-31 2023.01.14 .xls";
        int lastIndexOf = fileName.lastIndexOf(".");
        System.out.println("prefix：" + fileName.substring(0, lastIndexOf));
        System.out.println("suffix：" + fileName.substring(lastIndexOf));
        System.out.println("fileName：" + fileName.substring(0, fileName.indexOf(" ")));
    }
}
